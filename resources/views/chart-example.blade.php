@extends('common.layout')

@section('content')



<div class="portlet light">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-cogs font-green-sharp"></i>
            <span class="caption-subject font-green-sharp bold uppercase">Interactive Chart</span>
        </div>
        <div class="tools">
            <a href="javascript:;" class="collapse" data-original-title="" title="">
            </a>
            <a href="#portlet-config" data-toggle="modal" class="config" data-original-title="" title="">
            </a>
            <a href="javascript:;" class="reload" data-original-title="" title="">
            </a>
            <a href="javascript:;" class="remove" data-original-title="" title="">
            </a>
        </div>
    </div>
    <div class="portlet-body">
        <div id="chart_2" class="chart" style="padding: 0px; position: relative;">
            <canvas class="flot-base" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 1100px; height: 300px;" width="1375" height="375"></canvas><div class="flot-text" style="position: absolute; top: 0px; left: 0px; bottom: 0px; right: 0px; font-size: smaller; color: rgb(84, 84, 84);"><div class="flot-x-axis flot-x1-axis xAxis x1Axis" style="position: absolute; top: 0px; left: 0px; bottom: 0px; right: 0px; display: block;"><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 68px; top: 282px; left: 61px; text-align: center;">2</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 68px; top: 282px; left: 135px; text-align: center;">4</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 68px; top: 282px; left: 208px; text-align: center;">6</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 68px; top: 282px; left: 282px; text-align: center;">8</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 68px; top: 282px; left: 352px; text-align: center;">10</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 68px; top: 282px; left: 425px; text-align: center;">12</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 68px; top: 282px; left: 498px; text-align: center;">14</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 68px; top: 282px; left: 572px; text-align: center;">16</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 68px; top: 282px; left: 645px; text-align: center;">18</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 68px; top: 282px; left: 719px; text-align: center;">20</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 68px; top: 282px; left: 792px; text-align: center;">22</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 68px; top: 282px; left: 866px; text-align: center;">24</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 68px; top: 282px; left: 939px; text-align: center;">26</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 68px; top: 282px; left: 1013px; text-align: center;">28</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 68px; top: 282px; left: 1086px; text-align: center;">30</div></div><div class="flot-y-axis flot-y1-axis yAxis y1Axis" style="position: absolute; top: 0px; left: 0px; bottom: 0px; right: 0px; display: block;"><div class="flot-tick-label tickLabel" style="position: absolute; top: 269px; left: 16px; text-align: right;">0</div><div class="flot-tick-label tickLabel" style="position: absolute; top: 248px; left: 9px; text-align: right;">10</div><div class="flot-tick-label tickLabel" style="position: absolute; top: 227px; left: 9px; text-align: right;">20</div><div class="flot-tick-label tickLabel" style="position: absolute; top: 207px; left: 9px; text-align: right;">30</div><div class="flot-tick-label tickLabel" style="position: absolute; top: 186px; left: 9px; text-align: right;">40</div><div class="flot-tick-label tickLabel" style="position: absolute; top: 165px; left: 9px; text-align: right;">50</div><div class="flot-tick-label tickLabel" style="position: absolute; top: 145px; left: 9px; text-align: right;">60</div><div class="flot-tick-label tickLabel" style="position: absolute; top: 124px; left: 9px; text-align: right;">70</div><div class="flot-tick-label tickLabel" style="position: absolute; top: 104px; left: 9px; text-align: right;">80</div><div class="flot-tick-label tickLabel" style="position: absolute; top: 83px; left: 9px; text-align: right;">90</div><div class="flot-tick-label tickLabel" style="position: absolute; top: 62px; left: 1px; text-align: right;">100</div><div class="flot-tick-label tickLabel" style="position: absolute; top: 42px; left: 1px; text-align: right;">110</div><div class="flot-tick-label tickLabel" style="position: absolute; top: 21px; left: 1px; text-align: right;">120</div><div class="flot-tick-label tickLabel" style="position: absolute; top: 1px; left: 1px; text-align: right;">130</div></div></div><canvas class="flot-overlay" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 1100px; height: 300px;" width="1375" height="375"></canvas><div class="legend"><div style="position: absolute; width: 86px; height: 34px; top: 14px; right: 12px; opacity: 0.85; background-color: rgb(255, 255, 255);"> </div><table style="position:absolute;top:14px;right:12px;;font-size:smaller;color:#545454"><tbody><tr><td class="legendColorBox"><div style="border:1px solid #ccc;padding:1px"><div style="width:4px;height:0;border:5px solid rgb(209,38,16);overflow:hidden"></div></div></td><td class="legendLabel">Unique Visits</td></tr><tr><td class="legendColorBox"><div style="border:1px solid #ccc;padding:1px"><div style="width:4px;height:0;border:5px solid rgb(55,183,243);overflow:hidden"></div></div></td><td class="legendLabel">Page Views</td></tr></tbody></table></div></div>
    </div>
</div>


@endsection

@section('footer-scripts')
    <script src="/js/charts.js"></script>

    <script>
        jQuery(document).ready(function() {
            Metronic.init(); // init metronic core components
            Layout.init(); // init current layout



            var ChartsFlotcharts = function() {
                return {
                    initCharts: function() {
                        if (!jQuery.plot) {
                            return;
                        }

                        var data = [];
                        var totalPoints = 250;

                        //Interactive Chart
                        function chart2() {
                            if ($('#chart_2').size() != 1) {
                                return;
                            }

                            var envois = [
                                    [1, 12000],
                                    [2, 18000],
                                    [3, 28000],
                            ];
                            var ouvertures = [
                                [1, 1400],
                                [2, 2400],
                                [3, 10000],
                            ];

                            var clicks = [
                                [1, 70],
                                [2, 120],
                                [3, 230],
                            ];

                            var bounces = [
                                [1, 7000],
                                [2, 5200],
                                [3, 1250],
                            ];

                            var plot = $.plot($("#chart_2"), [{
                                data: envois,
                                label: "Envoyés",
                                lines: {
                                    lineWidth: 1,
                                },
                                shadowSize: 0

                            }, {
                                data: ouvertures,
                                label: "Ouvertures",
                                lines: {
                                    lineWidth: 1,
                                },
                                shadowSize: 0
                            }, {
                                data: bounces,
                                label: "Bounces",
                                lines: {
                                    lineWidth: 1,
                                },
                                shadowSize: 0
                            }, {
                                data: clicks,
                                label: "Clicks",
                                lines: {
                                    lineWidth: 1,
                                },
                                shadowSize: 0
                            }],
                            {
                                series: {
                                    lines: {
                                        show: true,
                                        lineWidth: 2,
                                        fill: true,
                                        fillColor: {
                                            colors: [{
                                                opacity: 0.05
                                            }, {
                                                opacity: 0.01
                                            }]
                                        }
                                    },
                                    points: {
                                        show: true,
                                        radius: 3,
                                        lineWidth: 1
                                    },
                                    shadowSize: 2
                                },
                                grid: {
                                    hoverable: true,
                                    clickable: true,
                                    tickColor: "#eee",
                                    borderColor: "#eee",
                                    borderWidth: 1
                                },
                                colors: ["#d12610", "#37b7f3", "#52e136", "#555555"],
                                xaxis: {
                                    ticks: 11,
                                    tickDecimals: 0,
                                    tickColor: "#eee",
                                },
                                yaxis: {
                                    ticks: 11,
                                    tickDecimals: 0,
                                    tickColor: "#eee",
                                }
                            });


                            function showTooltip(x, y, contents) {
                                $('<div id="tooltip">' + contents + '</div>').css({
                                    position: 'absolute',
                                    display: 'none',
                                    top: y + 5,
                                    left: x + 15,
                                    border: '1px solid #333',
                                    padding: '4px',
                                    color: '#fff',
                                    'border-radius': '3px',
                                    'background-color': '#333',
                                    opacity: 0.80
                                }).appendTo("body").fadeIn(200);
                            }

                            var previousPoint = null;
                            $("#chart_2").bind("plothover", function(event, pos, item) {
                                $("#x").text(pos.x.toFixed(2));
                                $("#y").text(pos.y.toFixed(2));

                                if (item) {
                                    if (previousPoint != item.dataIndex) {
                                        previousPoint = item.dataIndex;

                                        $("#tooltip").remove();
                                        var x = item.datapoint[0].toFixed(0),
                                                y = item.datapoint[1].toFixed(0);

                                        showTooltip(item.pageX, item.pageY, item.series.label + " le " + x + " : " + y);
                                    }
                                } else {
                                    $("#tooltip").remove();
                                    previousPoint = null;
                                }
                            });
                        }

                        chart2();

                    }
                };
            }();

            ChartsFlotcharts.initCharts();
        });

    </script>

@endsection