@extends('common.layout')

@section('content')
    <div class="portlet light">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-cogs font-green-sharp"></i>
                <span class="caption-subject font-green-sharp bold uppercase">
                Configuration DNS
                </span>
            </div>
            <div class="actions">
            </div>
        </div>
        <div class="portlet-body">

          <textarea name="textarea" rows="10" cols="50" class="form-control">{{$dns}}</textarea>

          <textarea name="textarea" rows="10" cols="50" class="form-control">{{$rdns}}</textarea>
        </div>
    </div>
@endsection
