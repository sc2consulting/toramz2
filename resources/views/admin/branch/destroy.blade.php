<h2> Supprimer un type </h2>


{!! Form::model(new \App\Models\Branch, array('route' => array('branch.destroy'), 'method'=> 'delete')) !!}

@include('admin.branch.form')

{{ Form::submit('Delete') }}
{{ Form::close() }}
