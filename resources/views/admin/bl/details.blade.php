@extends('common.layout')

@section('content')
    <div class="portlet light">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-cogs font-green-sharp"></i>
                <span class="caption-subject font-green-sharp bold uppercase">
                Réglages avancés
                </span>
            </div>
            <div class="actions">
              <a href="/outils/bl/"> <button type="button" class="btn btn-success">Retour</button> </a>
            </div>
        </div>
        <div class="portlet-body">
            <table class="table table-striped table-hover table-bordered">
                <tr>
                    <th>Date</th>
                    <th>DATA</th>
                </tr>

                @foreach($bl_data_record as $bl_data)

                <tr>
                    <td> {{$bl_data->created_at}}</td>
                    <td style="width:50%;">
                      {{ var_dump(json_decode($bl_data->bl_record)) }}
                      {{-- var_dump(json_decode($bl_data->bl_record['host'])) --}}</td>
                </tr>

                @endforeach

            </table>
        </div>
    </div>
@endsection
