<div class="form-group">
    <label for="input_name" class="col-sm-2 control-label">Nom</label>
    <div class="col-sm-10">
        {!! Form::text('nom', Input::old('nom'), ['class' => 'form-control']) !!}
    </div>
</div>
<div class="form-group">
    <label for="input_url" class="col-sm-2 control-label">URL</label>
    <div class="col-sm-10">
        {!! Form::textarea('url',Input::old('url'),array('id' => 'url', 'class' => 'form-control')) !!}
    </div>
</div>
