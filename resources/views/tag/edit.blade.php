@extends('common.layout')

@section('content')
    {!! Form::model($tag, array('method' => 'PATCH', 'route' => array('tag.update', $tag->id), 'class'=>'form-horizontal')) !!}

    <div class="portlet light">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-cogs font-green-sharp"></i>
                <span class="caption-subject font-green-sharp bold uppercase">
                    Editer un Tag
                </span>
            </div>
            <div class="actions">
                <div class="btn-group btn-group-devided">
                    <button type="submit" class="btn btn-danger">Enregistrer</button>
                </div>
            </div>
        </div>
        <div class="portlet-body">
          @if(isset($newtag))
          <div class="alert alert-dismissable alert-success">
            Le tag est enregistré dans la base de données.
          </div>

          @endif
            <div class="row">
                <div class="col-md-5">
                    @include('tag.partial-fields')
                </div>
            </div>
        </div>
    </div>

    </div>

{!! Form::close() !!}

@endsection