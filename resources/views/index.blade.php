@extends('template')

@section('content')

      <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">

                  <div class="x_title">
                    <h2>Home <small></small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br>
                    <h5><i class="fa fa-database font-red-soft"></i>
                      <span class="caption-subject font-red-soft bold uppercase">Nombre total de bases : <span id="lesbases">{{$totalbase}}</span> </span>
                    </h5>
                    <h5><i class="fa fa-envelope font-red-soft"></i>
                      <span class="caption-subject font-red-soft bold uppercase">Nombre total de campagnes : <span id="lescampagnes">{{$totalcampagne}}</span></span>
                    </h5>
                    <h5><i class="fa fa-envelope font-red-soft"></i>
                      <span class="caption-subject font-red-soft bold uppercase">Nombre de nouvelles campagnes créées aujourd'hui : <span id="lescampagnestoday">{{$campagnestoday}}</span></span>
                    </h5>

                    <h5><i class="fa fa-newspaper-o font-yellow-gold"></i>
                      <span class="caption-subject font-yellow-gold bold uppercase">Nombre de campagnes par bases : </span>
                    </h5>

                    <table class="table table-striped table-hover">
                        <tr>
                            <th>Nom de la base</th>
                            <th>Nombre de campagnes</th>

                        </tr>
                        @foreach($lesbases as $labase)
                        <?php
                          $lescampagnesparbases = \DB::table('campagnes')->where('base_id','=',$labase->id)->count();
                         ?>

                            <tr>
                                <td>{{$labase->nom}}</td>
                                <td>{{$lescampagnesparbases}}</td>

                            </tr>
                        @endforeach
                        <?php
                            $lescampagnesmultibases = \DB::table('campagnes')->where('base_id','=',0)->count();
                        ?>
                            <tr>
                                <td>Multi-bases</td>
                                <td>{{$lescampagnesmultibases}}</td>
                            </tr>
                    </table>


                    <h5><i class="fa fa-newspaper-o font-blue-sharp"></i>
                      <span class="caption-subject font-blue-sharp bold uppercase">Les 5 dernières campagnes :</span>
                    </h5>
                    <div style="overflow-x:auto;">
                    <table class="table table-striped table-hover">
                        <tr>
                            <th>Base</th>
                            <th>Nom</th>
                            <th>Référence</th>
                            <th>Date de création</th>

                        </tr>
                        @foreach($campagnes as $campagne)
                            <tr>
                                <td>@if($campagne->base_id == 0) Multi-bases @else{{$campagne->base->nom}}@endif</td>
                                <td>{{$campagne->nom}}</td>
                                <td>{{$campagne->ref}}</td>
                                <td>{{$campagne->created_at}}</td>
                            </tr>
                        @endforeach
                    </table>
                  </div>

                    <h5><i class="fa fa-clock-o font-blue-sharp"></i>
                      <span class="caption-subject font-blue-sharp bold uppercase">Les 5 dernières plannifications en cours :</span>
                    </h5>

                    <table class="table table-hover table-bordered table-striped">
                        <tr>
                            <th>Date</th>
                            <th>Base</th>
                            <th>Campagne</th>

                        </tr>

                        @foreach($plannings as $planning)
                            <tr>
                                <td @if ($planning->date_campagne >= date('Y-m-d')) style="background:#EFE" @else style="background:#EEF" @endif>{{ $planning->date_campagne }}</td>
                                <td>@if($planning->campagne->base_id == 0) Multi-bases @else{{$planning->campagne->base->nom}}@endif</td>
                                <td>{{ $planning->campagne->nom }}</td>
                            </tr>
                        @endforeach

                    </table>
                  </div>


      </div>
      </div>

          @endsection
