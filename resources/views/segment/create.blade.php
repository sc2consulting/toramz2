@extends('common.layout')

@section('content')

<div class="portlet light">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-cogs font-green-sharp"></i>
            <span class="caption-subject font-green-sharp bold uppercase">Créer un segment</span>
        </div>
    </div>
    <div class="portlet-body">

        @if(isset($message) && $message != '')
        <div class="alert alert-dismissable alert-warning">
        Problème ! {{$message}}
        </div>
        @endif

        {!! Form::model(new \App\Models\Segment, array('route' => array('segment.store'), 'class'=>'form-horizontal') ) !!}
        @include('segment.partial-fields')
        {!! Form::close() !!}

    </div>
</div>

@endsection
