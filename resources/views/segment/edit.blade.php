@extends('common.layout')

@section('content')

    <div class="portlet light">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-cogs font-green-sharp"></i>
                <span class="caption-subject font-green-sharp bold uppercase">
                    Editer un Segment
                </span>
            </div>
        </div>
        <div class="portlet-body">
            <div class="row">
                <div class="col-md-5">
                    {!! Form::model($segment, array('method' => 'PATCH', 'route' => array('segment.update', $segment->id), 'class'=>'form-horizontal')) !!}

                    @include('segment.partial-fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

@endsection