<div class="form-group">
    <label for="input_name" class="col-sm-2 control-label">Nom*</label>
    <div class="col-sm-10">
        {!! Form::text('nom', Input::old('nom'), ['class' => 'form-control', 'required' => 'required']) !!}
    </div>
</div>

<div class="form-group">
    <label for="input_routeur" class="col-sm-2 control-label">Routeur*</label>
    <div class="col-sm-10">
        <?php
            $routeurs = ['' => 'Choisir'] + \App\Models\Routeur::pluck('nom', 'id')->toArray();
        ?>

        {!! Form::select('routeur_id', $routeurs, \Input::old('routeur_id'), ['class' => 'form-control', 'required' => 'required', 'id' => 'selectRouteur'] ) !!}

    </div>
</div>
<div class="form-group">
    <label for="input_name" class="col-sm-2 control-label">Pass / API Key*</label>
    <div class="col-sm-10">
        {!! Form::text('password', Input::old('password'), ['class' => 'form-control', 'required' => 'required']) !!}
    </div>
</div>


<div class="form-group siteid_container" style="display:none;">
    <label for="input_name" class="col-sm-2 control-label">Id Site</label>
    <div class="col-sm-10">
        {!! Form::text('site_id', Input::old('site_id'), ['class' => 'form-control', 'required' => 'required'] ) !!}
    </div>
</div>

<div class="form-group">
    <label for="input_name" class="col-sm-2 control-label">Domaine*</label>
    <div class="col-sm-10">
        {!! Form::text('domaine', Input::old('domaine'), ['class' => 'form-control', 'required' => 'required']) !!}
    </div>
</div>
<div class="form-group">
    <label for="input_name" class="col-sm-2 control-label">Capacité quotidienne*</label>
    <div class="col-sm-10">
        {!! Form::text('quota', Input::old('quota'), ['class' => 'form-control', 'required' => 'required']) !!}
    </div>
</div>
<div class="form-group nbcampagnes_container">
    <label for="input_name" class="col-sm-2 control-label"> Nombre de campagnes maximum / J*</label>
    <div class="col-sm-10">
        {!! Form::text('nb_campagnes', Input::old('nb_campagnes'), ['class' => 'form-control', 'required' => 'required']) !!}
    </div>
</div>
<div class="form-group isbat_container">
    <label for="input_is_bat" class="col-sm-2 control-label">BAT</label>
    <div class="col-sm-10">
        {!! Form::checkbox('is_bat', 'is_bat', Input::old('is_bat'), ['class' => 'form-control', 'id' => 'is_bat_check']) !!}
    </div>
</div>
<div class="form-group batlist_container" style="display:none">
    <label for="input_bat_liste" class="col-sm-2 control-label">Liste BAT</label>
    <div class="col-sm-10">
        {!! Form::text('bat_liste', Input::old('bat_liste'), ['class' => 'form-control', 'id' => 'bat_list']) !!}
    </div>
</div>

<div class="form-group">
    <label for="input_name" class="col-sm-2 control-label">Type</label>
    <div class="col-sm-10">
        <?php
        $types = array('' => 'Choisir','nofr' => 'Autres','fr' => 'France','orange' => 'Orange');

        ?>
        {!! Form::select('type', $types, \Input::old('type'), ['class' => 'form-control', 'required' => 'required'] ) !!}
    </div>
</div>

<div class="form-group fais_container">
    <label for="input_name" class="col-sm-2 control-label">FAIs - Quota</label>
    <div class="col-sm-3">
    @foreach($fais as $fai)
            <div class="checkbox">
                <label>
                @if (isset($sender))
                <?php
                    $checked = (!is_null($sender->fais) && $sender->fais()->where('fais.id', $fai->id)->count()>0);
                    $fai_sender = $sender->fai_sender()->where('fai_sender.fai_id', $fai->id)->first();
                ?>
                @else
                    <?php $checked = false; ?>
                @endif
                    <input name="fai[]" type="checkbox" value="{{ $fai->id }}" @if($checked) checked @endif /> {{ $fai->nom }} -

                @if (!empty($fai_sender))
                    <input style="width: 100px;" name="fai_quota[{{$fai->id}}]" type="number" value="{{$fai_sender->quota}}" /> <br />
                @else
                    <input style="width: 100px;" name="fai_quota[{{$fai->id}}]" type="number" value="0" /> <br />
                @endif
                </label>
            </div>
    @endforeach
    </div>
</div>


<?php
    $branches = \App\Models\Branch::pluck('name', 'id')->toArray();
?>

<div class="form-group">
    <label for="input_name" class="col-sm-2 control-label">Branche</label>
    <div class="col-sm-10">
        {!! Form::select('branch_id', $branches, Input::old('branch_id'), ['class' => 'form-control', 'required' => 'required'] ) !!}
    </div>
</div>

<div>
    <i>*Obligatoire</i>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>

<!-- jQuery UI -->
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>

<script type="text/javascript">
    $(document).ready(function(){
        $('.fais_container').css('display', 'block');
        $('.batlist_container').css('display', 'block');
        $('.siteid_container').css('display', 'block');
        $('.nbcampagnes_container').css('display', 'block');

        //Showing specific field per 'Routeur'
        $("#selectRouteur").change(function(e) {

            $('.fais_container').css('display', 'block');
            $('.batlist_container').css('display', 'none');
            $('.siteid_container').css('display', 'none');
            $('.nbcampagnes_container').css('display', 'none');

            $('.siteid_container input').val('0');
            $('.nbcampagnes_container input').val('1');

            if($('#selectRouteur option:selected').text() == 'Maildrop') {
                $('.batlist_container').css('display', 'block');
                $('.nbcampagnes_container input').val('');
                $('.nbcampagnes_container').css('display', 'block');
            }

            if($('#selectRouteur option:selected').text() == 'Mindbaz') {
                $('.siteid_container input').val('');
                $('.siteid_container').css('display', 'block');
                $('.batlist_container').css('display', 'block');

                @if(\DB::table('settings')
                ->where('parameter', 'mindbaz_mode')
                ->where('value', 'list')
                ->first() != null)
                $('.fais_container').css('display', 'none');

                @endif
            }

            if($('#selectRouteur option:selected').text() == 'MailForYou') {
                $('.isbat_container').css('display', 'none');
                $('.fais_container').css('display', 'none');
            }

            if($('#selectRouteur option:selected').text() == 'Smessage') {

            }

            if($('#selectRouteur option:selected').text() == 'Phoenix') {

            }
        });
    });
</script>
