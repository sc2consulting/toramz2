@extends('common.layout')

@section('content')

    <div class="portlet light">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-cogs font-green-sharp"></i>
                <span class="caption-subject font-green-sharp bold uppercase">
                    Senders
                </span>
            </div>
            <div class="actions">
                <div class="btn-group btn-group-devided">
                    <a title="Modifier volume" href="/sender/changevolume" class="btn btn-warning">Multi-Edit (volume)</a>
                    @if(getenv('OVH_API') && (\Auth::User()->user_group->name == 'Admin' or \Auth::User()->user_group->name == 'Super Admin' ) )
                    <a title="Modifier volume" href="/sender/multidomain/" class="btn btn-info">Multi-Edit OVH NDD</a>
                    @endif
                    <a title="Ajouter Sender" href="/sender/create" class="btn btn-success"><i class="fa fa-plus" aria-hidden="true"></i></a>
                </div>
            </div>
        </div>
        <div class="portlet-body">

          <div class="row">
            <div class="span8">
                {!! Form::open(array('action' => 'SenderController@index', 'method' => 'get', 'id' => 'routeurTri')) !!}
                &nbsp;&nbsp;&nbsp;
                {!! Form::label('recherche', ' ') !!}
                {!! Form::text('recherche', $recherche) !!}
                <button type="submit" class="btn btn-default">Rechercher</button>
                <p>
                    <select id="routeur_id" name="routeur_id" class="form-control input-medium" onchange='filterByRouteur();'>
                        <option value ="0" onclick="return false;" >Filtrer par routeur </option>
                        @foreach ($routeurinfo as $lerouteur)
                            <option value="{{$lerouteur->id}}" @if($lerouteur->id == $routeur_id) selected="selected" @endif>{{$lerouteur->nom}}</option>
                        @endforeach
                    </select>
                </p>
                {!! Form::close() !!}
            </div>
            </div>
            <div class="row">
              <br />

            </div>

            <table class="table table-hover table-advance table-striped">
                <tr>
                    <th width="2%">ID</th>
                    <th>Routeur</th>
                    <th>Nom</th>
                    <th>From</th>
                    <th>Type</th>
                    <th width="15%">Reste/Capacité J</th>
                    <th width="5%">Reste/Nb campagnes J</th>
                    <th width="15%">%</th>
                    <th>Etat</th>
                    <th colspan="2">Actions</th>
                </tr>

                @foreach($senders as $sender)
                    <tr>
                        <td>{{ $sender->id }}</td>

                        <td>{{ $sender->routeur->nom }}</td>

                        <td>{{ $sender->nom }}</td>
                        <td>{{ $sender->domaine }}</td>
                        <td>
                          <?php
                            if($sender->type == 'fr'){
                              echo 'France';
                            }

                            if($sender->type == 'nofr'){
                              echo 'Autres';
                            }

                            if($sender->type == 'orange'){
                              echo 'Orange';
                            }

                           ?>

                        </td>
                        <td>{{ big_number($sender->quota_left) }} / {{ big_number($sender->quota) }}</td>
                        <td>{{ big_number($sender->nb_campagnes_left) }} / {{ big_number($sender->nb_campagnes) }}</td>
                        <td>
                            <?php
                            $percent = ($sender->quota > 0) ? round($sender->quota_left * 100 / $sender->quota) : 0;
                            if ($percent > 50) {
                                $class = "success";
                            } elseif ($percent <= 50 && $percent > 25) {
                                $class = 'warning';
                            } else {
                                $class = 'danger';
                            }
                            ?>
                            <div class="progress">
                                <div class="progress-bar progress-bar-{{$class}}" role="progressbar" aria-valuenow="{{ $sender->quota_left }}"
                                     aria-valuemin="0" aria-valuemax="{{ $sender->quota }}" style="width:{{ $percent }}%">
                                </div>
                            </div>
                        </td>
                        <td>
                          @if($sender->is_enable == '1')
                           <a title="Désactiver" class="btn btn-success" href="/sender/{{$sender->id}}/disable"><i class="fa fa-toggle-off" aria-hidden="true"></i></a>
                          @else
                           <a title="Activer" class="btn btn-danger" href="/sender/{{$sender->id}}/enable"><i class="fa fa-toggle-on" aria-hidden="true"></i></a>
                          @endif
                        </td>
                        <td>
                            <a title="Modifier" href="/sender/{{$sender->id}}/edit" class="btn btn-primary"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                        </td>
                        <td>
                            {!! Form::open(array('route' => array('sender.destroy', $sender->id), 'method' => 'delete')) !!}
                            <button title="Supprimer" class="btn default red" type="submit"><i class="fa fa-times" aria-hidden="true"></i></button>
                            {!! Form::close() !!}
                        </td>
                    </tr>

                @endforeach
            </table>
            {!! $senders->appends(['routeur_id' => $routeur_id])->render() !!}

        </div>
    </div>

@endsection

@section('footer-scripts')
<!-- jQuery -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>

<!-- jQuery UI -->
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>

<script type="text/javascript">
    $(document).ready(function(){
        {!! FormAutocomplete::selector('#recherche')->db_senders('senders', ['nom', 'domaine']) !!}
    });
    function filterByRouteur() {
        document.getElementById("routeurTri").submit();
    }
</script>
@endsection
