@extends('template')

@section('content')

<!-- jQuery -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>

<!-- jQuery UI -->
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>

<script type="text/javascript">
    $(document).ready(function(){

    });
    function filterByRouteur() {
        document.getElementById("routeurTri").submit();
    }
</script>

<div class="row">
  <div class="span8">
      {!! Form::open(array('action' => 'SenderController@index', 'method' => 'get', 'id' => 'routeurTri')) !!}
      &nbsp;&nbsp;&nbsp;
      {!! Form::label('recherche', ' ') !!}
      {!! Form::text('recherche', $recherche) !!}
      <button type="submit" class="btn btn-default">Rechercher</button>
      <p>
          <select id="routeur_id" name="routeur_id" class="form-control input-medium" onchange='filterByRouteur();'>
              <option value ="0" onclick="return false;" >Filtrer par routeur </option>
              @foreach ($routeurinfo as $lerouteur)
                  <option value="{{$lerouteur->id}}" @if($lerouteur->id == $routeur_id) selected="selected" @endif>{{$lerouteur->nom}}</option>
              @endforeach
          </select>
      </p>
      {!! Form::close() !!}
  </div>
  </div>
  <div class="row">
    <br />

  </div>

      <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">

                  <div class="x_title">
                    <h2>Senders</h2>

                    <ul class="nav navbar-right panel_toolbox">
                      <a href="/sender/change_volume_new" class="btn btn-warning">Multi-Edit (volume)</a>
                      <a href="/sender/create" class="btn btn-warning">Ajouter un sender</a>
                      <!--<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>


                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                      -->
                    </ul>


                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br>
                    <div class="row">
                      <div style="overflow-x:auto;">
                      <table class="table table-striped table-hover table-bordered">
                          <tr>
                              <th width="2%">ID</th>
                              <th>Routeur</th>
                              <th>Nom</th>
                              <th>From</th>
                              <th>Type</th>
                              <th width="15%">Reste/Capacité J</th>
                              <th width="5%">Reste/Nb campagnes J</th>
                              <th width="15%">%</th>
                              <th>Etat</th>
                              <th colspan="2">Actions</th>
                          </tr>

                          @foreach($senders as $sender)
                              <tr>
                                  <td>{{ $sender->id }}</td>

                                  <td>{{ $sender->routeur->nom }}</td>

                                  <td>{{ $sender->nom }}</td>
                                  <td>{{ $sender->domaine }}</td>
                                  <td>
                                    <?php
                                      if($sender->type == 'fr'){
                                        echo 'France';
                                      }

                                      if($sender->type == 'nofr'){
                                        echo 'Autres';
                                      }

                                      if($sender->type == 'orange'){
                                        echo 'Orange';
                                      }

                                     ?>

                                  </td>
                                  @if(!function_exists('big_number'))
                                  <?php
                                  function big_number($value) {
                                        return number_format($value, 0, ',',' ');
                                  }
                                  ?>
                                  @endif
                                  <td>{{ big_number($sender->quota_left) }} / {{ big_number($sender->quota) }}</td>
                                  <td>{{ big_number($sender->nb_campagnes_left) }} / {{ big_number($sender->nb_campagnes) }}</td>
                                  <td>
                                      <?php
                                      $percent = ($sender->quota > 0) ? round($sender->quota_left * 100 / $sender->quota) : 0;
                                      if ($percent > 50) {
                                          $class = "success";
                                      } elseif ($percent <= 50 && $percent > 25) {
                                          $class = 'warning';
                                      } else {
                                          $class = 'danger';
                                      }
                                      ?>
                                      <div class="progress">
                                          <div class="progress-bar progress-bar-{{$class}}" role="progressbar" aria-valuenow="{{ $sender->quota_left }}"
                                               aria-valuemin="0" aria-valuemax="{{ $sender->quota }}" style="width:{{ $percent }}%">
                                          </div>
                                      </div>
                                  </td>
                                  <td>
                                    @if($sender->is_enable == '1')
                                     <a title="Désactiver" class="btn btn-success" href="/sender/{{$sender->id}}/disable"><i class="fa fa-toggle-off" aria-hidden="true"></i></a>
                                    @else
                                     <a title="Activer" class="btn btn-danger" href="/sender/{{$sender->id}}/enable"><i class="fa fa-toggle-on" aria-hidden="true"></i></a>
                                    @endif
                                  </td>
                                  <td>
                                      <a title="Modifier" href="/sender/{{$sender->id}}/edit" class="btn btn-primary"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                  </td>
                                  <td>
                                      {!! Form::open(array('route' => array('sender.destroy', $sender->id), 'method' => 'delete')) !!}
                                      <button title="Supprimer" class="btn default red" type="submit"><i class="fa fa-times" aria-hidden="true"></i></button>
                                      {!! Form::close() !!}
                                  </td>
                              </tr>

                          @endforeach
                      </table>
                      {!! $senders->appends(['routeur_id' => $routeur_id])->render() !!}

                  </div>

                </div>

      </div>
      </div>
      </div>

          @endsection
