@extends('common.layout')

@section('content')
    <meta http-equiv="refresh" content="999">
    <div class="portlet light">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-cogs font-green-sharp"></i>
                <span class="caption-subject font-green-sharp bold uppercase">Logs</span>
            </div>
            <div class="actions">
                <div class="btn-group btn-group-devided">
                    <a href="/" class="btn btn-primary">Retour</a>
                </div>
            </div>
        </div>
        <div class="portlet-body">
          <div class="list-group">

          @foreach($lignetab as $ligne)
            <?php echo '<code>' . $ligne . '</code>' ?>
          @endforeach
        </div>
    </div>

@endsection
