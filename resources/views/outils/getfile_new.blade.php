@extends('template')

@section('content')
     <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
               <div class="x_title">
                    <h2>Télécharger <small></small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <a href="/downl"><button class="btn btn-primary" href="">Retour</button></a>
                    </ul>
                    <div class="clearfix"></div>
               </div>
               <div class="x_content">
                    <br>
                    <div class="row">
                         <table class="table table-striped table-bordered">
                              <tr>
                                   <th>Nom fichier</th>
                                   <th>Action</th>
                              </tr>
                              @foreach ($fichiers as $fichier)
                                   <tr>
                                        <td>{{\File::name($fichier)}}</td>
                                        <td>
                                             <a href="/downl/deletefile/{{basename($fichier)}}" class="btn btn-danger btn-default btn-xs"><span>Supprimer</span></a>-
                                             <a href="/downl/file/{{basename($fichier)}}"class="btn btn-success btn-default btn-xs"><span >Télécharger</span></a>
                                        </td>
                                   </tr>
                              @endforeach
                         </table>
                    </div>
               </div>
          </div>
     </div>
@endsection
