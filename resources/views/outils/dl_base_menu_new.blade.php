@extends('template')

@section('content')

      <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">

                  <div class="x_title">
                    <h2>Liste des bases <small></small></h2>
                    <ul class="nav navbar-right panel_toolbox">

                      <a href="/downl/files/" type="submit" class="btn btn-info btn-xs">Liste des fichiers</a>

                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br>
                    <div class="row">

                      <table class="table table-bordered table-striped table-hover">
                          <tr>
                              <th>Identifiant</th>
                              <th>Code</th>
                              <th>Nom</th>

                              <th>Génération de fichier</th>
                          </tr>

                          @foreach($bases as $b)
                              <tr>
                                  <td>{{ $b->id }}</td>
                                  <td>{{ $b->code }}</td>
                                  <td>{{ $b->nom }}</td>

                                  <td>

                                    <button id="launch_export_mail_{{$b->id}}" href="/downl/base/{{$b->id}}" type="submit" class="btn btn-success btn-sm">Email</button>
                                    <button id="launch_export_all_{{$b->id}}" href="/downl/champs/{{$b->id}}" type="submit" class="btn btn-primary btn-sm">Email et champs</button>
                                    <button id="launch_export_desabo_{{$b->id}}" href="/downl/basedesabo/{{$b->id}}" type="submit" class="btn btn-danger btn-sm">Désinscrits</button>
                                    <button id="launch_export_md5_{{$b->id}}" href="/downl/basemd5/{{$b->id}}" type="submit" class="btn btn-warning btn-sm">HASH (md5)</button>
                                    <button id="launch_export_sha_{{$b->id}}" href="/downl/sha256/{{$b->id}}" type="submit" class="btn btn-warning btn-sm">SHA256</button>

                                  </td>

                              </tr>
                          @endforeach
                      </table>

                  </div>


      </div>
      </div>
      </div>


      <script>

      $("[id*=launch_export_mail_]").click(function(e){


      var id_button = $(this).attr('id');
      var href_button = $(this).attr('href');

      console.log(id_button);
      console.log(href_button);

      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': '<?php echo csrf_token(); ?>'
          }
      });

      $.ajax({
          url: "http://" + document.domain + href_button,
          type: 'GET',

      });
      swal({
        title: "<h1>Lancement de l'export EMAIL</h1>",
        text: "Veuillez vérifier <a href='http://" + document.domain + "/notifications'>la page de notifications</a> pour voir quand votre import sera terminé",
        html: true
      });
      });

      $("[id*=launch_export_all_]").click(function(e){


      var id_button = $(this).attr('id');
      var href_button = $(this).attr('href');

      console.log(id_button);
      console.log(href_button);

      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': '<?php echo csrf_token(); ?>'
          }
      });

      $.ajax({
          url: "http://" + document.domain + href_button,
          type: 'GET',

      });
      swal({
        title: "<h1>Lancement de l'export EMAIL et Champs</h1>",
        text: "Veuillez vérifier <a href='http://" + document.domain + "/notifications'>la page de notifications</a> pour voir quand votre import sera terminé",
        html: true
      });
      });


      $("[id*=launch_export_desabo_]").click(function(e){


      var id_button = $(this).attr('id');
      var href_button = $(this).attr('href');

      console.log(id_button);
      console.log(href_button);

      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': '<?php echo csrf_token(); ?>'
          }
      });

      $.ajax({
          url: "http://" + document.domain + href_button,
          type: 'GET',

      });
      swal({
        title: "<h1>Lancement de l'export unsubscribe</h1>",
        text: "Veuillez vérifier <a href='http://" + document.domain + "/notifications'>la page de notifications</a> pour voir quand votre import sera terminé",
        html: true
      });
      });

      $("[id*=launch_export_md5_]").click(function(e){


      var id_button = $(this).attr('id');
      var href_button = $(this).attr('href');

      console.log(id_button);
      console.log(href_button);

      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': '<?php echo csrf_token(); ?>'
          }
      });

      $.ajax({
          url: "http://" + document.domain + href_button,
          type: 'GET',

      });
      swal({
        title: "<h1>Lancement de l'export HASH MD5</h1>",
        text: "Veuillez vérifier <a href='http://" + document.domain + "/notifications'>la page de notifications</a> pour voir quand votre import sera terminé",
        html: true
      });
      });


      $("[id*=launch_export_sha_]").click(function(e){


      var id_button = $(this).attr('id');
      var href_button = $(this).attr('href');

      console.log(id_button);
      console.log(href_button);

      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': '<?php echo csrf_token(); ?>'
          }
      });

      $.ajax({
          url: "http://" + document.domain + href_button,
          type: 'GET',

      });
      swal({
        title: "<h1>Lancement de l'export HASH SHA</h1>",
        text: "Veuillez vérifier <a href='http://" + document.domain + "/notifications'>la page de notifications</a> pour voir quand votre import sera terminé",
        html: true
      });
      });


      </script>


@endsection
