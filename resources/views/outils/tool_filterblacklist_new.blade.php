@extends('template')

@section('content')
     <div class="row">
          <div class="col-xs-12">
               <i class="fa fa-cogs fa-2x text-success"></i>
               <span class="lead text-success">Filtrer avec la blacklist</span>
               <br>
               <br>
          </div>
          <div class="col-xs-12">
               <a href="/outils/listemanager"><button class="btn btn-primary" href="">Menu ListManager</button></a>
               <hr>
          </div>
          <div class="col-xs-12">
               @foreach ($fichiers as $fichier)
                    <div class="checkbox">
                         <label><input type="radio" value="{{basename($fichier)}}" name="file[]">{{\File::name($fichier)}}</label>
                    </div>
               @endforeach
               <hr>
               <div class="col-xs-12">
                    <button type="submit" id="submitbutton" class="btn btn-success">Modifier la liste</button>
                    {!! Form::close() !!}
               </div>
          </div>
     </div>
@endsection
@section('footer')
     <script type="text/javascript">
          $("#submitbutton").click(function(e){
               // var n = $( "input:checked" ).length;
               var valuebox = $( "input:checked" ).val();
               $.ajaxSetup({
                   headers: {
                       'X-CSRF-TOKEN': '<?php echo csrf_token(); ?>'
                   }
               });
               $.ajax({
                   url: "http://" + document.domain + "/tool/filterblacklist/" + valuebox,
                   type: 'GET',
               });
               swal({
                 title: "<h1>Lancement du filtrage</h1>",
                 text: "Veuillez vérifier <a href='http://" + document.domain + "/notifications'>la page de notifications</a> pour voir quand votre import sera terminé",
                 html: true
               });
          });
     </script>
@endsection
