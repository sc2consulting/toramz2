@extends('common.layout')

@section('content')
    <div class="portlet light">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-cogs font-green-sharp"></i>
                <span class="caption-subject font-green-sharp bold uppercase">Modifier une liste</span>
            </div>
            <div class="actions">
                <div class="btn-group btn-group-devided">
                    <a href="/outils/listemanager"><button class="btn btn-primary" href="">Menu ListManager</button></a>
                </div>
            </div>
        </div>
        <div class="portlet-body">
            <div class="form-group">
                <label for="listename">{{implode($fichier)}}</label>
            </div>

            {!! Form::open(array('url' => 'outils/listemanager/modifier','files' => true, 'method' => 'post')) !!}


            <label for="base">Choisir la base à comparer :
                <select name="base" class="form-control">
                @foreach($bases as $b)
                        <option @if(isset($base) && $base->id == $b->id) selected @endif value="{{$b->id}}" name="base">{{$b->nom}}</option>
                @endforeach
                </select>
            </label>

            <label for="blackliste">Choisir la blackliste à comparer :
                <select name="blackliste" class="form-control">
                @foreach ($blacklistes as $bl)
                        <option @if(isset($blackliste) &&  $blackliste == $bl->liste) selected @endif value="{{$bl->liste}}">{{$bl->liste}}</option>
                @endforeach
                </select>
            </label>

            <hr>
            <input type="hidden" name="choix" value="filtrerlistedbaction">
            <input type="hidden" name="varfichierbase" value="{{implode($fichier)}}">
            <button type="submit" class="btn btn-success">Valider</button>
            {!! Form::close() !!}
            @if(isset($stats))
                <table class="table table-striped table-hover">
                    <thead>
                        <td>Fichier</td>
                        <td>Uniques</td>
                        <td>Doublons</td>
                        {{--@foreach($fais as $f)--}}
                        {{--<td>{{$f->nom}}</td>--}}
                        {{--@endforeach--}}
                        <td>Actions</td>
                    </thead>
                @foreach($stats as $filename => $stats_fais)
                    <tr>
                        <td>{{$filename}}</td>
                        <td>{{$stats_fais['uniques']}}</td>
                        <td>{{$stats_fais['doublons']}}</td>
                        {{--@foreach($stats_fais as $s_fai)--}}
                        {{--<td>{{$s_fai}}</td>--}}
                        {{--@endforeach--}}
                        <td><a href="telecharger/{{basename($filename)}}"><span class="label label-success">Télécharger</span></a></td>
                    </tr>
                @endforeach
                </table>
            @endif
        </div>
    </div>
@endsection

@section('footer')

@endsection
