@extends('template')

@section('content')
     <div class="row">
         <div class="col-xs-12">
               <i class="fa fa-2x text-success fa-cogs"></i>
               <span class="lead text-success">Importer des fichiers liste</span>
               <br />
               <br />
          </div>

          <div class=" col-xs-12 btn-group btn-group-devided">
               <a href="/outils/listemanager"><button class="btn btn-primary" href="">Menu ListManager</button></a>
               <br />
               <br />
          </div>

          <div class="col-xs-12">
               <table class="table table-striped table-bordered">
                    <tr>
                         <th>Nom fichier</th>
                         <th>Action</th>
                    </tr>
                    @foreach ($fichiers as $fichier)
                         <tr>
                         <td>{{\File::name($fichier)}}</td>
                         <td>
                         <a href="supprimer/{{basename($fichier)}}" class="btn btn-danger btn-default btn-sm"><span>Supprimer</span></a>- <a href="telecharger/{{basename($fichier)}}"class="btn btn-success btn-default btn-sm"><span>Télécharger</span></a>
                         </td>
                         </tr>
                    @endforeach
               </table>
          </div>
          <hr>
          <div class="col-xs-12">
               {!! Form::open(array('url' => 'outils/listemanager/upload','files' => true, 'method' => 'post')) !!}
                    {!! Form::file('fichier') !!}
          <div>
          <hr>
          <div class="col-xs-12">
                     <button type="submit" class="btn btn-default">Enregistrer</button>
          </div>
          {!! Form::close() !!}
     </div>
@endsection

@section('footer')
@endsection
