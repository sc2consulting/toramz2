@extends('common.layout')

@section('content')

    <div class="portlet light">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-cogs font-green-sharp"></i>
                <span class="caption-subject font-green-sharp bold uppercase">
                    Ajouter configuration planning par defaut
                </span>
            </div>
            <div class="actions">
                <div class="btn-group btn-group-devided">
                    <a title="Retour" href="/planning" class="btn btn-success">Retour</a>
                </div>
            </div>

        </div>
        <div class="portlet-body">

          {!! Form::open(array('url' => 'planning/set_defaut_planning')) !!}
            <div class="row">
              {!! Form::label('Volume Total', null, ['class' => 'control-label']) !!}
              {!! Form::text('volume_total', '0', array_merge(['class' => 'form-control'])) !!}

              @foreach($lesfais as $lefai)
              <hr>
              {!! Form::label($lefai->nom, null, ['class' => 'control-label']) !!}
              {!! Form::text($lefai->id, '0', array_merge(['class' => 'form-control'])) !!}

              @endforeach

            </div>

            <button type="submit" class="btn btn-success" id="valider" style="margin-left:0px;">Valider</button>
            {!! Form::close() !!}

          </div>


@endsection
