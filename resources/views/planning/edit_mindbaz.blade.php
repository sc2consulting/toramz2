@extends('common.layout')

@section('content')
<form method="post" action="/campagne/{{$campagne->id}}/mindbaz_planning/">
    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
    <div class="portlet light">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-cogs font-green-sharp"></i>
                <span class="caption-subject font-green-sharp bold uppercase">Mindbaz Plannings (Campagne {{$campagne->ref}})</span>
            </div>
            <div class="actions">
                <div class="btn-group btn-group-devided">
                    <button type="submit" id="store" class="btn btn-success" > Enregistrer </button>
                    <button type="button" class="btn btn-primary" id="add_planning"> Ajouter une plannif </button>
                </div>
            </div>
        </div>
        <div class="portlet-body">
            <div class="row">
                <br />
            </div>
            <div class="table-responsive">
                <table id="tab_planning" class="table table-hover table-bordered table-striped">
                    <tr>
                        <th width="15%">Date et heure*</th>
                        <th width="10%">Sender</th>
                        <th width="30%">Cible</th>
                        <th width="30%">Exclure</th>
                        <th>Actions</th>
                    </tr>
                    <?php $ind = -1; ?>
                    @foreach($plannings as $planning)
                        <?php
                            $ind++;
                            $cr = \DB::table('campagnes_routeurs')->where('planning_id', $planning->id)->first();
                        ?>
                        <tr id="ligne_planning" class="planning existing_planning">
                            <input class="planning_id" type="hidden" name="already[]" value="{{$planning->id}}" />
                            <td>
                                <input class='date_campagne' type="date" name="dates_campagnes[]" value="{{$planning->date_campagne}}" @if ($planning->is_valid) readonly @endif required />
                                <input class='heure_campagne' type="time" name="heures_campagnes[]" value="{{$planning->time_campagne}}" @if ($planning->is_valid) readonly @endif required />
                            </td>
                            <td>
                            <select class="sender_choice form-control" name="senders_ids[]" readonly required>
                                @foreach($senders as $s)
                                    <option value="{{$s->id}}">{{$s->nom}}</option>
                                @endforeach
                            </select>
                            </td>
                            <td>
                                <div class="list_part">
                                <select class="form-control list_choice" name="list_ids[{{$ind}}][]" readonly="" required >
                                    @foreach($selected_lists[$planning->id] as $sl)
                                    <option value="{{json_encode($sl->GetTargetResult->idTarget)}}"> {{$sl->GetTargetResult->parameters->name}} </option>
                                    @endforeach
                                </select>
                                </div>
                                <select class="form-control" name="types[]" style="display:none" readonly required >
                                    <option class='typeChoice' value='anonyme' selected > Anonyme </option>
                                </select>
                            </td>
                            <td>
                                <select class="form-control excluded_list_choice" name="excluded_list_ids[{{$ind}}][]" readonly="" required >
                                    @if(isset($selected_excluded_lists[$planning->id]))
                                        @foreach($selected_excluded_lists[$planning->id] as $sel)
                                            <option value="{{json_encode($sel->GetTargetResult->idTarget)}}"> {{$sel->GetTargetResult->parameters->name}} </option>
                                        @endforeach
                                    @endif
                                </select>
                            </td>
                            <td class='actions'>
                                @if(!$planning->is_valid)
                                    <input type="button" class="sendbatmb" value="BAT" onclick="return false;">
                                    <input type="button" class="calculatespamscore" value="Calculer Spamscore" onclick="return false;">
                                    <span class="spamscoreresults"></span>
                                    <input type="button" class="validate" name="is_valid" value="Valider" onclick="return false;">
                                    <input type="button" class="supprimer" value="Supprimer" onclick="return false;">
                                @else
                                    Programmée
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
        <?php echo '<input type="hidden" id="indexRows" name="indexRows" value="'.$ind.'" />'; ?>
    </div>
</form>
@endsection

@section('footer-scripts')

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.6.2/chosen.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.4/jquery.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.6.2/chosen.jquery.js"></script>
    <script src="https://cdn.jsdelivr.net/chosen/1.1.0/chosen.proto.js"></script>
    <script src="http://labo.tristan-jahier.fr/chosen_order/chosen.order.jquery.min.js"></script>

    <script>
    $().ready(function() {
        var config = {
            allow_single_deselect:false,
            disable_search_threshold:10,
            no_results_text:'Oops, aucune liste trouvée',
            width:"60%"
        };

        $('.list_choice').chosen(config);

        $('.sender_choice').live('change', function () {

            var s_id = $(this).val();
            var r_id = {{$routeur->id}};

            var currentIndex = parseInt($('#indexRows').val()) ;

            var this_sender = $(this);

            if (s_id != undefined || s_id != 0) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': '<?php echo csrf_token(); ?>'
                    }
                });

                $.ajax({
                    url: '/sender/lists/ajax',
                    type: 'POST',
                    data: {
                        sender_id: s_id,
                        routeur_id: r_id
                    },
                    dataType: 'JSON',
                    success: function (data) {

                        var listids_html = '<select class="form-control list_choice" name="list_ids['+currentIndex+'][]" required multiple data-placeholder="Choisir une/plusieurs liste(s)" >';
                        var excludedlistids_html = '<select class="form-control excluded_list_choice" name="excluded_list_ids['+currentIndex+'][]" required multiple data-placeholder="Choisir une/plusieurs liste(s)" >';

                        for (var i = 0; i < data.length; i++) {
                            var objet = data[i];

                            listids_html += '<option value="' + objet.idTarget + '">' + objet.name + '(' + objet.lastUpdateDate + ')</option>';
                            excludedlistids_html += '<option value="' + objet.idTarget + '">' + objet.name + '(' + objet.lastUpdateDate + ')</option>';
                        }

                        listids_html += '</select>';
                        excludedlistids_html += '</select>';

                        this_sender.parents('tr').find('.list_part').empty();
                        this_sender.parents('tr').find('.list_part').append(listids_html);
                        this_sender.parents('tr').find('.list_choice').chosen(config);

                        this_sender.parents('tr').find('.excluded_list_part').empty();
                        this_sender.parents('tr').find('.excluded_list_part').append(excludedlistids_html);
                        this_sender.parents('tr').find('.excluded_list_choice').chosen(config);

                    },
                    error: function (e) {
                        console.log(e.responseText);
                    }
                });
            }
        });

        $("#add_planning").click(function(e) {
            e.preventDefault();
            var currentIndex = parseInt($('#indexRows').val()) + 1 ;
            var d = new Date();
            var senderhtml = '<select class="form-control sender_choice" name="senders_ids[]" required > <option class="senderChoice" value="0">Choisir un sender</option>';

            @foreach($senders as $s)
                senderhtml += '<option class="senderChoice" value="{{$s->id}}">{{$s->nom}}</option> ';
            @endforeach

            senderhtml += '</select>';

            d.setMinutes(d.getMinutes() + 5); //heure envoi defini a heure actuelle+5min par defaut
            $("#tab_planning").append("<tr class='planning new_planning'> <input type='hidden' name='already[]' value='0' /> <td> <input class='date_campagne' type='date' name='dates_campagnes[]' value='{{$today}}' required /> <input class='heure_campagne' type='time' name='heures_campagnes[]' value='"+('0'+d.getHours()).slice(-2)+":"+('0'+d.getMinutes()).slice(-2)+"'/> </td> <td>"+senderhtml+"</td>  <td> <div class='list_part'> </div> <select class='form-control' name='types[]' style='display:none' required > <option class='typeChoice new' value='anonyme' selected > Anonyme </option> </select> </td> <td> <div class='excluded_list_part'> </div> </td> <td> <button type='button' class='btn btn-danger js_supprimer'> Supprimer </button> <input type='hidden' name='is_valid[]' value='0' > </td> </tr>");
            $('.list_choice').chosen(config);
            $('#indexRows').val(currentIndex);
        });

        $('#tab_planning tr .js_supprimer').live("click", function(){
            $(this).parent().parent().remove();
            var currentIndex = parseInt($('#indexRows').val()) - 1 ;
            $('#indexRows').val(currentIndex);
        });

        function warnBeforeRedirect($element) {
//            On construit le message d'affichage avec les volumes globaux, par FAIs pour le faire confirmer auprès de l'utilisateur
            var text = "";
            var title = "Confirmez-vous l'édition des plannings suivants ?";
            var type = "warning";
            var showCancelButton = true;

            $(".existing_planning").each(function () {
                var date_campagne = $(this).find('.date_campagne').val();
                var heure_campagne = $(this).find('.heure_campagne').val();
                var sender_val = $(this).find('.sender_choice').val();
                var sender_text = $(this).find('.sender_choice option:selected').text();

                if(sender_val == 0){
                    title = "Oops, vous allez peut-être trop vite ?";
                    type = "error";
                    showCancelButton = false;
                    sender_text = '<span style="color: red">' + sender_text + '</span>';
                }

                var new_plann_text = '<br/><br/>Planning existant pour le '+date_campagne+' à '+heure_campagne+" <br/> Sender : "+sender_text+" <br/> ";
                text = text + new_plann_text;
            });

            $(".new_planning").each(function () {
                var date_campagne = $(this).find('.date_campagne').val();
                var heure_campagne = $(this).find('.heure_campagne').val();
                var sender_val = $(this).find('.sender_choice').val();
                var sender_text = $(this).find('.sender_choice option:selected').text();
                var list_val = $(this).find('.list_choice').val();
                var list_text = $(this).find('.list_choice option:selected').text();

                if(sender_val == 0){
                    title = 'Oops, vous allez peut-être trop vite ?';
                    type = 'error';
                    showCancelButton = false;
                    sender_text = '<span style="color: red">' + sender_text + '</span>';
                }

                if(list_val == 0){
                    title = 'Oops, vous allez peut-être trop vite ?';
                    type = 'error';
                    showCancelButton = false;
                    list_text = '<span style="color: red">' + list_text + '</span>';
                }

                var new_plann_text = '<br/><br/>Nouveau planning pour le '+date_campagne+' à '+heure_campagne+" <br/> Sender : "+sender_text+" <br/> Liste : "+list_text+" <br/>";
                text = text + new_plann_text;
            });

            swal
            ({
                        title: title,
                        text: text,
                        type: type,
                        showCancelButton: showCancelButton,
                        html: true
            },
            function(isConfirm) {
                if (isConfirm && type != 'error') {
//                            console.log($element.closest("form"));
                    $element.closest("form").submit();
                }
            });
        }

        $(".validate").click(function(e) {
            e.preventDefault();
            var this_planning = $(this);
            var plan_id = this_planning.parents('tr').find('.planning_id').val();

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': '<?php echo csrf_token(); ?>'
                }
            });

            $.ajax({
                url: '/planning/'+plan_id+'/validate',
                type: 'POST',
                data: {
                    planning_id: plan_id
                },
                dataType: 'JSON',
                success: function (data) {
                    this_planning.parents('tr').find('.calculatespamscore').remove();
                    this_planning.append('Programmée');
                    this_planning.remove();
                },
                error: function (e) {

                }
            });
        });

        $(".calculatespamscore").click(function(e) {
            e.preventDefault();
            var this_planning = $(this);
            var plan_id = this_planning.parents('tr').find('.planning_id').val();

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': '<?php echo csrf_token(); ?>'
        }
            });

            $.ajax({
                url: '/planning/'+plan_id+'/calculatespamscore',
                type: 'POST',
                data: {
                    planning_id: plan_id
                },
                dataType: 'JSON',
                success: function (data) {

//                    console.log(data);
                    var sa = data.spamassassin;
                    var vr = data.vaderetro;

                    var sa_score_text = '';
                    var vr_score_text = '';

                    $.each(sa, function(i, item) {
                        $.each(item, function(k, value){
                           sa_score_text += k+' '+value+'; ';
                        });
                    });

                    $.each(vr, function(i, item) {
                        $.each(item, function(k, value){
                            vr_score_text += k+' '+value+'; ';
                        });
                    });

                    this_planning.parents('tr').find('.spamscoreresults').append('<span> SpamScore : <br/> SpamAssassin = '+sa_score_text+'<br/> VadeRetro = '+vr_score_text+' </span>');
                    this_planning.remove();
                },
                error: function (e) {

                }
            });
        });

        $(".supprimer").click(function(e) {
            e.preventDefault();
            var this_planning = $(this);
            var plan_id = this_planning.parents('tr').find('.planning_id').val();

            var currentIndex = parseInt($('#indexRows').val()) - 1 ;
            $('#indexRows').val(currentIndex);

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': '<?php echo csrf_token(); ?>'
                }
            });

            $.ajax({
                url: '/planning/'+plan_id+'/delete',
                type: 'POST',
                data: {
                    planning_id: plan_id
                },
                dataType: 'JSON',
                success: function (data) {
                    this_planning.parents('tr').remove();
//                    $tr =
//                    $element.closest("form").submit();
                },
                error: function (e) {

                }
            });
        });

        $(".sendbatmb").click(function(e) {
            e.preventDefault();
            var this_planning = $(this);
            var plan_id = this_planning.parents('tr').find('.planning_id').val();

            var currentIndex = parseInt($('#indexRows').val()) - 1 ;
            $('#indexRows').val(currentIndex);

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': '<?php echo csrf_token(); ?>'
                }
            });

            $.ajax({
                url: '/planning/'+plan_id+'/sendbatmb',
                type: 'POST',
                data: {
                    planning_id: plan_id
                },
                dataType: 'JSON',
                success: function (data) {
                    if(data == true) {
                        toastr.success('Le BAT a bien été envoyé.', 'Ok !');
                    } else {
                        toastr.success("Une erreur s'est produite pendant l'envoi du BAT.", 'Erreur !');
                    }
                },
                error: function (e) {
//                    toastr.success("Une erreur s'est produite pendant l'envoi du BAT.", 'Erreur !');
                }
            });
        });


        $('#store').click(function(e){
            e.preventDefault();
            warnBeforeRedirect($(this));
        });

    });
</script>
@endsection
