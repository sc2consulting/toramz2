@extends('common.layout')

@section('content')

    <div class='container'>
        <h1>Génération de Liste d'envoi</h1>

        <form method="post">

            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

            <label class="title"> Base <span style='color:red'>*</span> </label> <br/>
            <label>CDD <input name="base" type="radio" value="cdd" <?php if( isset($_POST["base"]) && $_POST["base"] == "cdd" ) echo "checked"; else echo "checked"; ?> /> &nbsp; </label>
            <label>PDF <input name="base" type="radio" value="pdf" <?php if( isset($_POST["base"]) && $_POST["base"] == "pdf" ) echo "checked"; ?> /> &nbsp; </label>

            <hr>

            <label class="title"> Campagne <span style='color:red'>*</span> </label> <br/>
            <label> <input id='campagne' name="campagne" type="text" value="<?php if( isset($_POST["campagne"]) ) echo $_POST["campagne"]; ?>" style='width:200px;' /> &nbsp; </label>
            <div id='resultat'> </div>
            <hr>


            <label class="title"> Nombre de mails <span style='color:red'>*</span> </label>
            <br/>
            <label>&nbsp;&nbsp;<input type="number" name="mailNb" min="10" max="1000000" value="<?php if( isset($_POST["mailNb"]) ) echo $_POST["mailNb"]; else echo "50000"; ?>" /> </label>
            <br/><br/>

            <hr>

            <label class="title"> Envoi <span style='color:red'>*</span> </label>
            <div id='envoiChoix'>

                <label> &nbsp;&nbsp;Maintenant <input type="radio" name="choixenvoi" value="instant" onClick="displayBlock('instant');" /> </label>
                <label> &nbsp;&nbsp;Plus tard <input type="radio" name="choixenvoi" value="today" onClick="displayBlock('today');" /> </label>
                <label> &nbsp;&nbsp;Sur plusieurs jours <input type="radio" name="choixenvoi" value="days" onClick="displayBlock('days');" /> </label>
            </div>

            <div id='envoiBlock' style='display:none'>
                <p id='envoiBlock1'>
                    <label id='start_datelab' > <input width="175" type="datetime-local" name="start_date" /> </label>
                    <label id='end_datelab' style='display:none'> au <input width="175" type="datetime-local" name="end_date" /> </label>
                    <br/>

                    <label> Plateforme <span style='color:red'>*</span> <select id='plateforme1' name='plateforme[1]' class='plateforme' onchange='changeSenders(1);'> </select> </label>
                    <label class='labelSend'> Sender <span style='color:red'>*</span> <select multiple="multiple" id='sender1' name="sender[1][]" class="sender" > </select>  </label> <span onclick='more();' class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span>
                </p>
            </div>

            <input type='hidden' id="nbMore" value='1'>

            <hr>

            <label class="title"> FAI(s) <span style='color:red'>*</span> </label>
            <p class='details'> <a target='_blank' href='setpressionM.php' style='font-size:11px;' > Modifier config. pression marketing</a> <br/> <em style='display:none;'> (Lorsqu'il y a plus d'un FAI, merci d'indiquer le nombre de mails souhait&eacute; par FAI, <br/> si ce n'est pas pr&eacute;cis&eacute; le nombre de mails/FAI est al&eacute;atoire.) </em> </p>

            <table>
                <tr>
                    <td>
                        <label>Orange  <input class='fais' type="checkbox" name="fais[]" value="orange" <?php if( !is_null($_POST) and array_key_exists("fais", $_POST) and (in_array("orange", $_POST["fais"]) or in_array("tout", $_POST["fais"])) ) echo "checked" ?> /> &nbsp; 	</label> <br/>
                        <input class='pourFai' type="number" name="pourFai[orange]" style='width:70px;height:20px;' />
                    </td>

                    <td>
                        <label>Free  <input class='fais' type="checkbox" name="fais[]" value="free" <?php if( !is_null($_POST) and array_key_exists("fais", $_POST) and (in_array("free", $_POST["fais"]) or in_array("tout", $_POST["fais"])) ) echo "checked" ?>  /> &nbsp; </label> <br/>
                        <input class='pourFai' type="number" name="pourFai[free]" style='width:70px;height:20px;' />
                    </td>

                    <td>
                        <label>Hotmail  <input class='fais' type="checkbox" name="fais[]" value="hotmail" <?php if( !is_null($_POST) and array_key_exists("fais", $_POST) and (in_array("hotmail", $_POST["fais"]) or in_array("tout", $_POST["fais"])) ) echo "checked" ?> /> &nbsp; </label><br/>
                        <input class='pourFai' type="number" name="pourFai[hotmail]" style='width:70px;height:20px;' />
                    </td>

                    <td>
                        <label>Yahoo <input class='fais' type="checkbox" name="fais[]" value="yahoo" <?php if( !is_null($_POST) and array_key_exists("fais", $_POST) and (in_array("yahoo", $_POST["fais"]) or in_array("tout", $_POST["fais"])) ) echo "checked" ?> /> &nbsp; </label><br/>
                        <input class='pourFai' type="number" name="pourFai[yahoo]" style='width:70px;height:20px;' />
                    </td>

                    <br/>

                    <td>
                        <label>Gmail <input class='fais' type="checkbox" name="fais[]" value="gmail" <?php if( !is_null($_POST) and array_key_exists("fais", $_POST) and (in_array("gmail", $_POST["fais"]) or in_array("tout", $_POST["fais"])) ) echo "checked" ?> /> &nbsp; </label><br/>
                        <input class='pourFai' type="number" name="pourFai[gmail]" style='width:70px;height:20px;' />
                    </td>

                    <td>
                        <label>SFR <input class='fais' type="checkbox" name="fais[]" value="sfr" <?php if( !is_null($_POST) and array_key_exists("fais", $_POST) and (in_array("sfr", $_POST["fais"]) or in_array("tout", $_POST["fais"])) ) echo "checked" ?> /> &nbsp; </label><br/>
                        <input class='pourFai' type="number" name="pourFai[sfr]" style='width:70px;height:20px;' />
                    </td>

                    <td>
                        <label> Laposte <input class='fais' type="checkbox" name="fais[]" value="laposte" <?php if( !is_null($_POST) and array_key_exists("fais", $_POST) and (in_array("laposte", $_POST["fais"]) or in_array("tout", $_POST["fais"])) ) echo "checked" ?> /> &nbsp; </label><br/>
                        <input class='pourFai' type="number" name="pourFai[laposte]" style='width:70px;height:20px;' />
                    </td>

                    <td>
                        <label>Autre <input class='fais' type="checkbox" name="fais[]" value="autre" <?php if( !is_null($_POST) and array_key_exists("fais", $_POST) and (in_array("autre", $_POST["fais"]) or in_array("tout", $_POST["fais"])) ) echo "checked" ?> /> &nbsp; </label><br/>
                        <input class='pourFai' type="number" name="pourFai[autre]" style='width:70px;height:20px;' />
                    </td>

                    <td>
                        <label>Tout <input type="checkbox" name="fais[]" value="tout" <?php if( !is_null($_POST) and array_key_exists("fais", $_POST) and in_array("tout", $_POST["fais"]) ) echo "checked" ?> onClick='checkAll();'/> &nbsp; </label>
                    </td>
                </tr>

            </table>
            <br/>
            <hr>

            <label class="title"> Activit&eacute; des mails</label>
            <p class='details'> <em> (Facultatif) </em> </p>


            <br/>
            <label>Ouvreurs  <input name='statutMail' type="radio" value="ouvreurs"<?php if( isset($_POST["statutMail"]) && $_POST["statutMail"] == "ouvreurs" ) echo "checked"; ?> onClick="afficherJours();" /> &nbsp; </label>
            <label>Cliqueurs <input name='statutMail' type="radio" value="cliqueurs"<?php if( isset($_POST["statutMail"]) && $_POST["statutMail"] == "cliqueurs"  ) echo "checked"; ?> onClick="afficherJours();" /> &nbsp; </label>

            <div id='jours' <?php if( empty($_POST["nbDays"]) ) echo "style='display:none;'"; else echo "style='display:inline;'";?>>
                <label>de moins de <input name='nbDays' type="number" min="1" max="365" value="<?php if( isset($_POST["nbDays"]) ) echo $_POST["nbDays"];?>" /> jours &nbsp; </label>
            </div>

            <br/><br/>

            <input type="hidden" name="envoi" value="generation" />
            <input type="submit" name="test" value="Chercher des mails" />

        </form>
        <br/>



@endsection

@section('footer')
    <script type="text/javascript">

        $.fn.setNow = function (onlyBlank) {
            var now = new Date($.now())
                    , year
                    , month
                    , date
                    , hours
                    , minutes
                    , seconds
                    , formattedDateTime
                    ;

            year = now.getFullYear();
            month = now.getMonth().toString().length === 1 ? '0' + (now.getMonth() + 1).toString() : now.getMonth() + 1;
            date = now.getDate().toString().length === 1 ? '0' + (now.getDate()).toString() : now.getDate();
            hours = now.getHours().toString().length === 1 ? '0' + now.getHours().toString() : now.getHours();
            minutes = now.getMinutes().toString().length === 1 ? '0' + now.getMinutes().toString() : now.getMinutes()+5;
            seconds = now.getSeconds().toString().length === 1 ? '0' + now.getSeconds().toString() : now.getSeconds();

            //formattedDateTime = year + '-' + month + '-' + date + 'T' + hours + ':' + minutes + ':' + seconds;
            formattedDateTime = year + '-' + month + '-' + date + 'T' + hours + ':' + minutes + ':' + '00';

            if ( onlyBlank === true && $(this).val() ) {
                return this;
            }

            $(this).val(formattedDateTime);

            return this;
        }

        $.fn.setAfter = function (onlyBlank) {
            var now = new Date($.now())
                    , year
                    , month
                    , date
                    , hours
                    , minutes
                    , seconds
                    , formattedDateTime
                    ;

            year = now.getFullYear();
            month = now.getMonth().toString().length === 1 ? '0' + (now.getMonth() + 1).toString() : now.getMonth() + 1;
            date = now.getDate().toString().length === 1 ? '0' + (now.getDate()).toString() : now.getDate()+1;
            hours = now.getHours().toString().length === 1 ? '0' + now.getHours().toString() : now.getHours();
            minutes = now.getMinutes().toString().length === 1 ? '0' + now.getMinutes().toString() : now.getMinutes()+5;
            seconds = now.getSeconds().toString().length === 1 ? '0' + now.getSeconds().toString() : now.getSeconds();

            //formattedDateTime = year + '-' + month + '-' + date + 'T' + hours + ':' + minutes + ':' + seconds;
            formattedDateTime = year + '-' + month + '-' + date + 'T' + '23' + ':' + '59' + ':' + '00';

            if ( onlyBlank === true && $(this).val() ) {
                return this;
            }

            $(this).val(formattedDateTime);

            return this;
        }

        function multiSelect()
        {
            var numero = document.getElementById('nbMore').value;
            var cal = parseInt(numero,10);
            var n = cal.toString();
            var idsend = "#sender"+n;
            //alert(idsend);

            $(idsend).multipleSelect({
                filter: true,
                placeholder : 'Choisir un compte',
                width: '250px'
            });

        }

        function displayBlock(param)
        {

            multiSelect();

            $("#envoiBlock").show();
            if(param == 'instant')
            {
                $("#end_datelab").hide();
                $("#start_datelab").hide();
            }

            if(param == 'today')
            {
                $("#start_datelab").show();

                $(function () {
                    // Handler for .ready() called.
                    $('#start_datelab input[type="datetime-local"]').setNow();

                });
                $("#end_datelab").hide();
            }

            if(param == 'days')
            {
                $("#start_datelab").show();
                $("#end_datelab").show();
                $(function () {
                    // Handler for .ready() called.
                    $('#start_datelab input[type="datetime-local"]').setNow();
                    $('#end_datelab input[type="datetime-local"]').setAfter();

                });
            }
        }
        function chargeSelects(num)
        {
            //Chargement du select (plateformes et comptes/senders)
            var elt = document.getElementById('plateforme'+num);
            var eleme = document.getElementById('sender'+num);
            var compt = 1;

            $.ajax({
                type: 'POST',
                dataType: "json",
                url: 'autocomplet2.php',
                data: 'select=plateforme',

                success: function(data){
                    $.each(data, function(index, element) {
                        var option = document.createElement("option");
                        var plateforme = index;
                        option.text = plateforme;
                        option.value = plateforme;
                        elt.appendChild(option);
                        if(compt==1)
                        {
                            $.each(element, function(ind, ele) {

                                var hiddenOption = document.createElement("option");
                                hiddenOption.text = ind;
                                hiddenOption.value = ind;
                                eleme.appendChild(hiddenOption);

                            });
                        }
                        compt++;
                    });
                    multiSelect();
                }
            });

        }

        function sleep(milliseconds)
        {
            var start = new Date().getTime();
            for (var i = 0; i < 1e7; i++) {
                if ((new Date().getTime() - start) > milliseconds){
                    break;
                }
            }
        }

        function more()
        {
            var numero = document.getElementById('nbMore').value;
            var cal = parseInt(numero,10)+1;
            var appendHTML = "<p id='envoiBlock"+cal+"'> <label> Plateforme <select id='plateforme"+cal+"' name='plateforme["+cal+"]' class='plateforme' onchange='changeSenders("+cal+");'> </select> </label> <label class='labelSend'> Sender <select multiple='multiple' id='sender"+cal+"' name='sender["+cal+"][]' class='sender'> </select> </label> <span onclick='more();' class='glyphicon glyphicon-plus-sign' aria-hidden='true'></span></p> " ;

            $( "#envoiBlock" ).append( appendHTML );
            document.getElementById("nbMore").value = cal;
            chargeSelects(cal);
        }

        function changeSenders(numero)
        {
            var plateforme = document.getElementById('plateforme'+numero).value;
            var eltsend = document.getElementById('sender'+numero);

            $.ajax({
                type: 'POST',
                dataType: "json",
                url: 'autocomplet2.php',
                data: 'sender='+plateforme,

                success: function(data){
                    $('#sender'+numero).empty();
                    //alert('#sender'+numero+' is empty now');
                    $.each(data, function(index, element) {
                        var option = document.createElement("option");
                        var sender = index;
                        option.name = "sender";
                        option.text = sender;
                        option.value = sender;
                        eltsend.appendChild(option);
                    });
                    multiSelect();
                }
            });
        }

        $(document).ready(function()
        {
            //Auto-completion pour les campagnes existantes
            $("#campagne").on("keyup", function(e) {

                $('#resultat').show('fast');
                campagne_val = document.getElementById('campagne').value;
                base_val = $('input[name=base]:checked').val();
                //alert(base_val);
                $.post("autocomplet2.php", {
                            campagne : campagne_val,
                            base : base_val },
                        function(data) {
                            $('#resultat').html(data);
                        });
            });

            //Chargement des selects (plateformes et comptes/senders)
            chargeSelects(1);
            //return false;

        });

        function modifChamp (camp)
        {
            document.getElementById("campagne").value = camp;
        }

        function afficherJours()
        {
            if(document.getElementById('jours').style.display != 'inline'){
                document.getElementById('jours').style.display = 'inline';
            }
            else
            {
                document.getElementById('jours').style.display = 'none';
            }
        }

        function checkAll()
        {
            var inputs = document.getElementsByClassName('fais');
            for(i = 0; i < inputs.length; i++)
            {
                if(inputs[i].type == 'checkbox' && inputs[i].checked == false )
                {
                    inputs[i].checked = true;
                }
                else if(inputs[i].type == 'checkbox' && inputs[i].checked == true )
                {
                    inputs[i].checked = false;
                }
            }
        }

    </script>
@endsection