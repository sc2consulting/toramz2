@extends('common.layout')

@section('content')

    <div class="portlet light">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-cogs font-green-sharp"></i>
                <span class="caption-subject font-green-sharp bold uppercase">Statistiques Phoenix API</span>
            </div>
            <div class="actions">
                <div class="btn-group btn-group-devided">
                    <a href="/stats" class="btn btn-success">Retour</a>
                    <!-- <a href="/button2" class="btn btn-primary">Bouton2</a> -->
                </div>
            </div>
        </div>
        <div class="portlet-body">

          <div class="caption">
              <i class="fa fa-cogs font-blue"></i>
              <span class="caption-subject font-blue bold uppercase">Liste des campagnes Phoenix :</span>
          </div>

          <table class="table table-hover table-bordered table-striped">
          <tr>

              <th>Référence</th>
              <th>Ouvreurs</th>
              <th>Cliqueurs</th>
              <th>Désinscription</th>
              <th>Action</th>
          </tr>

          @foreach($phoenixstats as $phoenix)

          <tr>
              <td>{{$phoenix->reference}} | <span>Créé le {{$phoenix->created_at}}</span></td>
              <td>{{$phoenix->ouvreurs}}</td>
              <td>{{$phoenix->cliqueurs}}</td>
              <td>{{$phoenix->desabo}}</td>

              <td><a href="/stats/routeur/phoenix/{{ $phoenix->reference }}"/> <button type="button" class="btn btn-warning">+ de détails</button></a></td>
          </tr>
          @endforeach

        </table>
        </div>
    </div>

@endsection
