@extends('common.layout')

@section('content')
<div class="portlet light">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-cogs font-green-sharp"></i>
            <span class="caption-subject font-green-sharp bold uppercase">Statistiques Mindbaz API</span>
        </div>
        <div class="actions">
            <div class="btn-group btn-group-devided">
                <a href="/stats" class="btn btn-success">Retour</a>
            </div>
        </div>
    </div>
    <div class="portlet-body">
      <div class="caption">
          <i class="fa fa-cogs font-blue"></i>
          <span class="caption-subject font-blue bold uppercase">Liste des campagnes Mindbaz :</span>
      </div>

        <table class="table table-hover table-bordered table-striped">
            <tr>
              <th>Référence</th>
              <th>Ouvreurs</th>
              <th>Cliqueurs</th>
              <th>Désinscription</th>
              <th>Erreurs</th>
              <th>Action</th>
            </tr>
            @foreach($statsrouteurv2 as $mindbaz)
            <tr>
              <td>{{$mindbaz->reference}} |  <span>Créé le {{$mindbaz->created_at}}</span></td>
              <td>{{$mindbaz->openers}}</td>
              <td>{{$mindbaz->clickers}}</td>
              <td>{{$mindbaz->unsubscribers}}</td>
              <td>{{$mindbaz->soft_bounces+$mindbaz->hard_bounces+$mindbaz->spam_bounces+$mindbaz->spam_complaints}}</td>
              <td><a href="/stats/routeur/mindbaz/{{ $mindbaz->reference }}"> <button type="button" class="btn btn-warning">+ de détails</button></a></td>
            </tr>
      @endforeach
      </table>
    </div>
</div>
@endsection
