@extends('common.layout')

@section('content')
    <div class="portlet light">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-cogs font-green-sharp"></i>
                <span class="caption-subject font-green-sharp bold uppercase">
                    Notifications
                </span>
            </div>
            <div class="actions">
                <div class="btn-group btn-group-devided">
                </div>
            </div>
        </div>
        <div class="portlet-body">
            <div class="timeline">
                @foreach($notifs as $notification)
                    @if(!is_null($notification->user) && ($notification->user_id == 1 or $notification->user_id == \Auth::User()->id ))
                        <div class="timeline-item">
                            <div class="timeline-badge">
                            </div>
                            <div class="timeline-body">
                                <div class="timeline-body-arrow">
                                </div>
                                <div class="timeline-body-head">
                                    <div class="timeline-body-head-caption">
                                        <span class="timeline-body-title font-blue-madison">{{ $notification->user->name }}</span>
                                        <span class="timeline-body-time font-grey-cascade">{{ $notification->created_at }}</span>
                                    </div>
                                    <div class="timeline-body-head-actions">
                                        @if (strlen($notification->url) > 0)
                                            <a class="btn btn-primary" href="{{$notification->url}}">Détails</a>
                                        @endif
                                    </div>
                                </div>
                                <div class="timeline-body-content">
                            <span class="font-grey-cascade">
                            {{ $notification->message }}
                            </span>
                                </div>
                            </div>
                        </div>
                    @endif
                @endforeach
            </div>
            {!! $notifs->render() !!}
        </div>
    </div>
@endsection
