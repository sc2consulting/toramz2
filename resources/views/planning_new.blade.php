@extends('template')

@section('content')
    @if(!function_exists('big_number'))
        <?php
        function big_number($value) {
            return number_format($value, 0, ',',' ');
        }
        ?>
    @endif
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_title">
                <h2>Planning des campagnes</h2>
                <ul class="nav navbar-right panel_toolbox"></ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <br>
                <div class="row">
                    <form id="baseTri" action="/planning" method="get">
                        <p>
                            <select id="base_id" name="base_id" class="form-control input-medium" onchange='filterByBase();'>
                                <option> Filtrer par base </option>
                                @foreach ($basesinfo as $labase)
                                    <option value="{{$labase->id}}" @if($labase->id == $base_id) selected="selected" @endif>{{$labase->nom}}</option>
                                @endforeach
                                <option value="0" @if( is_numeric($base_id) && $base_id == 0) selected="selected" @endif> Multi-bases </option>
                            </select>
                        </p>
                    </form>
                </div>
            </div>

            <div class="col-md-12 col-xs-12">
                <a href="/planning/<?php echo date('Y-m-d');?>" class="btn btn-info">Plannings d'aujourdhui</a>
                <a href="#" data-opened="false" id="datepicker-calendar" class="btn btn-info">Visualiser les plannings d'une date</a>
                <a href="#" id="calendarPlannings" class="btn btn-info">Calendrier des plannings</a>
                <div class="container">
                    <div style="display:none" id="datepicker-plannings"></div>
                    <div id="calendar-plannings" style="display:none" class="row">
                        <div class="col-md-6">
                            <div id="plannings-calendar"></div>
                        </div>
                        <div class="col-md-6">
                            <h4>Légendes:</h4>
                            <div id="references-plannings-colors"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="x_content">
                <br>
                <div class="row">
                    <div class="table-responsive">
                        <div style="overflow-x:auto;">
                            <table class="table table-striped table-hover table-bordered">
                                <tr>
                                    <th style="text-align:center;">ID</th>
                                    <th style="text-align:center;">Date</th>
                                    <th style="text-align:center;">Base</th>
                                    <th style="text-align:center;">Campagne</th>
                                    <th style="text-align:center;">Volume <br /> demandé</th>
                                    <th style="text-align:center;">Tokens</th>
                                    <th style="text-align:center;">Volume <br /> sélectioné</th>
                                    <th style="text-align:center;">Segments</th>
                                    <th style="text-align:center;">Envoyée</th>
                                    <th style="text-align:center;">Routeur</th>
                                    <th style="text-align:center;">Action</th>
                                </tr>
                                @foreach($plannings as $leplanning)
                                    <tr>
                                        <td style="text-align:center;">{{ $leplanning->id }}</td>
                                        <td @if ($leplanning->date_campagne == date('Y-m-d')) style="background:#EFE" @elseif($leplanning->date_campagne < date('Y-m-d')) style="background:#EEF"  @else style="background:#fff2e6" @endif>{{date('d-m-Y', strtotime($leplanning->date_campagne))}} {{date('H:i',strtotime($leplanning->time_campagne))}}</td>
                                        <td style="text-align:center;">{{ $leplanning->code }}@if(is_null($leplanning->code)) multi @endif</td>
                                        <td style="text-align:center;">{{ $leplanning->ref }}</td>
                                        <td style="text-align:center;">{{ big_number($leplanning->volume) }}</td>
                                        <td style="text-align:center;"> @if ($leplanning->tokens_at == null)  <span style="color:#900" class="green icon-check"></span> @else<span style="color:#090" class="green icon-check"> {{ \Carbon\Carbon::parse($leplanning->tokens_at)->format('H:i') }}</span>@endif </td>
                                        <td style="text-align:center;"> @if ($leplanning->volume_selected == null)  - @else<span style="color:purple;"> {{ big_number($leplanning->volume_selected) }}</span>@endif </td>
                                        <td style="text-align:center;"> <a target="_blank" href="/planning/{{$leplanning->id}}/senders">
                                                @if ($leplanning->segments_at == null)  <span style="color:#900" class="green icon-check"></span>
                                                @else<span style="color:#090" class="green icon-check"> {{ \Carbon\Carbon::parse($leplanning->segments_at)->format('H:i') }}</a></span>

                                            <br>
                                            <span>
                                                        <?php

                                                $count_account = \DB::table('campagnes_routeurs')
                                                    ->select('sender_id')
                                                    ->where('planning_id', $leplanning->id)
                                                    ->count();

                                                echo ('Nb Compte(s) : ' . $count_account);

                                                ?>
                                                    </span>
                                            <br>

                                            <?php

                                            $la_branch = \DB::table('branches')
                                                ->select('name')
                                                ->where('id', $leplanning->branch_id)
                                                ->first();

                                            echo $la_branch->name;
                                            ?>

                                            @endif </td>
                                        <td style="text-align:center;"> @if ($leplanning->sent_at == null)  <span style="color:#900" class="green icon-check"></span> @else<span style="color:#090" class="green icon-check"> {{ \Carbon\Carbon::parse($leplanning->sent_at)->format('H:i') }}</span>@endif </td>
                                        <td style="text-align:center;">
                                            <?php
                                            $plan = \App\Models\Planning::find($leplanning->id);
                                            echo $plan->routeur->nom . '<hr>';
                                            if(!empty($plan->selectedsender)) {
                                                echo $plan->selectedsender->nom;
                                            }
                                            ?>
                                        </td>
                                        <td style="text-align:center;">
                                            <a class="btn btn-primary btn-sm" href="/campagne/{{$leplanning->campagne_id}}/@if($is_mindbaz_list && $plan->routeur->nom=='Mindbaz'){{'mindbaz_'}}@endif{{'planning'}}">Planifier</a>
                                            <a class="btn btn-primary btn-sm" href="/campagne/{{$leplanning->campagne_id}}/edit">Editer HTML</a>
                                        </td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            {!! $plannings->appends(['base_id' => $base_id])->render() !!}
        </div>
    </div>
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

    </script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.9.0/fullcalendar.css">
    <link rel="stylesheet" media="print" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.9.0/fullcalendar.print.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment-with-locales.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.9.0/fullcalendar.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.9.0/locale-all.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>


    <script type="text/javascript">
        let basesColor = [];
        let token = '{{csrf_token()}}';
        function getRandomColor() {
            var letters = '0123456789ABCDEF';
            var color = '#';
            for (var i = 0; i < 6; i++) {
                color += letters[Math.floor(Math.random() * 16)];
            }
            return color;
        }

        function createReference(name,color){
            let reference = document.createElement('p');
            let square = document.createElement('div');
            reference.classList.add('col-md-6');
            reference.classList.add('col-xs-6');
            square.style.borderColor = "dark";
            square.style.width = "20px";
            square.style.height = "20px";
            square.style.backgroundColor = color;
            square.classList.add('col-md-6');
            square.classList.add('col-xs-6');
            reference.innerHTML = name
            reference.append(square);
            return reference
        }

        $(document).ready(()=>{
            $('#datepicker-calendar').click(()=>{
                if(!$('#datepicker-calendar').data('opened')){
                    $('#datepicker-plannings').fadeIn();
                    $('#datepicker-calendar').data("opened",true);
                }else{
                    $('#datepicker-plannings').fadeOut();
                    $('#datepicker-calendar').data("opened",false);
                }
            });
            $( "#datepicker-plannings" ).datepicker({
                onSelect:()=>{
                    var dateSelected = $('#datepicker-plannings').datepicker('option',"dateFormat","yy-mm-dd")[0].value;
                    window.location.href = `/planning/${dateSelected}`;
                }
            });
        });
        @foreach($bases as $base)
            basesColor.push({
                name:"{!!$base->code!!}",
                color:getRandomColor()
            });
        document.getElementById('references-plannings-colors').append(createReference(basesColor[basesColor.length -1].name,basesColor[basesColor.length -1].color));
    @endforeach

    </script>
    <script src="/js/calendar-tor.js"></script>
        <script type="text/javascript">
            function filterByBase() {
                document.getElementById("baseTri").submit();
            }
        </script>
    @endsection
