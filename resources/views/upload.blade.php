@extends('common.layout')

@section('content')

<div class="portlet light">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-cogs font-green-sharp"></i>
            <span class="caption-subject font-green-sharp bold uppercase">Upload repoussoirs (en MD5)</span>
        </div>
    </div>

    <div class="portlet-body">
       <div class="text-content">
         <div class="span7 offset1">
            @if(Session::has('success'))
              <div class="alert-box success">
              <h2>{!! Session::get('success') !!}</h2>
              </div>
            @endif
            <div class="secure">Charger un fichier md5</div>
            {!! Form::open(array('url'=>'apply/upload','method'=>'POST', 'files'=>true)) !!}
             <div class="control-group">
              <div class="controls">
              {!! Form::file('fichier') !!}
          <p class="errors">{!!$errors->first('fichier')!!}</p>
        @if(Session::has('error'))
        <p class="errors">{!! Session::get('error') !!}</p>
        @endif
            </div>
            </div>
            <div id="success"> </div>
          {!! Form::submit('Envoyer', array('class'=>'send-btn')) !!}
          {!! Form::close() !!}
          </div>
       </div>
    </div>
</div>

@endsection