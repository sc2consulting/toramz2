@extends('template')

@section('content')
    <div class="container">
          <div class="row">
               <div class="col-xs-12 col-sm-offset-4 col-md-offset-4 col-lg-offset-4">
                    <i class="fa fa-2x fa-cogs text-success"></i>
                    <span class="lead text-success">Statistiques générales</span>
               </div>
          </div>
          <br>
               <!-- <div class="actions">
               <div class="btn-group btn-group-devided">
               {{--<a href="/campagne/create" class="btn btn-success">Créer une campagne</a>--}}
               {{--<a href="/theme" class="btn btn-primary">Gérer les Thèmes</a>--}}
               </div></div> -->
          <div class="row">
               <div class="col-sm-4 col-md-4 col-lg-4"></div>
               <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                    <table class="table table-hover table-striped table-bordered table-condensed">
                         <tr>
                              <th>Destinataires (total)</th>
                              <td>{{ \DB::table('destinataires')->where('base_id', $base->id)->count() }}</td>
                         </tr>
                         <tr>
                              <th>Destinataires actifs</th>
                              <td>{{ \DB::table('destinataires')->where('statut', 0)->where('base_id', $base->id)->count() }}</td>
                         </tr>
                         <tr>
                              <th>Désinscrits</th>
                              <td>{{ \DB::table('destinataires')->where('statut','>',2)->where('base_id', $base->id)->count() }}</td>
                         </tr>
                         <tr>
                              <th>Bounces</th>
                              <td>{{ \DB::table('destinataires')->where('statut', DESTINATAIRE_BOUNCE)->where('base_id', $base->id)->count() }}</td>
                         </tr>
                    </table>
               </div>
               <div class="col-sm-4 col-md-4 col-lg-4"></div>
          </div>


          <div class="row">
               <div class="col-xs-12 col-sm-offset-4 col-md-offset-4 col-lg-offset-4 ">
                    <i class="fa fa-2x fa-cogs text-success"></i>
                    <span class="lead text-success">Répartition des Actifs par FAI</span>
               </div>
          </div>
          <br>
               <!-- <div class="actions">
               <div class="btn-group btn-group-devided">
               {{--<a href="/campagne/create" class="btn btn-success">Créer une campagne</a>--}}
               {{--<a href="/theme" class="btn btn-primary">Gérer les Thèmes</a>--}}
               </div></div> -->
          <div class="row">
               <div class="col-sm-4 col-md-4 col-lg-4"></div>
               <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                    <table class="table table-hover table-striped table-bordered table-condensed">
                         @foreach($fais as $fai)
                              @if ($fai->fai_id > 0)
                                   <tr>
                                        <th>{{ \App\Models\Fai::find($fai->fai_id)->nom }}</th>
                                        <td>{{ $fai->num }}</td>
                                   </tr>
                              @endif
                         @endforeach
                    </table>
               </div>
               <div class="col-sm-4 col-md-4 col-lg-4"></div>
          </div>

          <div class="row">
               <div class="col-xs-12 col-sm-offset-4 col-md-offset-4 col-lg-offset-4 ">
                    <i class="fa fa-2x fa-cogs text-success"></i>
                    <span class="lead text-success">Performances par Origine (Liste)</span>
               </div>
          </div>
          <br>
               <!-- <div class="actions">
               <div class="btn-group btn-group-devided">
               {{--<a href="/campagne/create" class="btn btn-success">Créer une campagne</a>--}}
               {{--<a href="/theme" class="btn btn-primary">Gérer les Thèmes</a>--}}
               </div></div> -->
          <?php $listes = \Db::table('destinataires')->selectRaw('origine, count(*) as num')->where('base_id', $base->id)->groupBy('origine')->get(); ?>
          @foreach($listes as $liste)
          <h2>{{ $liste->origine  }}</h2>

          <div class="row">
               <div class="col-sm-4 col-md-4 col-lg-4"></div>
               <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                    <table class="table table-hover table-striped table-bordered table-condensed">
                         <tr>
                              <th>Total</th>
                              <td>{{ $liste->num }}</td>
                         </tr>
                         <tr>
                              <th>Actifs</th>
                              <td>
                                   {{ \Db::table('destinataires')->where('base_id', $base->id)->where('origine', $liste->origine)->where('statut', 0)->count() }}
                              </td>
                         </tr>
                         <tr>
                              <th>Bounces</th>
                              <td>
                                   {{ \Db::table('destinataires')->where('base_id', $base->id)->where('origine', $liste->origine)->where('statut', DESTINATAIRE_BOUNCE)->count() }}
                              </td>
                         </tr>
                         <tr>
                              <th>Désinscrits</th>
                              <td>
                                   {{ \Db::table('destinataires')->where('base_id', $base->id)->where('origine', $liste->origine)->where('statut', DESTINATAIRE_OPTOUT)->count() }}
                              </td>
                         </tr>
                         <tr>
                              <th>Abuse</th>
                              <td>
                                   {{ \Db::table('destinataires')->where('base_id', $base->id)->where('origine', $liste->origine)->where('statut', DESTINATAIRE_ABUSED)->count() }}
                              </td>
                         </tr>
                    </table>
               </div>
               <div class="col-sm-4 col-md-4 col-lg-4"></div>
          </div>
          @endforeach
     </div>
@endsection

@section('footer-scripts')
     <script src="/js/charts.js" />
     <script>
          $.ready(function() {
               ChartsFlotcharts.init();
               ChartsFlotcharts.initCharts();
               ChartsFlotcharts.initPieCharts();
               ChartsFlotcharts.initBarCharts();
               $.plot($("#chart_ouvreurs"), [ [[0, 0], [1, 1]] ], { yaxis: { max: 1 } });
          })
     </script>
@endsection
