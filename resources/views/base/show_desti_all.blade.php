@extends('common.layout')

@section('content')

    <div class="portlet light">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-cogs font-green-sharp"></i>
                <span class="caption-subject font-green-sharp bold uppercase">
                    Destinataires toutes bases confondues
                </span>
            </div>
            <div class="actions">
                <div class="btn-group btn-group-devided">
                    {{--<a href="/campagne/create" class="btn btn-success">Créer une campagne</a>--}}
                    {{--<a href="/theme" class="btn btn-primary">Gérer les Thèmes</a>--}}
                </div>
            </div>
        </div>
        <div class="portlet-body">

          <span>Rechercher : </span>
         {!! Form::open(array('url' => 'base/searchmailall','files' => true, 'method' => 'post')) !!}
         {!! Form::label('recherche', ' ') !!}
         {!! Form::text('recherche', $recherche) !!}
         <button type="submit" class="btn btn-success btn-xs">Valider</button>
         @if($desti->count()>0)
         <input value="Exporter" name="export" type="submit" class="btn btn-xs btn-info">
         <a href="/base/searchmailall/desabo/{{$recherche}}" class="btn btn-xs btn-info">Désabonner</a>
         @endif
         {!! Form::close() !!}
         <br />
         <br />


          <table class="table table-bordered table-striped table-hover">
              <tr>
                  <th>ID</th>
                  <th>Mail</th>
                  <th>Nom</th>
                  <th>Prénom</th>
                  <th>Date naissance</th>
                  <th>Ville</th>
                  <th>Département</th>
                  <th>Sexe</th>
                  <th>Statut</th>
                  <th>Base_id</th>

              </tr>

              @foreach($desti as $d)
                  <tr>
                      <td>{{ $d->id }}</td>
                      <td>{{ $d->mail }}</td>
                      <td>{{ $d->nom }}</td>
                      <td>{{ $d->prenom }}</td>
                      <td>{{ $d->datenaissance }}</td>
                      <td>{{ $d->ville }}</td>
                      <td>{{ $d->departement }}</td>
                      <td>
                       <?php
                        if($d->civilite === 0){
                          echo 'Femme';
                        }
                        if($d->civilite == 1){
                          echo 'Homme';
                        }
                        if($d->civilite === null || $d->civilite == 3){
                          echo 'N/A';
                        }
                       ?>

                      </td>
                      <td>
                        <?php
                          if($d->statut > 0){
                            echo '<span style="color:#c0392b"> Désabonné </span>';
                          } else {
                            echo '<span style="color:#27ae60"> Abonné </span>';
                          }
                        ?>

                      </td>

                    <td>
                    {{ $d->base_id }}
                  </td>
                  </tr>
              @endforeach
          </table>
          {!! $desti->appends(['recherche' => $recherche])->render() !!}
        </div>
    </div>

@endsection
