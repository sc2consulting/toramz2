@extends('common.layout')

@section('content')
<div class="portlet light">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-cogs font-green-sharp"></i>
            <span class="caption-subject font-green-sharp bold uppercase">Configuration planning par defaut</span>
        </div>
        <div class="actions">
            <div class="btn-group btn-group-devided">
              <a title="Retour" href="/planning" class="btn btn-success">Retour Planning</a>
            </div>
        </div>
    </div>
    <div class="portlet-body">

      <div class="row">
        {!! Form::open(array('url' => 'planning/defaut/'.$id.'/edit','files' => true, 'method' => 'post')) !!}


        <div class="form-group">
            <label for="name_config" class="col-sm-2 control-label"><strong>Nom</strong></label>
                <input type="text" name="name_config" id="name_config" value="{{$data_nom->value}}"/>
        </div>

        <div class="form-group">
            <label for="routeur_id" class="col-sm-2 control-label"><strong>Routeur</strong></label>
            <select class="target" name="routeur_id">
              @foreach($routeurs as $r)
              @if($r->id == $data_routeur->value)
              <option value="{{$r->id}}" selected="selected">{{$r->nom}}</option>
              @else
              <option value="{{$r->id}}">{{$r->nom}}</option>
              @endif
              @endforeach
            </select>
        </div>

        <div class="form-group">
            <label for="routeur_id" class="col-sm-2 control-label"><strong>Branche</strong></label>
            <select class="target" name="la_branche" >
              @foreach($branche as $b)             

              @if($b->id == $branch_defaut->value)
              <option value="{{$b->id}}" selected="selected">{{$b->name}}</option>
              @else
              <option value="{{$b->id}}">{{$b->name}}</option>
              @endif

              @endforeach
            </select>
        </div>

        <div class="form-group">
            <label for="volume_total" class="col-sm-2 control-label"><strong>Volume Total</strong></label>
            <input type="number" name="volume_total" id="volume_total" value="{{$data_volume_total->value}}"/>
        </div>

        @if($freq_envoi->value == 0)
          <div class='m4y_freq'><input class='form-control freq_send' type='hidden' name='freq_envoi' value='0'></div>
        @else
          <div class='m4y_freq'>
            <div class="form-group">
              <label for="freq_envoi" class="col-sm-2 control-label">
                <strong>Fréquence envoi</strong>
              </label>
              <input type='number' name='freq_envoi' value='{{$freq_envoi->value}}'>
            </div>
          </div>
        @endif

        @if($nbr_compte->value == 0)
          <div class='nbr_compte'><input class='form-control nbr_compte' type='hidden' name='nbr_compte' value='0'></div>
        @else
          <div class='nbr_compte'>
            <div class="form-group">
              <label for="nbr_compte" class="col-sm-2 control-label">
                <strong>Nombre de comptes</strong>
              </label>
              <input type='number' name='nbr_compte' value='{{$nbr_compte->value}}'>
            </div>
          </div>
        @endif

        <div class="form-group">
            <label for="choix_du_compte" class="col-sm-2 control-label"><strong>Choix du compte ?</strong></label>
            @if($choix_compte->value == 0)
            <input type="radio" id="choix_du_compte" name="choix_du_compte" value="1"> Oui
            <input type="radio" id="choix_du_compte" name="choix_du_compte" value="0" checked> Non
            @else
            <input type="radio" id="choix_du_compte" name="choix_du_compte" value="1" checked> Oui
            <input type="radio" id="choix_du_compte" name="choix_du_compte" value="0"> Non
            @endif
        </div>

        @foreach($data_fai as $d)

        <?php
        $lefai = \DB::table('fais')->select('nom')->where('id',$d->fai_id)->first();
        ?>
        <div class="form-group">
            <label for="{{$d->fai_id}}" class="col-sm-2 control-label"><strong>{{$lefai->nom}}</strong></label>


                <input type="number" name="{{$d->fai_id}}" id="{{$d->fai_id}}" value="{{$d->volume}}"/>

        </div>

        @endforeach

        @foreach($fai_restant as $fr)

        <div class="form-group">
            <label for="{{$fr->id}}" class="col-sm-2 control-label">
              <strong>{{$fr->nom}}</strong>
            </label>
                <input type="number" name="{{$fr->id}}" id="{{$fr->id}}" value=""/>
        </div>

        @endforeach



        {!! Form::hidden('id_config', $id) !!}
        <button type="submit" class="btn btn-success">Modifier</button>
        {!! Form::close() !!}
        <i>* Laisser vide si pas de volume particulier pour un fai<i>
      </div>
    </div>
</div>
@endsection

@section('footer')


@endsection

@section('footer-scripts')

<script>
$( ".target" ).change(function() {

  if($(this).find('option:selected').text() == 'MailForYou'){

    console.log('change - MailForYou');

    $('.m4y_freq').empty();
    $('.m4y_freq').append('<div class="form-group"><label for="freq_envoi" class="col-sm-2 control-label"><strong>Fréquence envoi</strong></label><input type="number" name="freq_envoi" id="freq_envoi" value="0"/></div>');

    $('.nbr_compte').empty();
    $('.nbr_compte').append('<div class="form-group"><label for="nbr_compte" class="col-sm-2 control-label"><strong>Nombre de comptes</strong></label><input type="number" name="nbr_compte" id="nbr_compte" value="0"/></div>');
  } else if($(this).find('option:selected').text() == 'Edatis') {
    console.log('change - Edatis');

    $('.m4y_freq').empty();
    $('.m4y_freq').append('<input type="hidden" name="freq_envoi" id="freq_envoi" value="0"/>');
    $('.nbr_compte').empty();
    $('.nbr_compte').append('<div class="form-group"><label for="nbr_compte" class="col-sm-2 control-label"><strong>Nombre de comptes</strong></label><input type="number" name="nbr_compte" id="nbr_compte" value="0"/></div>');

  } else {
    console.log('change - Autre');

    $('.m4y_freq').empty();
    $('.m4y_freq').append('<input type="hidden" name="freq_envoi" id="freq_envoi" value="0"/>');
    $('.nbr_compte').empty();
    $('.nbr_compte').append("<input class='form-control nbr_compte' type='hidden' name='nbr_compte' value='0'>");
  }
});
</script>

@endsection
