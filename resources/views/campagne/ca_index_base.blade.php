@extends('template')

@section('content')


      <?php

      // pas bien à améliorer

      // init des total

      $totalcabrut = 0;
      $totalcanet = 0;
      $totalcout = 0;
      $totalvolume = 0;
      $totalmarge = 0;



      ?>

      <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">

                  <div class="x_title">
                    <h2>Chiffre d'affaire -
                      <small> <a href="/ca" class="btn btn-warning">Retour Ca Campagne</a>
                        <a href="/ca/mois/{{$current_month}}" class="btn btn-success">Mois courant</a>
                        @foreach($nb_mois as $ancien_mois)
                          <a href="/ca/mois/{{$ancien_mois}}" class="btn btn-danger">{{$ancien_mois}}</a>
                        @endforeach
                      </small>
                    </h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <p>Dernière mise à jour calcul : {{$maj_info}}</p>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br>
                    <div class="row">

                      <div style="overflow-x:auto;">
                        <table class="table table-striped table-hover table-bordered">
                            <tr>
                              <th>Base</th>
                              <th>CA Brut</th>
                              <th>CA Net</th>
                              <th>Cout routage</th>
                              <th>Volume total</th>
                              <th>Marge</th>
                              <th>eCPM</th>


                            </tr>
                            @foreach($ca_records_base as $c_b)
                            <tr>
                                <td class="col-xs-1">{{$c_b->nom}}</td>
                                <td class="col-xs-1">{{big_number($c_b->ca_brut)}} €</td>
                                <td class="col-xs-1">{{big_number($c_b->ca_net)}} €</td>
                                <td class="col-xs-1">{{big_number($c_b->base_cout_total)}} €</td>
                                <td class="col-xs-1">{{big_number($c_b->base_volume_total)}} </td>

                                <td class="col-xs-1">
                                  <?php

                                  $gains = $c_b->ca_net - $c_b->base_cout_total;

                                  if($gains < 0){
                                    echo '<span class="text-danger">'.round($gains,2).' €</span>';
                                  } else {
                                    echo '<span class="text-success">'.round($gains,2).' €</span>';
                                  }

                                  ?>
                                </td>


                                <td class="col-xs-1">
                                  <?php

                                  if($c_b->base_volume_total > 1000){
                                    $ecpm = ($c_b->ca_net / $c_b->base_volume_total) * 1000;
                                    echo round($ecpm,3) . " €";
                                  } else {
                                    echo 'N/A';
                                  }


                                  // Partie total

                                  $totalcabrut = $totalcabrut + $c_b->ca_brut;
                                  $totalcanet = $totalcanet + $c_b->ca_net;
                                  $totalcout = $totalcout + $c_b->base_cout_total;
                                  $totalvolume = $totalvolume + $c_b->base_volume_total;
                                  $totalmarge = $totalmarge + $gains;


                                  ?>
                                </td>

                            </tr>
                            @endforeach

                            <tr>
                              <td>Totaux</td>
                              <td><b>{{big_number($totalcabrut)}} €</b></td>
                              <td><b>{{big_number($totalcanet)}} €</b></td>
                              <td><b>{{big_number($totalcout)}} €</b></td>
                              <td><b>{{big_number($totalvolume)}}</b></td>
                              <td><b>{{big_number($totalmarge)}} €</b></td>

                            </tr>
                        </table>

                  </div>

                  </div>


                </div>


      </div>
      </div>



    <script>
    $().ready(function() {


    $("input").keyup(function(e){

      var value = $(this).val();
      var db_id = $(this).attr("id");
      var input_type = $(this).attr("name");
      var lemois = $(this).attr("mois");


      console.log('ID campagne : ' + db_id + ' / Valeur :' + value + ' / colonne : ' + input_type);
      // console.log(input_type);
      var cc = $('input[id="'+db_id+'"][name="ca_brut"]').val();
      $('input[id="'+db_id+'"][name="ca_brut"]').change(function() {
        $('input[id="'+db_id+'"][name="ca_net"]').val(cc);
      });


      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': '<?php echo csrf_token(); ?>'
          }
      });

      $.ajax({
          url: '/ajax/ca',
          type: 'POST',
          data: {
              valeur: value,
              type: input_type,
              id: db_id,
              mois: lemois
          },
          dataType: 'JSON',
          success: function (data) {

              // on pourrai mettre du vert dans le label
              console.log('ok');
          },
          error: function () {
              console.log('erreur');
          },
         complete : function(){
            console.log('complete');
         }
      });




    });

    });



    </script>

@endsection
