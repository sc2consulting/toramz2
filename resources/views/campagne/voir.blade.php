
<?php if (!isset($menu)) { $menu = ''; } ?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="fr" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="fr" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="fr">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8"/>
    <title>TOR - light</title>

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8">

    <link rel="stylesheet" href="/css/bootstrap.css">
    <link rel="stylesheet" href="/css/layout.css">
    <link rel="stylesheet" href="/css/tor.css">

    <script src="/js/base.js"></script>
    <script src="/js/layout.js"></script>

    <meta name="robots" value="noindex,nofollow" />

    <link rel="stylesheet" href="/css/tor.css">

    <script src="/js/toastr/toastr.min.js"></script>
    <link rel="stylesheet" href="/js/toastr/toastr.min.css">

	<!-- temporaire pour test -->



				<!-- jQuery -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>

<!-- jQuery UI -->
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>


    <div class="portlet light">

        <div class="portlet-body">


            <div class="row">

                <div class="col-md-7">
                    From : {{ $campagne->expediteur }} - Objet : {{ $campagne->objet }}<br />

                    <div style="border:solid 1px black">{!! $campagne->html !!}</div>
                </div>
            </div>

        </div>
    </div>



@section('footer-scripts')
    <script>
        $().ready(function() {
            console.log('ok');

           $("#send_campagne").click(function(e) {
               e.preventDefault();
               $(this).attr("disabled", true);

               var campagne_dest = $('#campagne_dest').val();

               $.post('/campagne/{{ $campagne->id }}/send_bat', { _token: '{{ csrf_token() }}'}, function(data) {
                   console.log(data);
                   $("#send_campagne").attr("disabled", false);

                   if (data == 'ok') {
                       toastr.success('Le BAT a bien été envoyé.', 'Ok !');
                   } else {
                       toastr.success("Une erreur s'est produite pendant l'envoi du BAT.", 'Erreur !');
                   }
               });
           });

            $("#send_recap").click(function(e) {
                e.preventDefault();

                var campagne_dest = $('#campagne_dest').val();
                console.log('Envoi du RECAP à '+campagne_dest);

                $.post('/campagne/{{ $campagne->id }}/send_recap', { _token: '{{ csrf_token() }}'}, function(data) {
                    console.log('sent');
                    console.log(data);


                });
            });

        });
    </script>
@endsection
