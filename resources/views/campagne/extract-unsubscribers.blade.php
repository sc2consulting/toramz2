@extends('common.layout')

@section('content')

    <div class="portlet light">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-cogs font-green-sharp"></i>
                <span class="caption-subject font-green-sharp bold uppercase">
                    Extraire les désinscrits par campagne (global)
                </span>
            </div>
            <div class="actions">
                <div class="btn-group btn-group-devided">
                   
                    <a href="/campagne/" class="btn red">Retour</a>
                    

                </div>
            </div>
        </div>
        <div class="portlet-body">

            <form action="/campagne/unsubscribers" method="post">
                {{csrf_field()}}
                <div>
                    Campagne
                    <select class="form-control" name="campagne">
                        @foreach($campagnes as $c)
                            <option value="{{$c->nom}}">{{$c->nom}}</option>
                        @endforeach
                    </select>
                </div>
                <div>
                    Clair/MD5
                    <select class="form-control" name="column">
                        <option value="mail">Mail</option>
                        <option value="md5">MD5</option>
                    </select>
                </div>
                <button id="extract-unsubscribers" class="btn btn-success">Extraire</button>
            </form>



        </div>
    </div>

@endsection

