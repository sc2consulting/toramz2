@extends('common.layout')

@section('content')

    {!! Form::model(new \App\Models\Planning, array('method' => 'post', 'route' => array('storePlanning'), 'class'=>'form-horizontal') ) !!}

    <div class="portlet light">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-cogs font-green-sharp"></i>
                <span class="caption-subject font-green-sharp bold uppercase">
Campagne {{ $campagne->nom }} : Planning - Theme
                </span>
            </div>
            <div class="actions">
                <div class="btn-group btn-group-devided">
                    <button type="submit" class="btn btn-success"> Enregistrer </button>
                    <button type="button" class="btn btn-primary" id="add_planning"> Ajouter une plannif </button>

                    <a href="/upload" class="btn red">Importer MD5</a>
                </div>
            </div>
        </div>
        <div class="portlet-body">
            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
            <table id="tab_planning" class="table table-striped table-bordered">

                <tr>
                    <th>Date</th>
                    <th>Volume Global</th>
                    <th>Volume Orange</th>
                    <th>Géolocalisation</th>
                    <th>Actions</th>
                </tr>
                @foreach ($plannings as $planning)
                    <tr id="ligne_planning" class="planning">
                        <td>
                            <input type="date" name="dates_campagnes[]" value="{{$planning->date_campagne}}"/>
                        </td>
                        <td>
                            <input type="number" name="volumes[]" placeholder="0" value="{{$planning->volume}}"/>
                        </td>

                        <td>
                            <input type="number" name="volumes_orange[]" placeholder="0" value="{{$planning->volume_orange}}"/>
                            <input type="hidden" name="already[]" value="{{$planning->id}}" />
                        </td>

                        <td>
                            *** pas besoin dans cette page
                        </td>

						@if ($planning->sent_at == null)
						<td><button type="button" class="btn btn-danger">Supprimer</button></td>
						@else
						<td><button type="button" class="btn btn-default" disabled="disabled">Supprimer</button></td>
						@endif
                    </tr>
                @endforeach
            </table>

            @foreach ($lesthemes as $theme)
            <div class="raw">
              <input name="theme[]" id="{{$theme->theme_id}}" type="checkbox" value="{{$theme->theme_id}}">
            <span class='text-danger'>{{$theme->theme_id}}</span>
            <div class="raw">
            <span class='text-warning'>Nombre total de mails :
            <?php

            // a refaire ptet
            // lire la table planning geoloc
            $volumetotal = \DB::table('volume_theme')->where('base_id', $campagne->base_id)->where('theme_id',$theme->theme_id)->first();
            // echo 'nouveau volume :' . $volumetotalzone->nombre_total_mail;
             // var_dump($volumetotalzone->nombre_total_mail);
             echo $volumetotal->nombre_ip;
              // $volumetotalzone
              // $volumetotalzone = \DB::table('tokens')->where('base_id', $campagne->base_id)->where('geoloc',$departement->code)->where('priority','1')->count();
              // echo 'ancien volume :' . $volumetotalzone;


            ?>

          </span>
            <br />
            <span class='text-primary'>
            Mails déjà selectionnés :


            <?php

              // $volumetokenized = \DB::table('tokens')->where('base_id', $campagne->base_id)->where('geoloc',$departement->code)->whereNotNull('campagne_id')->count();
              // echo 'ancien volume :' . $volumetokenized;


              // $volumetokenized = \DB::table('planning_geoloc')->where('base_id', $campagne->base_id)->where('departement',$departement->code)->first();
              // echo $volumetokenized->nombre_select_mail;

            ?>
            </span>
            <br />
            <span class='text-success'>
            Mails restants :

            <?php

              // $volumerestant = $volumetotalzone - $volumetokenized;

              // $volumerestant = \DB::table('planning_geoloc')->where('base_id', $campagne->base_id)->where('departement',$departement->code)->first();

              // echo $volumerestant->nombre_restant;
            ?>
            </span>
            </div>

            <hr>
          </div>
            @endforeach

            <input type="hidden" name="campagne_id" value="{{$campagne->id}}" />
        </div>
    </div>
    {!! Form::close() !!}


@endsection

@section('footer-scripts')
    <script>
        $().ready(function()
        {
            $('#tab_planning tr button').live("click", function() {
                $(this).parent().parent().remove();
            });

            $("#add_planning").click(function(e) {
                e.preventDefault();
                $("#tab_planning").append("<tr class='planning'><td> <input type='date' name='dates_campagnes[]' value='{{$today}}'/></td><td> <input name='volumes[]' value='0' /> </td><td> <input name='volumes_orange[]' value='0' /> <input type='hidden' name='already[]' value='0' /> </td> <td> N/A </td> <td> <button type='button' class='btn btn-danger'> Supprimer </button> </td> </tr>");


            });
        });
    </script>
@endsection
