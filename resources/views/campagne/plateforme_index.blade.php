@extends('template')

@section('content')

      <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">

                  <div class="x_title">
                    <h2>Chiffre d'affaire -
                      <small> <a href="/ca" class="btn btn-warning">Retour</a> <a href="/plateforme/add" class="btn btn-success">Ajouter</a>
                      </small>
                    </h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br>
                    <div class="row">

                      <div style="overflow-x:auto;">
                        <table class="table table-striped table-hover table-bordered">
                            <tr>
                              <th>Nom</th>
                              <th>Lien</th>
                              <th>Nombre de campagnes</th>

                            </tr>
                            @foreach($plateforme as $pf)
                            <tr>
                                <td class="col-xs-1">{{$pf->nom_plateforme}}</td>
                                <td class="col-xs-1">{{$pf->lien}} </td>
                                <td class="col-xs-1">[0]</td>

                            </tr>
                            @endforeach
                        </table>

                  </div>

                  </div>


                </div>


      </div>
      </div>


@endsection
