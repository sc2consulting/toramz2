@extends('template')

@section('content')

      <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">

                  <div class="x_title">
                    <h2>Importer un zip d'images pour une campagne <small></small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <a href="/campagne"><button class="btn btn-primary" href="">Retour</button></a>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br>
                    <div class="row">


                        <table class="table table-striped table-bordered">
                          <tr>
                              <th>Nom fichier</th>
                              <!-- <th>Action</th> -->
                          </tr>
                        @foreach ($fichiers as $fichier)
                            <tr>
                              <td>{{\File::name($fichier)}}</td>
                              <!-- <td> -->
                                <!-- <a href="supprimer/{{basename($fichier)}}"><span class="label label-danger">Supprimer</span></a> - <a href="telecharger/{{basename($fichier)}}"><span class="label label-success">Télécharger</span></a> -->
                              <!-- </td> -->
                            </tr>
                        @endforeach
                        </table>
                          <hr>

                          {!! Form::open(array('url' => 'dezip','files' => true, 'method' => 'post')) !!}

                                  {!! Form::file('fichier') !!}
                          
                              <hr>

                                  <button type="submit" class="btn btn-default">Enregistrer</button>

                          {!! Form::close() !!}

                          <br>
                          <br>

                            <span class="caption-subject font-green-sharp bold uppercase">
                                Importer un zip d'images pour une campagne
                            </span>
                            <br />
                            <span>Les liens vers les dossier d'images sur le serveur sont les suivants :</span>
                            <br />
                            <br />


                          @foreach ($lesdirectoryp as $directory)

                          http://{{ getenv('CLIENT_URL') }}/campagnes/{{ basename($directory) }}/[nomimage]<br />

                          @endforeach



                  </div>


      </div>
      </div>
      </div>

          @endsection
