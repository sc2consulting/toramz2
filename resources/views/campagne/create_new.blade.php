@extends('template')

@section('content')

      <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">

                  <div class="x_title">
                    <h2>Ajouter une campagne <small></small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <!-- <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li> -->
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br>

                    {!! Form::model(new \App\Models\Campagne, array('route' => array('campagne.store'), 'class'=>'form-horizontal') ) !!}
                        @include('campagne.partial-fields')
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button id="store" type="submit" class="btn btn-default">Enregistrer</button>
                            </div>
                        </div>
                    {!! Form::close() !!}

                  </div>


      </div>
      </div>

          @endsection
