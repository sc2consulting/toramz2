@extends('common.layout')

@section('content')
<div class="portlet light">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-cogs font-green-sharp"></i>
                <span class="caption-subject font-green-sharp bold uppercase">Créer un BAT</span>
            </div>
            <div class="actions">
                <div class="btn-group btn-group-devided">

                </div>
            </div>
        </div>
        <div class="portlet-body">
            {!! Form::model(new \App\Models\Campagne, array('route' => array('campagne.store'), 'class'=>'form-horizontal') ) !!}
                @include('campagne.partial-fields')
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button id="store" type="submit" class="btn btn-default">Enregistrer</button>
                    </div>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection

@section('footer-scripts')
    @yield('footer-scripts-extra')
@endsection