@extends('common.layout')

@section('content')
    {!! Form::model(new \App\Models\Planning, array('method' => 'post', 'route' => array('storePlanning'), 'class'=>'form-horizontal') ) !!}
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-cogs font-green-sharp"></i>
                    <span class="caption-subject font-green-sharp bold uppercase">
                    Planning : {{$campagne->ref}} (@if(is_null($campagne->base)) Multi-bases @else {{$campagne->base->nom}} @endif)
                    </span>
                </div>
                <div class="actions">
                    <div class="btn-group btn-group-devided">
                        <button type="submit" id="store" class="btn btn-success" > Enregistrer </button>
                        <button type="button" class="btn btn-primary" id="add_planning"> Ajouter une plannif </button>

                    </div>
                </div>
            </div>
            <div class="portlet-body">

              <div class="row">

                <a href="/planning/defaut" class="btn btn-success btn-sm">Ajouter une autre configuration par defaut</a>
                @foreach($planning_config_defaut as $planning_defaut)
                <?php
                  $info = \DB::table('planning_defaut')->select('value')->where('config_id',$planning_defaut->config_id)->where('config_key','name_config')->first();
                 ?>

                 <a href="#" id="add_planning_config_{{$planning_defaut->config_id}}" class="btn btn-primary btn-sm">{{$planning_defaut->config_id}} - {{$info->value}}</a>
                @endforeach


              </div>

              <hr>


                <?php
                    $pattern = '/routeurerror/';
                    if(preg_match($pattern, $_SERVER['REQUEST_URI'])){
                        echo '<div class="alert alert-dismissable alert-danger">
                        <strong> Attention, vous devez renseigner un routeur ! </strong>
                        </div>';
                    }
                    $branches = \App\Models\Branch::all();
                ?>
                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                <input type="hidden" id="baseId" name="base_id" value="@if(isset($campagne->base_id)){{$campagne->base_id}}@else 0 @endif" >
                <table id="tab_planning" class="table table-striped table-bordered">
                    <tr>
                        <th>Date et heure*</th>
                        <th>Volume Global</th>
                        <th class="col-md-3">Volume FAIs</th>
                        <th>Segment</th>
                        <th>Type</th>
                        <th>Routeur</th>
                        <th>Actions</th>
                    </tr>
                    <?php $ind = -1; ?>
                    @foreach ($plannings as $planning)
                        <?php $ind++; ?>
                        <tr id="ligne_planning" class="planning existing_planning">
                            <td>
                              <input class='date_campagne' type="date" name="dates_campagnes[]" value="{{$planning->date_campagne}}" @if ($planning->sent_at != null) readonly @endif required />
                              <input class='heure_campagne' type="time" name="heures_campagnes[]" value="{{$planning->time_campagne}}" @if ($planning->sent_at != null) readonly @endif/>
                            </td>
                            <td class="cell_volume_global">

                              <input class='volume_global' type="number" name="volumes[]" placeholder="0" value="{{$planning->volume}}" @if ($planning->sent_at != null) readonly @endif required />
                              @if($planning->freq_send > 0)
                              <div>Fréquence d'envoi / H</div>
                              <input type="number" name="freq_send[]" value="{{$planning->freq_send}}"/>
                              @else
                              <input type="hidden" name="freq_send[]" value="0"/>
                              @endif

                              @if($planning->nbr_sender > 0)
                              <div>Nombre de comptes</div>
                              <input type="number" name="nbr_compte[]" value="{{$planning->nbr_sender}}"/>
                              @else
                              <input type="hidden" name="nbr_compte[]" value="0"/>
                              @endif

                            </td>
                            <td>
                                @foreach($planning->volumesfais as $vf)
                                    <label> {{$vf->fai->nom}} <input class="existing_fai" data-value="{{$vf->fai->nom}}" type="number" name="volumes_fais[{{$ind}}][{{$vf->fai_id}}]" placeholder="0" value="{{$vf->volume}}"  @if($planning->tokens_at != null) disabled="" @endif /> </label> <br/>
                                @endforeach
                                @if(count($planning->volumesfais) == 0)
                                    <input type="hidden" name="volumes_fais[{{$ind}}][0]" placeholder="0" value="0"/>
                                @endif
                                <?php $volume_total = 0; ?>
                                <div> <i> Volume réel envoyé : </i>
                                @foreach($stats[$planning->id] as $s)
                                    <br/> <span> {{$fais[$s->fai_id]}} - {{$s->volume_used}}</span>
                                    <?php $volume_total+= $s->volume_used; ?>
                                @endforeach
                                    <br/> <span> total - {{$volume_total}}</span>
                                </div>
                                <input type="hidden" name="already[]" value="{{$planning->id}}" />
                            </td>
                            <td>
                                <select class="choixSegment form-control" name="segments_ids[]" @if ($planning->segments_at != null or $planning->sent_at != null) readonly @endif required >
                                    <option value="0">-</option>
                                    @foreach($segments as $s)
                                    <option value="{{$s->id}}" @if($segments_select[$planning->id] == $s->id) selected @endif>{{$s->nom}}</option>
                                    @endforeach
                                </select>
                                <div class='loading-gif'><img src='/images/icon/loading_spinner.gif' alt='loading gif'></div>
                                <div class='ajax-result'></div>
                            </td>
                            <td>
                                <select class="form-control" name="types[]" @if ($planning->segments_at != null or $planning->sent_at != null) readonly @endif required >
                                    <option class='typeChoice' value='public' @if($planning->type=='public') selected @endif > Public </option>
                                    <option class='typeChoice' value='anonyme' @if($planning->type=='anonyme') selected @endif> Anonyme </option>
                                </select>
                                <select class='form-control' name='branches_id[]' @if ($planning->segments_at != null or $planning->sent_at != null) readonly @endif >
                                    @foreach($branches as $brc)
                                    <option class='branchChoice new' value='{{$brc->id}}' @if($brc->id == $planning->branch_id) selected @endif > {{$brc->name}} </option>
                                    @endforeach
                                </select>
                            </td>
                            <td>
                                <select class="form-control routeur_choice" name="routeurs_ids[]" @if ($planning->segment_at != null) readonly @endif required >
                                    @foreach($lesrouteurs as $r)
                                        <option class="routeurChoice" value="{{$r->id}}" @if($planning->routeur_id == $r->id) selected @endif >{{$r->nom}}</option>
                                    @endforeach
                                </select>
                                <?php
                                    $r_senders = \DB::table('senders')->where('routeur_id', $planning->routeur_id)->get();
                                    $one_sender_selected = \DB::table('planning_senders')->where('planning_id', $planning->id)->first();
                                ?>
                                <!-- ici code en plus pour afficher choix choisi -->
                                <select class="form-control sender_choice" name="sender_ids[]" readonly required >
                                    <option value="0">-</option>
                                    @foreach($r_senders as $rs)
                                        <option value="{{$rs->id}}" @if(isset($one_sender_selected) && $one_sender_selected->sender_id == $rs->id) selected @endif >{{$rs->nom}}</option>
                                    @endforeach
                                </select>
                                <div class="lafrequence"></div>
                            </td>
                            <td>
                                <button type="button" class="btn btn-danger" id="suppr" @if ($planning->sent_at != null) disabled="" @endif>Supprimer</button>
                                <form type="checkbox" id="isDisplay" name="isDisplay">Afficher la plannification
                            </td>
                        </tr>
                    @endforeach
                </table>
                <input type="hidden" name="campagne_id" value="{{$campagne->id}}" />
            </div>
            <p><i>*Quand se déclenche le processus de sélection de destinataires, d'import et d'envoi.</i></p>
        </div>
    {!! Form::close() !!}
    <?php echo '<input type="hidden" id="indexRows" name="indexRows" value="'.$ind.'" />'; ?>
@endsection

@section('footer-scripts')
    <script>
        $().ready(function() {

            var currentXhr;
            $('.choixSegment').live("change", function() {

                currentXhr && currentXhr.readyState != 4 && currentXhr.abort(); //

                var choix = $(this).val();

                if(choix != 0) {

                    //Affiche le logo de chargement, vide les autres resultats
                    $('.ajax-result').text('');
                    var select = $(this);

                    select.next('.loading-gif').css('display','block');

                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': '<?php echo csrf_token(); ?>'
                        }
                    });
                    var segment_id = $(this).val();

                    var currentXhr = $.ajax({
                        url: '/segment/ajax',
                        type: 'POST',
                        data : {
                            segment_id : segment_id,
                            base_id : $('#baseId').val()
                        },
                        dataType: 'JSON',
                        success: function (data) {

//                            console.log(segment_id);
//                            console.log(data);

                            var count = data;

                            var potentiel = " destinataires potentiels";
                            if(count < 2) {
                                var potentiel = " destinataire potentiel";
                            }

                            //Retire Loading Gif
                            select.next('.loading-gif').css('display','none');
                            select.next().next('.ajax-result').text(count + potentiel);

                        },
                        error: function(e) {
                            console.log(e.responseText);
                        }
                    });
                }
            });

            $('#tab_planning tr button').live("click", function(){
                $(this).parent().parent().remove();
                var currentIndex = parseInt($('#indexRows').val()) - 1 ;
                $('#indexRows').val(currentIndex);
            });

            $('#store').click(function(e){
                e.preventDefault();
                warnBeforeRedirect($(this));
            });

            $("#add_planning").click(function(e) {

                var currentIndex = parseInt($('#indexRows').val()) + 1 ;
                var d = new Date();
                var routeurhtml = '<select class="form-control routeur_choice" name="routeurs_ids[]" required > <option class="routeurChoice" value="0">Choisir un routeur</option>';
                @foreach($lesrouteurs as $r)
                        routeurhtml += '<option class="routeurChoice" value="{{$r->id}}">{{$r->nom}}</option> ';
                @endforeach
                routeurhtml += '</select> <div class="senders"></div>';

                d.setMinutes(d.getMinutes() + 5); //heure envoi defini a heure actuelle+5min par defaut

                // var routeur_text = $(this).find('.routeur_choice option:selected').text();
                // console.log(routeur_text);

                $("#tab_planning").append("<tr class='planning new_planning'> <td> <input class='date_campagne' type='date' name='dates_campagnes[]' value='{{$today}}' required /> <input class='heure_campagne' type='time' name='heures_campagnes[]' value='"+('0'+d.getHours()).slice(-2)+":"+('0'+d.getMinutes()).slice(-2)+"'/> </td> <td class='cell_volume_global'> <input class='volume_global' name='volumes[]' type='number' value='0' required /> </td><td> <select class='form-control' name='fais'>@foreach($fais as $fid => $fnom)<option name='{{$fnom}}' value='{{$fid}}'>{{$fnom}}</option>@endforeach</select><input name='volumefai' value='0' /> <i class='fa fa-check moreFaiVolume' aria-hidden='true' data-index='"+currentIndex+"'></i> <div class='checkedFais'> </div> <input type='hidden' name='already[]' value='0' /> </td> <td><select class='form-control' name='segments_ids[]'><option value='0'>-</option>@foreach($segments as $s)<option value='{{$s->id}}'>{{$s->nom}}</option>@endforeach</select> <div class='loading-gif'><img src='/images/icon/loading_spinner.gif' alt='loading gif'></div> <div class='ajax-result'></div> </td> <td> <select class='form-control' name='types[]' required > <option class='typeChoice new' value='public'> Public </option> <option class='typeChoice new' value='anonyme'> Anonyme </option> </select> <select class='form-control' name='branches_id[]' required > @foreach($branches as $brc) <option class='branchChoice new' value='{{$brc->id}}'> {{$brc->name}} </option> @endforeach </select> </td> <td>"+routeurhtml+"<div class='lafrequence'></div><div id='testnbr' class='lenombredecomptes'>*</div></td> <td> <button type='button' class='btn btn-danger'> Supprimer </button> </td> </tr>");

                $('#indexRows').val(currentIndex);
            });


            $("[id*=add_planning_config_]").click(function(e){
              // e.preventDefault();
              /*
              #add_planning_1
              #add_planning_2
              #add_planning_3
              */
              // var c_id = $(this).find("[id*=add_planning_config_]").val();
              var c_id = this.id;
//              console.log(c_id);

              var currentIndex = parseInt($('#indexRows').val());
              var sendersajax = null;

              currentIndex = currentIndex + 1 ;
              console.log(currentIndex);

              var d = new Date();
              var routeurhtml = '<div class="routeurjquery"><select class="form-control routeur_choice" name="routeurs_ids[]" required > <option class="routeurChoice" value="0">Choisir un routeur</option>';
              @foreach($lesrouteurs as $r)
                      routeurhtml += '<option class="routeurChoice" value="{{$r->id}}">{{$r->nom}}</option> ';
              @endforeach


              routeurhtml += '</select> <div class="senders"></div></div>';

              d.setMinutes(d.getMinutes() + 5); //heure envoi defini a heure actuelle+5min par defaut

              // var routeur_text = $(this).find('.routeur_choice option:selected').text();
              // console.log(routeur_text);

              $("#tab_planning").append("<tr class='planning new_planning'> <td> <input class='date_campagne' type='date' name='dates_campagnes[]' value='{{$today}}' required /> <input class='heure_campagne' type='time' name='heures_campagnes[]' value='"+('0'+d.getHours()).slice(-2)+":"+('0'+d.getMinutes()).slice(-2)+"'/> </td> <td class='cell_volume_global'> <input class='volume_global' name='volumes[]' type='number' value='0' required /> </td><td> <div class='fai_defaut'></div><input type='hidden' name='already[]' value='0' /> <div class='loading-gif'><img src='/images/icon/loading_spinner.gif' alt='loading gif'></div> <div class='ajax-result'></div> </td><td> *</td> <td> <select class='form-control' name='types[]' required > <option class='typeChoice new' value='public'> Public </option> <option class='typeChoice new' value='anonyme'> Anonyme </option> </select> <select class='form-control' name='branches_id[]' required > @foreach($branches as $brc) <option class='branchChoice new' value='{{$brc->id}}'> {{$brc->name}} </option> @endforeach </select> </td> <td>"+routeurhtml+"<div class='lafrequence'></div><div id='testnbr' class='lenombredecomptes'>*</div></td> <td> <button type='button' class='btn btn-danger'> Supprimer </button> </td> </tr>");

              $('#indexRows').val(currentIndex);

              $.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': '<?php echo csrf_token(); ?>'
                  }
              });

              // je fais une requete ajax pour aller chercher les values et renvoyer un tab
              $.ajax({
                  url: '/planning/defaut/ajax',
                  type: 'POST',
                  data: {
                      config_id: c_id,
                      campagne_id: {{$campagne->id}}
                  },
                  dataType: 'JSON',
                  success: function (data) {

                    console.log(data[0]);

                    if(data[0]['senders_choice'] == null){
                      console.log('Sender par defaut');
                      // je met le code de base

                    senderAjax =  '<select class="form-control sender_choice" name="sender_ids[]" required=""> <option value="0">-</option></select>';

                    } else {

                      senderAjax = '<select class="form-control sender_choice" name="sender_ids[]" required=""> <option value="0">-</option>';
                      data[0]['senders_choice'].forEach(function(sendersObj){
                        console.log(sendersObj['nom']);
                        console.log(sendersObj['id']);

                        senderAjax += '<option value="'+sendersObj['id']+'">'+sendersObj['nom']+'</option>';

                      })

                      senderAjax +=  '</select>';
                    }


                    for(var i= 0; i < data.length; i++)
                    {
                         $('.fai_defaut:last').append('<br/> <label class="new_fai" data-value="'+data[i]['fai_nom']+'" > '+data[i]['fai_nom']+' <input class="new_volume" name="volumes_fais['+currentIndex+']['+data[i]['fai_id']+']" value="'+data[i]['volume']+'" /> </label>');

                    }
                    // console.log(data[0]['volume_global']);
                    $('.routeurjquery:last').empty();
                    $('.routeurjquery:last').append('<select class="form-control routeur_choice" name="routeurs_ids[]" required > <option class="routeurChoice" value="'+data[0]['lerouteurid']+'">'+data[0]['nomrouteur']+'</option></select>'+senderAjax+'<input class="form-control freq_send" name="freq_send[]" value="'+data[0]['freq_envoi']+'" type="hidden"><input class="form-control nbr_compte" name="nbr_compte[]" type="text" value="'+data[0]['nbr_compte']+'">');

                    // $('.routeurjquery:last').append('<select class="form-control routeur_choice" name="routeurs_ids[]" required > <option class="routeurChoice" value="'+data[0]['lerouteurid']+'">'+data[0]['nomrouteur']+'</option></select><select class="form-control sender_choice" name="sender_ids[]" required=""> <option value="0">-</option></select><input class="form-control freq_send" name="freq_send[]" value="'+data[0]['freq_envoi']+'" type="hidden"><input class="form-control nbr_compte" name="nbr_compte[]" type="text" value="'+data[0]['nbr_compte']+'">');

                    $('.volume_global:last').val(data[0]['volume_total']);
                    // console.log(data[0]['freq_envoi']);
                    /*
                    if (typeof data[0]['volume_total'] === 'undefined') {
                      console.log('ok');
                    }
                    */

                    $('.senders').empty();

                    console.log(data[0]['nomrouteur']);

                  },
                  error: function (e) {
                      console.log(e.responseText);
                  }

              });


              // console.log(data[i]);

              // $('.volume_global:last');

              // $('.routeurjquery:last').empty();
              // $('.routeurjquery:last').append('<select class="form-control routeur_choice" name="routeurs_ids[]" required > <option class="routeurChoice" value="'+data[0]['volume_total']+'">Routeur Auto</option></select><select class="form-control sender_choice" name="sender_ids[]" required=""> <option value="0">-</option></select><input class="form-control freq_send" name="freq_send[]" value="0" type="hidden"><input class="form-control nbr_compte" name="nbr_compte[]" type="text" value="2">');


            });


            $(document).on('change','.routeur_choice', function() {
                var r_id = $(this).val();
                var r_volume = $(this).parent().parent().find('.volume_global').val();

                var this_routeur = $(this);

                if(r_id != undefined || r_id != 0) {
                  $.ajaxSetup({
                      headers: {
                          'X-CSRF-TOKEN': '<?php echo csrf_token(); ?>'
                      }
                  });

                  $.ajax({
                      url: '/routeur/senders/ajax',
                      type: 'POST',
                      data: {
                          routeur_id: r_id,
                          volume: r_volume
                      },
                      dataType: 'JSON',
                      success: function (data) {
                        //  console.log(data);
                          var routeurhtml = '<select class="form-control sender_choice" name="sender_ids[]" required > <option value="0">-</option>';

                          for(var i= 0; i < data.length; i++) {
                              var id = data[i]["id"];
                              var nom = data[i]["nom"];
                              var quota_left = data[i]["quota_left"];
                              var type = data[i]["type"];

                              if(type == null)
                              {
                                type = 'N/A';
                              }
                              routeurhtml += '<option value="'+id+'">'+nom+' - '+type+' - ('+quota_left+')</option>';
                          }

                          this_routeur.parent().find('.senders').empty();
                          this_routeur.parent().find('.senders').append(routeurhtml);

                      },
                      error: function (e) {
                          console.log(e.responseText);
                      }
                  });
                }
            });

            <?php
                $default_freq = getenv('M4Y_FREQ_SEND');
                // ajouter nbr de comptes par defaut ?
            ?>

            var defaultvalue = '';

            @if($default_freq>0)
                var defaultvalue = {{$default_freq}};
            @endif

            //Rajouter dans blad une div pour clean la div a chaque changement (par exemple)
            $(document).on('change','.routeur_choice',function(){
              // console.log($(this).parent().find('.lafrequence').empty());
              // je regarde ici si j'ai le tiret
              // console.log($(this).parent().find('.sender_choice').text());

              $(this).parents('td').find('.lenombredecomptes').empty();
              $(this).parent().find('.lafrequence').empty()

                if($(this).find('option:selected').text() == 'MailForYou'){
                    $(this).parent().find('.lafrequence').append("Fréquence envoi / H <br><input class='form-control freq_send' type='text' name='freq_send[]' />");
                    $(this).parents('td').find('.lenombredecomptes').append("Nombre de comptes ? <br><input class='form-control nbr_compte' type='text' name='nbr_compte[]' />");
                    $(this).parent().find('.lafrequence .freq_send').val(defaultvalue);
                } else if($(this).find('option:selected').text() == 'Edatis') {

                  $(this).parent().find('.lafrequence').append("<input class='form-control freq_send' type='hidden' name='freq_send[]' value='0'>");
                  $(this).parents('td').find('.lenombredecomptes').append("Nombre de comptes ? <br><input class='form-control nbr_compte' type='text' name='nbr_compte[]' />");

                } else {
                    $(this).parent().find('.lafrequence').append("<input class='form-control freq_send' type='hidden' name='freq_send[]' value='0'><input class='form-control freq_send' type='hidden' name='nbr_compte[]' value='0'>");
                    // $(this).parents('td').find('.lenombredecomptes').append("<input class='form-control freq_send' type='hidden' name='nbr_compte[]' value='0'>");
               }
            });


            $(document).on('change','.sender_choice',function(){
              if($(this).find('option:selected').text() == '-'){

              $(this).parents('td').find('.lenombredecomptes').append("Nombre de comptes ? <br><input class='form-control nbr_compte' type='text' name='nbr_compte[]' />");

            } else {

              $(this).parents('td').find('.lenombredecomptes').empty();
              $(this).parents('td').find('.lenombredecomptes').append("<input class='form-control nbr_compte' type='hidden' name='nbr_compte[]' value='0'>");
            }
            });



            $('.moreFaiVolume').live("click", function(){

                var index = $(this).attr('data-index');
                var volume = $(this).prev('input')[0].value;
//                console.log($(this).prev('input').prev('select')[0].selectedOptions[0].innerText);
                var fai_id = $(this).prev('input').prev('select')[0].selectedOptions[0].value;
                var fai_nom = $(this).prev('input').prev('select')[0].selectedOptions[0].innerHTML;

                $(this).next('.checkedFais').append('<br/> <label class="new_fai" data-value='+fai_nom+' > '+fai_nom+' <input class="new_volume" name="volumes_fais['+index+']['+fai_id+']" value="'+volume+'" /> </label>');
                $(this).prev('input').prev('select')[0].selectedOptions[0].remove();
            });

        });

        function warnBeforeRedirect($element) {
//            On construit le message d'affichage avec les volumes globaux, par FAIs pour le faire confirmer auprès de l'utilisateur
            var text = "";

            var title = "Confirmez-vous l'édition des plannings suivants ?";
            var type = "warning";
            var showCancelButton = true;

            $(".existing_planning").each(function () {
                var date_campagne = $(this).find('.date_campagne').val();
                var heure_campagne = $(this).find('.heure_campagne').val();
                var routeur_val = $(this).find('.routeur_choice').val();
                var routeur_text = $(this).find('.routeur_choice option:selected').text();
                var volume_global = new Intl.NumberFormat().format($(this).find('.volume_global').val());

                var sender_ids = $(this).find('.sender_choice').val();
                var sender_ids_text = $(this).find('.sender_choice option:selected').text();

                if(routeur_val == 0){
                    title = "Oops, vous allez peut-être trop vite ?";
                    type = "error";
                    showCancelButton = false;
                    routeur_text = '<span style="color: red">' + routeur_text + '</span>';
                }
                if(volume_global == 0){
                    title = "Oops, vous allez peut-être trop vite ?";
                    type = "error";
                    showCancelButton = false;
                    volume_global = '<span style="color: red"> Choisir un volume non nul </span>';
                }

                /*
                if(routeur_text == 'MailForYou'){
                  if(sender_ids_text == '-'){
                      title = 'Oops, vous allez peut-être trop vite ?';
                      type = 'error';
                      showCancelButton = false;
                      volume_global = '<span style="color: red">Vous devez choisir un Sender MailForYou</span>';
                  }
                }
                */

                var new_plann_text = '<br/><br/>Planning existant pour le '+date_campagne+' à '+heure_campagne+" <br/> Routeur : "+routeur_text+" <br/> Volume global : "+volume_global+" <br/>";

                $(this).find('.existing_fai').each(function(){
                    new_plann_text = new_plann_text + '<br/> - '+$(this).attr('data-value') + ' : ' +new Intl.NumberFormat().format($(this).val());
                });
                text = text + new_plann_text;
            });

            $(".new_planning").each(function () {
                var date_campagne = $(this).find('.date_campagne').val();
                var heure_campagne = $(this).find('.heure_campagne').val();
                var routeur_val = $(this).find('.routeur_choice').val();
                var routeur_text = $(this).find('.routeur_choice option:selected').text();
                var volume_global = new Intl.NumberFormat().format($(this).find('.volume_global').val());

                var sender_ids = $(this).find('.sender_choice').val();
                var sender_ids_text = $(this).find('.sender_choice option:selected').text();

                var freq = $(this).find('.freq_send').val();
                // console.log(freq);

                if(routeur_val == 0){
                    title = 'Oops, vous allez peut-être trop vite ?';
                    type = 'error';
                    showCancelButton = false;
                    routeur_text = '<span style="color: red">' + routeur_text + '</span>';
                }
                if(volume_global == 0){
                    title = 'Oops, vous allez peut-être trop vite ?';
                    type = 'error';
                    showCancelButton = false;
                    volume_global = '<span style="color: red">' + volume_global + '</span>';
                }


                if(routeur_text == 'MailForYou'){
                  /*
                  if(sender_ids_text == '-'){
                      title = 'Oops, vous allez peut-être trop vite ?';
                      type = 'error';
                      showCancelButton = false;
                      text = '<span style="color: red">Vous devez choisir un Sender MailForYou</span>';
                  }
                  */

                  if(freq == 0 || freq == null || freq == ''){
                      title = 'Oops, vous allez peut-être trop vite ?';
                      type = 'error';
                      showCancelButton = false;
                      text = '<span style="color: red">La fréquence doit être supérieure à 0</span>';
                  }



                }

                var new_plann_text = '<br/><br/>Nouveau planning pour le '+date_campagne+' à '+heure_campagne+" <br/> Routeur : "+routeur_text+" <br/> Volume global : "+volume_global;
                $(this).find('.new_fai').each(function(){
                    new_plann_text = new_plann_text + '<br/> - '+$(this).attr('data-value') + ' : ' +new Intl.NumberFormat().format($(this).children('.new_volume').val());
                });
                text = text + new_plann_text;
            });

            swal
            ({
                title: title,
                text: text,
                type: type,
                showCancelButton: showCancelButton,
                html: true
            },
                function(isConfirm) {
                    if (isConfirm && type != 'error') {
//                        console.log($element.closest("form"));
                        $element.closest("form").submit();
                    }
            });
        }

    </script>
@endsection
