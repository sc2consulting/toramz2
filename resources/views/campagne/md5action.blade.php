@extends('common.layout')

@section('content')



<div class="portlet light">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-cogs font-green-sharp"></i>
            <span class="caption-subject font-green-sharp bold uppercase">
                Listage MD5 sur
                <span style="color:#6699ff">{{$campagne->nom}}</span> de la base :
                <span style="color:blueviolet">{{$campagne->base->nom}}</span>
            </span>
        </div>
        <div class="actions">
            <div class="btn-group btn-group-devided">
                <a href="/upload" class="btn red">Importer MD5</a>
                <a href="/campagne" class="btn red">Retour</a>
            </div>
        </div>
    </div>
    <div class="portlet-body">
        <table class="table table-striped table-hover">
            <span class="caption-subject font-blue bold uppercase">Résultat du scan avec la base {{$campagne->base->nom}}</span>
            <tr>
                <td>
                    Correspondants
                </td>
                <td>
                    Insérés
                </td>
            </tr>
            <tr>
                <td>
                    {{$matched}}
                </td>
                <td>
                    {{$inserted}}
                </td>
            </tr>
        </table>
    </div>
</div>


@endsection