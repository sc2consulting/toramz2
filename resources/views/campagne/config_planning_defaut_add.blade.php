@extends('common.layout')

@section('content')
<div class="portlet light">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-cogs font-green-sharp"></i>
            <span class="caption-subject font-green-sharp bold uppercase">Configuration planning par defaut</span>
        </div>
        <div class="actions">
            <div class="btn-group btn-group-devided">
              <a title="Retour" href="/planning" class="btn btn-success">Retour Planning</a>
            </div>
        </div>
    </div>
    <div class="portlet-body">

      <div class="row">
        {!! Form::open(array('url' => 'planning/defaut/add','files' => true, 'method' => 'post')) !!}

        <div class="form-group">
            <label for="name_config" class="col-sm-2 control-label"><strong>Nom</strong></label>
                <input type="text" name="name_config" id="name_config" value=""/>
        </div>

        <div class="form-group">
            <label for="routeur_id" class="col-sm-2 control-label"><strong>Routeur</strong></label>
            <select class="target" name="routeur_id" >
              @foreach($routeurs as $r)
              <option value="{{$r->id}}">{{$r->nom}}</option>
              @endforeach
            </select>
        </div>

        <div class="form-group">
            <label for="routeur_id" class="col-sm-2 control-label"><strong>Branche</strong></label>
            <select class="target" name="la_branche" >
              @foreach($branche as $b)
              <option value="{{$b->id}}">{{$b->name}}</option>
              @endforeach
            </select>
        </div>

        <div class="form-group">
            <label for="volume_total" class="col-sm-2 control-label"><strong>Volume Total</strong></label>
            <input type="number" name="volume_total" id="volume_total" value="0"/>
        </div>

        <div class="form-group">
            <label for="choix_du_compte" class="col-sm-2 control-label"><strong>Choix du compte ?</strong></label>
            <input type="radio" id="choix_du_compte" name="choix_du_compte" value="1" required> Oui
            <input type="radio" id="choix_du_compte" name="choix_du_compte" value="0" required> Non
        </div>

        <div class='m4y_freq'><input class='form-control freq_send' type='hidden' name='freq_envoi' value='0'></div>
        <div class='nbr_compte'><input class='form-control nbr_compte' type='hidden' name='nbr_compte' value='0'></div>


        @foreach($data as $d)

        <div class="form-group">
            <label for="{{$d->id}}" class="col-sm-2 control-label"><strong>{{$d->nom}}</strong></label>
                <input type="number" name="{{$d->id}}" id="{{$d->id}}" value=""/>
        </div>

        @endforeach

        <button type="submit" class="btn btn-success">Modifier</button>
        {!! Form::close() !!}
      </div>
    </div>
</div>
@endsection

@section('footer')


@endsection

@section('footer-scripts')
    <script>
    $( ".target" ).change(function() {
      if($(this).find('option:selected').text() == 'MailForYou'){

        $('.m4y_freq').empty();
        $('.m4y_freq').append('<div class="form-group"><label for="freq_envoi" class="col-sm-2 control-label"><strong>Fréquence envoi</strong></label><input type="number" name="freq_envoi" id="freq_envoi" value="0"/></div>');

        $('.nbr_compte').empty();
        $('.nbr_compte').append('<div class="form-group"><label for="nbr_compte" class="col-sm-2 control-label"><strong>Nombre de comptes</strong></label><input type="number" name="nbr_compte" id="nbr_compte" value="0"/></div>');
      } else if($(this).find('option:selected').text() == 'Edatis') {

        $('.m4y_freq').empty();
        $('.m4y_freq').append('<input type="hidden" name="freq_envoi" id="freq_envoi" value="0"/>');

        $('.nbr_compte').empty();
        $('.nbr_compte').append('<div class="form-group"><label for="nbr_compte" class="col-sm-2 control-label"><strong>Nombre de comptes</strong></label><input type="number" name="nbr_compte" id="nbr_compte" value="0"/></div>');

      } else {
        $('.m4y_freq').empty();
        $('.m4y_freq').append('<input type="hidden" name="freq_envoi" id="freq_envoi" value="0"/>');
        $('.nbr_compte').empty();
        $('.nbr_compte').append("<input class='form-control nbr_compte' type='hidden' name='nbr_compte' value='0'>");
      }
    });
    </script>





@endsection
