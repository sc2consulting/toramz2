<input type="hidden" name="_token" value="{{ csrf_token() }}" />

<div class="form-group">
    <label for="input_html" class="col-sm-2 control-label">Nom</label>
    <div class="col-sm-10">
        {!! Form::text('nom', Input::old('nom'), ['class' => 'form-control']) !!}
    </div>
</div>

<div class="form-group">
    <label for="input_html" class="col-sm-2 control-label">Quota pression</label>
    <div class="col-sm-10">
        {!! Form::input('number','quota_pression', Input::old('quota_pression'), ['class' => 'form-control']) !!}
    </div>
</div>

<div class="form-group">
    <label for="input_html" class="col-sm-2 control-label">Quota sender <br/> <i style="font-size: 12px;">0 = pas de limite</i> </label>
    <div class="col-sm-10">
        {!! Form::input('number', 'quota_sender', Input::old('quota_sender'), ['class' => 'form-control']) !!}
    </div>
</div>

<div class="form-group">
    <label for="input_html" class="col-sm-2 control-label">Quota campagne <br/> <i style="font-size: 12px;">0 = pas de limite</i> </label>
    <div class="col-sm-10">
        {!! Form::input('number','quota_campagne', Input::old('quota_campagne'), ['class' => 'form-control']) !!}
    </div>
</div>

<div class="form-group">
    <label for="input_html" class="col-sm-2 control-label">Domaines* </label>
    <div class="col-sm-10">
        {!! Form::text('domains', Input::old('domains'), ['class' => 'form-control']) !!}
    </div>
</div>

<div class="form-group">
    <label for="input_html" class="col-sm-2 control-label"> Pourcentage par défaut par envoi <br/> <i style="font-size: 12px;">0 = pas de limite</i> </label>
    <div class="col-sm-10">
        {!! Form::text('default_planning_percent', Input::old('default_planning_percent'), ['class' => 'form-control']) !!}
    </div>
</div>

<i> * à séparer avec le délimiteur | </i>

<div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
        <button type="submit" class="btn btn-success">Enregistrer</button>
    </div>
</div>
