@extends('common.layout')

@section('content')


    <div class="portlet light">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-cogs font-green-sharp"></i>
                <span class="caption-subject font-green-sharp bold uppercase">
Ajouter un FAI
                </span>
            </div>
        </div>
        <div class="portlet-body">

            {!! Form::model(new \App\Models\Fai, array('route' => array('fai.store'), 'method'=> 'post', 'class'=>'form-horizontal')) !!}
            @include('fai.form')
            {!! Form::close() !!}

        </div>
    </div>

@endsection