<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8"/>
    <title>Connexion</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8">
    <meta content="" name="description"/>
    <meta content="" name="author"/>

    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>

    <link rel="stylesheet" href="/css/bootstrap.css">
    <link rel="stylesheet" href="/css/layout.css">
    <link rel="stylesheet" href="/css/tor.css">
    <link href="/css/login.css" rel="stylesheet" type="text/css"/>

    <link rel="shortcut icon" href="favicon.ico"/>
</head>
<body class="login">
<div class="menu-toggler sidebar-toggler">
</div>

@section('content')

<div class="content" style="background-color:white">
	<h1>Changez votre mot de passe</h1>
</div>
<div class="content" style="background-color:white">
<div id="changePassword">
	<form action="/motdepasse/new" method="POST">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<div class="form-group">
		    <label for="email">Adresse email</label>
		    {!! Form::email('email', Input::old('email'), array('class'=>'form-control', 'placeholder' => 'Votre email')) !!}
		</div>

		{!! Form::submit('Ok', array('class' => 'btn btn-primary')) !!}
	{!! Form::close() !!}
</div>
</div>
