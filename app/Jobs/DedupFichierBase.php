<?php

namespace App\Jobs;

use App\Jobs\Job;

use App\Models\Notification;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;

class DedupFichierBase extends Job implements SelfHandling
{

    use InteractsWithQueue, SerializesModels;
    public $base_id;
    public $file;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($base_id,$file)
    {
        $this->base_id = $base_id;
        $this->file = $file;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        $monfichier = $this->file;

        \Log::info('JOB Fichier > ' . $this->file . ' base_id > ' . $this->base_id );
        // this destinatairess

        // je prend ma base en chunck
        \Log::info('Fichier > ' . $monfichier . ' > debut export filter bdd : ' . date('Ymd H:i:s'));
        $original = storage_path('listes/listmanager/' . $this->file);
        $handle = fopen($original, 'r');

        \DB::table('destinataires')
					->select('mail')
          ->where('base_id', $this->base_id)
					->chunk(20000, function($base_dest){

					foreach($base_dest as $bd){
							$this->destinataires[$bd->mail]=null;
						}
					});

          while($ligne = fgets($handle))
          {
            $tablisteorigine[trim($ligne)] = null;

          }

        $uniques = array_diff_key($tablisteorigine, $this->destinataires);

        $contentuniques = '';
        foreach ($uniques as $key => $value) {
          $contentuniques.= $key ."; \n";
        }

        $filename = storage_path('listes/download/') . 'filtrage_'. $monfichier ."_".count($uniques)."_uniques-base-bdd[".$this->base_id."].txt";

        $fp = fopen($filename,"w+");
        fwrite($fp, $contentuniques);

        \Log::info('Fichier > ' . $monfichier . ' > fin export filter dedup base : ' . date('Ymd H:i:s'));
        Notification::create([
            'user_id' => 1,
            'level' => 'info',
            'is_important' => true,
            'message' => "Fichier filter dedup bdd " .$monfichier. " success"
        ]);



        // je charge mon fichier
    }
}
