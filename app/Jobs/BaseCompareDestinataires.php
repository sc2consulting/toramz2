<?php

namespace App\Jobs;

use App\Models\Base;
use App\Models\Notification;
use App\Models\Destinataire;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Jobs\SendersPopulate;

class BaseCompareDestinataires extends Job implements SelfHandling, ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    public $base_id;
    public $file;
    public $hash;
    public $select;
    private $tab = array();
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct($base_id, $hash, $file)
    {
        $this->base_id = $base_id;
        $this->hash = $hash;
        $this->select = $this->hash;
        $this->file = storage_path()."/compare_$hash/".$file;
    }

    /**
     * Execute the command.
     *
     * @return void
     */
    public function handle()
    {

        $base_id = $this->base_id;
        $file = $this->file;

        if($this->hash != 'hash' and $this->hash != 'mail'){
            \Log::error("[BaseCompareDestinataires] : Le fichier $file n'est ni un fichier de hash ni un fichier de mail.");
            exit;

        }

        if (!is_file($file)) {
            \Log::error('File not found : '.$file);
        }

        \Log::info("[BaseCompareDestinataires] : dans la base ($base_id) (QUEUE COMMAND) du fichier -- '.$file");

        $ts = date('Y-m-d H:i:s');

        \App\Helpers\Profiler::start('compare');

        set_time_limit(0);
        ini_set('max_execution_time', 0);

        $base = Base::find($base_id);

        $extension = \File::extension($file);
        $name = \File::name($file);

        $fh = fopen($file, 'r');

        $total = 0;
        $already = 0;
        $new = 0;
        $error = 0;

        $liste = str_replace(".$extension",'',$name);

        \DB::table('destinataires')
            ->select($this->select)
            ->where('base_id',$base_id)
            ->where('statut', 0)
            ->chunk(25000, function ($base_destinataires) {
                foreach ($base_destinataires as $b) {
                    $this->tab[$b->{$this->select}] = "";
                }
            });

        while (!feof($fh))
        {
            $cells = [];

            $row = fgets($fh, 1024);
            $row = trim($row, "\r\n\t ");

            if (strpos($row, ';') !== false) {
                $cells = explode(';', $row);
            } elseif (strpos($row, '|') !== false) {
                $cells = explode('|', $row);
            } else {
                $cells[] = trim($row, " \r\n\t\"");
            }

            foreach($cells as $cell)
            {
                $cell = trim($cell, " \r\n\t\"");

                if(!isset($this->tab[$cell])) {
                    $new++;
                    continue;
                }

                $already++;
                break;
            }
        }

        if (isset(\Auth::User()->id)) {
            $user_id = \Auth::User()->id;
        } else {
            $user_id = 1;
        }

        Notification::create([
            'user_id' => $user_id,
            'level' => 'info',
            'is_important' => true,
            'message' => "Fichier $name comparé avec la base $base->nom. $new nouvelles @, $already doublons et $error erreurs."
        ]);

        \Log::info("[BaseCompareDestinataires] : End of Compare Destinataires dans la base ($base_id) (QUEUE COMMAND) du fichier -- '.$file");
        $report = \App\Helpers\Profiler::report();
        \Log::info("[BaseCompareDestinataires] : End of Compare Destinataires dans la base ($base_id) (QUEUE COMMAND) du fichier -- '.$file\n".$report);

    }
}
