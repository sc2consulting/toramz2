<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Symfony\Component\Process\Process as Process;

class JobTest implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;


    protected $variableAPasserEnParam;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $variable)
    {
        //
        $this->variableAPasserEnParam = $variable;
    }

    /**
     * Execute the job.
     *
     * @return void
*/
    public function handle()
    {
        for($i = 0; $i < 3; $i++)
        {
            \Log::info("Test Job :".$i);
            sleep(3);
        }
        \Log::info("Test Job Is Finished");
    }
}
