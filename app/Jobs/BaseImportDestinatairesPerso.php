<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

use App\Models\Base;
use App\Models\Notification;



class BaseImportDestinatairesPerso extends Job implements SelfHandling, ShouldQueue
{

      use InteractsWithQueue, SerializesModels;

      public $base_id;
      public $file;
      public $champsperso;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($base_id,$file,$champsperso)
    {
      $this->base_id = $base_id;
      $this->file = storage_path().'/listes/perso/'.$file;
      $this->champsperso = $champsperso;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
      $base_id = $this->base_id;
      $file = $this->file;
      $champsperso = $this->champsperso;

      $col = '';
      $val = '';
      // car cell[i] commence à 0
      $nbrcelladd = -1;
      // $cell[i];

      if (!is_file($file)) {
          $this->error('File not found : '.$file);
      }

      \Log::info("Import Destinataires dans la base ($base_id) (QUEUE COMMAND) du fichier -- '.$file");
      // $insertData = [$base_id, $fai->id, md5($cell), $liste, $ts, $ts];

      // \Log::info(json_encode($insertData));

        // if champsperso


      foreach ($champsperso as $cp) {
        // \Log::info($cp);
        $col .= ', ' . $cp;
        $val .= ', :' . $cp;

        $nbrcelladd +=1;

      }

      \Log::info("NBR cells : " . $nbrcelladd);
      // \Log::info(json_encode($insertData));

      $bulk_count = 0;
      $ts = date('Y-m-d H:i:s');

      $pdo = \DB::connection()->getPdo();
      $sql = "INSERT IGNORE INTO destinataires (base_id, fai_id, hash, liste, created_at, updated_at".$col.") VALUES (:base_id, :fai_id, :hash, :liste, :ts1, :ts2".$val.")";
      $stmt = $pdo->prepare($sql);
      $pdo->beginTransaction();

      $fais = \App\Models\Fai::all();
      $default_fai = \App\Models\Fai::find(6);

      set_time_limit(0);
      ini_set('max_execution_time', 0);

      $base = Base::find($base_id);

      $extension = \File::extension($file);
      $name = \File::name($file);

      $fh = fopen($file, 'r');

      $total = 0;
      $inserted = 0;
      $rejected = 0;
      $already = 0;

      $base_id = $base->id;
      $liste = str_replace(".$extension",'',$name);

      $bulk = [];
      while (!feof($fh)) {
          $cells = [];

          $row = fgets($fh, 1024);
          $row = trim($row, "\r\n\t ");

          if (strpos($row, ';') !== false) {
              $cells = explode(';', $row);
          } elseif (strpos($row, '|') !== false) {
              $cells = explode('|', $row);
          } else {
              $cells[] = trim($row, " \r\n\t\"");
          }

          foreach($cells as $cell) {
              $fai = $default_fai;

              if (filter_var($cell, FILTER_VALIDATE_EMAIL)) {
                  $total++;

                  foreach($fais as $k_fai) {
                      foreach($k_fai->get_domains() as $dom) {
                          if (strpos($cell,$dom) !== false) {
                              $fai = $k_fai;
                              break 2;
                          }
                      }
                  }

                  if (!isset($fai->nom)) {
                      var_dump($fai);
                      var_dump($cell);
                      die();
                  }

                  $insertData = [$base_id, $fai->id, md5($cell), $liste, $ts, $ts];

                  foreach ($champsperso as $idelacells => $o) {
                    $insertData[] = $cells[$idelacells];
                  }

                  \Log::info("End of Import Destinataires dans la base ($base_id) (QUEUE COMMAND) du fichier -- '.$file");

                  // if(isset()){
                  //  }
                  // \Log::info(json_encode($insertData));
                  // \Log::info(json_encode($champsperso));


                  $stmt->execute($insertData);

                  $bulk_count++;
                  if ($bulk_count >= 250) {
                      $pdo->commit();
                      $pdo->beginTransaction();
                      $bulk_count = 0;
                  }

                  $inserted++;
                  continue;
              } else {
                  $rejected++;
              }
          }
      }

      $pdo->commit();

      if (isset(\Auth::User()->id)) {
          $user_id = \Auth::User()->id;
      } else {
          $user_id = 1;
      }

      Notification::create([
          'user_id' => $user_id,
          'level' => 'info',
          'is_important' => true,
          'message' => "Fichier $name importé. $inserted insérés, $already déjà présents, $rejected rejetés."
      ]);

      \Log::info("End of Import Destinataires dans la base ($base_id) (QUEUE COMMAND) du fichier -- '.$file");

      // on repasse sur la base
      // $base_id
      // base id

      // select des desti qui on un prénom de set
      // not civilite 1 or 0 or null
      $desti_maj = \DB::table('destinataires')
      ->where('base_id',$base_id)
      ->whereNull('civilite')
      ->whereNotNull('prenom')
      ->get();

      // lecture de la table prenom
      // \Log::info(json_encode($desti_maj));
      // re set le desti
      foreach ($desti_maj as $d) {

        $resultprenom = \DB::table('prenoms')
        ->where('prenom',$d->prenom)
        ->Orwhere('prenom',ucfirst($d->prenom))
        ->first();

        // \Log::info($resultprenom->sexe);
        $civil = 3;
        if($resultprenom->sexe == 'F'){
          $civil = 0;
        } else if ($resultprenom->sexe == 'M'){
          $civil = 1;
        }

        // update

        \DB::table('destinataires')
        ->where('id', $d->id)
        ->update(['civilite' => $civil]);

      }

      \Log::info("Lancement Update après import civilite");

      \App\Helpers\Profiler::report();



      // \Log::info($sql);

    }
}
