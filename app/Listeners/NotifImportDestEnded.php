<?php

namespace App\Listeners;

use App\Events\DestinatairesImported;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class NotifImportDestEnded
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  DestinatairesImported  $event
     * @return void
     */
    public function handle(DestinatairesImported $event)
    {
        \App\Models\Notification::create([
            'level' => 'info',
            'user_id' => \Auth::User()->id,
            'message' => "Fichier ".$event->infos['file'].' inséré : '. $event->infos['inserted'].' insérés, '.$event->infos['already'].' déjà présents',
            'is_important' => true
        ]);
    }
}
