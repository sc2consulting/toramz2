<?php namespace App\Http\Controllers;

use App\Models\UserGroup;
use App\Classes\Routeurs\RouteurMailForYou;

class UserController extends Controller {

    public function __construct()
    {
        $this->middleware('auth', ['except' => ['NewMdp','indexNewMdp']]);
        $this->middleware('admin', ['except' => ['NewMdp','indexNewMdp']]);
    }

    public function index() {

        $users = \App\Models\User::where('is_valid', 1)->get();
        return view('user.index_new')
            ->withUsers($users)
            ->withMenu('utilisateurs');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $user_group = UserGroup::all()->pluck('name','id');
        return view('user.create_new')
            ->withMenu('utilisateurs')
            ->with('user_group',$user_group);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $user = new \App\Models\User();

        $user->name = \Input::get('name');
        $user->email = \Input::get('email');

        $user->is_bat = true;

        if(\Input::get('is_bat') != 'on') {
            $user->is_bat = false;
        }

        if(\Input::get('password') != "") {
            if(\Input::get('password') != \Input::get('passwordconfirm')){
                // pas bon
            } else {
                $user->password = \Hash::make(\Input::get('password'));
                $user->is_valid = 1;
            }
        }

        if( \Auth::User()->user_group->name == 'Super Admin' ) {
            $user->user_group_id = \Input::get('user_group_id');
        }

        $user->save();

        return \redirect('user');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $user_group = UserGroup::all()->lists('name','id');
        $user = \App\Models\User::find($id);
        return view('user.edit_new')
            ->withMenu('utilisateurs')
            ->withUser($user)
            ->with('user_group',$user_group);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        $user = \App\Models\User::find($id);
        $user->name = \Input::get('name');
        $user->email = \Input::get('email');

        $user->is_bat = true;

        if(\Input::get('is_bat') != 'on') {
            $user->is_bat = false;
        }

        if(\Input::get('password') != "") {
            // si les deux mots de passe ne sont pas les mêmes
            if(\Input::get('password') != \Input::get('passwordconfirm')){
                // pas bon
            } else {
                $user->password = \Hash::make(\Input::get('password'));
            }
        }

        if( \Auth::User()->user_group->name == 'Super Admin' ) {
            $user->user_group_id = \Input::get('user_group_id');
        }

        $user->save();

        return \redirect('user');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $user = \App\Models\User::find($id);
        $user->delete();

        return \redirect('user');
    }

    public function NewMdp()
    {
        // $routeurphoenix = new RouteurMailForYou();
        $string = "";
        $chaine = "abcdefghijklmnpqrstuvwxy";
        srand((double)microtime()*1000000);

        for($i=0; $i<8; $i++)
        {
            $string .= $chaine[rand()%strlen($chaine)];
        }

        $m = '<html><meta charset="utf-8"/>
			<link rel="stylesheet" href="/css/bootstrap.css">
			<link rel="stylesheet" href="/css/layout.css">
			<link rel="stylesheet" href="/css/tor.css">
			<link rel="stylesheet" href="/css/ajout.css">
			<link rel="stylesheet" href="/css/tor.css">
			<div class="alert alert-dismissable alert-success">
			Votre nouveau de passe vient a été envoyé à sur ' . \Input::get('email') . ' | <a href="/"/>Revenir sur la page de login</a> </div>
			</html>';
        echo $m;
        $password = \Hash::make($string);
        $email = \Input::get('email');
        // DB::table('users')->where('email', $email)->update(['password' => $password]);

        $html = 'Votre nouveau mot de passe est : ' . $string;

         // $data = array( 'email' => $email);

        // \Mail::raw($html, function($message) use ($data)
        /*
        \Mail::raw($html, function($message)
        {
            $message->subject('TEST MDP');
            $message->from('reset_mdp@sc2consulting.fr', 'Reset MDP');
            $message->to('fabien@lead-factory.net');
        });
        */

        // Backup your default mailer
        $backup = \Mail::getSwiftMailer();

        // Setup your gmail mailer
        $transport = \Swift_SmtpTransport::newInstance('ssl0.ovh.net', 465, 'ssl');
        $transport->setUsername('reset_mdp@sc2consulting.fr');
        $transport->setPassword('bon1jourmdp');
        // Any other mailer configuration stuff needed...
        $imapinfo = new \Swift_Mailer($transport);
        // Set the mailer as gmail
        \Mail::setSwiftMailer($imapinfo);
        // Send your message
        // Mail::send();
        $data = array( 'email' => $email, 'first_name' => 'Admin', 'from' => 'reset_mdp@sc2consulting.fr', 'from_name' => 'Admin' );

        \Mail::send( 'mail.resetmdp', ['html' => $html], function( $message ) use ($data)
        {
            $message->subject( 'Changement de mot de passe' );
            $message->to( $data['email'] );
            $message->from( $data['from'], $data['first_name']);
        });

        // Restore your original mailer
        \Mail::setSwiftMailer($backup);

    }

    public function IndexNewMdp()
    {
        return view('password.reset');
    }
}
