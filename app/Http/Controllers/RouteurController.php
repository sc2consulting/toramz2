<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Routeur;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class RouteurController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $routeurs = Routeur::all();
        return view('admin.routeur.index_new')
            ->withMenu('admin')
            ->withRouteurs($routeurs);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function ajaxGetSenders()
    {

        $routeur_id = \Input::get('routeur_id');
        $volume_sender = \Input::get('volume');

        \Log::info("ajaxGetSenders : ROUTEUR_ID $routeur_id");
        \Log::info("ajaxGetSenders : VOLUME PLANNING_ID $volume_sender");

        $senders = array();

        if($routeur_id > 0){
            \Log::info("ajaxGetSenders : IN IF");

            $senders = \DB::table('senders')
                ->select('id', 'nom', 'quota_left','type')
                ->where('routeur_id', $routeur_id)
                ->where('quota', '>', 0)
//                            ->where('quota_left', '>', $volume_sender)
//                            ->where('nb_campagnes_left', '>', 0)
                ->where('is_enable',1)
                ->get();

            \Log::info('ajaxGetSenders : COUNT SENDERS -- '.count($senders));
//            \Log::info('ajaxGetSenders : JSON SENDERS -- '.json_encode($senders));

            return json_encode($senders);
        }
    }

    public function changeStatusRouteurEnable($id)
    {
        \DB::table('settings')->where('context', 'routeur')->where('info', $id)->update(['value' => '1']);
        \DB::table('routeurs')->where('id', $id)->update(['is_active' => 1]);
        return \redirect('/admin/routeur');
    }

    public function changeStatusRouteurDisable($id)
    {
        \DB::table('settings')->where('context', 'routeur')->where('info', $id)->update(['value' => '0']);
        \DB::table('routeurs')->where('id', $id)->update(['is_active' => 0]);
        return \redirect('/admin/routeur');
    }

}
