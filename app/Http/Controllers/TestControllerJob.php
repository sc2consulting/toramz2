<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Jobs\JobTest;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Artisan;
use Symfony\Component\Process\Process as Process;
use Symfony\Component\Process\Exception\ProcessFailedException;

class TestControllerJob extends Controller
{
    /**
     * Store a new podcast.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        // Create podcast...
        for($i=0;$i<5;$i++)
        {
            JobTest::dispatch(3)->onQueue('tube' . rand(1, 3));
            //$process = new Process('php C:/laragon/www/toramz2/artisan TestCommandJob');
            //$process->start();
            \Log::info("Job Launch : ".$i);
        }

        return redirect('/');
    }
}
