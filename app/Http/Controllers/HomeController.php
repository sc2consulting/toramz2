<?php namespace App\Http\Controllers;

use App\Models\Base;
use App\Models\Bounce;
use App\Models\Notification;
use App\Models\Planning;
// fab1
use App\Http\Controllers\Auth;
use App\Models\Campagne;
// use App\Http\Requests;
// use App\Http\Controllers\Request;
// use App\Http\Controllers\DB;
use Illuminate\Http\Request;
use DB;

class HomeController extends Controller
{
	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		// partie planning
		$plannings = Planning::orderBy('date_campagne', 'desc')->take(5)->get();

		// partie campagnes
		$campagnes = Campagne::orderBy('created_at','desc')->limit(5)->get();

		$totalbase = \DB::table('bases')->count();
		$totalcampagne = \DB::table('campagnes')->count();

		// campagnes de la journée
		$today = date("Y-m-d");
		$campagnestoday = \DB::table('campagnes')->where('created_at','LIKE','%'. $today . '%')->count();

		$lesbases = \DB::table('bases')->get();


		return view('index')
			->withPlannings($plannings)
			->withBases(\App\Models\Base::all())
			->withCampagnes($campagnes)
			->with('totalbase', $totalbase)
			->with('totalcampagne', $totalcampagne)
			->with('campagnestoday', $campagnestoday)
            ->with('lesbases', $lesbases);
	}

	public function notifications()
	{
		return view('notifications')
			->with('menu', 'notifications' )
			->with('page_title', "Suivi de l'activité <small>en temps réel</small>" )
			->withNotifs(Notification::orderBy('id', 'desc')->paginate(30));
	}

	public function planning()
	{
		$offsetPage = 30;

        $base_id = \Input::get('base_id');

		$basesinfo = \DB::table('bases')->orderBy('nom', 'asc')->get();

		$planningQuery = DB::table('plannings')
			->select(\DB::raw('plannings.*, campagnes.ref, bases.code'))
            ->where('plannings.is_display', 1)
			->join('campagnes', 'campagnes.id', '=', 'plannings.campagne_id')
            ->leftJoin('bases', 'bases.id', '=', 'campagnes.base_id');

		if(!is_null($base_id) && is_numeric($base_id) ){
			$planningQuery->where('base_id', $base_id);
		}

		$plannings = $planningQuery
			->orderBy('date_campagne', 'desc')
			->orderBy('time_campagne', 'desc')
			->paginate($offsetPage);

		$tri = true;

        $is_mindbaz_list = false;

        $mindbaz = \DB::table('routeurs')
            ->where('nom', 'Mindbaz')
            ->where('is_active', 1)
            ->first();

        if(!empty($mindbaz)){
            $mindbaz_setting = \DB::table('settings')
                ->where('parameter', 'mindbaz_mode')
                ->first();

            if($mindbaz_setting->value == 'list'){
                $is_mindbaz_list = true;
            }
        }

        return view('planning_new', ['plannings' => $plannings])
			->withPageTitle('Planning général')
            ->with('is_mindbaz_list', $is_mindbaz_list)
			->withMenu('planning')
			->withBases(\App\Models\Base::where('is_active',1)->get())
			->with('basesinfo',$basesinfo)
			->with('tri',$tri)
			->with('base_id', $base_id);
    }

    public function hidingPlanning()
    {
        $offsetPage = 30;

        $base_id = \Input::get('base_id');

        $basesinfo = \DB::table('bases')->orderBy('nom', 'asc')->get();

        $planningQuery = DB::table('plannings')
            ->select(\DB::raw('plannings.*, campagnes.ref, bases.code'))
            ->where('plannings.is_display', 0)
            ->join('campagnes', 'campagnes.id', '=', 'plannings.campagne_id')
            ->leftJoin('bases', 'bases.id', '=', 'campagnes.base_id');

        if(!is_null($base_id) && is_numeric($base_id) ){
            $planningQuery->where('base_id', $base_id);
        }

        $plannings = $planningQuery
            ->orderBy('date_campagne', 'desc')
            ->orderBy('time_campagne', 'desc')
            ->paginate($offsetPage);

        $tri = true;

        $is_mindbaz_list = false;

        $mindbaz = \DB::table('routeurs')
            ->where('nom', 'Mindbaz')
            ->where('is_active', 1)
            ->first();

        if(!empty($mindbaz)){
            $mindbaz_setting = \DB::table('settings')
                ->where('parameter', 'mindbaz_mode')
                ->first();

            if($mindbaz_setting->value == 'list'){
                $is_mindbaz_list = true;
            }
        }

        return view('planning_new', ['plannings' => $plannings])
            ->withPageTitle('Planning Cachés')
            ->with('is_mindbaz_list', $is_mindbaz_list)
            ->withMenu('planning')
            ->withBases(\App\Models\Base::where('is_active',1)->get())
            ->with('basesinfo',$basesinfo)
            ->with('tri',$tri)
            ->with('base_id', $base_id);
    }

    public function showCalendarPlannings()
    {
        $planningDate = "2018";
        \Log::info(date('Y-m-d',strtotime($planningDate)));
        $plannings = DB::table('plannings')
                  ->select(\DB::raw('plannings.*,campagnes.ref,bases.code'))
                  ->join('campagnes','campagnes.id','=','plannings.campagne_id')
                  ->leftJoin('bases','bases.id','=','campagnes.base_id')
                  ->where('date_campagne','like',date('Y',strtotime($planningDate))."%")
                  ->orderBy('date_campagne', 'desc')
                  ->orderBy('time_campagne', 'desc')
                  ->get();
        $bases = \App\Models\Base::where('is_active',1)->get();
        echo json_encode(["bases"=>$bases,"plannings"=>$plannings]);
        return;
    }

    public function showPlanningDay($planningDate)
    {
		$offsetPage = 30;
		$basesinfo = \DB::table('bases')->orderBy('nom', 'asc')->get();
        $base_id = \Input::get('base_id');

        $plannings = DB::table('plannings')
                  ->select(\DB::raw('plannings.*,campagnes.ref,bases.code'))
                  ->join('campagnes','campagnes.id','=','plannings.campagne_id')
                  ->leftJoin('bases','bases.id','=','campagnes.base_id')
                  ->where('date_campagne',date('Y-m-d',strtotime($planningDate)))
                  ->orderBy('date_campagne', 'desc')
                  ->orderBy('time_campagne', 'desc')
                  ->paginate($offsetPage);

        $tri = true;
        $is_mindbaz_list = false;
        $mindbaz = \DB::table('routeurs')
            ->where('nom', 'Mindbaz')
            ->where('is_active', 1)
            ->first();

        if(!empty($mindbaz)){
            $mindbaz_setting = \DB::table('settings')
                ->where('parameter', 'mindbaz_mode')
                ->first();

            if($mindbaz_setting->value == 'list'){
                $is_mindbaz_list = true;
            }
        }

        return view('planning_new', ['plannings' => $plannings])
            ->withPageTitle('Planning d\'aujourdhui')
            ->with('is_mindbaz_list', $is_mindbaz_list)
            ->withMenu('planning')
            ->withBases(\App\Models\Base::all())
            ->with('basesinfo',$basesinfo)
            ->with('tri',$tri)
            ->with('base_id', $base_id);
    }

	public function bounces()
	{
		$bounces = Bounce::with('destinataires')->orderBy('created_at', 'desc')->take(100)->get();

		return view('stats.bounces')
			->withPageTitle('100 derniers NPAI, toutes campagnes confondues')
			->withMenu('bounces')
			->withBounces($bounces);
	}

	public function unsubscribeById(){

		$todayhms = date("Y-m-d H:i:s");
		$unsub = \Input::get('Id');
		$message = "";

		if (!empty($unsub)){
			//recupère l'id
			$id = \DB::table('destinataires')->select('id')->where('id', $unsub)->get();
			//verifie si il existe
			if(empty($id)){
			 	$message = "$unsub n'existe pas";
			}
			//MAJ si id existe
			else{
				\DB::table('destinataires')->where('id', $unsub)->update(['statut' => 3 , 'updated_at' => $todayhms, 'optout_at' =>$todayhms, 'optout_file' => 'via_form_tor']);
				$message = "l'id n°" . $unsub . " a été désabonner de toutes les bases.";
			}
		}
			return view('unsubscribe')
			->withMessage($message);
	}

	public function unsubscribe()
	{
		$today = date("Y-m-d");
		$todayhms = date("Y-m-d H:i:s");
		$unsub = \Input::get('email');
		$message = "";

		if(!empty($unsub))
		{
			$emails = \DB::table('destinataires')
				->select('id')
				->where('mail',$unsub)
				->get();

				\Log::info("Rqt sur les mails");

			if(count($emails) == 0) {
				$message = "$unsub n'existe pas.";
			} else {

				$ids = array();
				foreach($emails as $email) {
//                  \DB::statement("DELETE from tokens where date_active='$today' and destinataire_id = $email->id");
					$ids[] = $email->id;

					\DB::statement("UPDATE destinataires SET statut = 3, updated_at = '$todayhms', optout_at ='$todayhms', optout_file ='via_form_tor' where id = " . $email->id . " and optout_file IS NULL");
					// $message = "$unsub a été désinscrit de toutes les bases.";
					\Log::info("UPDATE sur desti id : " . $email->id);
				}
				$message = "$unsub a été désinscrit de toutes les bases.";

//  Optimisation de la requete ci dessus DELETE from tokens...
				\DB::table('tokens')
					->where('date_active',$today)
					->whereIn('destinataire_id',$ids)
					->delete();
			}
		}


		return view('unsubscribe')
//            ->withPageTitle('Désincrire manuellement')
			->withMenu('unsubscribe')
			->withMessage($message);
	}



	public function choixstatsemail()
	{
		$bases = Base::orderBy('nom', 'asc')->get();

		return view('choixbasestats')->withBases($bases);

	}

	public function statsbaseemail(Request $request)
	{

		$baseid = $request->input('baseid');

		$bases = Base::orderBy('nom', 'asc')->where('id',$baseid)->get();

		$nbrorigine = DB::table('destinataires')
			->select( DB::raw('count(*) as total'))
			->where('base_id',$baseid)->first();

		$campagneid = DB::table('campagnes')->select('id')->where('base_id',$baseid)->get();

		$repounbr = 0;

		foreach ($campagneid as $idc){
			$nbr2 = DB::table('repoussoirs')
				->select( DB::raw('count(*) as totalrepoussoirs'))
				->where('campagne_id',$idc->id )
				->first();
			$repounbr = $repounbr + $nbr2->totalrepoussoirs;
		}

		$resultat = 0;
		$resultat = $nbrorigine->total - $repounbr;

		return view('basestats')->withBases($bases)->with('nbrorigine',$nbrorigine)->with('repounbr',$repounbr)->with('resultat',$resultat);
	}


	public function deconnexion()
	{

		\Auth::logout();

		return redirect('/');
	}

	public function logs(){

		$file = storage_path() . '/logs/laravel-' . date("Y-m-d") . '.log';
		$file2 = storage_path('logs\laravel-' . date("Y-m-d") . '.log');

		$lignetab = array();

		$f = fopen($file, "r");

		$content = null;
		while(!feof($f)) {

			$lignetab[] = fgets($f) . "<br />";
		}
		fclose($f);

		return view('logs')->with('lignetab',$lignetab);

	}

	public function faq(){
        return view('docs.faq');
    }

}
