<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Fai;
use Illuminate\Http\Request;

class FaisController extends Controller {

    public function __construct()
    {
        $this->middleware('admin');

    }

    /**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{

        $fais = Fai::all();

        return view('fai.index_new')
            ->withMenu('fais')
            ->withFais($fais);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        return view('fai.create_new')
            ->withMenu('fais');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
        $fai = new Fai();
        $fai->nom  = \Input::get('nom');
        $fai->quota_pression = \Input::get('quota_pression');
//        $fai->quota_sender = \Input::get('quota_sender');
        $fai->quota_campagne = \Input::get('quota_campagne');
        $fai->domains = \Input::get('domains');
        $fai->default_planning_percent = \Input::get('default_planning_percent');
        $fai->save();

        return \redirect('fai')->withMessage('SAVE OK');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        $fai = Fai::find($id);
        return view('fai.edit_new')
            ->withMenu('fais')
            ->withFai($fai);

	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
        $fai = Fai::find($id);
        $fai->nom = \Input::get('nom');
        $fai->quota_pression = \Input::get('quota_pression');
//        $fai->quota_sender = \Input::get('quota_sender');
        $fai->quota_campagne = \Input::get('quota_campagne');
		$fai->domains = \Input::get('domains');
        $fai->default_planning_percent = \Input::get('default_planning_percent');
        $fai->save();

        return redirect('fai');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$fai = Fai::find($id);
        $fai->delete();

        return \redirect('fai');
	}
}
