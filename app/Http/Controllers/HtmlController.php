<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class HtmlController extends Controller {
    public function __construct()
    {
        $this->middleware('auth');
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        $campagne = \App\Models\Campagne::find($id);
        $campagne->html = html_entity_decode($campagne->html);

        return view('html.show')->withCampagne($campagne);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


  public function check_kit($id){

    $check = \DB::table('campagnes')->where('id',$id)->first();
    $htmlsource = $check->html;
    $tablinks = array();
    $tabimg = array();

    $tablinksblade = array();
    $tabimgblade = array();

    $domaine = 'http://tor.sc2consulting.fr/'; // ajouter get_env
    // $url = 'campagne/1854/routeur_6';
    // $lien = $domaine . $url;

    $output = preg_match_all('/<a.+href=[\'"]([^\'"]+)[\'"].*>/Ui', $htmlsource, $matches);
    //pour chacuns des element trouvés
    for ($i = 0; $i < $output; $i++) {
        $tablinks[] = $matches[1][$i];
    }

    $output2 = preg_match_all('/"([^\s]+\.(jpg|png))"/', $htmlsource, $matches2);
    for ($ii = 0; $ii < $output2; $ii++) {
        $tabimg[] = $matches2[1][$ii];
    }

    foreach ($tabimg as $v) {
      $r = stristr($v, 'http://');
      if($r === false){
        $v = $domaine . $v;
      }
      // echo $v . '<br />';
      $client = curl_init();
      curl_setopt($client, CURLOPT_URL, $v);
      curl_setopt($client, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($client, CURLOPT_SSL_VERIFYPEER, false);
      curl_setopt($client,CURLOPT_CUSTOMREQUEST,'GET');

      $reponse = curl_exec($client);
      $reponsecode = curl_getinfo($client, CURLINFO_HTTP_CODE);
      curl_close($client);
      // var_dump($reponsecode);
      $tabimgblade[] = array($v,$reponsecode);

    }

    foreach ($tablinks as $l) {
      $l = stristr($l, 'http://');
      if($l === false){
        $l = $domaine . $l;
      }

      // echo $l . '<br />';
      $client = curl_init();
      curl_setopt($client, CURLOPT_URL, $l);
      curl_setopt($client, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($client, CURLOPT_SSL_VERIFYPEER, false);
      curl_setopt($client,CURLOPT_CUSTOMREQUEST,'GET');

      $reponse = curl_exec($client);
      $reponsecode = curl_getinfo($client, CURLINFO_HTTP_CODE);
      curl_close($client);
      // var_dump($reponsecode);
      $tablinksblade[] = array($l,$reponsecode);
    }

    // var_dump($tablinks);
    // var_dump($tabimg);
    // var_dump($tablinksblade);
    // var_dump($tabimgblade);
    return view('html.check')->with('tabimgblade',$tabimgblade)->with('tablinksblade',$tablinksblade);

  }

}
