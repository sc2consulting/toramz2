<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Classes\Routeurs\RouteurMailForYou;
use App\Classes\Routeurs\RouteurEdatis;
use App\Classes\Routeurs\RouteurMindbaz;
use App\Classes\Routeurs\RouteurMaildrop;
use App\Models\Routeur;
use App\Models\Sender;


class TestController extends Controller
{


    public function testapi()
    {

      $mindbaz = new RouteurMindbaz();
      $routeur = Routeur::where('nom','Mindbaz')->first();
      $senders_mindbaz = Sender::where('routeur_id', $routeur->id)->first();
      echo '<br>--------NPAI------------<br>';
      // var_dump($senders_mindbaz);
      // raymondmarcellin@orange.fr
      // fabien@lead-factory.net
      $desti = array('1eric@caramail.com','21dijon@culturevelo.com','nonohmi@free.fr');
      $r = $mindbaz->get_subscriberid_all_npai($senders_mindbaz,$desti);

      // var_dump($r);
      // dd($r->fld[4]->value);
      // echo '<br><br>';
      foreach ($r as $arrayresult) {
        // echo '<br>-------------------------<br>';
        // var_dump($v['fld']);

        foreach ($arrayresult as $ar) {
          echo '<br>-------------------------<br>';
          var_dump('1['.$ar->fld[1]->value.']');
          var_dump('2['.$ar->fld[2]->value.']');
          var_dump('3['.$ar->fld[3]->value.']');
          var_dump('4['.$ar->fld[4]->value.']');
          var_dump('5['.$ar->fld[5]->value.']');
          var_dump('6['.$ar->fld[6]->value.']');
          var_dump('7['.$ar->fld[7]->value.']');
          var_dump('8['.$ar->fld[8]->value.']');
          var_dump('9['.$ar->fld[9]->value.']');
          var_dump('10['.$ar->fld[10]->value.']');
          var_dump('11['.$ar->fld[11]->value.']');
          var_dump('12['.$ar->fld[12]->value.']');
          var_dump('13['.$ar->fld[13]->value.']');
          var_dump('14['.$ar->fld[14]->value.']');
          var_dump('15['.$ar->fld[15]->value.']');

          echo '<br>-------------------------<br>';
        }


      }

      echo '<br>--------DESABO------------<br>';


      $desti = array('a.chauveau@neuf.fr','07cach@gmail.com');
      $r = $mindbaz->get_subscriberid_all_npai($senders_mindbaz,$desti);

      // var_dump($r);
      // dd($r->fld[4]->value);
      // echo '<br><br>';
      foreach ($r as $arrayresult) {
        // echo '<br>-------------------------<br>';
        // var_dump($v['fld']);

        foreach ($arrayresult as $ar) {
          echo '<br>-------------------------<br>';
          var_dump('1['.$ar->fld[1]->value.']');
          var_dump('2['.$ar->fld[2]->value.']');
          var_dump('3['.$ar->fld[3]->value.']');
          var_dump('4['.$ar->fld[4]->value.']');
          var_dump('5['.$ar->fld[5]->value.']');
          var_dump('6['.$ar->fld[6]->value.']');
          var_dump('7['.$ar->fld[7]->value.']');
          var_dump('8['.$ar->fld[8]->value.']');
          var_dump('9['.$ar->fld[9]->value.']');
          var_dump('10['.$ar->fld[10]->value.']');
          var_dump('11['.$ar->fld[11]->value.']');
          var_dump('12['.$ar->fld[12]->value.']');
          var_dump('13['.$ar->fld[13]->value.']');
          var_dump('14['.$ar->fld[14]->value.']');
          var_dump('15['.$ar->fld[15]->value.']');

          echo '<br>-------------------------<br>';
        }


      }


      echo '<br>--------ABO------------<br>';


      $desti = array('02mamaison@orange.fr','01nathaliefournier@gmail.com');
      $r = $mindbaz->get_subscriberid_all_npai($senders_mindbaz,$desti);

      // var_dump($r);
      // dd($r->fld[4]->value);
      // echo '<br><br>';
      foreach ($r as $arrayresult) {
        // echo '<br>-------------------------<br>';
        // var_dump($v['fld']);

        foreach ($arrayresult as $ar) {
          echo '<br>-------------------------<br>';
          var_dump('1['.$ar->fld[1]->value.']');
          var_dump('2['.$ar->fld[2]->value.']');
          var_dump('3['.$ar->fld[3]->value.']');
          var_dump('4['.$ar->fld[4]->value.']');
          var_dump('5['.$ar->fld[5]->value.']');
          var_dump('6['.$ar->fld[6]->value.']');
          var_dump('7['.$ar->fld[7]->value.']');
          var_dump('8['.$ar->fld[8]->value.']');
          var_dump('9['.$ar->fld[9]->value.']');
          var_dump('10['.$ar->fld[10]->value.']');
          var_dump('11['.$ar->fld[11]->value.']');
          var_dump('12['.$ar->fld[12]->value.']');
          var_dump('13['.$ar->fld[13]->value.']');
          var_dump('14['.$ar->fld[14]->value.']');
          var_dump('15['.$ar->fld[15]->value.']');

          echo '<br>-------------------------<br>';
        }


      }


    }

    public function tst()
    {
    	return view('testSendCookies2');
    }
    public function getCookie(Request $request){
	    \Log::info("Query number: ".count($request["query"]));
	    \Log::info("Query array: ".print_r($request["query"],true));
	    if(empty($request["query"]["l"]) || empty($request["query"]["vid"]) || empty($request["query"]["hash"]) || empty($request["query"]["acheteur"]) ){
		    echo json_encode(["status"=>"failure"]);
		    die();
	    }

	echo json_encode(["status"=>"success"]);
	die();
    }
}
