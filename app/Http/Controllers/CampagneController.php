<?php

namespace App\Http\Controllers;

use App\Classes\Routeurs\RouteurMindbaz;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Base;
use App\Models\Bounce;
use App\Models\Campagne;
use App\Models\CampagneSegments;
use App\Models\CampagneStat;
use App\Models\Clic;
use App\Models\Ouverture;
use App\Models\PlanningFaiVolume;
use App\Models\Routeur;
use App\Models\Segment;
use App\Models\Theme;
use App\Models\Token;
use App\Models\Planning;
use App\Models\Fai;

// fab1
use Input;
use Validator;
use Redirect;
use Session;
use App\Fileentry;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Response;
// fin
use Illuminate\Http\Request;

use App\Jobs\CampagneImportRepoussoirs;

use PDepend\Util\Log;
use PhpParser\Node\Expr\Cast\Object_;

use \Carbon\Carbon;

// ajout test
use App\Classes\Routeurs\RouteurPhoenix;
use App\Classes\Routeurs\RouteurMailForYou;
use App\Classes\Routeurs\RouteurEdatis;
use App\Classes\Routeurs\RouteurMailKitchen;

use PHPMailer\PHPMailer\PHPMailer;


class CampagneController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $base_id = \Input::get('base_id');

        $recherche = '';

        $offsetPage = 30;

        $query = Campagne::orderBy('created_at', 'desc');

        if( !empty(\Input::get('recherche')) ){
            // recup de la ref et suppression de l'espace ajouté pour affichage
            $recherche = \Input::get('recherche');
            $search = explode(' | ', $recherche);

            $query->where('ref', 'LIKE', '%' . $search[0] . '%');
        }

        if(!is_null($base_id) && is_numeric($base_id) ){
            $query->where('base_id',$base_id);
        }

        $campagnes = $query->paginate($offsetPage);

        $basesinfo = \DB::table('bases')->orderBy('nom', 'asc')->get();

        $is_mindbaz_list = false;

        $mindbaz = \DB::table('routeurs')
            ->where('nom', 'Mindbaz')
            ->where('is_active', 1)
            ->first();

        if(!empty($mindbaz)){
            $mindbaz_setting = \DB::table('settings')
                ->where('parameter', 'mindbaz_mode')
                ->first();

            if($mindbaz_setting->value == 'list'){
                $is_mindbaz_list = true;
            }
        }

        return view('campagne.index_new', ['campagnes' => $campagnes])
        // return view('campagne.index', ['campagnes' => $campagnes])
            ->with('base_id', $base_id)
            ->with('recherche', $recherche)
            ->withMenu('campagnes')
            ->with('is_mindbaz_list', $is_mindbaz_list)
            ->with('basesinfo',$basesinfo);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {

      // dev
      $get_plateforme = \DB::table('plateformes_affi')
      ->orderBy('nom_plateforme','asc')
      ->get();

        return view('campagne.create_new')

            ->withMenu('campagnes')
            ->withBases(Base::pluck('nom', 'id'))
            ->withThemes(Theme::pluck('nom', 'id'))
            ->withSelectedBases(array())
            ->with('selected_bases', array())
            ->with('plateformes',$get_plateforme);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $bases = Base::select('id')
            ->where('is_active', 1)
            ->get();
        $fields = array_except(\Input::all(), '_token');
        $fields = array_except($fields, 'routeur');

        $fields['withtags'] = 1;
        $fields['hostimage'] = 1;

        //info si l'on doit rajouter les tags partenaires
        if(\Input::get('withtags') != 'on') {
            $fields['withtags'] = 0;
        }
        if(\Input::get('hostimage') != 'on') {
            $fields['hostimage'] = 0;
        }

        $bases_ids = $fields['base_id'];
        if( count($fields['base_id']) > 1 ) {
            $fields['base_id'] = 0;
            if(in_array(0, $bases_ids)){
                $bases_ids = array_pluck($bases, 'id');
            }
        } elseif (count($fields['base_id']) == 1 ) {
            $fields['base_id'] = $bases_ids[0];
            if($bases_ids[0] == 0){
                $bases_ids = array_pluck($bases, 'id');
            }
        }

        Campagne::unguard();
        $campagne = Campagne::create($fields);

        $routeurs = Routeur::all();

        foreach($routeurs as $r) {
            \Cache::forget('c' . $campagne->id . '-r'.$r->id);
        }

        //on mets à jour également la table campagne_base
        foreach($bases_ids as $bid) {
            \DB::table('campagne_base')->insert(['campagne_id' => $campagne->id, 'base_id' => $bid]);
        }

        // on indique que la campagne a bien été crée
        return redirect('campagne/'.$campagne->id.'/edit/')
            ->with('message', 'La campagne a bien été insérée en base.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $campagne = Campagne::find($id);
        $selected_bases = array();
        $count = "";

        $get_bases = \DB::table('campagne_base')
            ->select('base_id')
            ->where('campagne_id', $id)
            ->get();

        $selected_bases[] = $campagne->base_id;

        if(count($get_bases) > 0) {
            $selected_bases = array();
            $selected_bases = array_pluck($get_bases,'base_id');
        }

        $notification = \DB::table('notifications')
            ->select('message')
            ->where('message', 'LIKE', "%C$campagne->id-R%")
            ->orderBy('created_at', 'desc')
            ->first();

        if($notification != null) {
            preg_match("/\[.*\]/i", $notification->message, $output_array);
            if(isset($output_array[0])) {
                $count = $output_array[0];
            }
        }

        $get_plateforme = \DB::table('plateformes_affi')->get();

        return view('campagne.edit_new')
            ->withMenu('campagnes')
            ->withCampagne($campagne)
            ->withBases(Base::pluck('nom', 'id'))
            ->withThemes(Theme::pluck('nom', 'id'))
            ->withSelectedBases($selected_bases)
            ->with('selected_bases', $selected_bases)
            ->withCount($count)
            ->with('plateformes',$get_plateforme);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        $bases = Base::select('id')
            ->where('is_active', 1)
            ->get();

        $fields = array_except(\Input::all(), ['_token', '_method']);

        $fields['withtags'] = 1;
        $fields['hostimage'] = 1;

        //info si l'on doit rajouter les tags partenaires
        if(\Input::get('withtags') != 'on') {
            $fields['withtags'] = 0;
        }

        if(\Input::get('hostimage') != 'on') {
            $fields['hostimage'] = 0;
        }

        $bases_ids = $fields['base_id'];
        if( count($fields['base_id']) > 1 ) {
            $fields['base_id'] = 0;
            if(in_array(0, $bases_ids)){
                $bases_ids = array_pluck($bases, 'id');
            }
        } elseif (count($fields['base_id']) == 1 ) {

            $fields['base_id'] = $bases_ids[0];
            if($bases_ids[0] == 0){
                $bases_ids = array_pluck($bases, 'id');
            }
        }

        Campagne::unguard();
        $campagne = Campagne::find($id);
        $campagne->update($fields);

        //on mets à jour également la table campagne_base
        \DB::table('campagne_base')->where('campagne_id', $campagne->id)->delete();
        foreach($bases_ids as $bid) {
            \DB::table('campagne_base')->insert(['campagne_id' => $campagne->id, 'base_id' => $bid]);
        }

        \Cache::forget('c' . $campagne->id . '-r1');
        $routeurs = Routeur::all();

        foreach($routeurs as $r) {
            \Cache::forget('c' . $campagne->id . '-r'.$r->id);
        }

        // update campagne CA

        $ca_update = \DB::table('campagne_ca_relation')->where('id_campagne', $campagne->id)->orderBy('id','desc')->first();
        //var_dump($ca_update);
        // die();

        if(!is_null($ca_update)){
        \DB::table('campagnes_ca_concat')
            ->where('id', $ca_update->id_campagnes_ca)
            ->update(['plateforme_id' => $fields['plateforme_id']]);
          }

        return redirect('campagne/' . $campagne->id . '/edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    public function send_bat_maildrop($id)
    {
        $campagne = Campagne::find($id);

        \Log::info("BAT MD");

        $maildrop = new \App\Classes\Routeurs\RouteurMaildrop();
        $status = $maildrop->send_bat($id);

        // $smessage = new \App\Classes\Routeurs\RouteurSmessage();
        // $status2 = $smessage->send_bat($id);

        // $phoenix = new RouteurPhoenix();
        // $phoenix->send_bat($id);

        // \Log::info("BAT M4Y");

        // $routeur = new RouteurMailForYou();
        // $routeur->m4u_bat($id);

        $batstatus = false;

        // if ($status == 'ok' && $status2 == 'ok') {
        if ($status == 'ok') {
            \App\Models\Notification::create([
                'user_id' => \Auth::User()->id,
                'is_important' => 0,
                'level' => 'info',
                'message' => "Le BAT pour la Campagne " . $campagne->ref . " a été envoyé."
            ]);
            $batstatus='ok';
        }

        \Log::info("BAT End");
        return $batstatus;
    }

    public function send_bat_smessage($id)
    {
        $campagne = Campagne::find($id);

        \Log::info("BAT SM");

        $smessage = new \App\Classes\Routeurs\RouteurSmessage();
        $status = $smessage->send_bat($id);

        $batstatus = false;

        // if ($status == 'ok' && $status2 == 'ok') {
        if ($status == 'ok') {
            \App\Models\Notification::create([
                'user_id' => \Auth::User()->id,
                'is_important' => 0,
                'level' => 'info',
                'message' => "Le BAT pour la Campagne " . $campagne->ref . " a été envoyé."
            ]);
            $batstatus='ok';
        }

        \Log::info("BAT End");
        return $batstatus;
    }

    public function send_bat_edatis($campagne_id){

      $routeur = new RouteurEdatis();

      // a refaire après

      $a = array();
      $a[] = array('email' => 'fabien@lead-factory.net');
      $a[] = array('email' => 'adeline@sc2consulting.fr');
      $a[] = array('email' => 'olivier@plandefou.com');
      $a[] = array('email' => 'zouir@sc2consulting.fr');

      $routeur->send_bat($campagne_id,$a);

      // $batstatus=true;
      // return $batstatus;

    }


    public function choix_compte_bat_m4y($id)
    {

      $m4u = Routeur::where('nom','MailForYou')->first();
      $sender_m4y = \DB::table('senders')->where('routeur_id', $m4u->id)->get();
      return view('campagne.choix_bat_m4y_new')->with('sender_m4y',$sender_m4y)->with('id',$id);

    }

    public function send_bat_mailforyou_choix(){

      $campagne_id = \Input::get('campagne_id');
      $sender_id = \Input::get('sender_id');

      // var_dump($campagne_id);
      // var_dump($sender_id);
      $routeur = new RouteurMailForYou();
      $routeur->m4u_bat_choix_new($campagne_id,$sender_id);

      return redirect('campagne/'.$campagne_id.'/edit');

    }

    public function send_bat_mailforyou($id)
    {

      $routeur = new RouteurMailForYou();
      $routeur->m4u_bat($id);

      // charger un blade avec le

      return redirect('campagne/'.$id.'/edit');

    }

    public function send_bat_mindbaz($id)
    {

        $routeur = new RouteurMindbaz();
        $bat = $routeur->send_bat($id);

        return $bat;
    }

    public function send_recap($id)
    {
        $campagne = Campagne::find($id);

        $dest = \Input::get('dest');

        echo "Envoi du RECAP $campagne->nom à $dest";
        die();
    }

    public function showStats($id)
    {
        $campagne = Campagne::find($id);
        $envoyes = Token::where('campagne_id', $campagne->id)->count();
        $ouvreurs = Ouverture::where('campagne_id', $campagne->id)->count();
        $cliqueurs = Clic::where('campagne_id', $campagne->id)->count();
        $bounces = Bounce::where('campagne_id', $campagne->id)->count();

        $segments = CampagneSegments::where('campagne_id', $campagne->id)->get();

        $campagne_stats = CampagneStat::where('campagne_id', $campagne->id)->orderBy('date')->get();

        return view('campagne.stats_new')
            ->withPageTitle('Campagne ' . $campagne->nom . ' : statistiques')
            ->withMenu('campagnes')
            ->withCampagne($campagne)
            ->withCampagneStats($campagne_stats)
            ->withEnvoyes($envoyes)
            ->withOuvreurs($ouvreurs)
            ->withCliqueurs($cliqueurs)
            ->withBounces($bounces)
            ->withSegments($segments);
    }

    public function editPlanning($id)
    {
        $campagne = Campagne::find($id);
        $segments = Segment::select('id', 'nom')->get();
        $allfais = Fai::all();

        $mindbaz = Routeur::where('nom','Mindbaz')->first();

        $m4u = Routeur::where('nom','MailForYou')->first();

        $sender_md = '';
        // optimiser cette rqt
        $sender_m4y = \DB::table('senders')->where('routeur_id', $m4u->id)->get();
        // var_dump($sender_m4y);
        $segments_select = array();
        // var_dump($segments_select);

        $fais = array();

        foreach($allfais as $f){
            $fais[$f->id] = $f->nom;
        }

        $lesrouteurs = Routeur::where('is_active', 1)->get();

        $setting = \DB::table('settings')
            ->where('parameter', 'mindbaz_mode')
            ->where('value', 'list')
            ->first();

        //TODO : condition pour faire "MINDBAZ" en version segmentation_tor ou liste_alexandre
        $planningQuery = Planning::where('campagne_id', $campagne->id);

        if(!empty($setting)){
            $planningQuery->where('routeur_id', '!=', $mindbaz->id);
        }

        $plannings = $planningQuery->get();

        $stats = array();
        foreach($plannings as $planning)
        {
            $stats[$planning->id] = \DB::table('fais_stats')
                ->select('fai_id',\DB::raw('sum(volume_used) as volume_used'))
                ->where('planning_id', $planning->id)
                ->groupBy('fai_id')
                ->get();

            $segments_get = \DB::table('plannings_segments')
                ->where('planning_id', $planning->id)
                ->first();

            $segments_select[$planning->id] = 0;

            if(!empty($segments_get)){
                $segments_select[$planning->id] = $segments_get->segment_id;
            }
        }

//        print_r($segments);
//        dd($segments_select);

        $today = date("Y-m-d");

        $planning_config_defaut = \DB::table('planning_defaut')->select('config_id')->distinct()->get();

        // var_dump($planning_config_defaut);

        return view('campagne.planning_new')
//            ->withPageTitle('Campagne ' . $campagne->nom . ' : planning')
            ->withMenu('campagnes')
            ->withCampagne($campagne)
            ->withPlannings($plannings)
            ->withToday($today)
            ->with('lesrouteurs', $lesrouteurs)
            ->with('fais', $fais)
            ->with('stats',$stats)
            ->with('segments_select',$segments_select)
            ->with('segments',$segments)
            ->with('sender_m4y',$sender_m4y)
            ->with('planning_config_defaut',$planning_config_defaut);
    }

    public function storePlanning()
    {
        $segments = Segment::select('id', 'nom')->get();
        $dates_campagnes = $heures_campagnes = $volumes = array();

        $campagne = Campagne::find(\Input::get('campagne_id'));

        $planning_existants = Planning::where('campagne_id',$campagne->id)->get();
        $dates_campagnes = \Input::get('dates_campagnes');
        $heures_campagnes = \Input::get('heures_campagnes');
        $segments_ids = \Input::get('segments_ids');

        $volumes = \Input::get('volumes');
        $volumesfais = \Input::get('volumes_fais');

        $branches_id = \Input::get('branches_id');


        $freq_send = \Input::get('freq_send');
        $nbr_compte = \Input::get('nbr_compte');

        $isDisplay = array();
        for($i=0;$i<count($volumes);$i++)
        {
            array_push($isDisplay,\Input::get('isDisplay'.$i));
        }

        if(!empty($volumesfais)) {
//            $volumes_fais = array_values($volumesfais);
        }
        else {
//            $volumes_fais = array();
        }

        $volumes_fais = $volumesfais;

        $already = \Input::get('already');
        $newplannifs = 0;

        $routeurplanning = \Input::get('routeurs_ids');

        $sender_ids = \Input::get('sender_ids');

        // partie protection en dur
        $types =  \Input::get('types');

        $allfais = Fai::all();
        $fais = array();

        // var_dump($volumesfais);
        // die();

        foreach($allfais as $f)
        {
            $fais[$f->id] = $f->nom;
        }

        foreach ($planning_existants as $p)
        {
            //count(already) dans la cas ou on supprime tous les plannings (dans le formulaire)
            if (count($already) == 0 or !in_array($p->id, $already)) {

                //TODO : FIX conflits
                // enregistrer à la fois planning normal et planifier MB
                // car supprime les planifs 'MB'

                \DB::table('tokens')
                    ->where('planning_id', $p->id)
                    ->update(['campagne_id' => NULL, 'planning_id' => 0]);
                \DB::table('campagnes_routeurs')
                    ->where('planning_id', $p->id)
                    ->delete();
                \DB::table('plannings_fais_volumes')
                    ->where('planning_id',$p->id)
                    ->delete();
                \DB::table('plannings_segments')
                    ->where('planning_id',$p->id)
                    ->delete();
                \DB::table('fais_stats')
	                ->where('planning_id',$p->id)
                    ->delete();
				$count = \DB::table('tokens_stats')
					->where('planning_id',$p->id)
					->first();

                $p->delete();
            }
        }

        Planning::unguard();

        $stats = $segments_select = array();

//        dd($volumes);

        for($i=0;$i<count($volumes);$i++)
        {
            //Ca veut dire que l'on créé un nouveau planning sinon
            $planning = Planning::find($already[$i]);

            if(!$planning && $already[$i]==0) {
                $planning = new Planning();
                $newplannifs++;
            }

            $segments_select[$planning->id] = 0;

            if($planning && !empty($planning->sent_at)){
                $stats[$planning->id] = \DB::table('fais_stats')
                    ->select('fai_id',\DB::raw('sum(volume_used) as volume_used'))
                    ->where('planning_id', $planning->id)
                    ->groupBy('fai_id')
                    ->get();

                $segments_get = \DB::table('plannings_segments')
                    ->where('planning_id', $planning->id)
                    ->first();

                if(!empty($segments_get)){
                    $segments_select[$planning->id] = $segments_get->segment_id;
                }
                continue;
            }

            if(empty($routeurplanning[$i]) || $routeurplanning[$i] == '0'){
                return redirect('campagne/'.$campagne->id.'/planning/routeurerror')->withFais($fais)->withStats($stats);
            } else {
                $planning->routeur_id = $routeurplanning[$i];
            }

            $planning->campagne_id = $campagne->id;
            $planning->date_campagne = $dates_campagnes[$i];
            $planning->time_campagne = $heures_campagnes[$i];

            $planning->volume = $volumes[$i];

            $planning->type = $types[$i];
            $planning->branch_id = $branches_id[$i];

            // freq
            $planning->freq_send = $freq_send[$i];
            $planning->nbr_sender = $nbr_compte[$i];
            \Log::info($isDisplay);
            \Log::info($dates_campagnes);
            if($isDisplay[$i] == "on")
            {
                $planning->is_display = 1;
            }
            else
            {
                $planning->is_display = 0;
            }


            $planning->save();

            // FLAG TODO - faire mieux
            if($already[$i] == 0 && !empty($sender_ids[$i])){
                \DB::table('planning_senders')->insert([
                  'planning_id' => $planning->id,
                  'sender_id' => $sender_ids[$i],
                  'created_at' => date("Y-m-d H:i:s"),
                  'updated_at' => date("Y-m-d H:i:s")
                ]);
            }

            \DB::table('plannings_segments')
                ->where('planning_id', $planning->id)
                ->delete();

            if(!empty($segments_ids[$i])){
                \DB::table('plannings_segments')
                    ->insert(
                        [
                            'planning_id' => $planning->id,
                            'segment_id' => $segments_ids[$i]
                        ]);
                $segments_select[$planning->id] = $segments_ids[$i];
            }

//            var_dump($volumes_fais);

            if(!isset($volumes_fais[$i])){
                continue;
            }

            foreach($volumes_fais[$i] as $k => $vf)
            {
                if($k == 0){
                    continue;
                }

                $planfaivol = PlanningFaiVolume::firstOrCreate(['planning_id' => $planning->id, 'fai_id' => $k]);

                $planfaivol->volume = $vf;
                $planfaivol->save();
            }
        }

        if($newplannifs > 0) {
            \App\Models\Notification::create([
                'user_id' => \Auth::User()->id,
                'is_important' => 1,
                'level' => 'info',
                'message' => "a créé $newplannifs nouvelles(s) plannif(s) pour " . $campagne->ref
            ]);
        }

//        die();
        return redirect('campagne/'.$campagne->id.'/planning')
            ->withFais($fais)
            ->withStats($stats)
            ->with('segments_select',$segments_select)
            ->with('segments',$segments);
    }

    public function destroyPlanning($id)
    {
        $planning = Planning::find($id);
        $planning->delete();
    }

    public function showHtml($id)
    {
        $campagne = Campagne::find($id);
        $routeur = Routeur::find(1);

        echo $campagne->generateHtml($routeur);
    }

    // fab1 import md5
    public function choixfichiermd5($id)
    {
        $directory = storage_path('repoussoirs');
        $files = File::allFiles($directory);

        return view('campagne.md5_new')
            ->withCampagne(Campagne::find($id))
            ->withBases(Base::pluck('nom', 'id'))
            ->withThemes(Theme::pluck('nom', 'id'))
            ->with('files', $files);
    }

    public function add_repoussoirs()
    {
        $fichiermd5 = basename(\Input::get('file'));
        $campagneid = \Input::get('campagneid');
        $this->dispatch (new CampagneImportRepoussoirs($campagneid, $fichiermd5));
        return json_encode(array('status'=>'ok'));
    }

    public function upload_repoussoirs()
    {
        $directory = storage_path('repoussoirs');

        // getting all of the post data
        $file = array('fichier' => Input::file('fichier'));

        // On garde le vrai nom de fichier
        $name = basename(\Input::file('fichier')->getClientOriginalName());

        $rules = array('fichier' => 'required',); //mimes:jpeg,bmp,png and for max size max:10000
        // doing the validation, passing post data, rules and the messages
        $validator = \Validator::make($file, $rules);
        if ($validator->fails()) {
            // send back to the page with the input data and errors
            return \Redirect::to('upload')->withInput()->withErrors($validator);
        } else {
            // checking file is valid.
            if (\Input::file('fichier')->isValid()) {
                $extension = Input::file('fichier')->getClientOriginalExtension(); // getting image extension
                $fileName = $name . '_' . time() . '.md5.' . $extension; // renameing image
                \Input::file('fichier')->move($directory, $fileName); // uploading file to given path
                // sending back with message
                \Session::flash('success', 'Upload réussi');

                return \Redirect::to('upload');
            } else {
                // sending back with error message.
                \Session::flash('error', 'uploaded file is not valid');
                return \Redirect::to('upload');
            }
        }
    }

    public function previsualisation($id)
    {
        return view('campagne.voir')->withCampagne(Campagne::find($id));
    }

    public function editmessage($id)
    {
        $newcamp = true;

        return view('campagne.edit')
            ->withMenu('campagnes')
            ->withCampagne(Campagne::find($id))
            ->withBases(Base::pluck('nom', 'id'))
            ->withThemes(Theme::pluck('nom', 'id'))->with("newcamp",$newcamp);
    }

    // ajout 08/01/16
    public function editPlanningTheme($id)
    {
        $campagne = Campagne::find($id);
        $lesthemes = \DB::table('volume_theme')->where('base_id',$campagne->base_id)->where('nombre_ip','>','50')->orderBy('nombre_ip','desc')->get();

        $lesplannings = \DB::table('plannings')->where('campagne_id','=',$id)->get();
        $plannings = Planning::where('campagne_id', $campagne->id)->get();
        $today = date("Y-m-d");

        return view('campagne.planning-theme')
//            ->withPageTitle('Campagne ' . $campagne->nom . ' : planning')
            ->withMenu('campagnes')
            ->withCampagne($campagne)
            ->withPlannings($plannings)
            ->withToday($today)->with('lesthemes',$lesthemes)
            ->with('lesplannings',$lesplannings );
    }

    public function showRouteurHtml($id,$routeur_id)
    {
        $routeur = Routeur::find($routeur_id);
        $campagne = Campagne::find($id);
        if($campagne) {
            if($routeur->nom == 'MailForYou'){
                $html = $campagne->generateHtmlM4u($routeur);
            } else {

                $html = $campagne->generateHtml($routeur);
            }

            return view('campagne.smessagehtml')->withHtml($html);
        }
    }

    // a remodif pour + propre
    public function showRouteurHtmlAnonyme($id,$routeur_id)
    {
        $routeur = Routeur::find($routeur_id);
        $campagne = Campagne::find($id);
        if($campagne) {
            $html = $campagne->generateHtmlAnonyme($routeur);
            return view('campagne.smessagehtml')->withHtml($html);
        }
    }

    public function emailexport($routeur_name,$fichier)
    {
        return \File::get(storage_path("$routeur_name/$fichier"));
    }

    public function calculvolumetotal($idcampagne)
    {
        $leplanning = \DB::table('plannings')->where('campagne_id','=',$idcampagne)->get();
        $total = 0;
        $totalfinal = null;
        foreach ($leplanning as $key => $pla)
        {
            $total = $pla->volume + $total;
        }
        return $total;
    }

    public function volumetotal()
    {
        $lescampagnes = \DB::table('campagnes')->orderBy('id','desc')->take(100)->get();
        return view('campagne.volumetotal')->with('lescampagnes',$lescampagnes);
    }

    public function dernier_envoi($id)
    {
        $leplanning = \DB::table('plannings')->where('campagne_id','=',$id)->orderBy('id','desc')->first();
        if($leplanning == null){
            return 'Pas encore plannifié';
        } else {
            return $leplanning->updated_at;
        }
    }

    public function dupliquercampagne($id)
    {
        \Eloquent::unguard();
        $campagne_to_duplicate = Campagne::find($id);

        $campagne = Campagne::create([
            'html' => $campagne_to_duplicate->html,
            'base_id' => $campagne_to_duplicate->base_id,
            'theme_id' => $campagne_to_duplicate->theme_id,
            'nom' => $campagne_to_duplicate->nom,
            'date_crea' => $campagne_to_duplicate->date_crea,
            'ref' => 'Copie_' . $campagne_to_duplicate->ref,
            'expediteur' => $campagne_to_duplicate->expediteur,
            'objet' => $campagne_to_duplicate->objet,
            'cle' => $campagne_to_duplicate->cle,
            'toptext' => $campagne_to_duplicate->toptext,
            'bottomtext' => $campagne_to_duplicate->bottomtext,
            'belowtext' => $campagne_to_duplicate->belowtext,
            'deletetext' => $campagne_to_duplicate->deletetext,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
            'withtags' => $campagne_to_duplicate->withtags,
            'expediteur_anonyme' => $campagne_to_duplicate->expediteur_anonyme,
            'volume_total' => $campagne_to_duplicate->volume_total
        ]);

        $bases_ids = \DB::table('campagne_base')
            ->select('base_id')
            ->where('campagne_id', $campagne_to_duplicate->id)
            ->get();

        if(count($bases_ids) > 0) {
            foreach ($bases_ids as $b) {
                \DB::table('campagne_base')->insert(['campagne_id' => $campagne->id, 'base_id' => $b->base_id]);
            }
        }

        Campagne::reguard();

        return redirect('campagne');
    }

    public function upload_import_img_server()
    {
        $directory = storage_path('zip');
        $fichiers = \File::files($directory);

        $directoryp = public_path('campagnes');
        $lesdirectoryp = \File::directories($directoryp);

        return view('campagne.upload_img_new')->with('fichiers', $fichiers)->with('lesdirectoryp',$lesdirectoryp);
    }

    public function import_img_server()
    {

        $name = \Input::file('fichier')->getClientOriginalName();
        \Input::file('fichier')->move(storage_path() . '/zip', $name);

        $directory = storage_path('zip');
        $fichiers = \File::allFiles($directory);
        $zip = new \ZipArchive;
        $lessextension = pathinfo($name);

        $directoryp = public_path('campagnes');
        $lesdirectoryp = \File::directories($directoryp);

        if ($zip->open(storage_path('zip/' . $name)) === TRUE) {
            $zip->extractTo(public_path('campagnes/' . $lessextension['filename'] ));
            $zip->close();
            echo 'ok';
        } else {
            echo 'échec';
        }

        return redirect('/dezip');
    }

    public function extractUnsubscribers()
    {
        $all_campagnes_names = \DB::table('campagnes')
        ->select('nom')
        ->distinct()
        ->get();

        $column = 'mail';
        $if_md5 = \Input::get('column');

        if($if_md5 == 'md5'){
            $column = 'hash';
        }

        $selected_campagne = \Input::get('campagne');

        if(!empty($selected_campagne)){
            $corr_camp = \DB::table('campagnes')
                ->select('id')
                ->where('nom', $selected_campagne)
                ->get();

            $today =  Carbon::today()->format('Ymd');
            $filename = storage_path() . "/desinscrits/campagne-desinscrits-$selected_campagne-$today.csv";
            $ressource_fichier = fopen($filename, 'w+');

            \DB::table('desinscrits')
                ->select($column)
                ->whereIn('campagne_id', array_pluck($corr_camp, 'id'))
                ->chunk(10000, function ($desinscrits) use ($ressource_fichier, $column) {
                foreach ($desinscrits as $d) {
                    fputs($ressource_fichier, $d->{$column} . "\n");
                }
            });

            return response()->download($filename);
        }

        return view('campagne.extract-unsubscribers')
            ->withMenu('campagnes')
            ->withCampagnes($all_campagnes_names);

    }

    public function ajaxGetPlanningDefaut(){

      \Log::info("ajaxGetPlanningDefaut");
      // $config_id = \Input::get('config_id');
      // \Log::info(print_r($config_id));

      $config_id_array = explode('_',\Input::get('config_id'));

      foreach ($config_id_array as $e) {
        if(is_numeric($e)){
          $config_id = $e;
        }
      }

      $planningdefaut = array();
      $data = \DB::table('planning_defaut')
      ->where('config_id', $config_id)
      ->whereNotNull('fai_id')
      ->get();
      // \Log::info(print_r($data));
      $nbrplanningcampagne = \DB::table('plannings')->where('campagne_id',\Input::get('campagne_id'))->count();
      \Log::info($nbrplanningcampagne);
      $nbrplanningcampagne = $nbrplanningcampagne + 1;

      foreach ($data as $d) {
        $fai = \DB::table('fais')->where('id',$d->fai_id)->first();

        $le_routeur_id = \DB::table('planning_defaut')
        ->select('value')
        ->where('config_id', $config_id)
        ->where('config_key','routeur_id')
        ->first();

        $nomrouteur = \DB::table('routeurs')
        ->select('nom')
        ->where('id', $le_routeur_id->value)
        ->first();

        $le_volume_total = \DB::table('planning_defaut')
        ->select('value')
        ->where('config_id', $config_id)
        ->where('config_key','volume_total')
        ->first();

        $freq_envoi = \DB::table('planning_defaut')
        ->select('value')
        ->where('config_id', $config_id)
        ->where('config_key','freq_envoi')
        ->first();

        $nbr_compte = \DB::table('planning_defaut')
        ->select('value')
        ->where('config_id', $config_id)
        ->where('config_key','nbr_compte')
        ->first();

        $choix_compte = \DB::table('planning_defaut')
        ->select('value')
        ->where('config_id', $config_id)
        ->where('config_key','choix_compte')
        ->first();

        $branch_defaut = \DB::table('planning_defaut')
        ->select('value')
        ->where('config_id', $config_id)
        ->where('config_key','branch_defaut')
        ->first();

        $nombranch = \DB::table('branches')
        ->select('name')
        ->where('id',$branch_defaut->value)
        ->first();


        if($choix_compte->value == true || $choix_compte->value == '1'){
            // a refaire avec une condition après
            $senders_choice = \DB::table('senders')
            ->select('id','nom')
            ->where('routeur_id', $le_routeur_id->value)
            // ->where('quota_left','>',0)
            // ->where('campagne_left','>',0)
            ->get();
          } else {
            $senders_choice = null;
          }

        $planningdefaut[] = array('fai_id' => $d->fai_id,
        'fai_nom' => $fai->nom,
        'volume' => $d->volume,
        'volume_total' => $le_volume_total->value,
        'offsetplanning' => $nbrplanningcampagne,
        'lerouteurid' => $le_routeur_id->value,
        'freq_envoi' => $freq_envoi->value,
        'nbr_compte' => $nbr_compte->value,
        'nomrouteur' => $nomrouteur->nom,
        'senders_choice' => $senders_choice,

        'branch_defaut' => $branch_defaut->value,
        'branch_nom' => $nombranch->name


        );
      }


      return $planningdefaut;

    }


    public function config_defaut_planning(){

        $data = \DB::table('planning_defaut')->select('config_id')->distinct()->get();
        return view('campagne.config_planning_defaut')
        ->with('data',$data);

    }

    public function config_defaut_planning_add(){

        $data = \DB::table('fais')->get();
        $routeurs = \DB::table('routeurs')->get();
        $branche = \App\Models\Branch::all();

        return view('campagne.config_planning_defaut_add')
        ->with('data',$data)
        ->with('routeurs',$routeurs)
        ->with('branche',$branche);

    }

    public function config_defaut_edit($id){

        $a_fai_not_null = array();
        $branche = \App\Models\Branch::all();
        $data_fai = \DB::table('planning_defaut')
        ->where('config_id',$id)
        ->whereNotNull('fai_id')->get();

        foreach ($data_fai as $df) {
          $a_fai_not_null[] = $df->fai_id;
        }

        $fai_restant = \DB::table('fais')
        ->whereNotIn('id', $a_fai_not_null)->get();

        $data_nom = \DB::table('planning_defaut')
        ->select('value')
        ->where('config_key','name_config')
        ->where('config_id',$id)->first();

        $data_routeur = \DB::table('planning_defaut')
        ->select('value')
        ->where('config_key','routeur_id')
        ->where('config_id',$id)->first();

        $data_volume_total = \DB::table('planning_defaut')
        ->select('value')
        ->where('config_key','volume_total')
        ->where('config_id',$id)->first();

        $freq_envoi = \DB::table('planning_defaut')
        ->select('value')
        ->where('config_id', $id)
        ->where('config_key','freq_envoi')
        ->first();

        $nbr_compte = \DB::table('planning_defaut')
        ->select('value')
        ->where('config_id', $id)
        ->where('config_key','nbr_compte')
        ->first();

        $choix_compte = \DB::table('planning_defaut')
        ->select('value')
        ->where('config_id', $id)
        ->where('config_key','choix_compte')
        ->first();

        $branch_defaut = \DB::table('planning_defaut')
        ->select('value')
        ->where('config_id', $id)
        ->where('config_key','branch_defaut')
        ->first();

        $routeurs = \DB::table('routeurs')->get();

        return view('campagne.config_planning_defaut_edit')
        ->with('data_fai',$data_fai)
        ->with('id',$id)
        ->with('data_nom',$data_nom)
        ->with('data_routeur',$data_routeur)
        ->with('data_volume_total',$data_volume_total)
        ->with('routeurs',$routeurs)
        ->with('freq_envoi',$freq_envoi)
        ->with('nbr_compte',$nbr_compte)
        ->with('fai_restant',$fai_restant)
        ->with('choix_compte',$choix_compte)
        ->with('branch_defaut',$branch_defaut)
        ->with('branche',$branche);

    }

    public function config_defaut_edit_store(){

      $fields = array_except(\Input::all(), '_token');

      // var_dump($fields);
      // die();
      foreach ($fields as $k => $f) {

        if(is_numeric($k)){
          if($f != ''){

            $fai_check = \DB::table('planning_defaut')
            ->where('config_id', $fields['id_config'])
            ->where('fai_id', $k)->first();

            if($fai_check == null){

              \DB::table('planning_defaut')->insert(
                  ['config_id' => $fields['id_config'],'fai_id' => $k, 'volume' => $f, 'value' => $f]
              );

            } else {

              \DB::table('planning_defaut')
              ->where('config_id', $fields['id_config'])
              ->where('fai_id', $k)
              ->update(['volume' => $f]);
            }
          } else {
            \DB::table('planning_defaut')
            ->where('config_id', $fields['id_config'])
            ->where('fai_id', $k)
            ->delete();
            // je delete la ligne
          }
        }

        if($k == 'name_config'){
          \DB::table('planning_defaut')
          ->where('config_id', $fields['id_config'])
          ->where('config_key', 'name_config')
          ->update(['value' => $fields['name_config']]);
        }

        if($k == 'routeur_id'){
          \DB::table('planning_defaut')
          ->where('config_id', $fields['id_config'])
          ->where('config_key', 'routeur_id')
          ->update(['value' => $fields['routeur_id']]);
        }

        if($k == 'volume_total'){
          \DB::table('planning_defaut')
          ->where('config_id', $fields['id_config'])
          ->where('config_key', 'volume_total')
          ->update(['value' => $fields['volume_total']]);
        }

        if($k == 'freq_envoi'){
          \DB::table('planning_defaut')
          ->where('config_id', $fields['id_config'])
          ->where('config_key', 'freq_envoi')
          ->update(['value' => $fields['freq_envoi']]);
        }

        if($k == 'nbr_compte'){
          \DB::table('planning_defaut')
          ->where('config_id', $fields['id_config'])
          ->where('config_key', 'nbr_compte')
          ->update(['value' => $fields['nbr_compte']]);
        }

        if($k == 'choix_du_compte'){
          \DB::table('planning_defaut')
          ->where('config_id', $fields['id_config'])
          ->where('config_key', 'choix_compte')
          ->update(['value' => $fields['choix_du_compte']]);
        }

        if($k == 'la_branche'){
          \DB::table('planning_defaut')
          ->where('config_id', $fields['id_config'])
          ->where('config_key', 'branch_defaut')
          ->update(['value' => $fields['la_branche']]);
        }




      }


      return redirect('/planning/defaut');
    }

    public function config_defaut_edit_delete($id){

        \DB::table('planning_defaut')->where('config_id', $id)
        ->delete();
        return redirect('/planning/defaut');
      }

      public function config_defaut_planning_add_store(){

        $max = \DB::table('planning_defaut')->max('config_id');
        $max = $max + 1;
        $fields = array_except(\Input::all(), '_token');

        // var_dump($fields);
        // die();

          foreach ($fields as $k => $f) {
            if(is_numeric($k)){
              if($f != ''){
                \DB::table('planning_defaut')->insert(
                    ['config_id' => $max,'fai_id' => $k, 'volume' => $f, 'value' => $f]
                );
              }
            }

          if($k == 'name_config'){
            \DB::table('planning_defaut')->insert(
                ['config_id' => $max,'fai_id' => NULL,
                'volume' => NULL,
                'config_key' => 'name_config',
                'value' => $f]
            );
          }

          if($k == 'routeur_id'){
            \DB::table('planning_defaut')->insert(
                ['config_id' => $max,'fai_id' => NULL,
                'volume' => NULL,
                'config_key' => 'routeur_id',
                'value' => $f]
            );
          }

          if($k == 'volume_total'){
            \DB::table('planning_defaut')->insert(
                ['config_id' => $max,'fai_id' => NULL,
                'volume' => NULL,
                'config_key' => 'volume_total',
                'value' => $f]
            );
          }

          if($k == 'freq_envoi'){
            \DB::table('planning_defaut')->insert(
                ['config_id' => $max,'fai_id' => NULL,
                'volume' => NULL,
                'config_key' => 'freq_envoi',
                'value' => $f]
            );
          }

          if($k == 'nbr_compte'){
            \DB::table('planning_defaut')->insert(
                ['config_id' => $max,'fai_id' => NULL,
                'volume' => NULL,
                'config_key' => 'nbr_compte',
                'value' => $f]
            );
          }

          if($k == 'choix_du_compte'){
            \DB::table('planning_defaut')->insert(
                ['config_id' => $max,'fai_id' => NULL,
                'volume' => NULL,
                'config_key' => 'choix_compte',
                'value' => $f]
            );
          }

          if($k == 'la_branche'){
            \DB::table('planning_defaut')->insert(
                ['config_id' => $max,'fai_id' => NULL,
                'volume' => NULL,
                'config_key' => 'branch_defaut',
                'value' => $f]
            );
          }


        }


        return redirect('/planning/defaut');


      }


      // partie CA

      public function CampagneCa_show(){

        $date_j_limit = date("d",strtotime('-2 day'));
        $date_m = date("m");
        $date_limit = date("Y-m-d H:i:s",strtotime('-30 day'));
        $moisdelacampagne = date_parse(date("Y-m-d H:i:s"));
        $moisEnCours= date('m');
        $lastdayMonthbefore = date("Y-m-d H:i:s", mktime(0,0,0,date("m"), 0, date("Y")));
        $firstdayMonthafter = date("Y-m-d H:i:s", mktime(0,0,0,date("m"),31,date("Y")));

        $lesbases = \DB::table('bases')
        ->where('is_active',1)->get();
     //    var_dump($lesbases); die();

        $lesplatesformes = \DB::table('plateformes_affi')
        ->orderBy('nom_plateforme','asc')
        ->get();

     //    $ca_records = \DB::table('campagnes_ca')
     //    ->join('campagnes','campagnes_ca.campagne_id','=','campagnes.id')
     //    ->join('bases','campagnes.base_id','=','bases.id')
     //    ->join('plateformes_affi','campagnes.plateforme_id','=','plateformes_affi.id')
     //    // ->where('campagnes_ca.created_at','>', $date_limit)
     //    ->where('campagnes_ca.mois_compta', $moisdelacampagne['month'])
     //    ->where('bases.is_active', 1)
     //    ->get();


        $ca_concat = \DB::table('campagnes_ca_concat')
         ->where('campagnes_ca_concat.mois_compta', '=', $moisEnCours)
         ->where('campagnes_ca_concat.ca_volume_total', '>', 0)
         ->orderBy('nom', 'asc')
        ->get();
 //        $nom = $ca_concat[0]->nom;
 //        var_dump($ca_concat[0]->nom); die();
  //var_dump($ca_concat); die();
        $maj_info = \DB::table('campagnes_ca_concat')->max('updated_at');

        return view('campagne.ca_index')
        ->with('ca_concat',$ca_concat)
        ->with('moisdelacampagne',$moisdelacampagne)
        ->with('lesbases', $lesbases)
        ->with('lesplatesformes',$lesplatesformes)
        ->with('maj_info',$maj_info);

      }


      public function CampagneCa_showByBase($id){

        // récuperer l'id de la base
        // select avec les bases

        $date_j_limit = date("d",strtotime('-2 day'));
        $date_m = date("m");
        $date_limit = date("Y-m-d H:i:s",strtotime('-30 day'));
        $moisdelacampagne = date_parse(date("Y-m-d H:i:s"));

        $lesbases = \DB::table('bases')->get();
        $lesplatesformes = \DB::table('plateformes_affi')
        ->orderBy('nom_plateforme','asc')
        ->get();

        $maj_info = \DB::table('campagnes_ca_concat')->max('updated_at');

     //    $ca_records = \DB::table('campagnes_ca')
     //    ->join('campagnes','campagnes_ca.campagne_id','=','campagnes.id')
     //    ->join('bases','campagnes.base_id','=','bases.id')
     //    ->join('plateformes_affi','campagnes.plateforme_id','=','plateformes_affi.id')
     //    // ->where('campagnes_ca.created_at','>', $date_limit)
     //    ->where('campagnes_ca.mois_compta', $moisdelacampagne['month'])
     //    ->where('bases.id', $id)
     //    ->get();
        $ca_concat = \DB::table('campagnes_ca_concat')
        ->where('campagnes_ca_concat.mois_compta', '=', $moisdelacampagne['month'])
         ->where('campagnes_ca_concat.base_id', '=', $id)
         ->orderBy('nom', 'asc')
        ->get();

        return view('campagne.ca_index')
        ->with('ca_concat',$ca_concat)
        ->with('moisdelacampagne',$moisdelacampagne)
        ->with('lesbases', $lesbases)
        ->with('lesplatesformes',$lesplatesformes)
        ->with('maj_info',$maj_info);


      }


      public function CampagneCa_showByPlateforme($id){

           $date_j_limit = date("d",strtotime('-2 day'));
           $date_m = date("m");
           $date_limit = date("Y-m-d H:i:s",strtotime('-30 day'));
           $moisdelacampagne = date_parse(date("Y-m-d H:i:s"));

           $lesbases = \DB::table('bases')->get();
           $lesplatesformes = \DB::table('plateformes_affi')
           ->orderBy('nom_plateforme','asc')
           ->get();

           $maj_info = \DB::table('campagnes_ca_concat')->max('updated_at');

           $ca_concat = \DB::table('campagnes_ca_concat')
      //    ->join('campagnes','campagnes_ca.campagne_id','=','campagnes.id')
      //    ->join('bases','campagnes.base_id','=','bases.id')
      //    ->join('plateformes_affi','campagnes.plateforme_id','=','plateformes_affi.id')
           // ->where('campagnes_ca.created_at','>', $date_limit)
           ->where('campagnes_ca_concat.mois_compta', $moisdelacampagne['month'])
           ->where('plateforme_id', $id)
           ->get();

           return view('campagne.ca_index')
           ->with('ca_concat',$ca_concat)
           ->with('moisdelacampagne',$moisdelacampagne)
           ->with('lesbases', $lesbases)
           ->with('lesplatesformes',$lesplatesformes)
           ->with('maj_info',$maj_info);

      }


      public function plateforme_affi_Show(){

        // liste des plateforme
        // nombre de campagne par plateforme
        $plateforme = \DB::table('plateformes_affi')
        // ->join('campagnes','campagnes.plateforme_id','=','plateformes_affi.id')
        ->orderBy('nom_plateforme','asc')
        ->get();

        return view('campagne.plateforme_index')
        ->with('plateforme',$plateforme);

        // var_dump($plateforme);
      }


      public function plateforme_affi_add(){

        return view('campagne.plateforme_add');

      }

      public function plateforme_affi_Store(){

        $fields = array_except(\Input::all(), '_token');
        var_dump($fields);

        \DB::table('plateformes_affi')->insert(
            ['nom_plateforme' => $fields['nom_plateforme'],
             'lien' => $fields['lien']]
        );

         return redirect('/plateforme');


      }


      public function plateforme_affi_Edit(){



      }


      public function CampagneCa_show_mois($id){

        $ca_records_base = \DB::table('campagnes_ca_base')
        ->join('bases','campagnes_ca_base.base_id','=','bases.id')
        ->where('campagnes_ca_base.periode_compta',$id)
        ->where('bases.is_active', 1)
        ->get();

        $current_month = date("m");

        // faire l'affichage des derniers mois
        $nb_mois = array();
        for ($i = 1; $i <= 3; $i++) {
            $nb_mois[] = date("m",strtotime('-'.$i.' month'));
        }

        $maj_info = \DB::table('campagnes_ca_base')->max('updated_at');

        return view('campagne.ca_index_base')
        ->with('current_month', $current_month)
        ->with('ca_records_base',$ca_records_base)
        ->with('nb_mois',$nb_mois)
        ->with('maj_info',$maj_info);


      }


      public function CampagneCa_add_mois($id){
        // old fonction forcage
        $today = date("Y-m-d H:i:s");
        $ca_records_check = \DB::table('campagnes_ca')
        ->where('campagne_id',$id)
        ->where('mois_compta', $date_m = date("m"))
        ->get();

        if(count($ca_records_check) == 0){
          // pas de records je peux insert
          \DB::table('campagnes_ca')->insert(
              ['campagne_id' => $id,
              'ca_brut' => 0,
              'ca_net'=> 0,
              'aaf' => 0,
              'envoi_facture' => 0,
              'state' => 1,
              'created_at' => $today,
              'updated_at' => $today,
              'mois_compta' => date("m")]
          );
        }

        return \redirect('/ca');

      }

      public function CampagneCa_search(){

        $recherche = \Input::get('recherche');

//         $r_bdd = \DB::table('campagnes_ca_concat')->where('nom','LIKE','%'.$recherche.'%')->get();
// var_dump($r_bdd); die();
     //    $array_id = array();
        $moisEnCours= date('m');
     //    foreach ($r_bdd as $r) {
     //    $array_id[] = $r->id;
     //    }

        $date_j_limit = date("d",strtotime('-2 day'));
        $date_m = date("m");
        $date_limit = date("Y-m-d H:i:s",strtotime('-30 day'));
        $moisdelacampagne = date_parse(date("Y-m-d H:i:s"));

        $lesbases = \DB::table('bases')
        ->where('is_active',1)->get();

        $lesplatesformes = \DB::table('plateformes_affi')
        ->orderBy('nom_plateforme','asc')
        ->get();

     //    $ca_records = \DB::table('campagnes_ca')
     //    ->join('campagnes','campagnes_ca.campagne_id','=','campagnes.id')
     //    ->join('bases','campagnes.base_id','=','bases.id')
     //    ->join('plateformes_affi','campagnes.plateforme_id','=','plateformes_affi.id')
     //    // ->where('campagnes_ca.created_at','>', $date_limit)
     //    ->where('campagnes_ca.mois_compta', $moisdelacampagne['month'])
     //    ->where('bases.is_active', 1)
     //    ->whereIn('campagnes_ca.campagne_id',$array_id)
     //    ->get();

        $ca_concat = \DB::table('campagnes_ca_concat')
         ->where('campagnes_ca_concat.mois_compta', '=', $moisEnCours)
         ->where('nom','LIKE','%'.$recherche.'%')
        ->get();

        $maj_info = \DB::table('campagnes_ca')->max('updated_at');

        return view('campagne.ca_index')
        ->with('ca_concat',$ca_concat)
        ->with('moisdelacampagne',$moisdelacampagne)
        ->with('lesbases', $lesbases)
        ->with('lesplatesformes',$lesplatesformes)
        ->with('maj_info',$maj_info);
      }



      public function CampagneCaForce(){
        // old fonction forcage
        $today = date("Y-m-d H:i:s");
        $recherche = \Input::get('ajouter');

        $lacampa = \DB::table('campagnes')
        ->where('ref',$recherche)
        ->first();

        // var_dump($lacampa);
        // die();

        $ca_records_check = \DB::table('campagnes_ca_concat')
        ->where('nom',$lacampa->nom)
        ->where('base_id',$lacampa->base_id)
        ->where('mois_compta', $date_m = date("m"))
        ->get();

        if(count($ca_records_check) == 0){
          // pas de records je peux insert
          \DB::table('campagnes_ca_concat')->insert(
              ['base_id' => $lacampa->base_id,
              'nom' => $lacampa->nom,
              'plateforme_id' => $lacampa->plateforme_id,
              'ca_brut' => 0,
              'ca_net'=> 0,
              'aaf' => 0,
              'envoi_facture' => 0,
              'state' => 1,
              'ca_volume_total' => 10,
              'created_at' => $today,
              'updated_at' => $today,
              'mois_compta' => date("m")]);
        }

        $recup = \DB::table('campagnes_ca_concat')
        ->where('base_id', $lacampa->base_id)
        ->where('plateforme_id',$lacampa->plateforme_id)
        ->where('nom',$lacampa->nom)
        ->where('mois_compta',date("m"))
        ->first();

        $checkrelation=\DB::table('campagne_ca_relation')
        ->where('id_campagnes_ca', $recup->id)
        ->where('base_id_ca', $lacampa->base_id)
        ->where('id_campagne', $lacampa->id)
        ->first();

        if($checkrelation == null){
             // reste à gerer le non rinsert
             \DB::table('campagne_ca_relation')->insert([
                  'id_campagnes_ca'=> $recup->id,
                  'base_id_ca'=> $lacampa->base_id,
                  'id_campagne'=>$lacampa->id,
                  'created_at' => $today,
                  'updated_at' => $today
             ]);
        }

        return \redirect('/ca');

      }

      public function send_bat_mailkitchen($id)
      {

        $lacampagne = Campagne::where('id',$id)->first();
        $campagne = Campagne::find($id);
        \Log::info("BAT MK");
        $responseBAT = array('status'=>'success','message'=>"BAT envoyé");
        $routeur = new RouteurMailKitchen();
        $mailkitchen = Routeur::where('nom','MailKitchen')->first();
        \Log::info("[CampagneController@send_bat_maillitchen] Création du BAT");

        $html = $lacampagne->generateHtml($mailkitchen);

        $campaignid = $routeur->CreateCampaign('BAT'.$campagne->ref.'-'.time(),$campagne->expediteur,$campagne->objet, $html,'322655');
        if(is_null($campaignid)){
            $responseBAT = array('status'=>'error','message'=>"Erreur: Impossible de créer la campagne.");
        }
        sleep(30);
        \Log::info("[CampagneController@send_bat_maillitchen] Envoi du BAT");
        \Log::info("[CampagneController@send_bat_maillitchen]" . json_encode($campaignid));

        $successSending = $routeur->sendBatAt($campaignid);
        if(!$successSending){
            $responseBAT = array('status'=>'error','message'=>"Erreur: Impossible d'envoyer le BAT.");
        }
        echo json_encode($responseBAT);
        return;
      }


    public function bat_amz($id){

        $lacampagne = Campagne::where('id',$id)->first();

        $emails = ['olivier.karczewski@sfr.fr', 'fabien@lead-factory.net', 'fabienp@sc2consulting.fr','zouir@sc2consulting.fr', 'jordan.bensmihen@free.fr', 'yves-comptoirdesdeals@orange.fr', 'olivier@plandefou.com'];

        foreach ($emails as $m) {
            $mail = new PHPMailer;

            $mail->isSMTP();
            // $mail->CharSet = "text/html; charset=UTF-8;";
            /*

            // hold
                  $mail->setFrom('hello@information-lesobjetsdunet.com', utf8_decode($lacampagne->expediteur));
                  $mail->addAddress($m, 'Recipient Name');
                  $mail->Username = 'AKIARG25HAHLBB4XIC3W';
                  $mail->Password = 'BLeIi0l81SwdgUStfUCG74lGk+PAUiZzb323X3w5JCs5';
                  //$mail->addCustomHeader('X-SES-CONFIGURATION-SET', 'ConfigSet');
                  $mail->Host = 'email-smtp.us-west-2.amazonaws.com';

            */

            /*
            // new
            $mail->setFrom('hello@info-lesobjetsdunet.com', utf8_decode($lacampagne->expediteur));
            $mail->addAddress($m);
            $mail->Username = 'AKIARG25HAHLBB4XIC3W';
            $mail->Password = 'BLeIi0l81SwdgUStfUCG74lGk+PAUiZzb323X3w5JCs5';
            //$mail->addCustomHeader('X-SES-CONFIGURATION-SET', 'ConfigSet');
            $mail->Host = 'email-smtp.us-west-2.amazonaws.com';
            */


            /*

                  $mail->setFrom('hello@info-lesaffairesdumois.com', utf8_decode($lacampagne->expediteur));
                  $mail->addAddress($m);
                  $mail->Username = 'AKIAX5CBT7BIVK27HUM6';
                  $mail->Password = 'BGiPcYu0mFoCY10vm6nftMw8IpIyncZLcjnLg4Xyngfe';
                  //$mail->addCustomHeader('X-SES-CONFIGURATION-SET', 'ConfigSet');
                  $mail->Host = 'email-smtp.us-west-2.amazonaws.com';

            */

                  $mail->setFrom('hello@voyaneo.com', utf8_decode($lacampagne->expediteur));
                  $mail->addAddress($m);
                  $mail->Username = 'AKIAV43XT4Q5XBO4L44S';
                  $mail->Password = 'BHb8MKi97foBrggO4277hEg86G5knFEVwPJBKEGcYi71';
                  //$mail->addCustomHeader('X-SES-CONFIGURATION-SET', 'ConfigSet');
                  $mail->Host = 'email-smtp.us-west-2.amazonaws.com';




            /*
            $mail->setFrom('hello@ouisti-pix.fr', utf8_decode($lacampagne->expediteur));
            $mail->addAddress($m);
            $mail->Username = 'AKIA5WCK4CM73RJ44EPK';
            $mail->Password = 'BO+kgZiCPOskQInlD/gCztsBt4mDTFm/YqeXMVXm+kJy';
//$mail->addCustomHeader('X-SES-CONFIGURATION-SET', 'ConfigSet');
            $mail->Host = 'email-smtp.us-west-2.amazonaws.com';
            */


            $mail->Subject = utf8_decode($lacampagne->objet);
            $html = str_replace('[eamz]',$m, $lacampagne->html);

            $caracteres = array ("é","è","à","ç","‘","’","´","ê","ù","î","€","À","É","°");
            $caracteres_HTML = array ("&eacute;","&egrave;","&agrave;","&ccedil;","&lsquo;","&rsquo;","´","&ecirc;","&ugrave;","&icirc;","&euro;","&Agrave;","&Eacute;","&ordm;");
            $html = str_replace($caracteres,$caracteres_HTML,$html);
            $mail->Body =utf8_encode($html);
            // $mail->AltBody  =  "En exclusivité ces 5 sets de couteaux Japonais ultra tranchants. Dotés d'une précision et d'un design unique : ces couteaux de qualité professionnels vous permettront une découpe fluide et agréable.";
            $mail->SMTPAuth = true;

            $mail->SMTPSecure = 'tls';
            $mail->Port = 587;
            $mail->isHTML(true);

            /* $mail->AltBody = "

            Ce sac &agrave; dos antivol est le v&eacute;ritable cauchemar des pickpockets ! Son secret ? Des fermetures invisibles et accessibles uniquement via un syst&egrave;me unique et astucieux. Con&ccedil;u avec un tissu r&eacute;sistant et waterproof, ce sac sera le compagnon parfait lors de vos sorties. Il est &eacute;quip&eacute; dun port USB externe avec c&acirc;ble de chargement int&eacute;gr&eacute;, vous pouvez facilement recharger votre t&eacute;l&eacute;phone sans avoir &agrave; ouvrir votre sac pour prendre votre batterie externe.
            "; */

            if(!$mail->send()) {
                echo "Email not sent. " , $mail->ErrorInfo , PHP_EOL;}
            else
            {    echo "Email sent! @ " . $m , PHP_EOL;
            }

            unset($mail);

        }

    }


}
