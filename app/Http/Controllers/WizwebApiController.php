<?php namespace App\Http\Controllers;

use App\Models\Base;

class WizwebApiController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth_api');
    }

    /**
     * Show the application dashboard to the user.
     *
     * @return Response
     */
    public function index()
    {
        \DB::disableQueryLog();
        \App\Helpers\Profiler::start('wizweb');

        $md5_list_post = json_decode(file_get_contents('php://input'), true);

        $md5_list= array();
        $bases_query = array();
        $bases = array();

        $base_id = \Input::get('base_id');

        if(empty($base_id)){
            die();
        }

        if(!empty($base_id)){
            $bases_query = \DB::table('base_wizweb')
                ->select('base_id')
                ->where('trigram', $base_id)
                ->get();
        }

        if(count($bases_query) > 0){
            $bases = array_pluck($bases_query, 'base_id');
        } else{
            $bases[] = 0;
        }

        try {
            $pdo = \DB::connection()->getPdo();

        } catch (\PDOException $e) {
            $error_response = array(
                'status' => 'SYSTEM_ERROR' ,
                'message' => $e->getMessage(),
                'error_code' => 45
            );
            echo json_encode($error_response);
            exit;
        }

        foreach($md5_list_post as $md5_account){
            $md5_list[] = $md5_account["md5"];

        }

        $matched_users = array();


        foreach($bases as $bid) {

            if ($bid == 0) {
                $base_id_query = "";
            } else {
                $base_id_query = " base_id = $bid and";
            }

            $stat_qm_str = implode(',', array_fill(0, count($md5_list), '?'));

            $query = "SELECT "
                . "hash AS md5,"
                . "mail AS email,"
                . "CASE statut "
                . " WHEN 0 THEN 'OK' "
                . " ELSE 'OPTOUT' "
                . "END as status, "
                . "CASE civilite "
                . " WHEN 0 THEN 'F' "
                . " WHEN 1 THEN 'M' "
                . " ELSE '' "
                . "END AS gender, "
                . "IFNULL(nom,'') AS lastname, "
                . "IFNULL(prenom,'') AS firstname "
                . "FROM destinataires WHERE$base_id_query hash IN (" . $stat_qm_str . ")";

            //echo "\nQuery -- $query";
            \Log::info("\nQuery -- $query");

            $stmt = $pdo->prepare($query);

            for ($i = 0; $i < count($md5_list); $i++) {
                $stmt->bindValue($i + 1, $md5_list[$i], \PDO::PARAM_STR);
            }
            $stmt->execute();

            $users_resultset = $stmt->fetchAll(\PDO::FETCH_GROUP | \PDO::FETCH_UNIQUE | \PDO::FETCH_ASSOC);
            $matched_users = array_merge($matched_users, $users_resultset);
        }

//        \Log::info(json_encode($users_resultset));

        // ajout des nomatch au resultat de la base de données
        $nomatch_md5 = array_diff($md5_list , array_keys($matched_users));

//        \Log::info(json_encode($nomatch_md5));

        array_walk($nomatch_md5,
            function($md5) use(&$matched_users)
            {
                $matched_users[$md5] = array(
                    'status' => 'NOMATCH'
                );
            });

        $result = array();

        foreach($matched_users as $key => $user){

            $result[] = array('md5' => $key) + $user;

        }

//        echo json_encode($result);
        if(isset($stmt)) {
            $stmt->closeCursor();
            $stmt = NULL;
        }

//        $report = \App\Helpers\Profiler::report('wizweb');
//        \Log::info($report);

        return response()->json($result);
    }
}
