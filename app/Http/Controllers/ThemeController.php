<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Theme;

use Illuminate\Http\Request;

class ThemeController extends Controller {

    public function __construct()
    {
        $this->middleware('auth');
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        $themes = Theme::all();
        return view('theme.index_new')
            ->withMenu('themes')
            ->withThemes($themes);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        return view('theme.create_new')
            ->withMenu('themes');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
        $theme = new Theme();
        $theme->nom = \Input::get('nom');
        $theme->save();

        return \redirect('theme')->withMessage('SAVE OK');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$theme = Theme::find($id);
        return view('theme.edit_new')
            ->withMenu('themes')
            ->withTheme($theme);

	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
        $theme = Theme::find($id);
        $theme->nom = \Input::get('nom');
        $theme->save();

        return \redirect('theme')
            ->withMessage('UPDATE OK');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$theme = Theme::find($id);
        $theme->delete();

        return \redirect('theme');
	}

  public function makeinactif($id){

    $theme = Theme::find($id);
    $theme->etat_actif = '0';
    $theme->save();

    return \redirect('theme');
  }

  public function makeactiver($id){

    $theme = Theme::find($id);
    $theme->etat_actif = '1';
    $theme->save();

    return \redirect('theme');
  }


}
