<?php

namespace App\Console;

use App\Commands\Command;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
        Commands\TestAPIJookey::class,

        Commands\checkSendAtPlanning::class,
        Commands\ListingMail::class,
        Commands\TestCommandJob::class,
        Commands\CommandBis::class,

        Commands\Abuse::class, //not used in code

        Commands\BasesDeactivate::class, //not used in code

        Commands\BaseAddDestinataires::class,

        Commands\BaseImportClics::class,
        Commands\BaseImportDesinscrits::class,
        Commands\BaseInsertDesinscrits::class,
        Commands\BaseImportBounces::class,
        Commands\BaseExport::class,

        // après le soucis de pdf4
        Commands\BaseImportDesinscritsOne::class,

        Commands\BaseImportMindbaz::class,

        Commands\TokensManage::class,
        Commands\TokensReinit::class, // not used in code
        Commands\TokensReset::class,
        Commands\TokensStats::class,

        // Campagnes : Phases (tokens, segment)
        Commands\CampagneAddRepoussoirs::class,
        Commands\CampagneLaunchSend::class,
        Commands\CampagneResetRepoussoirs::class,
        Commands\CampagneSegment::class,
        Commands\CampagneTokens::class,

        //Phase sélection de destinataires (sans prise en compte de pression marketing)
        Commands\CampagneDestinataires::class,

        Commands\CampagneTokensOld::class,
        Commands\CampagnePrepare::class,


        Commands\CampagneSegmentMaildrop::class,
        Commands\CampagneSegmentMaildropOld::class,
        Commands\CampagneSendMaildrop::class,

        // 'App\Console\Commands\CampagneSegmentSmessage',
        // 'App\Console\Commands\CampagneSendSmessage',

        // 'App\Console\Commands\CampagneSegmentPhoenix',
        // 'App\Console\Commands\CampagneSendPhoenix',

        // 'App\Console\Commands\CampagneSegmentMailForYou',
        // 'App\Console\Commands\CampagneSendMailForYou',


        // Stats
        Commands\CampagneClicks::class,
        Commands\CampagneBounces::class,
        Commands\CampagneLightBounces::class,
        Commands\CampagneStats::class,
        Commands\CampagneOptout::class,


        Commands\ComputeClicsMaildrop::class,
        // 'App\Console\Commands\ComputeClicsMailForYou',
        Commands\ComputeClicsMindbaz::class,

        Commands\ComputeOpeningsMindbaz::class,

        Commands\get_optout_and_bounces::class,// gets only bounces for smessage && phoenix

        Commands\CampagneUnsubscribeMaildrop::class,
        // 'App\Console\Commands\CampagneUnsubscribeSmessage',
        // 'App\Console\Commands\CampagneUnsubscribePhoenix',
        // 'App\Console\Commands\CampagneUnsubscribeMailforyou',
        Commands\CampagneUnsubscribeMindbaz::class,

        Commands\CampagneBouncesMaildrop::class,
        // 'App\Console\Commands\CampagneBouncesMailForYou',
        Commands\CampagneBouncesMindbaz::class,

        Commands\FaisStats::class,
        Commands\OuverturesStats::class,
        Commands\OptoutStats::class,

        // Cleaning or reset commands
        Commands\SenderResetQuota::class,
        Commands\ResetStatsTokens::class,
        Commands\ListeCleanMaildrop::class,

        // Geoloc commandes
        // 'App\Console\Commands\geolocmajdestinataire',
        // 'App\Console\Commands\geolocmajvolume',
        Commands\majstatsplanning::class,

        // 'App\Console\Commands\StatsRouteurSmessage',
        Commands\StatsRouteurMaildrop::class,
        // 'App\Console\Commands\StatsRouteurPhoenix',
        // 'App\Console\Commands\StatsRouteurMailforyou',
        Commands\StatsRouteurMindbaz::class,


        //IP commmands
        Commands\etatip::class,
        Commands\CheckEmailBl::class,

        Commands\CheckSenderMd::class,
        Commands\MailStats::class,

        //Wizweb ftp command
        Commands\WizwebApiFtp::class,

        //Traitement unsubscribe
        Commands\UnsubscribeTor::class,

        // pour recup les reponses
        Commands\EmailReply::class,

        //Back up only necessary files commands
        Commands\ServerDump::class,
        Commands\ServerImport::class,
        Commands\ServerCheck::class,

        //Plannings monitoring
        Commands\PlanningCheck::class,


        // TODO : import des cliqueurs version de Fab à comparer avec Ade
        Commands\ImportFichierCliqueurs::class,
        Commands\ParseCampagnesRouteursClic::class,
        Commands\DeleteFileCliqueurs::class,
        Commands\FichierInformationDestiCsv::class,

        Commands\ComputeClics::class, // not used in code
        Commands\ComputePixel::class, // not used in code
        Commands\DestinataireUpdateFai::class, // not used in code
        Commands\dispatch_destinataire_domaine::class, // not used in code  (asso domain <-> desti)

        // DEV
        Commands\TestCommand::class, // not used in code
        // NOSQL : Commands Arango
        // 'App\Console\Commands\DumpOuverturesToArango',
        // 'App\Console\Commands\DumpOuverturesToArangoCron',
        // 'App\Console\Commands\ArangoTokenSegment',
        // 'App\Console\Commands\ArangoMajCollection',
        // 'App\Console\Commands\DumpArangoGeneral',

        // pour alerter si bug m4y encore
        Commands\Alert_Routing::class,

        // test import fichier auto
        // 'App\Console\Commands\MailForYouImportBlacklist',

        Commands\MajSettings::class,
        Commands\CheckBL::class,

        // Arango dev
        // 'App\Console\Commands\ArangoDumpDesti',
        // 'App\Console\Commands\ArangoGenerateTokens',

        Commands\StatsBaseDetails::class,

        // edatis
        // 'App\Console\Commands\CampagneSendEdatis',
        // 'App\Console\Commands\CampagneUnsubscribeEdatis',
        // 'App\Console\Commands\StatsRouteurEdatis',

        Commands\ExtractCsv::class,
        Commands\FichierInfoDate::class,
        // 'App\Console\Commands\CampagneCaImport',
        // 'App\Console\Commands\CampagneCaCalculMois',
        // 'App\Console\Commands\CampagneCaRoi',
        // 'App\Console\Commands\CampagneCaConcat',
        // 'App\Console\Commands\CampagneCaRecupOldCampagne',

        Commands\ExtractEmailOuvreurs::class,
        Commands\ExtractBddJson::class,
        Commands\exportMindbazUnsubscribe::class,
        Commands\ExtractEmailOuvreursCampagneId::class,
        Commands\BaseImportMindbazChoixSender::class,
        Commands\BaseImportMindbazBrute::class,
        Commands\BaseImportHardBounceOne::class,
        Commands\ExtractOuvreursTheme::class,
        Commands\ExtractOuvreursBdd::class,

        Commands\exportMindbazUnsubscribescope::class,
        // 'App\Console\Commands\BouncesCompilation',

        Commands\OuvMDv2::class,
        Commands\DedupConsole::class,

        Commands\SandBox::class,

        Commands\TokensStats::class,
        Commands\TokenWeek::class,
        Commands\TokensManageOne::class,
        Commands\ExtractOuvreursThemeV2::class,
        Commands\ExtractOuvreursBddV2::class,

        Commands\RepoussoirGlobal::class,
        Commands\DailyBlackliste::class,
        Commands\md5clear::class,

        Commands\CampagneSegmentMailKitchen::class,
        Commands\CampagneUnsubscribeMailKitchen::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
