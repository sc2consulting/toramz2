<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class TestAPIJookey extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'testapi:jookey';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //setup config jookey
        $config = new \MailWizzApi_Config(array(
            'apiUrl'        => 'http://my-newsletter.pro/api',
            'publicKey'     => '6ae6529dc93ee9805ea8da40e45990e8fc940232',
            'privateKey'    => '088f944274d4061dbf4e78bcba935ee596ca5f58'
        ));

        \MailWizzApi_Base::setConfig($config);

        date_default_timezone_set('UTC');

        $endpoint = new \MailWizzApi_Endpoint_Lists();

        $response = $endpoint->getLists($pageNumber = 1, $perPage = 10);
// DISPLAY RESPONSE
        print_r($response->body);
        return 0;
    }
}
