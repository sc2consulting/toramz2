<?php


namespace App\Console\Commands;


use App\Jobs\JobTest;
use Illuminate\Console\Command;

class ProcessMultiJob extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ProcessMultiJob {nomFichier} {nbTubeUse}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command qui lance le job avec le fichier lié';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        \Log::info('Confirm Command Launch');
        JobTest::dispatch($this->argument('nomFichier'))->onQueue('tube'.$this->argument('nbTubeUse'));
        \DB::table('senders')
            ->where('nom', $this->argument('nomFichier'))
            ->update([
                'nb_campagnes_left' => \DB::table('senders')->where('nom', $this->argument('nomFichier'))->first()->nb_campagnes_left - 1,
                'quota_left' => \DB::table('senders')->where('nom', $this->argument('nomFichier'))->first()->quota_left - count(file( storage_path() . '/mailsmultitask/'.$this->argument('nomFichier').'.csv'))
            ]);

    }

}
