<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Classes\Routeurs\RouteurMailForYou;
use App\Models\Routeur;

class ComputeClicsMailForYou extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'clics:compute_mailforyou';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Récupère les cliqueurs de MailForYou et lance job : insertion cliqueurs dans la DB > table > clics';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // \\

        $routeur = new RouteurMailForYou();

        $today = date('Ymd');
        $two_weeks_ago = date('Y-m-d 00:00:00', strtotime('-2 weeks'));
        $mailforyou = Routeur::where('nom','MailForYou')->first();
        $senders = \DB::table('senders')->select('id')->where('routeur_id', $mailforyou->id)->get();

        $informations = \DB::table('campagnes_routeurs')
            ->where('created_at','>',$two_weeks_ago)
            ->whereIn('sender_id', array_pluck($senders, 'id'))
            ->get();

        // var_dump($informations);

        $content = "";

        foreach ($informations as $lignecampagnerouteur){

          $emails = array();
          $lacampagne = \DB::table('campagnes')->where('id', $lignecampagnerouteur->campagne_id)->first();
          $data = $routeur->LectureStatsRoutage($lignecampagnerouteur->cid_routeur);

          if(isset($data)){
            foreach ($data->records as $v) {
              if(isset($v['8'])){
                if($v['8'] != 0){
                  // var_dump($v);
                  // var_dump($v['1']);
                    $emails[] = $v['1'];
                }
              }
            }

            $desti = \DB::table('destinataires')
                ->select('id')
                ->whereIn('mail', $emails)
                ->get();
            // je foreach d pour remplir mon fichier
            // $content .= "$lacampagne->id;$c;$lacampagne->theme_id\n";
            // il manque campagne id
            foreach ($desti as $d) {
              $content .= "$lacampagne->id;".$d->id.";$lacampagne->theme_id\n";

            }
          }
//          var_dump($emails);
        }
        var_dump($content);

        $file = "clics_m4y_$today.csv";
        $filename = storage_path() . '/cliqueurs/clics_m4y_' . $today . '.csv';
        $handle = fopen($filename, 'w');
        fwrite($handle, $content);
        fclose($handle);

        // ajouter le call vers la fct qui insert le fichier dans bdd
        \Log::info("[ClicsMailForYou] : launch of base:import_clics command");
        \Artisan::call('base:import_clics', ['file' => $file]);
        \Log::info("[ClicsMailForYou] : launch of base:import_clics command OK");



    }
}
