<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Classes\Routeurs\RouteurMailForYou;
use App\Models\Routeur;

class CampagneUnsubscribeMailforyou extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'campagne:unsubscribe_mailforyou';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Récupère les désabonnés de MailForYou et lance le job de désinscription sur toutes les bases';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

      \Log::info("[CampagneUnsubscribeMailforyou] : Début");

      $routeur = new RouteurMailForYou();
      $today = date('Ymd');
      $two_weeks_ago = date('Y-m-d 00:00:00', strtotime('-2 weeks'));
      $mailforyou = Routeur::where('nom','MailForYou')->first();
      $senders = \DB::table('senders')->select('id')->where('routeur_id', $mailforyou->id)->get();

      $informations = \DB::table('campagnes_routeurs')
          ->where('created_at','>',$two_weeks_ago)
          ->whereIn('sender_id', array_pluck($senders, 'id'))
          ->get();

      // var_dump($informations);
      // $unsubscribers = array();
      echo "empty file \n";
      $file = storage_path() . '/desinscrits/unsubscribe_m4y_' . $today . '.csv';
      $fp = fopen($file,"w+");
      fclose($fp);

      foreach ($informations as $lignecampagnerouteur){
        $chaine = '';

        \Log::info("CampagneUnsubscribeMailforyou : Campagne $lignecampagnerouteur->campagne_id - Planning $lignecampagnerouteur->planning_id");
        if(empty($lignecampagnerouteur->cid_routeur)){
            \Log::info("CampagneUnsubscribeMailforyou : empty cid_routeur (Campagne $lignecampagnerouteur->campagne_id - Planning $lignecampagnerouteur->planning_id)");
            continue;
        }

        // $data = $routeur->LectureStatsRoutage($lignecampagnerouteur->cid_routeur);
        $data = $routeur->LectureStatsRoutage_new($lignecampagnerouteur->cid_routeur,$lignecampagnerouteur->sender_id);
//        var_dump($data);

        if(isset($data)){
          if(isset($data->records)){

            foreach ($data->records as $v) {
              if($v['5'] != 0){
                  $chaine .= $v['1'] . ";;$lignecampagnerouteur->campagne_id;$lignecampagnerouteur->planning_id\n";
              }
            }

              echo "create file \n";
              $file = storage_path() . '/desinscrits/unsubscribe_m4y_' . $today . '.csv';
              $fp = fopen($file,"a+");
              fwrite($fp, $chaine);
              fclose($fp);

          }

        }
      }

        echo "Lancement fichier \n";
        \Log::info("Debut [import_desinscrits] unsubscribe M4Y" . "[unsubscribe_m4y_" . $today . ".csv]");
        \Artisan::call('base:import_desinscrits', ['file' => "unsubscribe_m4y_" . $today . ".csv"]);
        \Log::info("Fin update [import_desinscrits] du fichier unsubscribe m4y" . "[unsubscribe_m4y_" . $today . ".csv]");

        // \Log::info("Debut [insert_desinscrits] M4Y" . "[unsubscribe_m4y_" . $today . ".csv]");
        // \Artisan::call('base:insert_desinscrits', ['file' => "unsubscribe_m4y_" . $today . ".csv"]);
        // \Log::info("Fin [insert_desinscrits] du fichier unsubscribe M4Y" . "[unsubscribe_m4y_" . $today . ".csv]");

    }
}
