<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Helpers\Profiler;

class ServerCheck extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'server:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check the state of the server & report to users if not okay';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
		\Log::info("ServerCheck : started");
		Profiler::start("ServerCheck");
        
        //TODO : left ram/cpu info 
        
        $mail = false;
        $disk = array();
        //$ram = "";
		$mysql = array();
		$nginx = array();
		
		$disk_msg = 'Aucune information';
		//$ram_msg = 'Aucune information';
	  	$mysql_msg = 'Aucune information';
	  	$nginx_msg = 'Aucune information';
        
        //Check Disk
      	exec("df -h|grep /$|grep -o [0-9]*%|grep -o [0-9]*", $disk);
      
	  	if(!isset($disk[0]) or (isset($disk[0]) && $disk[0] > 70) ){
		  	$mail = true;
		  	if(isset($disk[0])){
		  	$disk_msg = $disk[0]."%";
		  	}
	  	}
	  	      	      
        //Check RAM
		//$ram = exec("free -h");
		      	        
        //Check MySQL
        exec("service mysql status | grep running", $mysql);
        
        if(!isset($mysql[0]) or (isset($mysql[0]) && stripos($mysql[0], 'running') === FALSE )){
		  	$mail = true;
		  	if(isset($mysql[0])){
			  	$mysql_msg = $mysql[0];
		  	}
	  	}
        
      	//Check Nginx
      	exec("service nginx status|grep running", $nginx);
      	
      	if(!isset($nginx[0]) or (isset($nginx[0]) && stripos($nginx[0], 'running') === FALSE )){
		  	$mail = true;
		  	if(isset($nginx[0])){
			  	$nginx_msg = $nginx[0];
		  	}
	  	}
	  	
	  	print_r($mail);
	  	print_r($disk);
	  	print_r($mysql);
	  	print_r($nginx);
	  	
	  	
        $report = Profiler::report("ServerCheck");	  	
	  	
	  	if($mail){
	  	\Mail::send('mail.mailserverprob',
	          [
	          'profiler' => $report,
	          'disk' => $disk_msg,
	          'mysql' => $mysql_msg,
	          'nginx' => $nginx_msg,
	          'url' => getenv('CLIENT_URL')
	          ], function ($message) {
	            $destinataires = \App\Models\User::where('is_valid', 1)
	                ->where('user_group_id', 1)
	                ->where('email', '!=', "")
	                ->get();
	                
	            $message->from('rapport@lead-factory.net', 'Tor')->subject("Alerte serveur : ".getenv('CLIENT_URL'));
	            foreach($destinataires as $d){
	                $message->to($d->email);
	            }
	          
	        });
        }

       
        \Log::info("ServerCheck : ended \n$report");

    }

}