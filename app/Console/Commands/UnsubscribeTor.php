<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class UnsubscribeTor extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'unsubscribe:get';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $datedujour = date('Ymd');
        $hier = date("Ymd",strtotime("-1 day")); // for Mindbaz, because their files are generated at 5:00 AM

        if(empty(getenv('SERVERS_TEAM')))
        {
            \Log::info(".env n'a pas la variable SERVERS_TEAM");
            exit;
        }

        $tor_ami = getenv('SERVERS_TEAM');
        $servers = explode(',',$tor_ami);

        foreach ($servers as $s)
        {
            // Partie Phoenix
            $url = $s . 'mailexport/unsubscribe/phoenix/unsubscribe_ph_' . $datedujour . '.csv';
            $url_name = parse_url($s, PHP_URL_HOST);
            $test_url = get_headers($url);
            if($test_url[0] !== "HTTP/1.0 500 Internal Server Error"){
                $content_url = file_get_contents($url);
                $handle = fopen(storage_path()."/desinscrits/". $url_name ."_unsubscribe_ph_" . $datedujour . ".csv", 'w');
                fwrite($handle, $content_url);
                \Artisan::call('base:import_desinscrits', ['file' => $url_name . "_unsubscribe_ph_" . $datedujour . ".csv"]);
                \Log::info("Fichier unsubscribe PH importé avec succès");
            } else {
                \Log::info("Fichier unsubscribe PH du jour introuvable");
            }
            // Partie MailDrop
            $url = $s . 'mailexport/unsubscribe/maildrop/unsubscribe_md_' . $datedujour . '.csv';
            $url_name = parse_url($s, PHP_URL_HOST);
            $test_url = get_headers($url);
            if($test_url[0] !== "HTTP/1.0 500 Internal Server Error"){
                $content_url = file_get_contents($url);
                $handle = fopen(storage_path()."/desinscrits/". $url_name ."_unsubscribe_md_" . $datedujour . ".csv", 'w');
                fwrite($handle, $content_url);
                \Artisan::call('base:import_desinscrits', ['file' => $url_name . "_unsubscribe_md_" . $datedujour . ".csv"]);
                \Log::info("Fichier unsubscribe MD importé avec succès");
            } else {
                \Log::info("Fichier unsubscribe MD du jour introuvable");
            }
            // Partie SMessage
            $url = $s . 'mailexport/unsubscribe/smessage/unsubscribe_sm_' . $datedujour . '.csv';
            $url_name = parse_url($s, PHP_URL_HOST);
            $test_url = get_headers($url);
            if($test_url[0] !== "HTTP/1.0 500 Internal Server Error"){
                $content_url = file_get_contents($url);
                $handle = fopen(storage_path()."/desinscrits/". $url_name ."_unsubscribe_sm_" . $datedujour . ".csv", 'w');
                fwrite($handle, $content_url);
                \Artisan::call('base:import_desinscrits', ['file' => $url_name . "_unsubscribe_sm_" . $datedujour . ".csv"]);
                \Log::info("Fichier unsubscribe SM importé avec succès");
            } else {
                \Log::info("Fichier unsubscribe SM du jour introuvable");
            }
            // Partie Mailforyou
            $url = $s . 'mailexport/unsubscribe/smessage/unsubscribe_m4y_' . $datedujour . '.csv';
            $url_name = parse_url($s, PHP_URL_HOST);
            $test_url = get_headers($url);
            if($test_url[0] !== "HTTP/1.0 500 Internal Server Error"){
                $content_url = file_get_contents($url);
                $handle = fopen(storage_path()."/desinscrits/". $url_name ."_unsubscribe_m4y_" . $datedujour . ".csv", 'w');
                fwrite($handle, $content_url);
                \Artisan::call('base:import_desinscrits', ['file' => $url_name . "_unsubscribe_m4y_" . $datedujour . ".csv"]);
                \Log::info("Fichier unsubscribe M4Y importé avec succès");
            } else {
                \Log::info("Fichier unsubscribe M4Y du jour introuvable");
            }
            // Partie Mindbaz
            $url = $s . 'mailexport/unsubscribe/smessage/unsubscribe_mb_' . $datedujour . '.csv';
            $url_name = parse_url($s, PHP_URL_HOST);
            $test_url = get_headers($url);
            if($test_url[0] !== "HTTP/1.0 500 Internal Server Error"){
                $content_url = file_get_contents($url);
                $handle = fopen(storage_path()."/desinscrits/". $url_name ."_unsubscribe_mb_" . $hier . ".csv", 'w');
                fwrite($handle, $content_url);
                \Artisan::call('base:import_desinscrits', ['file' => $url_name . "_unsubscribe_mb_" . $hier . ".csv"]);
                \Log::info("Fichier unsubscribe MB importé avec succès");
            } else {
                \Log::info("Fichier unsubscribe MB du jour introuvable");
            }
        }

        \Log::info("Fin du script de récupération des désinscrits des autres TOR");

        // verifier que les fichiers existent
        // modifier base import desabo pour savoir le nombre de update

    }
}
