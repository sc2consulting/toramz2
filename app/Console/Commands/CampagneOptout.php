<?php namespace App\Console\Commands;

use App\Classes\Routeurs\RouteurMaildrop;
use App\Classes\Routeurs\RouteurPhoenix;
use App\Classes\Routeurs\RouteurSmessage;
use App\Models\Planning;
use App\Models\Sender;
use App\Models\CampagneRouteur;
use App\Models\Campagne;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputArgument;

class CampagneOptout extends Command {

    /**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'campagne:optout';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Get optouts for one campaign';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

    public function handle()
    {

        // N'est pas call, penser à suppr peut être

        $planning = Planning::find($this->argument('planning_id'));
        $campagne = Campagne::find($planning->campagne_id);

        $camp_rout = CampagneRouteur::where('planning_id', $planning->id)->get();

        foreach($camp_rout as $row)
        {
            $sender = Sender::find($row->sender_id);

            \Log::info('Campagne:desinscrits - campagne '.$campagne->id.', sender '.$sender->id . ', routeur ' . $sender->routeur->nom);

            if ($sender->routeur->nom == 'Phoenix' or $sender->routeur->nom == 'Maildrop' ) {
               continue; //D'autres scripts s'occupent des désinscriptions PH ET MD
                //TODO : REFAIRE PAREIL POUR SMESSAGE
            } else {

              if(empty($row->cid_routeur)) {
                  continue;
              }

            }

            //Si MailDrop
            if ($sender->routeur->nom == 'Maildrop') {
                $routeurmaildrop = new RouteurMaildrop();
                $routeurmaildrop->addDesinscrits($sender->password, $campagne, $row->cid_routeur);
            }
            //Si SMessage
            if ($sender->routeur->nom == 'Smessage') {
                $routeursmessage = new RouteurSmessage();
                $routeursmessage->addDesinscrits($sender, $campagne, $row->cid_routeur);
            }
            //Si Phoenix
            if ($sender->routeur->nom == 'Phoenix') {
                $routeurphoenix = new RouteurPhoenix();
                \Log::info('ID PLANNING : ' . $row->id);
                echo 'ID PLANNING : ' . $row->id . "\n";
                $routeurphoenix->addDesinscrits($sender, $campagne, $row->listid);


            }
        }
    }

    protected function getArguments()
    {
        return [
            ['planning_id', InputArgument::REQUIRED, 'Planning id.'],
        ];
    }
}
