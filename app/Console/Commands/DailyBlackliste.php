<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class DailyBlackliste extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'daily:blackliste';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Blackliste totale en daily';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

      $bases = \DB::table('bases')
      ->where('is_active',1)
      ->get();

      foreach ($bases as $b) {
        /*
        \DB::table('blacklistes')
          ->select('email')
          ->chunk(20000, function($emailsbl){
            /* foreach($emailsbl as $eb){
              $this->destinataires[$bd->mail]="";
            }

            \DB::table('destinataires')
            ->where('base',$id)
            ->where('id_periode', $fields['id_periode'])
            ->update(['is_devalided'=>1,'devalid_pourcent' => $fields['taux_devalidation']]);

          });
          */

          $nbrtotaldesti = \DB::table('blacklistes')
          ->whereNotNull('email')
          ->distinct()
          ->count();

          $offset = 1000;
          $long = strlen($nbrtotaldesti) - substr_count($offset,'0');
          $nbretape = substr($nbrtotaldesti,0,$long);
          $skip = 0;

          for ($i = 1; $i <= $nbretape; $i++){

            echo 'Etape num : ' . $i . "\n";
            $skipslice = 0;
            $offsetslice = 0;
            $ble = \DB::table('blacklistes')
            ->select('email')
            ->skip($skip)
            ->take($offset)
            ->get();

            $arrayemail = array_pluck($ble, 'email');

            // var_dump($arrayemail);
            \DB::table('destinataires')
            ->where('base_id',$b->id)
            ->whereIn('mail', $arrayemail)
            ->update(['statut'=>270,'optout_at' => date('Y-m-d H:i:s')]);

            $skip = $skip + $offset;

          }


      }










    }
}
