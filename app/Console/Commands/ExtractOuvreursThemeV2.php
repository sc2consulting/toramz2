<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class ExtractOuvreursThemeV2 extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'extract:ouvreurs_actif {nom_fichier} {theme_id} {time_limit}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {


      \DB::table('ouv_api')
          ->select('mail','campagne_id')
          ->where('created_at','>', $this->argument('time_limit').'00:00:00')
          ->where('theme_id','=',$this->argument('theme_id'))
          ->chunk(10000, function ($lesadresses) {
              var_dump('Bulk 10000');
              $array_a = array_pluck($lesadresses,'mail');
              /*
              $lc = \DB::table('campagnes')
              ->where('');

              $array_a = array_pluck($lesadresses,'destinataire_id');

              $result_actif = \DB::table('destinataires')
              ->select('mail')
              ->whereIn('mail', $array_a)
              ->where('statut','=',0)
              ->get();
              $chaine = '';

              */

              $chaine = '';
              foreach ($array_a as $key => $v) {
                $chaine .= $v."; \n";
              }

              $file = storage_path() . '/listes/download/'.$this->argument('nom_fichier').'.csv';
              $fp = fopen($file,"a+");
              fwrite($fp, $chaine);
              fclose($fp);
              unset($chaine);


          });




          die();






      /// OLD


      $campagne_ouverture = \DB::table('campagnes')
      ->where('created_at','>', $this->argument('time_limit').'00:00:00')
      ->where('theme_id', $this->argument('theme_id'))
      ->get();

      foreach ($campagne_ouverture as $c) {

        $nb_ouvreurs = \DB::table('ouvertures')
        ->where('campagne_id', $c->id)
        ->count();
        echo ('Nombre ouvertures pour la campagne ' . $c->nom . ' : ' . $nb_ouvreurs ."\n");
        $offset = 1000;
        $skip = 0;

        $long = strlen($nb_ouvreurs) - substr_count($offset,'0');
        $nbretape = substr($nb_ouvreurs,0,$long);

        for ($i = 1; $i <= $nbretape; $i++) {

          echo 'Result ' . $i . ' / ' . $nbretape . "\n";

          $chaine = '';
          $destinatairesid = \DB::table('ouvertures')
          ->select('destinataire_id')
          ->where('campagne_id', $c->id)
          ->distinct()
          ->skip($skip)
          ->take($offset)
          ->get();

          $did = array_pluck($destinatairesid, 'destinataire_id');

          $destinatairesemails = \DB::table('destinataires')
          ->select('mail')
          ->where('statut','<','3')
          ->whereIn('id', $did)
          ->get();

          foreach ($destinatairesemails as $key => $v) {
            $chaine .= $v->mail."; \n";
          }

          $file = storage_path() . '/listes/download/'.$this->argument('nom_fichier').'.csv';
          $fp = fopen($file,"a+");
          fwrite($fp, $chaine);
          fclose($fp);

          $skip = $skip + $offset;
          unset($chaine);

        }



      }
    }
}
