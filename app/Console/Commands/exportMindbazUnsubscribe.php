<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Classes\Routeurs\RouteurMindbaz;
use App\Models\Routeur;
use App\Models\Sender;


class exportMindbazUnsubscribe extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'export:mb_unsubscribe {base_id} {sender_id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Export mindbaz via emails tor';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

      // ini_set('soap.wsdl_cache_enabled', '0');
      // ini_set('soap.wsdl_cache_ttl', '0');
      // exec('ulimit -S -n 2048');

       var_dump($this->argument('base_id'));

       $mindbaz = new RouteurMindbaz();
       $routeur = Routeur::where('nom','Mindbaz')->first();
       $senders_mindbaz = Sender::where('id', $this->argument('sender_id'))->first();
       $today = date("Ymd");
       $sleep = 0;
       // attention v1 petites bases
       echo "Lancement du script \n";

       $chaineUnsu = "";
       $chaineNpai = "";

       $destinataires = \DB::table('destinataires')
       ->select('mail')
       ->where('base_id',$this->argument('base_id'))
       ->where('statut', '<', 3)
       // ->get();
       ->count();

       echo "NB desti analyse : " . $destinataires . "\n";

       $offset = 10000;
       $skip = 0;
       $long = strlen($destinataires) - substr_count($offset,'0');
       $nbretape = substr($destinataires,0,$long);

       for ($i = 1; $i <= $nbretape; $i++) {

         echo "Partie : " . $i . "\n";
         $destinataires = \DB::table('destinataires')
         ->select('mail')
         ->where('base_id',$this->argument('base_id'))
         ->where('statut', '<', 3)
         ->skip($skip)
         ->take($offset)
         ->get();
         echo "Recup desti OK \n";

         $chaineUnsu = '';
         $chaineNpai = '';

         $destinataires = array_pluck($destinataires, 'mail');
         $counter = 0;

           $r = $mindbaz->get_subscriberid_all_npai($senders_mindbaz,$destinataires);
           foreach ($r as $arrayresult) {

             foreach ($arrayresult as $ar) {
               $counter = $counter + 1;
              // echo "C_" . $counter . "\n";
               // var_dump($counter);
               // var_dump($ar->fld[1]->value);
               // var_dump($ar->fld[4]->value);
               if($ar->fld[4]->value != NULL){
                 $chaineUnsu .= $ar->fld[1]->value . "\n";
               }
               if($ar->fld[7]->value == 7){
                 $chaineNpai .= $ar->fld[1]->value . "\n";
               }

             }
            }

            $file = storage_path() . '/desinscrits/unsubscribe_b'.$this->argument('base_id').'_s'.$this->argument('sender_id').'_mb_api_' . $today . '.csv';
            $fp = fopen($file,"a+");
            fwrite($fp, $chaineUnsu);
            fclose($fp);
            unset($chaineUnsu);

            $file = storage_path() . '/desinscrits/hardbounce_b'.$this->argument('base_id').'_s'.$this->argument('sender_id').'_mb_api_' . $today . '.csv';
            $fp = fopen($file,"a+");
            fwrite($fp, $chaineNpai);
            fclose($fp);
            unset($chaineNpai);

          $skip = $skip + $offset;

          }


       \Log::info("[CampagneUnsubscribeMindbaz] : launch of BaseImportDesinscrits command");
       \Artisan::call('base:import_desinscritsone', ['file' => "unsubscribe_b".$this->argument('base_id')."_s".$this->argument('sender_id')."_mb_api_$today.csv",'base_id' => $this->argument('base_id')]);
       \Log::info("[CampagneUnsubscribeMindbaz] : launch of BaseImportDesinscrits command OK");


       \Log::info("[CampagneUnsubscribeMindbaz] : launch of BaseImportBounces command");
       \Artisan::call('base:import_hbounceone', ['file' => "hardbounce_b".$this->argument('base_id')."_s".$this->argument('sender_id')."_mb_api_$today.csv",'base_id' => $this->argument('base_id')]);
       \Log::info("[CampagneUnsubscribeMindbaz] : launch of BaseImportBounces command OK");

     }



    protected function getArguments()
    {
        return [
            ['base_id', InputArgument::REQUIRED, 'Base'],
            ['sender_id', InputArgument::REQUIRED, 'Sender'],
            ['url', InputArgument::REQUIRED, 'Link'],
        ];
    }
}
