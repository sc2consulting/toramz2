<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Classes\Routeurs\RouteurMindbaz;
use App\Models\Routeur;
use App\Models\Sender;

class CampagneUnsubscribeMindbaz extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'campagne:unsubscribe_mindbaz';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Récupère les désabonnés de Mindbaz et lance le job de désinscription sur toutes les bases.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        ini_set('default_socket_timeout', 1800 ); //pour ne pas avoir de timeout avec les sockets SOAP
        \Log::info("[CampagneUnsubscribeMindbaz] : Début");
        $today = date("Ymd");

        $mindbaz = new RouteurMindbaz();

        $filepath = storage_path('desinscrits').'/unsubscribe_all_routeurs.csv';

        //We unsubscribe as well from Tor -> Mindbaz
        exec('cat '.storage_path('desinscrits').'/unsubscribe_*_'.$today.'.csv > '.$filepath);

        $handle = fopen($filepath, 'r');

        $subscribers_emails = array();

        $total_unsubscribers = 0;

        while($csv=fgetcsv($handle, 1024, ';'))
        {
            if(!filter_var($csv[0], FILTER_VALIDATE_EMAIL)) {
                continue;
            }
                $subscribers_emails[]=$csv[0];
        }

        $bulk_emails_5000 = array();

        if( count($subscribers_emails) > 0) {
            $bulk_emails_5000 = array_chunk($subscribers_emails, 4000);
        }

        $routeur = Routeur::where('nom','Mindbaz')->first();
        $senders_mindbaz = Sender::where('routeur_id', $routeur->id)->get();

        foreach($senders_mindbaz as $sm)
        {
            foreach($bulk_emails_5000 as $bke) {
                $subscribers_ids = array();
                $resultat = $mindbaz->get_subscribersids($sm, $bke);
                foreach ($resultat as $subscriber) {
//                echo "\n".$subscriber->idSubscriber;
                    $subscribers_ids[] = $subscriber->idSubscriber;
                    $total_unsubscribers++;
                }

                if (count($subscribers_ids) > 0) {
//                    $bulk_5000 = array_chunk($subscribers_ids, 4000);
                    // TODO: test & uncomment when it's ok
                    $mindbaz->set_unsubscribers($sm, $subscribers_ids);
                }
            }
        }

        $get = $mindbaz->get_unsubscribers();

        if($get) {
            \Log::info("[CampagneUnsubscribeMindbaz] : launch of BaseImportDesinscrits command");
            \Artisan::call('base:import_desinscrits', ['file' => "unsubscribe_mb_$today.csv"]);
            \Log::info("[CampagneUnsubscribeMindbaz] : launch of BaseImportDesinscrits command OK");
        }


//        exit;
        exec('rm '.$filepath);
        \Log::info("[CampagneUnsubscribeMindbaz] : finished -- count($total_unsubscribers)".json_encode($get));
    }
}
