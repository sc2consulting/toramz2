<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Classes\Routeurs\RouteurMailForYou;
use App\Jobs\BaseImportBlacklistes;
use App\Models\Routeur;

class MailForYouImportBlacklist extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'base:red_list_m4y';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Importe la liste rouge globale de mailforyou';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $ts = date('Y-m-d H:i:s');
        $date = date('Y-m-d');
        $tblemail = array();
        $cblemail = '';
        $today = date('Ymd');
        $total = 0;
        $inserted = 0;
        $rejected = 0;
        $bulk_count = 0;

        set_time_limit(0);
        ini_set('max_execution_time', 0);

        $routeurm4y = Routeur::where('nom','MailForYou')->first();
        $recup = \DB::table('senders')->select('api_login','api_key')->where('routeur_id',$routeurm4y->id)->distinct()->get();

        foreach($recup as $r){

          $routeur = new RouteurMailForYou();

          echo 'Serveur : ' . $r->api_login . "\n";
          $data = $routeur->LectureListeRougeDestinataire_new(0,$r->api_login,$r->api_key);
          echo 'Remplissage tab ' . "\n";
           foreach ($data->records as $k => $v) {
             $cblemail .= $v[0] . "\n";
           }

           echo 'Create text bl ' . "\n";

           $file = storage_path() . '/blacklistes/m4y_red_list_' . $today . '.csv';
           $fp = fopen($file,"w+");
           fclose($fp);
           $fp = fopen($file,"a+");
           fwrite($fp, $cblemail);
           fclose($fp);

           echo 'Call BL ' . "\n";

           \Queue::push(new BaseImportBlacklistes('m4y_red_list_' . $today . '.csv'));

        }


    }
}
