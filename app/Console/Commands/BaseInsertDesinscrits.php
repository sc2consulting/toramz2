<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Notification;
use Symfony\Component\Console\Input\InputArgument;

class BaseInsertDesinscrits extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'base:insert_desinscrits';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Ajoute des désinscrits depuis un fichier situé dans le dossier desinscrits.';

    /**
     * Create a new command instance.ha voilà
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->bases = array();
    }

    /**
     * Execute the command.
     *
     * @return void
     */
    public function handle()
    {
        $file = storage_path().'/desinscrits/'.$this->argument('file');
        $filename = $this->argument('file');
        if (!is_file($file)) {
            $this->error('File not found : '.$file);
        }

        \Log::info("[BaseInsertDesinscrits] : Import desinscrits à partir du fichier -- $file");

        $ts = date('Y-m-d H:i:s');

        \App\Helpers\Profiler::start('base:import_desinscrits');

        $bulk_count = 0;

        set_time_limit(0);
        ini_set('max_execution_time', 0);

        $extension = \File::extension($file);
        $name = \File::name($file);

        $updateData = array();
        $insertData = array();

        $fh = fopen($file, 'r');

        $total = 0;
        $inserted = 0;

        $pdo = \DB::connection()->getPdo();
        $sql = "INSERT IGNORE INTO desinscrits (campagne_id, date_out, mail, hash) VALUES (:campagne_id, :date_out, :mail, :hash)";

        $stmt = $pdo->prepare($sql);
        $pdo->beginTransaction();

        $countInsert = 0;

        while (!feof($fh)) {

            $cells = [];
            $row = fgets($fh, 1024);
            $row = trim($row, "\r\n\t ");

            if (strpos($row, ';') !== false) {
                $cells = explode(';', $row);
            } elseif (strpos($row, '|') !== false) {
                $cells = explode('|', $row);
            } else {
                $cells[] = trim($row, " \r\n\t\"");
            }

            foreach($cells as $cell) {

                if (filter_var($cell, FILTER_VALIDATE_EMAIL)) {
                    $total++;

                    if($cells > 2) {

                        $date_out = $cells[1];
                        if(\DateTime::createFromFormat('Y-m-d', $date_out) === FALSE){
                            $date_out = date('Y-m-d');
                        }

                        $insertData = [$cells[2], $date_out, $cell, md5($cell)];

                        $stmt->execute($insertData);
                        $countInsert++;
                    }

                    if ($countInsert >= 5000) {
                        $pdo->commit();
                        $pdo->beginTransaction();

                        $countInsert = 0;
                    }

                    $inserted++;
                    continue;
                }
            }
        }

        if ($countInsert >= 0) {
            $pdo->commit();
            $pdo->beginTransaction();

            $countInsert = 0;
        }

        if (isset(\Auth::User()->id)) {
            $user_id = \Auth::User()->id;
        } else {
            $user_id = 1;
        }

        Notification::create([
            'user_id' => $user_id,
            'level' => 'info',
            'is_important' => 1,
            'message' => "Fichier desinscrits $name importé."
        ]);

        \Log::info("[BaseInsertDesinscrits] : End of Import desinscrits a partir fichier -- $file");
        \App\Helpers\Profiler::report("base:import_desinscrits");
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['file', InputArgument::REQUIRED, 'Path to the file']
        ];
    }
}
