<?php


namespace App\Console\Commands;


use Illuminate\Console\Command;
use Symfony\Component\Process\Process as Process;

class ProcessCommandToJob extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ProcessCommandToJob {nomFichierSansNombre} {id_planning} {NombreDeFichier}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command qui lance les process de commande qui lance le job pour les avoir en simultanés';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $process = null;
        if(!\DB::table('planning_percent_failure')->where('id_planning', $this->argument('id_planning'))->first())
        {
            \DB::table('planning_percent_failure')->insert(
                ['id_planning' => $this->argument('id_planning'),]
            );
        }
        for($i = 0; $i < $this->argument('NombreDeFichier'); $i++)
        {
            $this->info('Process : '.$i.' pour fichier : '.($i+1));
            $tube = $i+1;
            if($tube > 8)
            {
                $tube = $i%8;
            }
            $process = new Process('php artisan ProcessMultiJob AmazonTest1 '.($i+1).' '.$tube);
            $process->start();
        }

        $process->wait();
        return true;
    }
}
