<?php

namespace App\Console\Commands;

use App\Classes\Routeurs\RouteurMindbaz;
use App\Helpers\Profiler;
use Illuminate\Console\Command;
use App\Models\Routeur;

class CampagneBouncesMindbaz extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'campagne:bounces_mindbaz';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Ajoute les bounces Mindbaz dans la bdd toutes bases confondues';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        \Log::info("[CampagneBouncesMindbaz] : launch of CampagneBouncesMindbaz command");
        Profiler::start('bounces_mindbaz');
        $mindbaz = new RouteurMindbaz();

        $today = date('Ymd');

        $compteur = $mindbaz->get_bounces();

        $report = Profiler::report('bounces_mindbaz');
        \Log::info("CampagneBouncesMaildrop : end report $report");

        echo "Lancement import fichier \n";

        \Log::info("[import_bounces] du fichier bounces " . "[bounces_mb_" . $today . ".csv] (Count : $compteur)");
        \Log::info("[CampagneBouncesMindbaz] : launch of BaseImportBounces command");
        \Artisan::call('base:import_bounces', ['file' => "bounces_md_" . $today . ".csv"]);
        \Log::info("[CampagneBouncesMindbaz] : end of BaseImportBounces command");
    }
}
