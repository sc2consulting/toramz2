<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class BouncesCompilation extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'compil:bounces';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fait un gros fichier de hb';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // 20161019
        $content = '';
        for ($i=1000; $i > 0; $i--) {
          $datefichier = date('Ymd',strtotime('-'.$i . ' days'));
          var_dump($datefichier);

          $file = storage_path() . '/bounces/bounces_md_' . $datefichier . '.csv';
          if(file_exists($file)){

            echo 'Recup du content ' . $file . "\n";
            $handle = fopen($file, 'r');
            // file_get_contents();
            // var_dump($handle);
            // fwrite($handle, )

            while (!feof($handle)) {

              // $row = fgets($handle, 1024);
              // $row = trim($row, "\r\n\t ");

              // $content .= $row . "\n";
              // var_dump($row);

              $cells = [];
              $row = fgets($handle, 1024);
              $row = trim($row, "\r\n\t ");

              if (strpos($row, ';') !== false) {
                  $cells = explode(';', $row);
              } elseif (strpos($row, '|') !== false) {
                  $cells = explode('|', $row);
              } else {
                  $cells[] = trim($row, " \r\n\t\"");
              }

              foreach($cells as $cell) {

                  if (filter_var($cell, FILTER_VALIDATE_EMAIL)) {
                      $content .= $cell . "\n";
                      continue;
                  }
              }

            }

          }
        }

        $file_export = storage_path() . '/listes/download/allm_bounces' . $datefichier . '.csv';
        // var_dump($content);
        $fp = fopen($file_export,"a+");
        fwrite($fp, $content);
        fclose($fp);

    }
}
