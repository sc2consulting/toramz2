<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputArgument;

// ajout du routeur
use App\Classes\Routeurs\RouteurPhoenix;
use App\Models\Planning;
use App\Models\Campagne;
use App\Models\Routeur;
use App\Models\Sender;
use App\Models\CampagneRouteur;
use App\Models\Fai;

class CampagneSegmentPhoenix extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'campagne:segment_phoenix {planning_id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Traitement Phoenix';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $auto_setting = \DB::table('settings')
            ->where('parameter', 'is_auto')
            ->first();
        if(!$auto_setting or $auto_setting->value != 1){
            \Log::info('CampagneLaunchSend : AUTOMATIC CAMPAIGN NOT ACTIVE');
            return 0;
        }
        $routeur_setting = \DB::table('settings')
            ->where('parameter', 'is_phoenix')
            ->first();

        if(!$routeur_setting or $routeur_setting->value != 1){
            \Log::info('CampagneLaunchSend : PHOENIX DISABLE');
            return 0;
        }

        $bloc = \DB::table('senders_history')->max('bloc');
        if(empty($bloc)){
            $bloc = 0;
        }
        $this->max_bloc = $bloc + 1;

        $planning = Planning::find($this->argument('planning_id'));
        $planning->nb_trials++;
        $planning->save();

        $campagneid = $planning->campagne_id;

        echo "\nCampagne Segment ";

        $routeur = new RouteurPhoenix();

        $lacampagne = Campagne::where('id',$campagneid)->first();
        $phoenix = Routeur::where('nom','Phoenix')->first();

        // var_dump($phoenix);

        \Log::info("[CampagneSegmentPhoenix][P$planning->id] : Début Segmentation (Planning $planning->id/Campagne $lacampagne->id) {Trial n°$planning->nb_trials}");

        $fais = Fai::all();

        $mailsrestants = array(); //TODO : a gerer plus tard dans le cas des restes de mails de la decoupe lineaire a dispatcher vers les senders

        $selectedsender = array();

        $user_mono_sender = \DB::table('planning_senders')
            ->select('sender_id')
            ->where('planning_id', $planning->id)
            ->get();

        $anonyme = "";
        // if ici et on creé la var
        if($planning->type == 'anonyme'){
            $anonyme = "_a";
            $ano = true;
        }

        //supprimer les fichiers de destinataires pour eviter d'avoir des fichiers lourds dans le cas où le script se répète
        exec("rm ".storage_path()."/phoenix/*p$planning->id$anonyme*");
        // verifier si le fichier existe

        // var_dump($fais);
        // die();

        foreach($fais as $f)
        {
            $lesadressestokens = array();
            $lesadressestokens = \DB::table('tokens')
                ->select('destinataire_id')
                ->where('fai_id',$f->id)
                ->where('campagne_id',$campagneid)
                ->where('planning_id',$planning->id)
                ->where('date_active',$planning->date_campagne)
                ->get();
            $volumeFaiTotal = count($lesadressestokens);

            //dans le foreach car pour avoir tjs a jour les senders ayant tjs un quota_left non nul
            $phoenixsenders_query = Sender::where('quota_left','>',0)
                ->where('routeur_id',$phoenix->id);

            if(!empty($user_mono_sender)) {
                $phoenixsenders_query->whereIn('id', array_pluck($user_mono_sender, 'sender_id'));
            }

            $phoenixsenders = $phoenixsenders_query->get();

            // var_dump($phoenixsenders);
            // die();

            $fai_senders = \DB::table('fai_sender')
                ->where('fai_id',$f->id)
                ->whereIn('sender_id',$phoenixsenders->pluck('id')->all())
                ->orderByRaw('rand()')
                ->get();

            // var_dump($fai_senders);
            // die();

            // voir si c'est ca
            if(!$fai_senders){continue;}


            if(!empty($user_mono_sender)){
                $minQuotas = array($volumeFaiTotal);
                $minQuotas = $minQuotas + array_pluck($fai_senders, 'quota_left');
                $count = min($minQuotas);
                echo "\nFAI -- $f->id";
                var_dump($count);
                echo "\n";
            } else {
                $calcul = count($lesadressestokens) / count($fai_senders);
                // dd(count($lesadressestokens));
                // echo(count($calcul));
                //die();

                $count = round($calcul) + 1;
                // on prends le minimum entre le quota/sender

                // var_dump($count);

                if ($f->quota_sender > 0) {
                    $count = min($f->quota_sender, $count);
                }
            }

            $chunks=array();
            // un seul compte pour le moment
            $chunks = array_chunk($lesadressestokens,$count);

            $indSender = 0;
            \Log::info("CampagneSegmentPhoenix [$campagneid] : FAI- $f->nom -@- ".count($lesadressestokens)." -Sender- ".count($fai_senders)." -DIV- ".$count." -COUNTCHUNK- ".count($chunks));

            foreach($fai_senders as $fai_sender)
            {
                $sender = Sender::find($fai_sender->sender_id);
                if(!isset($chunks[$indSender])) {
                    break;
                }
                //si le sender n'a pas assez de places / jour pour envoyer on prends un autre sender
                //ou sinon on peut
                if($sender->quota_left < count($chunks[$indSender])) {
//                    array_merge($mailsrestants,$chunks[$indSender]);
                    continue;
                }

                $file = storage_path() . '/phoenix/s' . $sender->nom . '_c' . $lacampagne->id .'_p'.$planning->id. $anonyme.'.csv';

                $this->write_tokens($file, $chunks[$indSender]);

                // l'historique
                \DB::table('senders_history')
                    ->insert(
                        [
                            'quota_before' => null,
                            'sender_id' => $sender->id,
                            'fai_id' => $fai_sender->fai_id,
                            'planning_id' => $planning->id,
                            'quota_left_before' => $sender->quota_left,
                            'quota_left' => $sender->quota_left - count($chunks[$indSender]),
                            'created_at' => date("Y-m-d H:i:s"),
                            'updated_at' => date("Y-m-d H:i:s"),
                            'bloc' => $this->max_bloc
                        ]
                    );

                $sender->quota_left -= count($chunks[$indSender]);

                \DB::table('fai_sender')
                    ->where('fai_id',$fai_sender->fai_id)
                    ->where('sender_id',$sender->id)
                    ->update(['quota_left' => $fai_sender->quota_left - count($chunks[$indSender])]);

                $sender->save();

                $selectedsender[$sender->id] = $sender;
                $indSender++;
            }
        }

        foreach($selectedsender as $sid => $sender)
        {
            if($planning->type == 'public'){
                $ano = false;
            } else {
                $ano = true;
            }

            // var_dump($sender);
            // var_dump($sender->password);
            // le campagne create est dans phoenix send:q
            // $idcampagne = $routeur->create_campagne($sender,$planning,$anonyme);

            //  echo 'On fait notre liste';
            // $url = 'http://' . getenv('CLIENT_URL') . '/mailexport/Phoenix/'. str_slug($sender->nom) . '_c' . $lacampagne->id .'_p'.$planning->id. $anonyme . '.csv';

            $url = 'http://' . getenv('CLIENT_URL') . '/mailexport/phoenix/s'. $sender->nom . '_c' . $lacampagne->id .'_p'.$planning->id. $anonyme . '.csv';

            // url en dur pour test
            echo "Le lien est : " . "\n". $url;
//            $url = 'http://pastebin.com/raw/mBUVdBJv';
            $retourliste = $routeur->import_liste($sender->password, $sender, $url);

            // sauvegarder
            // taskid
            // listid
            // planning ID
            // var_dump($retourliste);

            \DB::table('campagnes_routeurs')->insert([
                'campagne_id' => $campagneid,
                'sender_id' => $sender->id,
                'cid_routeur' => 0,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
                'taskid' => '',
                'listid' => $retourliste,
                'planning_id' => $this->argument('planning_id')
            ]);

        }

        $planning->nb_trials = 0;
        $planning->segments_at = date('Y-m-d H:i:s');
        $planning->save();


        \Log::info("[CampagneSegmentPhoenix][P$planning->id] : Fin Segmentation (Planning $planning->id/Campagne $lacampagne->id)");

    }

    public function write_tokens($file, $tokens) {
        $fp = fopen($file,"a+");
        $content = "";
        foreach($tokens as $desti){
            $destinataire = \DB::table('destinataires')
                ->select('id','mail')
                ->where('id', $desti->destinataire_id)
                ->first();
//            $content .= $destinataire->mail.";;;".$destinataire->id."\n";
            $content .= $destinataire->mail."\n";
        }
        fwrite($fp,$content);
        fclose($fp);
    }



    protected function getArguments()
    {
        return [
            ['planning_id', InputArgument::REQUIRED, 'Planning id.'],
        ];
    }
}
