<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

// pour test
use App\Classes\Routeurs\RouteurMaildrop;
use App\Classes\Routeurs\RouteurPhoenix;
use App\Classes\Routeurs\RouteurSmessage;

class ImportFichierCliqueurs extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:fcliqueur {le_fichier} {info_envoi}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Importe les cliqueurs à partir de fichier texte';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        \DB::disableQueryLog();
        $dataset = array();
        $counterchunck = 0;
        $today = date("Y-m-d H:i:s");
        $date_active = date("Y-m-d");
        $tabimport = array();
        $minitab = null;

        $info_envoi = $this->argument('info_envoi');
        // je dois recup le campagne_id et ajouter theme
        // var_dump($info_envoi);

        $file = storage_path().'/cliqueurs/'.$this->argument('le_fichier');
        if (!is_file($file)) {
            $this->error('File not found : '.$file);
        }

        $pdo = \DB::connection()->getPdo();
        $sql = "INSERT IGNORE INTO clics (campagne_id, destinataire_id, created_at, updated_at, theme_id, clic_file, date_active) VALUES (:v_campagne, :v_destinataire, :v_created_at, :v_updated_at, :v_theme_id, :v_clic_file, :v_date_active)";
        $stmt = $pdo->prepare($sql);
        $pdo->beginTransaction();

        $fh = fopen($file, 'r');
        while (!feof($fh)){
          $row = fgets($fh, 1024);
          $row = trim($row);
          if(empty($row) || $row == null || $row == false){
            // ligne non conforme
          } else {

            $destinataire = \DB::table('destinataires')->select('id')->where('base_id',$info_envoi->base_id)->where('mail', $row)->first();
            var_dump($row);
            // var_dump($destinataire);

            if(empty($destinataire) || $destinataire == null || $destinataire == false){
              // on ne fait rien car le desti n'est pas dans la bdd
              echo 'vide';
            } else {
              // on fait un tableau pour insert

              // v1
             //
            //   \DB::table('clics')->insert(
            //   ['campagne_id' => $info_envoi->id,
            //    'destinataire_id' => $destinataire->id,
            //    'created_at' => $today,
            //    'updated_at' => $today,
            //    'theme_id' => $info_envoi->theme_id,
            //    'clic_file' => $this->argument('le_fichier')
             //
            //   ]
            //  );

             // v2
               $dataset[] = ['campagne_id' => $info_envoi->id,
                'destinataire_id' => $destinataire->id,
                'created_at' => $today,
                'updated_at' => $today,
                'theme_id' => $info_envoi->theme_id,
                'clic_file' => $this->argument('le_fichier')];

                $insertData = [$info_envoi->id,$destinataire->id, $today, $today, $info_envoi->theme_id, $this->argument('le_fichier'), $date_active];


                $stmt->execute($insertData);

            }
          }
        }

        echo 'INSERT Chunck' . "\n";
        // pdo jobs base import desti
        // a voir si les doublons etaient sur les meme campagnes
        // avoir si ca marche
        // $dataset = array_unique($dataset);

        // \DB::table('clics')->insert($dataset);

        // var_dump($insertData);


        $pdo->commit();

        // $dataset = array();

    }

    protected function getArguments()
    {
        return [
            ['le_fichier', InputArgument::REQUIRED, 'Nom du fichier'],
            ['info_envoi', InputArgument::REQUIRED, 'Le planning'],
        ];
    }
}
