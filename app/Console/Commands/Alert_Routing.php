<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class Alert_Routing extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'alert:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Informe des erreurs de routage';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      // check envoi 1x par heure
      $today = date("Y-m-d");
      $ids = array();
      $routeur_m4y = \DB::table('routeurs')->where('nom','MailForYou')->first();
      $sender_m4y = \DB::table('senders')->where('routeur_id',$routeur_m4y->id)->get();
      foreach ($sender_m4y as $v) {
        $ids[] = $v->id;
      }
      $campagnes_routeurs_check = \DB::table('campagnes_routeurs')->whereIn('sender_id',$ids)->where('cid_routeur',0)->get();
      foreach ($campagnes_routeurs_check as $c) {
        $checkiferror = \DB::table('alerte_erreur')->where('campagnes_routeurs_id',$c->id)->first();
          if($checkiferror === null){
            \DB::table('alerte_erreur')->insert([
            'campagnes_routeurs_id' => $c->id,
            'statut_alert' => 0,
            ]);
          }
      }

      // fin ajout des erreurs
      // partie envoi mail
      $alert = \DB::table('alerte_erreur')->where('statut_alert',0)->get();
      foreach ($alert as $a) {
        // recup en clair pour le mail d'info

        // routeur
        // sender id / en clair
        // la campagne
        // heure du bug

        $emailinfo = \DB::table('campagnes_routeurs')->where('id',$a->campagnes_routeurs_id)->first();
        // var_dump($emailinfo);
        $lacampagne = \DB::table('campagnes')->where('id',$emailinfo->campagne_id)->first();
        $lesender = \DB::table('senders')->where('id',$emailinfo->sender_id)->first();
        $txtmail = 'La campagne '. $lacampagne->nom . ' à rencontré un problème lors de son envoi le ' . $emailinfo->created_at;
        $txtmail .= ' <br /> ';
        $txtmail .= 'Le sender utilisé était ' . $lesender->nom ;
        // var_dump($txtmail);

        \Mail::raw($txtmail, function($message)
        {
            $message->subject('Problème envoi');
            $message->from('rapport@sc2consulting.fr', 'Alert');

            $destinataires = \App\Models\User::where('is_valid', 1)
                ->where('user_group_id', 1)
                ->where('email', '!=', "")
                ->get();

            foreach($destinataires as $d){
                $message->to($d->email);
            }
        });

        // faire un update après send

        \DB::table('alerte_erreur')
        ->where('campagnes_routeurs_id', $a->campagnes_routeurs_id)
        ->update(['statut_alert' => 1]);
        // FLAG a voir si suppr cette partie 
      }
    }
}
