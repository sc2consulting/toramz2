<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;
use App\Helpers\Profiler;
use App\Models\Destinataire;
use App\Models\Fai;
use App\Models\Token;
use App\Models\TokenStat;
use App\Models\Base;
use Symfony\Component\Console\Input\InputArgument;


class TokensStats extends Command {

    protected $bulk = 25;
    protected $chunk = 2000;

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'tokens:stats {planning_id}';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Manages Tokens';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
        $idplanning = $this->argument('planning_id');

        if($idplanning === NULL)
        {
           echo 'pas d\'argument';
           \Log::error('TokensStats : planning_id manquant');
           exit;	
        }
       
        \DB::disableQueryLog();
        \App\Helpers\Profiler::start('tokens:stats');

		$monplanning = \DB::table('plannings')
							->where('id',$idplanning)
							->first();
		
		$macampagne = \DB::table('campagnes')
						->where('id',$monplanning->campagne_id)
						->first();

        $today = date('Y-m-d');

        $bases = \DB::table('campagne_base')
            ->select('base_id')
            ->where('campagne_id', $macampagne->id)
            ->get();

        $lesfais = \DB::table('fais')->get();

        foreach($bases as $labase) {
            $resultat = \DB::table('tokens')
                ->where('base_id', $labase->base_id)
                ->where('date_active', date('Y-m-d'))
                ->count();

            $resultat_used = \DB::table('tokens')
                ->where('base_id', $labase->base_id)
                ->where('planning_id', $idplanning)
                ->where('date_active', date('Y-m-d'))
                ->count();

            \DB::statement("INSERT INTO tokens_stats (base_id,count,created_at,updated_at,count_used,planning_id) VALUES ('" . $labase->base_id . "','" . $resultat . "','" . date("Y-m-d H:i:s") . "','" . date("Y-m-d H:i:s") . "','" . $resultat_used . "','" . $idplanning . "')");

            \Log::info("Calcul des statistiques OK pour la plannif n : " . $idplanning);

            // a sortir d'ici \\ Y PENSER POUR LAUTRE CALCULL
            // \Artisan::call('fais:stats', ['planning_id' => $idplanning ]);
            // \Artisan::call('tokens:statsgeneral');

            foreach ($lesfais as $lefai) {

                $tokenscount = \DB::table('tokens')
                    ->where('base_id', $labase->base_id)
                    ->where('fai_id', $lefai->id)
                    ->where('date_active', date('Y-m-d'))
                    ->count();

                $tokenscountused = \DB::table('tokens')
                    ->where('base_id', $labase->base_id)
                    ->where('fai_id', $lefai->id)
                    ->where('planning_id', $idplanning)
                    ->where('date_active', date('Y-m-d'))
                    ->count();

                // echo 'Le nombre de tokens pour la base : ' . $labase->nom . ' et le fai : ' . $lefai->nom . "\n" ;

                \DB::statement("INSERT INTO fais_stats (base_id,fai_id,volume,created_at,updated_at,volume_used,planning_id) VALUES ('" . $labase->base_id . "','" . $lefai->id . "','" . $tokenscount . "','" . date("Y-m-d H:i:s") . "','" . date("Y-m-d H:i:s") . "','" . $tokenscountused . "','" . $idplanning . "')");
                // opty possible avec cette requete ?
                // select fai_id, count(fai_id) from tokens where planning_id = 927 group by fai_id;
            }

            \Log::info("Calcul des statistiques FAI OK pour la plannif n : " . $idplanning);
        }

        // ------------------------------------------

        \DB::table('tokens_stats_general')->truncate();
        $tokens_today_total = \DB::table('tokens')
						        ->where('date_active', date('Y-m-d'))
						        ->count();
        
        $tokens_today_used = \DB::table('tokens')
						        ->whereRaw('campagne_id is not null')
						        ->where('date_active', date('Y-m-d'))
						        ->count();
       
        $tokens_today_left = \DB::table('tokens')
       					        ->whereRaw('campagne_id is null')
						        ->where('date_active', date('Y-m-d'))
						        ->count();

        \DB::statement("INSERT INTO tokens_stats_general (volume,volume_used,volume_left,created_at,updated_at) VALUES ('". $tokens_today_total . "','". $tokens_today_used ."','". $tokens_today_left ."','". date("Y-m-d H:i:s") ."','". date("Y-m-d H:i:s")."')");
        \Log::info("Calcul des statistiques general OK - " .  date('Y-m-d'));
	}

      protected function getArguments()
      {
        return [
          ['planning_id', InputArgument::OPTIONAL, 'Planning id.'],
        ];
      }
}
