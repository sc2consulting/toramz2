<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class ExtractOuvreursBdd extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'extract:ouvreurs_bdd {nom_fichier} {base_id} {time_limit}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // tuy me prends les campagnes theme id / limite de temps

        $campagne_ouverture = \DB::table('campagnes')
        ->where('created_at','>', $this->argument('time_limit').'00:00:00')
        ->where('base_id', $this->argument('base_id'))
        ->get();

        foreach ($campagne_ouverture as $c) {

          $nb_ouvreurs = \DB::table('ouvertures')
          ->where('campagne_id', $c->id)
          ->count();
          echo ('Nombre ouvertures pour la campagne ' . $c->nom . ' : ' . $nb_ouvreurs ."\n");
          $offset = 1000;
          $skip = 0;

          $long = strlen($nb_ouvreurs) - substr_count($offset,'0');
          $nbretape = substr($nb_ouvreurs,0,$long);

          for ($i = 1; $i <= $nbretape; $i++) {

            echo 'Result ' . $i . ' / ' . $nbretape . "\n";

            $chaine = '';
            $destinatairesid = \DB::table('ouvertures')
            ->select('destinataire_id')
            ->where('campagne_id', $c->id)
            ->distinct()
            ->skip($skip)
            ->take($offset)
            ->get();

            $did = array_pluck($destinatairesid, 'destinataire_id');

            $destinatairesemails = \DB::table('destinataires')
            ->select('mail')
            ->where('statut','<','3')
            ->whereIn('id', $did)
            ->get();

            foreach ($destinatairesemails as $key => $v) {
              $chaine .= $v->mail."; \n";
            }

            $file = storage_path() . '/listes/download/'.$this->argument('nom_fichier').'.csv';
            $fp = fopen($file,"a+");
            fwrite($fp, $chaine);
            fclose($fp);

            $skip = $skip + $offset;
            unset($chaine);

          }



        }

        // lancement de la commande par nom campagne id

        // \Artisan::call('extract:ouvreurs_c', ['nom_fichier' => '','campagne_id' => "bounces_m4y_" . $today . ".csv" ]);

    }

    protected function getArguments()
    {
        return [

            ['nom_fichier', InputArgument::REQUIRED, 'Nom du fichier'],
            ['base_id', InputArgument::REQUIRED, 'Base ID'],
            ['time_limit', InputArgument::REQUIRED, 'Limite de temps'],

        ];
    }
}
