<?php


namespace App\Console\Commands;


use App\Jobs\JobTest;
use App\Models\Campagne;
use App\Models\Planning;
use App\Models\Routeur;
use Illuminate\Console\Command;

class CommandCampagneMultiThread extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'CommandCampagneMultiThreaded {planning_id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Crée les fichiers séparés des mails';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $planning = Planning::find($this->argument('planning_id'));

        $nombreaccount = $planning->nbr_sender;

        $bloc = \DB::table('senders_history')->max('bloc');
        if(empty($bloc)){
            $bloc = 0;
        }
        $this->max_bloc = $bloc + 1;

        \Log::info("Nombre de comptes MailKitchen qui seront utilisés : " . $nombreaccount);

        $planning->nb_trials = 2;
        $planning->save();

        $campagneid = $planning->campagne_id;
        $lacampagne = Campagne::where('id',$campagneid)->first();
        $amazon = Routeur::where('nom','Amazon')->first();

        /*
        $sender_envoi_m = \DB::table('senders')
            ->where('routeur_id', $amazon->id)
            // ->whereIn('id', $tabsender)
            ->where('nb_campagnes_left', '>', 0)
            ->where('quota_left', '>', 0)
            // ->where('branch_id', $planning->branch_id)
            ->get();

        if(count($sender_envoi_m) < 1){
            \Log::warning("[CampagneSegmentMailMultiTask] No sender available.Campaign cancelled");
            exit();
        }
        */

        $lesadressestokens = \DB::table('tokens')
            ->select('destinataire_id')
            ->where('campagne_id', $lacampagne->id)
            ->where('date_active', $planning->date_campagne)
            ->where('planning_id', $planning->id)
            ->get();

        // $factorQueue = count($sender_envoi_m);

        $factorQueue = 8;

        for($i=0; $i < $factorQueue; $i++)
        {
            if($i!=0) $increment=1;
            else $increment=0;
            $beginIndex = (round((count($lesadressestokens)/ $factorQueue)) * $i)+$increment;
            $nextIndex = round((count($lesadressestokens) / $factorQueue)) * ($i+1);
            if($nextIndex > count($lesadressestokens))
            {
                $nextIndex = $beginIndex - count($lesadressestokens);
            }
            $arrayAddressTokensToDo = array();

            for($j = $beginIndex ; $j< $nextIndex; $j++){
                $arrayAddressTokensToDo[$j] = $lesadressestokens[$j];
            }

            //AmazonTest est le nom du sender, si possible avoir un nom de sender qui soit le même pour un même routeur
            //Avec seulement le nombre qui change à la fin
            // echo 'Remplissage liste Sender : ' . $s->nom . "\n";
            $file = storage_path() . '/mailsmultitask/p_'.$planning->id.'_'.($i+1).'.csv';
            // var_dump('count aliste new : ' . count($lesadressestokens));
            $this->write_tokens($file, $arrayAddressTokensToDo);
        }
    }

    public function write_tokens($file, $tokens) {
        $fp = fopen($file,"w+");
        // $content = "";
        $content = null;
        foreach($tokens as $desti){
            $destinataire = \DB::table('destinataires')
                ->select('id','mail','hash')
                ->where('id', $desti->destinataire_id)
                ->first();
            $content .= $destinataire->mail . "\n";
            // $content .= $destinataire->mail . "\n";
            // $arraylistemfu[] = array($destinataire->mail);
        }
        fwrite($fp,$content);
        fclose($fp);
    }
}
