<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class BasesDeactivate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bases:deactivate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Désactiver toutes les bases.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        \DB::statement('UPDATE destinataires set statut = '.BASE_DEACTIVATED.' where base_id > 1');
    }
}
