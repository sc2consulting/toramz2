<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class dispatch_destinataire_domaine extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'dispatch:destinataires';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // ->toSql()
        $arrayresult = array();
        $nbrtotaldesti = \DB::table('destinataires')->whereNotNull('mail')->distinct()->count();
        // $offset = 100000;
        $offset = 10000;
        $long = strlen($nbrtotaldesti) - substr_count($offset,'0');
        $nbretape = substr($nbrtotaldesti,0,$long);
        $skip = 0;

        $domainestats = \DB::table('domaine_site')->get();

        for ($i = 1; $i <= $nbretape; $i++){

          // if($skip == 400000){
          //   echo 'LIMITE 400k' . "\n";
          //   break;
          // }

          // for debug
          // continue;

          echo 'Etape num : ' . $i . "\n";
          $skipslice = 0;
          $offsetslice = 0;
          $a = \DB::table('destinataires')->select('id')->orderBy('id')->skip($skip)->take($offset)->get();
          shuffle($a);

          foreach ($domainestats as $v) {

            $tableauidupdate = array();
            // $offsetslice = $v->pourcentage * 1000;
            $offsetslice = $v->pourcentage * 100;
             echo $v->domaine . "\n";
             echo 'Volume demande sur 10k = ' . $offsetslice . "\n";
            $arrayresult[$v->domaine] = array_slice($a, $skipslice, $offsetslice);
            $skipslice = $skipslice + $offsetslice;

            foreach ($arrayresult[$v->domaine] as $x) {
              $tableauidupdate[]=$x->id;
            }

            \DB::table('destinataires')
            ->whereIn('id', $tableauidupdate)
            ->update(['domaine_site_id' => $v->id]);

          }
          $skip = $skip + $offset;
        }

        echo 'Le skip a la fin est : ' . $skip . "\n";
        $r = $nbrtotaldesti - $skip;
        echo 'Il reste a traiter : ' . $r . "\n";

        $newoffset = 100;
        $long = strlen($r) - substr_count($newoffset,'0');
        $nbretape = substr($r,0,$long);
        echo 'Le nombre etape new : ' . $nbretape . "\n";

        for ($i = 1; $i <= $nbretape; $i++){

          echo 'Etape 2 num : ' . $i . "\n";
          $skipslice = 0;
          $offsetslice = 0;
          $a = \DB::table('destinataires')->select('id')->orderBy('id')->skip($skip)->take($newoffset)->get();
          shuffle($a);

          foreach ($domainestats as $v) {

            $tableauidupdate = array();
            // $offsetslice = $v->pourcentage * 1000;
            $offsetslice = $v->pourcentage * 1;
             echo $v->domaine . "\n";
             echo 'Volume demande sur 100 = ' . $offsetslice . "\n";
            $arrayresult[$v->domaine] = array_slice($a, $skipslice, $offsetslice);
            $skipslice = $skipslice + $offsetslice;

            foreach ($arrayresult[$v->domaine] as $x) {
              $tableauidupdate[]=$x->id;
            }

            \DB::table('destinataires')
            ->whereIn('id', $tableauidupdate)
            ->update(['domaine_site_id' => $v->id]);

          }


          $skip = $skip + $newoffset;

        }


        $a = \DB::table('destinataires')->select('id')->orderBy('id')->skip($skip)->take($newoffset)->get();
        shuffle($a);
        $idreste = array();
        foreach ($a as $destireste) {
          $idreste[] = $destireste->id;


        }

        \DB::table('destinataires')
        ->whereIn('id', $idreste)
        ->update(['domaine_site_id' => rand(1,2) ]);


    }
}
