<?php namespace App\Console\Commands;

use App\Classes\Routeurs\RouteurMaildrop;
use App\Classes\Routeurs\RouteurPhoenix;
use App\Classes\Routeurs\RouteurSmessage;
use App\Models\Sender;
use App\Models\CampagneRouteur;
use App\Models\Campagne;
use App\Models\Planning;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputArgument;

class CampagneClicks extends Command {

    /**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'campagne:clicks';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Get clicks for one campaign';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

    public function handle()
    {
        $planning = Planning::find($this->argument('planning_id'));
        $campagne = Campagne::find($planning->campagne_id);

        $camp_rout = CampagneRouteur::where('planning_id', $planning->id)->get();

        foreach($camp_rout as $row)
        {
            $sender = Sender::find($row->sender_id);

            \Log::info('Campagne:clicks - campagne '.$campagne->id.', sender '.$sender->id.', campagne_routeur '.$row->cid_routeur);

            //Si MailDrop
            if ($sender->routeur->nom == 'Maildrop') {
                $routeurmaildrop = new RouteurMaildrop();
                $routeurmaildrop->getClicks($sender->password, $campagne, $row->cid_routeur);
            }
            //Si SMessage
            if ($sender->routeur->nom == 'Smessage') {
                $routeursmessage = new RouteurSmessage();
                $routeursmessage->getClicks($sender, $campagne, $row->cid_routeur);
            }
            //Si Phoenix
            if ($sender->routeur->nom == 'Phoenix') {
                $routeurphoenix = new RouteurPhoenix();
                $routeurphoenix->getClicks($sender, $campagne, $row->cid_routeur);
            }
        }
    }

    protected function getArguments()
    {
        return [
            ['planning_id', InputArgument::REQUIRED, 'Planning id.'],
        ];
    }
}
