<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class BaseImportHardBounceOne extends Command
{
  /**
   * The console command name.
   *
   * @var string
   */
  protected $signature = 'base:import_hbounceone {file} {base_id}';

  /**
   * The console command description.
   *
   * @var string
   */
  protected $description = 'Ajoute des bounces pour une base depuis un fichier situé dans le dossier desinscrits.';

  /**
   * Create a new command instance.ha voilà
   *
   * @return void
   */
  public function __construct()
  {
      parent::__construct();
      $this->bases = array();
  }

  /**
   * Execute the command.
   *
   * @return void
   */
  public function handle()
  {
      $this->bases = \DB::table('bases')->select('id')->get();
      $file = storage_path().'/desinscrits/'.$this->argument('file');
      $filename = $this->argument('file');
      if (!is_file($file)) {
          $this->error('File not found : '.$file);
      }

      \Log::info("[BaseImportHbounce] : Import Hardbounces à partir du fichier -- $file");

      $ts = date('Y-m-d H:i:s');

      $bulk_count = 0;

      set_time_limit(0);
      ini_set('max_execution_time', 0);

      $extension = \File::extension($file);
      $name = \File::name($file);

      $updateData = array();

      $fh = fopen($file, 'r');

      $total = 0;
      $inserted = 0;

      while (!feof($fh)) {

          $cells = [];
          $row = fgets($fh, 1024);
          $row = trim($row, "\r\n\t ");

          if (strpos($row, ';') !== false) {
              $cells = explode(';', $row);
          } elseif (strpos($row, '|') !== false) {
              $cells = explode('|', $row);
          } else {
              $cells[] = trim($row, " \r\n\t\"");
          }

          foreach($cells as $cell) {

              if (filter_var($cell, FILTER_VALIDATE_EMAIL)) {
                  $total++;

                  $updateData[] = $cell;

                  $bulk_count++;
                  if (count($updateData) >= 5000) {
                      $this->writeUpdates($updateData, $filename);
                      $updateData = array();
                      $bulk_count = 0;
                  }

                  $inserted++;
                  continue;
              }
          }
      }

      if (count($updateData) > 0) {
          $this->writeUpdates($updateData, $filename);
          $updateData = array();
          $bulk_count = 0;
      }

      if (isset(\Auth::User()->id)) {
          $user_id = \Auth::User()->id;
      } else {
          $user_id = 1;
      }

      \Log::info("[BaseImportDesinscritsONE] : End of Import hardbounces a partir fichier -- $file");

  }

  /**
   * Get the console command arguments.
   *
   * @return array
   */
  protected function getArguments()
  {
      return [
          ['file', InputArgument::REQUIRED, 'Path to the file'],
          ['base_id', InputArgument::REQUIRED, 'Base a desabo']
      ];
  }

  private function writeUpdates($mails, $filename) {
      $now = date('Y-m-d H:i:s');
      foreach($this->bases as $b) {
          \DB::table('destinataires')
              // ->where('base_id',$this->argument('base_id'))
              ->where('base_id',$b->id)
              ->whereIn('mail', $mails)
              ->update(['statut' => 4, 'optout_at'=> $now, 'optout_file' => $filename]);
      }
  }
}
