<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Classes\Routeurs\RouteurMailKitchen;

class CampagneUnsubscribeMailKitchen extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'campagne:unsubscribe_mailkitchen';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Récupère les désabonnés de MailForYou et lance le job de désinscription sur toutes les bases';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

      $today = date('Ymd');
      $routeurm = new RouteurMailKitchen();
      $chaine = '';

      $file = storage_path() . '/desinscrits/unsubscribe_mk_' . $today . '.csv';
      $fp = fopen($file,"w+");


      for ($i=0; $i < 8; $i++) {
        $dateunsubscribe = date('Y-m-d', strtotime("-$i day"));
        $usub = $routeurm->GetUnsubscribers($dateunsubscribe);
        // echo($usub . "\n");
        foreach ($usub as $u) {
          var_dump($u['email']);
          $chaine .= $u['email'] . "\n";
        }

      }

      $file = storage_path() . '/desinscrits/unsubscribe_mk_' . $today . '.csv';
      $fp = fopen($file,"a+");
      fwrite($fp, $chaine);
      fclose($fp);

      echo "Lancement fichier \n";
      \Log::info("Debut [import_desinscrits] unsubscribe MK" . "[unsubscribe_mk_" . $today . ".csv]");
      \Artisan::call('base:import_desinscrits', ['file' => "unsubscribe_mk_" . $today . ".csv"]);
      \Log::info("Fin update [import_desinscrits] du fichier unsubscribe m4y" . "[unsubscribe_mk_" . $today . ".csv]");


    }
}
