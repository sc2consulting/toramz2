<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class MajSettings extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'settings:maj';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Maj dans settings des ids routeurs auto, etc';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $r = \DB::table('routeurs')->get();
        foreach ($r as $val) {
          $chcheck = $val->nom;
          $chcheck = strtolower($chcheck);
          $editsettings = \DB::table('settings')->where('parameter','LIKE','%' . $chcheck . '%')->where('context','routeur')->first();
          if(isset($editsettings->id)){
             var_dump($chcheck);
             \DB::table('settings')->where('id', $editsettings->id)->update(['info' => $val->id]);
          }
        }

    }
}
