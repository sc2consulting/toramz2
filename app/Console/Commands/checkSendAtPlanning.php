<?php


namespace App\Console\Commands;

use Illuminate\Console\Command;

class checkSendAtPlanning extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'checkSendAtPlanning';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command affiche les plannif envoyé aujourd hui';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $today = new date("Y-m-d");
        \DB::table('plannings')
            ->where('is_display', 0)
            ->where('date_campagne', $today)
            ->update(["is_display" => 1,]);
        return true;
    }

}
