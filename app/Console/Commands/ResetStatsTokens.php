<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class ResetStatsTokens extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'reset:statstokens';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Met a jour les stats générales';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      \DB::table('tokens_stats_general')->truncate();
      $tokens_today_total = \DB::table('tokens')->where('date_active', date('Y-m-d'))->count();
      $tokens_today_used = \DB::table('tokens')->where('date_active', date('Y-m-d'))->whereRaw('campagne_id is not null')->count();
      $tokens_today_left = \DB::table('tokens')->where('date_active', date('Y-m-d'))->whereRaw('campagne_id is null')->count();

      \DB::statement("INSERT INTO tokens_stats_general (volume,volume_used,volume_left,created_at,updated_at) VALUES ('". $tokens_today_total . "','". $tokens_today_used ."','". $tokens_today_left ."','". date("Y-m-d H:i:s") ."','". date("Y-m-d H:i:s")."')");
      \Log::info("Calcul des statistiques general OK - " .  date('Y-m-d'));
    }
}
