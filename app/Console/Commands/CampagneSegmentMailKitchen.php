<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Models\Planning;
use App\Classes\routeurs\RouteurMailKitchen;
use App\Models\Campagne;
use App\Models\Routeur;
use App\Models\Fai;
use App\Models\Sender;

class CampagneSegmentMailKitchen extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'campagne:segment_mailkitchen {planning_id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // je prend le volume total
        // on divise par le nombre de comptes
        // \Log::info("Suppression des fichiers");
        // exec("rm -rf * ".storage_path()."/mailkitchen/");
        $auto_setting = \DB::table('settings')
        ->where('parameter', 'is_auto')
        ->first();

        if(!$auto_setting or $auto_setting->value != 1){
            \Log::info('CampagneLaunchSend : AUTOMATIC CAMPAIGN NOT ACTIVE');
            return 0;
        }


        $planning = Planning::find($this->argument('planning_id'));

        $nombreaccount = $planning->nbr_sender;

        $bloc = \DB::table('senders_history')->max('bloc');
        if(empty($bloc)){
            $bloc = 0;
        }
        $this->max_bloc = $bloc + 1;

        \Log::info("Nombre de comptes MailKitchen qui seront utilisés : " . $nombreaccount);

        $planning->nb_trials = 2;
        $planning->save();

        $campagneid = $planning->campagne_id;
        echo "\nCampagne Segment \n";
        $routeurm = new RouteurMailKitchen();
        $lacampagne = Campagne::where('id',$campagneid)->first();
        $mailkitchen = Routeur::where('nom','MailKitchen')->first();

        // simple

        $campagne = Campagne::find($campagneid);

        $anonyme = "";
        if($planning->type == 'anonyme') {
            $anonyme = "_a";
            $ano = true;
        }

        \Log::info("[CampagneSegmentMailKitchen][P$planning->id] : Début Segmentation (Planning $planning->id/Campagne $lacampagne->id) {Trial n°$planning->nb_trials}");

        $fais = Fai::all();

        // mini deux comptes

        // à voir pour prendre plus intel les senders

        $sender_envoi_m = \DB::table('senders')
            ->where('routeur_id', $mailkitchen->id)
            // ->whereIn('id', $tabsender)
            ->where('nb_campagnes_left', '>', 0)
            ->where('quota_left', '>', 0)
            // ->where('branch_id', $planning->branch_id)
            ->get();

        if(count($sender_envoi_m) < 1){
            \Log::warning("[CampagneSegmentMailKitchen] No sender available.Campaign cancelled");
            exit();
        }
        $senderenvoi = array_slice($sender_envoi_m,0,$nombreaccount);

        $countFai = \DB::table('tokens')
        ->select('fai_id', \DB::raw('count(*) as total'))
        ->where('planning_id',$planning->id)
        ->groupBy('fai_id')
        ->get();

        foreach($countFai as $cf)
        {
            echo 'taile du count : ' . $cf->total . "\n";

            $sizechunck = $cf->total / $nombreaccount;

            var_dump('mon fai id  : ' . $cf->fai_id);
            var_dump('nombre par fai : ' . $cf->total);
            var_dump('taille array :' .  $sizechunck);
            $downset = 0;
            $offset = $sizechunck;


            $skip = 0;
            $take = $sizechunck;
            // foreach mes comptes pour remplir les listes avec chaque fai
            foreach ($senderenvoi as $s)
            {

              // On récupère les tokens PAR FAI
              $lesadressestokens = \DB::table('tokens')
                  ->select('destinataire_id')
                  ->where('campagne_id',$lacampagne->id)
                  ->where('fai_id',$cf->fai_id)
                  ->where('date_active',$planning->date_campagne)
                  ->where('planning_id',$planning->id)
                  ->skip($skip)->take($sizechunck)
                  ->get();

                // echo 'Remplissage liste Sender : ' . $s->nom . "\n";
                $file = storage_path() . '/mailkitchen/s' . $s->nom . '_c' . $lacampagne->id .'_p'.$planning->id. $anonyme.'.csv';
                // var_dump('count aliste new : ' . count($lesadressestokens));
                $this->write_tokens($file, $lesadressestokens);

                $sender = Sender::find($s->id);
                $quota_left = $sender->quota_left - $sizechunck;
                $sender->quota_left = $quota_left;
                // a mettre plus bas
                // $sender->nb_campagnes_left -= 1;
                $sender->save();
                $skip = $skip + $sizechunck;
            }

            $skip = 0;
        }



        foreach ($senderenvoi as $s)
        {
            // echo 'Import serveur ' . $s->nom;

            $liste = array();

            $chemin = storage_path() . '/mailkitchen/s' . $s->nom . '_c' . $lacampagne->id .'_p'.$planning->id. $anonyme.'.csv';

            $fp = fopen($chemin,"r");
            if ($fp) {
                while (!feof($fp))
                {
                    $buffer = fgets($fp);
                    $buffer = trim($buffer);

                    if($buffer !== ''){
                        // on casse la chaine ici ?
                        // $e = explode(";", $buffer);
                        // var_dump($buffer);
                        $liste[] = array($buffer);


                    }
                }
                fclose($fp);
            }


            $html = $lacampagne->generateHtml($mailkitchen);
            echo "Création de la Campagne \n";

            // changement html

            $campaignid = $routeurm->CreateCampaign($campagne->ref.'-'.$s->nom.'-'.time(),$campagne->expediteur,$campagne->objet, $html,$s->site_id);

            // sleep(45);
            echo "Le campagne id : \n";
            var_dump($campaignid);


            echo 'Creation liste desti' . "\n";
            // $retour = $routeur->CreationListeDestinataireFields_new($s->password,'Segment_'.time(),$s->api_login,$s->api_key);
            $listid = $routeurm->createList();
            if(is_null($listid)){
                sleep(120);
                $retry = 0;
                while($retry !== 2){
                    $listid = $routeurm->createList();
                    if(!is_null($listid)){
                        break;
                    }else{
                        $retry++;
                    }
                }
                if($retry == 2){
                    \Log::error("[CampagneSegmentMailKitchen] Cannot create list.Cancel planning");
                }
            }
            echo 'Import liste desti' . "\n";
            shuffle($liste);


            $decoupeliste = array_chunk($liste,499);

            foreach ($decoupeliste as $t)
            {
                echo 'Import Chunk' . "\n";
                $respond = $routeurm->populateList($listid, $t);

                if($respond == false){
                  \Log::info("[CampagneSegmentMailKitchen] Erreur Populate liste");
                  for ($i=0; $i < 300; $i++) {
                    sleep(10);
                    \Log::info("[CampagneSegmentMailKitchen] Retry " . $i);
                    $respondretry = $routeurm->populateList($listid, $t);
                    if($respondretry == true){
                        \Log::info("[CampagneSegmentMailKitchen] Retry OK " . $i);
                        break;
                    }
                  }
                }
                if($respond == true){
                  \Log::info("[CampagneSegmentMailKitchen] Import Chunck ok");
                  continue;
                }
            }

            // rajouter si PB
            echo "DelivrabilityTest \n";
            $delivtest = $routeurm->DelivrabilityTest($campaignid);
            if($delivtest['success'] == 0){
              sleep(45);
              $delivtest = $routeurm->DelivrabilityTest($campaignid);
            }
            sleep(45);
            if($delivtest['success'] == 0){
              \Log::info("[CampagneSegmentMailKitchen] DelivrabilityTest erreur ECHEC FORCE");
            } else {
              \Log::info("[CampagneSegmentMailKitchen] DelivrabilityTest erreur OK FORCE");
            }
            echo "Set Target \n";
            $routeurm->SetTarget($campaignid,$listid);
            sleep(60);
            echo "SEND Target \n";
            $routeurm->SendCampaign($campaignid);
            // \DB::table('campagnes_routeurs')->insert($array_campagne_routeurs_envoi);
            \Log::info("Save du Segment_at MK");
            // enlever sender nb_campagnes_left
            // pour lenvoi je vais parcourir campagne routeur sur le planning id
            $sender = Sender::find($s->id);
            $nb_campagnes_left = $sender->nb_campagnes_left - 1;
            $sender->nb_campagnes_left = $nb_campagnes_left;
            $sender->save();
        }

        // $planning->nb_trials = 0;
        $planning->segments_at = date('Y-m-d H:i:s');
        $planning->sent_at = date('Y-m-d H:i:s');
        $planning->save();
        \Log::info("[CampagneSegmentMailKitchen][P$planning->id] : Fin Segmentation (Planning $planning->id/Campagne $lacampagne->id)");

    }


    public function write_tokens($file, $tokens) {
        $fp = fopen($file,"a+");
        // $content = "";
        $content = null;
        foreach($tokens as $desti){
            $destinataire = \DB::table('destinataires')
                ->select('id','mail','hash')
                ->where('id', $desti->destinataire_id)
                ->first();
            $content .= $destinataire->mail . "\n";
            // $content .= $destinataire->mail . "\n";
            // $arraylistemfu[] = array($destinataire->mail);
        }
        fwrite($fp,$content);
        fclose($fp);
    }
}
