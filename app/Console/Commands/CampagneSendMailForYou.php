<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Classes\Routeurs\RouteurMailForYou;
use App\Models\Planning;
use App\Models\Campagne;

class CampagneSendMailForYou extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'campagne:send_m4u {planning_id}';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Lance les envois mailforyou';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $auto_setting = \DB::table('settings')
            ->where('parameter', 'is_auto')
            ->first();
        if(!$auto_setting or $auto_setting->value != 1){
            \Log::info('CampagneLaunchSend : AUTOMATIC CAMPAIGN NOT ACTIVE');
            return 0;
        }
        $routeur_setting = \DB::table('settings')
            ->where('parameter', 'is_mailforyou')
            ->first();

        if(!$routeur_setting or $routeur_setting->value != 1){
            \Log::info('CampagneLaunchSend : MailForYou DISABLE');
            return 0;
        }

        echo 'envoi m4u';
        $routeur = new RouteurMailForYou();
        $id_planning = $this->argument('planning_id');
        $planning = Planning::find($id_planning);
        var_dump($id_planning);

        $campagne = Campagne::find($planning->campagne_id);

        \Log::info("[CampagneSendMailForYou][P$planning->id] : Début du lancement des envois (Planning $planning->id/Campagne $campagne->id)");

        // je parcours les cid avec le planning id
        // ajouter dinstinct cid_routeur
        $envoi_a_faire = \DB::table('campagnes_routeurs')->where('planning_id',$id_planning)->where('taskid','0')->get();
        // $envoi_a_faire = \DB::table('campagnes_routeurs')->distinct()->where('planning_id',$id_planning)->get();

        // je lance les envois
        // var_dump($envoi_a_faire);
        foreach ($envoi_a_faire as $row) {
            $lesenderm4y = \DB::table('senders')->where('id',$row->sender_id)->first();
            echo $row->cid_routeur;
            $retour0 = $routeur->ValidationPlanificationRoutage_new($row->cid_routeur, $lesenderm4y->api_login,$lesenderm4y->api_key);

            var_dump($retour0);

            \Log::info("Code de la validation : [" . $retour0['code'] . "]");

            \DB::statement("UPDATE campagnes_routeurs SET taskid = 1 WHERE id = $row->id");

            if($retour0['code'] == 204){
                // si les infos sont bonnes renvoie toujours 204
                // mais pas si le serveur est en erreur
                // stuff protection
                // dans ce cas ok pour le sent_at et le update task id 1
                // \DB::statement("UPDATE campagnes_routeurs SET taskid = 1 WHERE id = $row->id");
                // $planning->sent_at = date('Y-m-d H:i:s');
                // $planning->save();
            }


        }

        // update task id à 1
        \Log::info("Save du Send_at M4Y");

        $planning->sent_at = date('Y-m-d H:i:s');
        $planning->save();

        \App\Models\Notification::create([
            'user_id' => 1, // /!\ User System
            'level' => 'info',
            'message' => "Campagne $campagne->ref (planning $planning->id) envoyée.",
            'url' => '/campagne/'.$campagne->id.'/stats'
        ]);

        $email_volume_demande = 'Volume demandé : ' . $planning->volume;
        $email_volume_orange = 'Volume orange demandé : ' . $planning->volume_orange;
        $email_volume_selected = 'Volume réel utilisé : ' . $planning->volume_selected;
        $email_date = 'Campagne programmée pour le ' . $planning->date_campagne . ' ' . $planning->time_campagne . ' / Envoi de la campagne terminé à ' . $planning->sent_at;

        $senders = \DB::select("select nom from senders where id in (select sender_id from campagnes_routeurs where planning_id = :planning_id)", ['planning_id' => $planning->id]);

        $used_senders = 'Compte(s) utilisé(s) : <br/>';

        foreach($senders as $s)
        {
            $used_senders .= "$s->nom <br/>";
        }

        \Mail::send('mail.mailinfocampagne',
            [
                'email_volume_demande' => $email_volume_demande,
                'email_volume_orange' => $email_volume_orange,
                'email_volume_selected' => $email_volume_selected,
                'email_date' => $email_date,
                'used_senders' => $used_senders,
                'url' => getenv('CLIENT_TITLE')
            ], function ($message) use ($campagne) {
                $destinataires = \App\Models\User::where('is_valid', 1)
                    ->where('user_group_id', 1)
                    ->where('email', '!=', "")
                    ->get();
                $message->from('rapport@lead-factory.net', 'Tor')->subject("Rapport MF : envoi de $campagne->ref [$campagne->id] terminé au " . date('d-m-Y H:i'));
                foreach($destinataires as $d){
                    $message->to($d->email);
                }
                return "true";
            });

        \Log::info("[CampagneSendMailForYou][P$planning->id] : Fin du lancement des envois (Planning $planning->id/Campagne $campagne->id)");
    }

    protected function getArguments()
    {
        return [
            ['planning_id', InputArgument::REQUIRED, 'Planning id.'],
        ];
    }
}
