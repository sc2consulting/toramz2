<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use \App\Models\Base;

use Symfony\Component\Console\Input\InputArgument;

class DedupConsole extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'dedup:filebdd {base_id} {file_a_dedup}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Dedup fichier grosses bdd';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $directory = storage_path('listes/listmanager');
        $fichier1 = $this->argument('file_a_dedup');
        $base = $this->argument('base_id');

        $base = Base::find($base);

				\DB::table('destinataires')
					->select('mail')
					->where('base_id',$base->id)
					->chunk(20000, function($base_dest){
						foreach($base_dest as $bd){
							$this->destinataires[$bd->mail]="";
						}
					});

				$tabliste1 = array();
				$tabliste2 = array();
				$tabliste3 = array();

				$original = storage_path('listes/listmanager/' . $fichier1);
				$handle = fopen($original, 'r');
				while($ligne = fgets($handle))
				{
					$tabliste1[trim($ligne)] = "";
				}
        /*
				foreach($blacklistes as $bl){
					$tabliste3[$bl->email] = "";
				}
        */

				$uniques = array_diff_key($tabliste1, $this->destinataires);
				$doublons = array_intersect_key($tabliste1, $this->destinataires);

				// $uniques_from_bl = array_diff_key($uniques, $tabliste3);
				// $doublons_from_bl = array_intersect_key($uniques, $tabliste3);

				//UNIQUES BASE//
				$filename = 'filtrage_'. $fichier1."_".count($uniques)."uniques-base-$base->nom.txt";

				$stats[$filename]['uniques'] = count($uniques);
				$stats[$filename]['doublons'] = count($doublons);

				$filepathUniques = 'listes/listmanager/'.$filename;
				$lefichier =	fopen(storage_path($filepathUniques), 'w');
				foreach ($uniques as $lemail => $rien) {
					fputs($lefichier, $lemail . "\n");
				}
				fclose($lefichier);
				//DOUBLONS BASE//
				$filename = 'filtrage_'. $fichier1."_".count($doublons)."doublons-base-$base->nom.txt";
				$filepathDoublons = 'listes/listmanager/'.$filename;
				$lefichier =	fopen(storage_path($filepathDoublons), 'w');
				foreach ($doublons as $lemail => $rien) {
					fputs($lefichier, $lemail . "\n");
				}
				fclose($lefichier);

        /*
				//UNIQUES BLACKLISTE//
				$filename = 'filtrage_'. $fichier1."_".count($uniques_from_bl)."uniques-base-$base->nom-BL-$blackliste.txt";
				$filepathUniques = 'listes/listmanager/'.$filename;

				$stats[$filename]['uniques'] = count($uniques_from_bl);
				$stats[$filename]['doublons'] = count($doublons_from_bl);

				$lefichier =	fopen(storage_path($filepathUniques), 'w');
				foreach ($uniques_from_bl as $lemail => $rien) {
					fputs($lefichier, $lemail . "\n");
				}
				fclose($lefichier);
				//DOUBLONS BLACKLISTE//
				$filepathDoublons = 'listes/listmanager/filtrage_'. $fichier1."_".count($doublons_from_bl)."doublons-base-$base->nom-BL-$blackliste.txt";
				$lefichier =	fopen(storage_path($filepathDoublons), 'w');
				foreach ($doublons_from_bl as $lemail => $rien) {
					fputs($lefichier, $lemail . "\n");
				}
				fclose($lefichier);

				$bases = Base::all();
				$blacklistes = \DB::table('blacklistes')
					->select('liste')
					->distinct()
					->get();
				$directory = storage_path('listes/listmanager');
				$fichiers = \File::allFiles($directory);

				$fichier[] = $fichier1;

*/



    }


    protected function getArguments()
    {
        return [
            ['base_id', InputArgument::REQUIRED, 'Base'],
            ['file_a_dedup', InputArgument::REQUIRED, 'file_a_dedup'],
        ];
}
}
