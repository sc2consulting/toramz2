<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Classes\Routeurs\RouteurMaildrop;
class CheckSenderMd extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check:sendermd';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Verifie la configuration des comptes MD';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $lerouteurid = \DB::table('routeurs')->where('nom','Maildrop')->first();
        $sendersmd = \DB::table('senders')->where('routeur_id',$lerouteurid->id)->get();
        $routeur = new RouteurMaildrop();

        foreach ($sendersmd as $lesender) {
          // var_dump($lesender);
          $retour = $routeur->CheckAccount($lesender->password);
          // var_dump($retour);

          if($retour === false){
            \Log::info('Le compte Maildrop : ' . $lesender->nom . ' est mal configuré');
          }

        }
    }
}
