<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Classes\Routeurs\RouteurPhoenix;

class StatsRouteurPhoenix extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'stats:majphoenix';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command qui met a jour les stats PH';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $routeur = new RouteurPhoenix();
        $lerouteur = \DB::table('routeurs')->where('nom','Phoenix')->first();
        $senderphoenix = \DB::table('senders')->where('routeur_id',$lerouteur->id)->get();
        $blocid = \DB::table('stats_phoenix')->max('bloc_maj');
        $blocid = $blocid + 1;
        foreach ($senderphoenix as $k => $v) {
          $info_cid = \DB::table('campagnes_routeurs')->where('sender_id',$v->id)->get();
          // methode propre
          foreach ($info_cid as $c) {
             $data = $routeur->campaign_getstat($v->password, $c->cid_routeur);
             // var_dump($data);
             if(isset($data->email_planned)){
             $error = $data->email_planned-$data->email_sent;
             $lacampagne = \DB::table('campagnes')->where('id',$c->campagne_id)->first();
             var_dump($lacampagne->ref);
            \DB::statement("INSERT INTO stats_phoenix (id_sender_phoenix,volume,ouvreurs,cliqueurs,erreurs,desabo, date_maj,bloc_maj, reference) VALUES ('" . $v->id . "','" . $data->email_sent . "','" . $data->open_unique . "','" . $data->click_unique . "','" . $error . "','". $data->unsubscribe . "','" . time() . "','". $blocid . "','". $lacampagne->ref ."')");

             }
        }

        // $lastmaj = \DB::table('stats_phoenix')->max('bloc_maj');
        // $consoliderstats = \DB::table('stats_phoenix')->where('bloc_maj',$lastmaj)->get();
        //
        // foreach ($consoliderstats as $item) {
        // $checkref = \DB::table('stats_phoenix_total')->where('reference',$item->reference)->where('bloc_maj',$lastmaj)->first(); // bloc maj
        //
        // if (is_null($checkref)) {
        //   \DB::statement("INSERT INTO stats_phoenix_total (reference, desinscriptions, ouvreurs, cliqueurs, erreurs, date_maj, bloc_maj) VALUES ('". $item->reference ."','" . $item->desinscriptions . "','" . $item->ouvreurs . "','" . $item->cliqueurs . "','" . $item->erreurs . "','" . time() . "','". $lastmaj ."')");
        //
        // } else {
        //
        //
        // }

          // fin methode propre

          // --------- Changement de méthode -----------

          // methode sale en attendant les dev
          // $data = $routeur->campaign_getallstat($v->password);
          // foreach($data as $d){
          //   if($d->VolTot > 100){
          //      $nomcampagne = $d->name;
          //      $nomcampagne = explode('-api',$nomcampagne);
          //     \DB::statement("INSERT INTO stats_phoenix (id_sender_phoenix,volume,ouvreurs,cliqueurs,erreurs,desabo,routeur_created, date_maj,bloc_maj, reference) VALUES ('" . . "','','')");
          //   }
          // }

          // on garde la date du createdON

        }

        // suite on a rempli la table et faut compiler la table total
        // $blocid = \DB::table('stats_phoenix')->max('bloc_maj');
        // $blocid = $blocid + 1;
        // var_dump($blocid);
        $lastmaj = \DB::table('stats_phoenix')->max('bloc_maj');
        $consoliderstats = \DB::table('stats_phoenix')->where('bloc_maj',$lastmaj)->get();

        foreach ($consoliderstats as $item) {
        $checkref = \DB::table('stats_phoenix_total')->where('reference',$item->reference)->where('bloc_maj',$lastmaj)->first(); // bloc maj

        if (is_null($checkref)) {
          \DB::statement("INSERT INTO stats_phoenix_total (volume_total, ouvreurs, cliqueurs, erreurs, desabo, date_maj, bloc_maj,reference) VALUES ('" . $item->volume . "','" . $item->ouvreurs . "','" . $item->cliqueurs . "','" . $item->erreurs . "','" . $item->desabo . "','" . time() . "','". $lastmaj ."','". $item->reference ."')");
        } else {
          // echo 'ligne deja presente';
          $totalvolume = $checkref->volume_total + $item->volume;
          $totaldesinscriptions = $checkref->desabo + $item->desabo;
          $totalcliqueurs = $checkref->cliqueurs + $item->cliqueurs;
          $totalerreurs = $checkref->erreurs + $item->erreurs;
          $totalouvreurs = $checkref->ouvreurs + $item->ouvreurs;

          \DB::statement("UPDATE stats_phoenix_total SET volume_total ='" . $totalvolume . "' WHERE reference ='" . $item->reference . "' AND bloc_maj = '" . $lastmaj . "'");
          \DB::statement("UPDATE stats_phoenix_total SET desabo ='" . $totaldesinscriptions . "' WHERE reference ='" . $item->reference . "' AND bloc_maj = '" . $lastmaj . "'");
          \DB::statement("UPDATE stats_phoenix_total SET cliqueurs ='" . $totalcliqueurs . "' WHERE reference ='" . $item->reference . "' AND bloc_maj = '" . $lastmaj . "'");
          \DB::statement("UPDATE stats_phoenix_total SET erreurs ='" . $totalerreurs . "' WHERE reference ='" . $item->reference . "' AND bloc_maj = '" . $lastmaj . "'");
          \DB::statement("UPDATE stats_phoenix_total SET ouvreurs ='" . $totalouvreurs . "' WHERE reference ='" . $item->reference . "' AND bloc_maj = '" . $lastmaj . "'");

        }
      }

      \Log::info('Récupération des statistiques Phoenix OK');
    }
}
