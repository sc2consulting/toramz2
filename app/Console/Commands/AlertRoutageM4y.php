<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Classes\Routeurs\RouteurMailForYou;

class AlertRoutageM4y extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'alert:checkm4y';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check les routages du jours';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
/*
array(9) {
  [0] =>
  string(2) "Id"
  [1] =>
  string(5) "State"
  [2] =>
  string(6) "ToSend"
  [3] =>
  string(4) "Sent"
  [4] =>
  string(14) "RecipientsRead"
  [5] =>
  string(14) "RecipientsClic"
  [6] =>
  string(11) "Unsubscribe"
  [7] =>
  string(11) "SoftBounces"
  [8] =>
  string(11) "HardBounces"
}
*/

        $arraycheck = null;
        $routeur = new RouteurMailForYou();
        $lerouteurid = \DB::table('routeurs')->where('nom','MailForYou')->first();
        $senderm4y = \DB::table('senders')->where('routeur_id',$lerouteurid->id)->get();
        foreach ($senderm4y as $s) {
          $arraycheck[] = $s->id;
        }

        var_dump(date('Y-m-d H:i:s',time()-1800));

        $envoi_a_check = \DB::table('campagnes_routeurs')
        ->whereIn('sender_id',$arraycheck)
        // 3 jours
        ->where('created_at','>',date('Y-m-d H:i:s',time()-259200))
        ->where('created_at','<',date('Y-m-d H:i:s',time()-1800))
        ->get();

        $lastmaj = \DB::table('etat_envoi_m4y')->max('bloc_maj');

        if($lastmaj == NULL){
          $lastmaj = 1;
        } else {
          $lastmaj = $lastmaj + 1;
        }
        var_dump($lastmaj);
        foreach ($envoi_a_check as $e) {
          $r = $routeur->LecturePlanificationRoutage_new($e->cid_routeur,$e->cid_routeur,$e->sender_id);

          var_dump($r->records[0][2]);
          var_dump($r->records[0][3]);

          if(isset($r)){

          \DB::table('etat_envoi_m4y')
            ->insert(
              [
                'planning_id' => $e->planning_id,
                'sender_id' => $e->sender_id,
                'campagne_id' => $e->campagne_id,
                'cid_routeur' => $e->cid_routeur,
                'bloc_maj' => $lastmaj,
                'tosend' => $r->records[0][2],
                'send' => $r->records[0][3],
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
              ]
          );
        }

        }

        // partie envoi email
        $alertmail = \DB::table('etat_envoi_m4y')->where('bloc_maj',$lastmaj)->get();
        foreach ($alertmail as $row) {
          // var_dump($row->tosend);
          // var_dump($row->send);

          /*
          if($row->tosend == 0){

          $lacampagne = \DB::table('campagnes')->where('id',$row->campagne_id)->first();
          $lesender = \DB::table('senders')->where('id',$row->sender_id)->first();
          $txtmail = 'La campagne '. $lacampagne->nom . ' à rencontré un problème lors de son envoi. Dernier controle le ' . $row->created_at;
          $txtmail .= ' <br /> ';
          $txtmail .= 'Le sender utilisé était ' . $lesender->nom ;
          // var_dump($txtmail);

          \Mail::raw($txtmail, function($message)
          {
              $message->subject('Problème envoi M4Y');
              $message->from('rapport@sc2consulting.fr', 'Alert');
              $message->to('fabien@lead-factory.net');
              $message->cc('adeline.sc2@gmail.com');
              $message->cc('olivier@plandefou.com');
              $message->cc('thibaud@sc2consulting.fr');

          });

          }
          */
        }

    }
}
