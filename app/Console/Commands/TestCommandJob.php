<?php

namespace App\Console\Commands;

use App\Jobs\JobTest;
use Illuminate\Console\Command;
use Symfony\Component\Process\Process as Process;

class TestCommandJob extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'TestCommandJob {varint}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $process = null;
        for($i = 0; $i < 3; $i++)
        {
            $process = new Process('php artisan CommandBis 3');
            $process->start();
        }

        $process->wait();
        return true;
    }
}
