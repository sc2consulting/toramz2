<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use PHPMailer\PHPMailer\PHPMailer;
use App\Models\Planning;
use App\Models\Campagne;

class CampagneSegmentAmazonVoyage extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'campagne:segment_amzvoy {planning_id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      $auto_setting = \DB::table('settings')
      ->where('parameter', 'is_auto')
      ->first();

      if(!$auto_setting or $auto_setting->value != 1){
        \Log::info('CampagneLaunchSend : AUTOMATIC CAMPAIGN NOT ACTIVE');
        return 0;
      }

      $planning = Planning::find($this->argument('planning_id'));
      $lacampagne = Campagne::where('id',$planning->campagne_id)->first();

      $lesadressestokens = \DB::table('tokens')
          ->select('destinataire_id')
          // ->where('campagne_id',$lacampagne->id)
          // ->where('fai_id',$cf->fai_id)
          ->where('date_active',$planning->date_campagne)
          ->where('planning_id',$planning->id)
          // ->skip($skip)->take($sizechunck)
          ->get();

          $file = storage_path() . '/amz/p'.$planning->id.'.csv';
          $this->write_tokens($file, $lesadressestokens);

      $handle = fopen($file, 'r');
      $count = 0;

      while($ligne = fgets($handle))
      {

      $count = $count + 1;
      $m = trim($ligne);

      $mail = new PHPMailer;

      $mail->isSMTP();

      $mail->setFrom('hello@voyaneo.com', utf8_decode($lacampagne->expediteur));
      $mail->addAddress($m, 'Recipient Name');
      $mail->Username = 'AKIAV43XT4Q5XBO4L44S';
      $mail->Password = 'BHb8MKi97foBrggO4277hEg86G5knFEVwPJBKEGcYi71';
      //$mail->addCustomHeader('X-SES-CONFIGURATION-SET', 'ConfigSet');
      $mail->Host = 'email-smtp.us-west-2.amazonaws.com';

      $mail->Subject = utf8_decode($lacampagne->objet);
      $html = str_replace('[eamz]',$m, $lacampagne->html);

          $caracteres = array ("é","è","à","ç","‘","’","´","ê","ù","î","€","À","É","°");
          $caracteres_HTML = array ("&eacute;","&egrave;","&agrave;","&ccedil;","&lsquo;","&rsquo;","´","&ecirc;","&ugrave;","&icirc;","&euro;","&Agrave;","&Eacute;","&ordm;");

      $html = str_replace($caracteres,$caracteres_HTML,$html);
      $mail->Body =utf8_encode($html);

      $mail->SMTPAuth = true;

      $mail->SMTPSecure = 'tls';
      $mail->Port = 587;
      $mail->isHTML(true);

      /* $mail->AltBody = "

      Ce sac &agrave; dos antivol est le v&eacute;ritable cauchemar des pickpockets ! Son secret ? Des fermetures invisibles et accessibles uniquement via un syst&egrave;me unique et astucieux. Con&ccedil;u avec un tissu r&eacute;sistant et waterproof, ce sac sera le compagnon parfait lors de vos sorties. Il est &eacute;quip&eacute; dun port USB externe avec c&acirc;ble de chargement int&eacute;gr&eacute;, vous pouvez facilement recharger votre t&eacute;l&eacute;phone sans avoir &agrave; ouvrir votre sac pour prendre votre batterie externe.
      "; */

      if(!$mail->send()) {
            echo "Email not sent. " , $mail->ErrorInfo , PHP_EOL;}
        else
            {    echo $count . " - Email sent! @ " . $m , PHP_EOL;
      }

      unset($mail);

    }


    $planning->segments_at = date('Y-m-d H:i:s');
    $planning->sent_at = date('Y-m-d H:i:s');
    $planning->save();
    \Log::info("[CampagneSegmentAMZ][P$planning->id] : Fin Segmentation (Planning $planning->id/Campagne $lacampagne->id)");
    \Log::info("[CampagneAMZ][P$planning->id] : Envoi OK");

    }


    public function write_tokens($file, $tokens) {
        $fp = fopen($file,"a+");
        $content = null;
        foreach($tokens as $desti){
            $destinataire = \DB::table('destinataires')
                ->select('id','mail','hash')
                ->where('id', $desti->destinataire_id)
                ->first();
            $content .= $destinataire->mail . "\n";
        }
        fwrite($fp,$content);
        fclose($fp);

      }
}
