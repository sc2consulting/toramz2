<?php namespace App\Console\Commands;

use App\Models\Planning;
use App\Models\Campagne;
use Illuminate\Console\Command;
use Carbon\Carbon;
use Symfony\Component\Console\Input\InputArgument;

class CampagneLightBounces extends Command {

    /**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'campagne:light_bounces';

	/**
	 * The console command description.
	 *
	 * @var string
	 */

	protected $description = 'Get bounces auto';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

    public function handle()
    {
        $now = Carbon::today();

        echo "\n NOW -- $now \n";

        $plannings = Planning::distinct('campagne_id')
            ->whereNotNull('sent_at')
            ->get();

//        echo "REQ -- ".Planning::distinct('campagne_id')
//            ->whereNotNull('sent_at')
//            ->toSql()."\n\n";

        foreach($plannings as $p)
        {

            $date = new Carbon($p->sent_at);
//            echo "\n Campagne sent at - $date";
            $diff = $now->diffInHours($date);

            //Si la campagne a été envoyé il y a plus deux heures on lance la récup

            if ($diff >= 2) {
//                echo "\n $p->campagne_id - $campagne->ref";
                \Artisan::call('campagne:bounces',['campagne_id' => $p->campagne_id]);
                $p->bounce_at = $now;
                $p->save();
            }
        }
        \App\Helpers\Profiler::report();
    }
}
