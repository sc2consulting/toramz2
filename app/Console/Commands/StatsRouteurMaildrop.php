<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Classes\Routeurs\RouteurMaildrop;

class StatsRouteurMaildrop extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'stats:majmaildrop';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command qui met a jour les stats MD';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        \Log::info('Debut du calcul des statistiques Maildrop');

        $routeur = new RouteurMaildrop();


        $lerouteurid = \DB::table('routeurs')
            ->where('nom','Maildrop')
            ->first();

        $maildrop_senders = \DB::table('senders')
            ->where('routeur_id', $lerouteurid->id)
            ->get();

        $infobase = \DB::table('campagnes_routeurs')
            ->select('cid_routeur', 'sender_id', 'campagne_id')
            ->where('created_at','>',date('Y-m-d H:i:s',time()-604800))
            ->whereIn('sender_id', array_pluck($maildrop_senders, 'id'))
            ->get();

        $id = null;
        $password = null;

        $blocid = \DB::table('stats_maildrop')
            ->max('bloc_maj');

        $blocid = $blocid + 1;

        foreach ($infobase as $datadb)
        {
            $recupinfo = \DB::table('senders')->where('id',$datadb->sender_id)->first();
            if($lerouteurid->id === $recupinfo->routeur_id ){

                $recupref = \DB::table('campagnes')->where('id',$datadb->campagne_id)->first();

                $id = $recupinfo->routeur_id;
                $password = $recupinfo->password;

                if(!empty($recupref)) {
                    echo 'Calcul pour la campagne : ' . $recupref->ref . ' CID :  ' . $datadb->cid_routeur . "\n";

                    \DB::statement("INSERT INTO stats_maildrop (id_maildrop, desinscriptions, ouvreurs, cliqueurs, erreurs, date_maj, bloc_maj, reference)
            VALUES (" . $datadb->cid_routeur . "," . $routeur->recup_desabo($password, $datadb->cid_routeur) . "," . $routeur->recup_ouverture($password, $datadb->cid_routeur) . "," . $routeur->recup_click($password, $datadb->cid_routeur) . "," . $routeur->recup_error($password, $datadb->cid_routeur) . ",
            " . time() . "," . $blocid . ",'" . $recupref->ref . "')");
                }
            }
        }

        $lastmaj = \DB::table('stats_maildrop')->max('bloc_maj');
        $consoliderstats = \DB::table('stats_maildrop')->where('bloc_maj',$lastmaj)->get();

        echo 'Partie consolidation' . "\n";

        foreach ($consoliderstats as $item)
        {
            $checkref = \DB::table('stats_maildrop_total')->where('reference',$item->reference)->where('bloc_maj',$lastmaj)->first(); // bloc maj

            if (is_null($checkref)) {
                \DB::statement("INSERT INTO stats_maildrop_total (reference, desinscriptions, ouvreurs, cliqueurs, erreurs, date_maj, bloc_maj) VALUES ('". $item->reference ."','" . $item->desinscriptions . "','" . $item->ouvreurs . "','" . $item->cliqueurs . "','" . $item->erreurs . "','" . time() . "','". $lastmaj ."')");
            } else {
                $totaldesinscriptions = $checkref->desinscriptions + $item->desinscriptions;
                $totalcliqueurs = $checkref->cliqueurs + $item->cliqueurs;
                $totalerreurs = $checkref->erreurs + $item->erreurs;
                $totalouvreurs = $checkref->ouvreurs + $item->ouvreurs;

                \DB::statement("UPDATE stats_maildrop_total SET desinscriptions ='" . $totaldesinscriptions . "' WHERE reference ='" . $item->reference . "' AND bloc_maj = '" . $lastmaj . "'");
                \DB::statement("UPDATE stats_maildrop_total SET cliqueurs ='" . $totalcliqueurs . "' WHERE reference ='" . $item->reference . "' AND bloc_maj = '" . $lastmaj . "'");
                \DB::statement("UPDATE stats_maildrop_total SET erreurs ='" . $totalerreurs . "' WHERE reference ='" . $item->reference . "' AND bloc_maj = '" . $lastmaj . "'");
                \DB::statement("UPDATE stats_maildrop_total SET ouvreurs ='" . $totalouvreurs . "' WHERE reference ='" . $item->reference . "' AND bloc_maj = '" . $lastmaj . "'");
            }
        }

        \Log::info('Récupération des statistiques maildrop OK');

        $valueblocmax = \DB::table('stats_maildrop_total')->max('bloc_maj');
        $statscampagneold = \DB::table('stats_maildrop_total')->where('bloc_maj',$valueblocmax - 1)->get();
        $statscampagne = \DB::table('stats_maildrop_total')->where('bloc_maj',$valueblocmax)->get();
        \Mail::send('stats.mailstats', ['statscampagne' => $statscampagne, 'statscampagneold' => $statscampagneold, 'type' => 'maildrop'], function ($m) use ($statscampagne) {
            $m->from('dev@lead-factory.net', 'Tor');
            $m->to('fabien@lead-factory.net', 'fabien')->subject(getenv('CLIENT_URL') . ' - Stats campagnes Maildrop ' . date('d-m-Y'));
            $m->cc('adeline.sc2@gmail.com');
        });

        \Log::info('Envoi du mail avec les stats maildrop OK');
    }
}
