<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class md5clear extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'md5:clear';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      $handle = fopen(storage_path('listes/listmanager/mars19advertiseme.csv'), 'r');
      while($ligne = fgets($handle))
        {
        // var_dump($ligne);
        $desti = \DB::table('destinataires')
        ->select('mail')
        ->where('hash',trim($ligne))
        ->first();

        // var_dump($desti);
        if($desti !== NULL){
        echo $desti->mail . "\n";
        }
       }
    }
}
