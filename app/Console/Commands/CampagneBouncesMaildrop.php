<?php

namespace App\Console\Commands;

use App\Classes\Routeurs\RouteurMaildrop;
use App\Helpers\Profiler;
use Illuminate\Console\Command;
use App\Models\Routeur;

class CampagneBouncesMaildrop extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'campagne:bounces_maildrop';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Ajoute les bounces Maildrop dans la bdd toutes bases confondues';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Profiler::start('bounces_maildrop');
        $routeur = new RouteurMaildrop();

        $today = date('Ymd');
        $two_weeks_ago = date('Y-m-d 00:00:00', strtotime('-1 week'));

        $maidlrop = Routeur::where('nom','Maildrop')->first();
        $senders = \DB::table('senders')
            ->select('id')
            ->where('routeur_id', $maidlrop->id)
            ->get();

        // taskid à 1 si des soucis
        $informations = \DB::table('campagnes_routeurs')
            ->where('created_at','>',$two_weeks_ago)
            ->whereIn('sender_id', array_pluck($senders, 'id'))
            ->get();

        $bounces = array();

        $compteur = 0;

        $file = storage_path() . '/bounces/bounces_md_' . $today . '.csv';

        $handle = fopen($file, 'w');

        $campagnes_error = array();
        $campagnes_error["laposte"] = array();
        $campagnes_error["free"] = array();
        $campagnes_error["orange"] = array();

        foreach ($informations as $lignecampagnerouteur)
        {
            \Log::info("CampagneBouncesMaildrop : Campagne $lignecampagnerouteur->campagne_id - Planning $lignecampagnerouteur->planning_id");
            if(empty($lignecampagnerouteur->cid_routeur)){
                \Log::info("CampagneBouncesMaildrop : empty cid_routeur (Campagne $lignecampagnerouteur->campagne_id - Planning $lignecampagnerouteur->planning_id)");
                continue;
            }

            $lesender = \DB::table('senders')->where('id', $lignecampagnerouteur->sender_id)->first();
            $lacampagne = \DB::table('campagnes')->where('id', $lignecampagnerouteur->campagne_id)->first();


            $get_bounces = $routeur->getBouncesNew($lesender->password, $lacampagne, $lignecampagnerouteur->cid_routeur);

            if(empty($get_bounces)){
                continue;
            }
            
            foreach ($get_bounces as $b)
            {
                //uniquement les hard bounces
                if($b->error_type == "INVALID_DOMAIN"
                    || $b->error_type == "MAILBOX_INACTIVE"
                    || $b->error_type == "USER_UNKNOWN")
                {
                    fwrite($handle , "$lacampagne->id;" . $b->email . ";" . $b->error_type . ";" . $b->reason . "\n");
                    $compteur++;
                }
//                    dd($b);

                if (strpos($b->reason, 'LPN007_405') !== FALSE) {
                    if(!isset($campagnes_error["laposte"][$lacampagne->ref])){
                        $campagnes_error["laposte"][$lacampagne->ref] = array();
                    }
//                    echo "\nLPN007_405";
//                    print_r($b);
                    if(in_array($lesender->nom, $campagnes_error["laposte"][$lacampagne->ref])) {
                        continue;
                    }
                    $campagnes_error["laposte"][$lacampagne->ref][] = $lesender->nom;
                }

                if (strpos($b->reason, '550 spam detected') !== FALSE && strpos($b->email, '@free.fr') !== FALSE) {
                    if(!isset($campagnes_error["free"][$lacampagne->ref])){
                        $campagnes_error["free"][$lacampagne->ref] = array();
                    }
//                    echo "\n550 spam detected free";
//                    print_r($b);
                    if(in_array($lesender->nom, $campagnes_error["free"][$lacampagne->ref])) {
                        continue;
                    }
                    $campagnes_error["free"][$lacampagne->ref][] = $lesender->nom;
                }

                /* TODO : a de-commenter si orange de nouveau utilise
                if (strpos($b->reason, 'OFR006_103') !== FALSE) {
                    if(!isset($campagnes_error["orange"][$lacampagne->ref])){
                        $campagnes_error["orange"][$lacampagne->ref] = array();
                    }
//                    echo "\nOFR006_103";
//                    print_r($b);
                    if(in_array($lesender->nom, $campagnes_error["orange"][$lacampagne->ref])) {
                        continue;
                    }
                    $campagnes_error["orange"][$lacampagne->ref][] = $lesender->nom;
                }
                if (strpos($b->reason, 'OFR006_102') !== FALSE) {
                    if(!isset($campagnes_error["orange"][$lacampagne->ref])){
                        $campagnes_error["orange"][$lacampagne->ref] = array();
                    }
//                    echo "\nOFR006_102";
//                    print_r($b);
                    if(in_array($lesender->nom, $campagnes_error["orange"][$lacampagne->ref])) {
                        continue;
                    }
                    $campagnes_error["orange"][$lacampagne->ref][] = $lesender->nom;
                }

                */
            }

        }

        fclose($handle);

        if(!empty($campagnes_error)) {
            \Mail::send('mail.mailsendersprob',
                [
                    'campagnes_error' => $campagnes_error,
                    'email_date' => \Carbon\Carbon::now(),
                    'url' => getenv('CLIENT_URL')
                ], function ($message) {
                    $destinataires = \App\Models\User::where('is_valid', 1)
                        ->where('user_group_id', 1)
                        ->where('email', '!=', "")
                        ->get();
                    $message->from(getenv('MAIL_USERNAME'), 'Tor')->subject("Rapport MD : comptes problématiques (/FAI)");
                    foreach ($destinataires as $d) {
                        $message->to($d->email);
                    }
                });
                \Log::info('CampagneBouncesMaildrop : mail with senders pb sent.');
        }

        $report = Profiler::report('bounces_maildrop');
        \Log::info("CampagneBouncesMaildrop : end report $report");

        echo "Lancement import fichier \n";

        \Log::info("[import_bounces] du fichier bounces MD" . "[bounces_md_" . $today . ".csv]");
        \Artisan::call('base:import_bounces', ['file' => "bounces_md_" . $today . ".csv"]);
    }
}
