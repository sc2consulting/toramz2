<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\DB;
use Carbon\Carbon;
use Symfony\Component\Console\Input\InputArgument;

class BaseExport extends Command
{
    protected $name ='base:export {base_id}';

    protected $description = 'Export mail from a base to file';
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
        parent::__construct();
    }

    /**
     * Execute the command.
     *
     * @return void
     */
    public function handle()
    {
        set_time_limit(0);
        ini_set('max_execution_time', 0);

        \DB::disableQueryLog();

        $id = $this->argument('base_id');
        $today =  Carbon::today()->format('Ymd');
        $filename = storage_path() . "/listes/base-$id-$today.csv";
        $ressource_fichier = fopen($filename, 'w+');
        \DB::table('destinataires')
            ->select('mail')
            ->where('base_id', $id)
            ->where('statut', '<', 3)
            ->chunk(50000, function ($emailbase) use ($ressource_fichier) {
                foreach ($emailbase as $e) {
                    fputs($ressource_fichier, $e->mail . "\n");
                }
            });
		echo "\nExtract finished (file:$filename)";
    }
    protected function getArguments(){
        return [
            ['base_id',InputArgument::OPTIONAL,'Base id'],
        ];
    }
}
