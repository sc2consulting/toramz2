<?php

namespace App\Console\Commands;

use App\Jobs\JobTest;
use Illuminate\Console\Command;

class CommandBis extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'CommandBis {varint}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        \Log::info('Confirm Command Launch');
        JobTest::dispatch(3)->onQueue('tube' . rand(1, 4));
    }

}
