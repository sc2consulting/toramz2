<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Classes\Routeurs\RouteurMindbaz;
use App\Models\Sender;

class StatsRouteurMindbaz extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'stats:majmindbaz';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command qui met a jour les stats MB';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        \Log::info('Debut du calcul des statistiques Maildrop');

        $routeur = new RouteurMindbaz();

        $lerouteurid = \DB::table('routeurs')
            ->where('nom','Mindbaz')
            ->first();

        $mindbaz_senders = \DB::table('senders')
            ->where('routeur_id', $lerouteurid->id)
            ->get();

        $infobase = \DB::table('campagnes_routeurs')
            ->join('plannings', 'campagnes_routeurs.planning_id', '=', 'plannings.id')
            ->select('campagnes_routeurs.cid_routeur', 'campagnes_routeurs.sender_id', 'campagnes_routeurs.campagne_id', 'campagnes_routeurs.planning_id')
            ->whereNotNull('plannings.sent_at')
            ->where('campagnes_routeurs.created_at','>',date('Y-m-d H:i:s',time()-604800))
            ->whereIn('campagnes_routeurs.sender_id', array_pluck($mindbaz_senders, 'id'))
            ->distinct('campagnes_routeurs.cid_routeur')
            ->get();

        $id = null;
        $password = null;

        $blocid = \DB::table('stats_mindbaz')->max('bloc_maj');
        $blocid = $blocid + 1;

        foreach ($infobase as $datadb)
        {

            $ts = date('Y-m-d H:i:s');
            $sender = Sender::find($datadb->sender_id);

            $planning_id = $datadb->planning_id;
            $cid_routeur = $datadb->cid_routeur;

            $recupref = \DB::table('campagnes')
                ->select('ref')
                ->where('id',$datadb->campagne_id)
                ->first();


            if(empty($recupref)) {
                continue;
            }

            $ref = $recupref->ref;
            $sent = 0;
            $openers = 0;
            $clickers = 0;
            $clickers_unsub = 0;
            $soft_bounces = 0;
            $hard_bounces = 0;
            $spam_bounces = 0;
            $spam_complaints = 0;
            $unsubs = 0;

            echo 'Calcul pour la campagne : ' . $recupref->ref . ' CID :  ' . $datadb->cid_routeur . "\n";

            //TODO : test stats to uncomment when ok
            $stats = $routeur->recup_stats($sender, $datadb->cid_routeur);

            if(empty($stats)){
                echo " empty stats";
                continue;
            }

            foreach($stats as $s)
            {
//                var_dump($s);
                $sent += $s->nbAddressSent;
                $openers += $s->nbOpeners;
                $clickers += $s->nbClickers;
                $clickers_unsub += $s->nbClickers_unsub;
                $soft_bounces += $s->nbSoftBounces;
                $hard_bounces += $s->nbHardBounces;
                $spam_bounces += $s->nbSpamBounces;
                $spam_complaints += $s->nbSpamComplaints;
                $unsubs += $s->nbUnsubs;
            }

            \DB::statement("INSERT INTO stats_mindbaz (planning_id, cid_routeur, reference, total_sent, openers, clickers, soft_bounces, hard_bounces, spam_bounces, spam_complaints, unsubscribers, date_maj, bloc_maj, created_at, updated_at)
            VALUES (" . $planning_id . "," . $cid_routeur . ",'" . $ref . "'," . $sent . "," . $openers. "," . ($clickers - $clickers_unsub). "," . $soft_bounces. "," . $hard_bounces . "," . $spam_bounces . "," . $spam_complaints . "," . $unsubs . "," . time() . "," . $blocid . ",'" .$ts  . "', '". $ts ."' )");

        }

        $lastmaj = \DB::table('stats_mindbaz')->max('bloc_maj');
        $consoliderstats = \DB::table('stats_mindbaz')->where('bloc_maj',$lastmaj)->get();

        echo 'Partie consolidation' . "\n";

        foreach ($consoliderstats as $item)
        {
            $checkref = \DB::table('stats_mindbaz_total')->where('reference',$item->reference)->where('bloc_maj',$lastmaj)->first(); // bloc maj
            if (is_null($checkref)) {
                \DB::statement("INSERT INTO stats_mindbaz_total (reference, unsubscribers, openers, clickers, hard_bounces, soft_bounces, spam_bounces, spam_complaints, total_sent, date_maj, bloc_maj) VALUES ('". $item->reference ."','" . $item->unsubscribers . "','" . $item->openers . "','" . $item->clickers . "','" . $item->hard_bounces . "','". $item->soft_bounces . "','". $item->spam_bounces ."','". $item->spam_complaints . "','". $item->total_sent . "','" . time() . "','". $lastmaj ."')");
            } else {
                $totalsent = $checkref->total_sent + $item->total_sent;
                $totaldesinscriptions = $checkref->unsubscribers + $item->unsubscribers;
                $totalcliqueurs = $checkref->clickers + $item->clickers;
                $totalhardbounces = $checkref->hard_bounces + $item->hard_bounces;
                $totalouvreurs = $checkref->openers + $item->openers;
                $totalspambounces = $checkref->spam_bounces + $item->spam_bounces;
                $totalspamcomplaints = $checkref->spam_complaints + $item->spam_complaints;
                $totalsoftbounces = $checkref->soft_bounces + $item->soft_bounces;

                \DB::statement("UPDATE stats_mindbaz_total SET unsubscribers ='" . $totaldesinscriptions . "' WHERE reference ='" . $item->reference . "' AND bloc_maj = '" . $lastmaj . "'");
                \DB::statement("UPDATE stats_mindbaz_total SET clickers ='" . $totalcliqueurs . "' WHERE reference ='" . $item->reference . "' AND bloc_maj = '" . $lastmaj . "'");
                \DB::statement("UPDATE stats_mindbaz_total SET hard_bounces ='" . $totalhardbounces . "' WHERE reference ='" . $item->reference . "' AND bloc_maj = '" . $lastmaj . "'");
                \DB::statement("UPDATE stats_mindbaz_total SET openers ='" . $totalouvreurs . "' WHERE reference ='" . $item->reference . "' AND bloc_maj = '" . $lastmaj . "'");
                \DB::statement("UPDATE stats_mindbaz_total SET soft_bounces ='" . $totalsoftbounces . "' WHERE reference ='" . $item->reference . "' AND bloc_maj = '" . $lastmaj . "'");
                \DB::statement("UPDATE stats_mindbaz_total SET spam_bounces ='" . $totalspambounces . "' WHERE reference ='" . $item->reference . "' AND bloc_maj = '" . $lastmaj . "'");
                \DB::statement("UPDATE stats_mindbaz_total SET spam_complaints ='" . $totalspamcomplaints . "' WHERE reference ='" . $item->reference . "' AND bloc_maj = '" . $lastmaj . "'");
                \DB::statement("UPDATE stats_mindbaz_total SET total_sent ='" . $totalsent . "' WHERE reference ='" . $item->reference . "' AND bloc_maj = '" . $lastmaj . "'");
            }
        }
//
        \Log::info('Récupération des statistiques Mindbaz OK');

//        $valueblocmax = \DB::table('stats_mindbaz_total')->max('bloc_maj');
//        $statscampagneold = \DB::table('stats_mindbaz_total')->where('bloc_maj',$valueblocmax - 1)->get();
//        $statscampagne = \DB::table('stats_mindbaz_total')->where('bloc_maj',$valueblocmax)->get();
//        \Mail::send('stats.mailstats', ['statscampagne' => $statscampagne, 'statscampagneold' => $statscampagneold, 'type' => 'mindbaz'], function ($m) use ($statscampagne) {
//        $m->from('dev@lead-factory.net', 'Tor');
//        $m->to('fabien@lead-factory.net', 'fabien')->subject(getenv('CLIENT_URL') . ' - Stats campagnes Mindbaz ' . date('d-m-Y'));
//        $m->cc('adeline.sc2@gmail.com');
//        });

//        \Log::info('Envoi du mail avec les stats Mindbaz OK');


    }
}
