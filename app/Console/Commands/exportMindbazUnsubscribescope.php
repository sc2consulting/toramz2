<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Classes\Routeurs\RouteurMindbaz;
use App\Models\Routeur;
use App\Models\Sender;


class exportMindbazUnsubscribescope extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'export:mb_unsubscribescope {base_id} {sender_id} {offset} {limit}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Export mindbaz via emails tor';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

      // ini_set('soap.wsdl_cache_enabled', '0');
      // ini_set('soap.wsdl_cache_ttl', '0');
      // exec('ulimit -S -n 2048');


       var_dump($this->argument('base_id'));

       $mindbaz = new RouteurMindbaz();
       $routeur = Routeur::where('nom','Mindbaz')->first();
       $senders_mindbaz = Sender::where('id', $this->argument('sender_id'))->first();
       $today = date("Ymd");
       $sleep = 0;
       // attention v1 petites bases
       echo "Lancement du script \n";

       $chaineUnsu = "";
       $chaineNpai = "";

       $destinataires = \DB::table('destinataires')
       ->select('id')
       ->where('base_id',$this->argument('base_id'))
       ->where('statut', '<', 3)
       ->offset($this->argument('offset'))
       ->limit($this->argument('limit'))
       ->get();

       $destinataires = array_pluck($destinataires, 'id');
       $counter = 0;
       // die();

       $destinataires = array_map('strval',$destinataires);

       $mailschunk = array_chunk($destinataires, 1000);

       // var_dump($mailschunk);

       foreach ($mailschunk as $arraychunked) {


         $chaineUnsu = '';
         $chaineNpai = '';

         $r = $mindbaz->get_subscriberbyidclient_all($senders_mindbaz,$arraychunked);
         foreach ($r as $arrayresult) {

           foreach ($arrayresult as $ar) {
             $counter = $counter + 1;

            echo "C_" . $counter . "\n";
             // var_dump($counter);
             var_dump($ar->fld[1]->value);
             // var_dump($ar->fld[4]->value);
             if($ar->fld[4]->value != NULL){
               $chaineUnsu .= $ar->fld[1]->value . "\n";
             }
             if($ar->fld[7]->value == 7){
               $chaineNpai .= $ar->fld[1]->value . "\n";
             }

           }
          }

          /*

          $file = storage_path() . '/desinscrits/unsubscribe_mb_api_' . $today . '.csv';
          $fp = fopen($file,"w+");
          fwrite($fp, $chaineUnsu);
          fclose($fp);

          \Log::info("[CampagneUnsubscribeMindbaz] : launch of BaseImportDesinscrits command");
          \Artisan::call('base:import_desinscritsone', ['file' => "unsubscribe_mb_api_$today.csv",'base_id' => $this->argument('base_id')]);
          \Log::info("[CampagneUnsubscribeMindbaz] : launch of BaseImportDesinscrits command OK");
          unset($chaineUnsu);


          $file = storage_path() . '/desinscrits/hardbounce_mb_api_' . $today . '.csv';
          $fp = fopen($file,"w+");
          fwrite($fp, $chaineNpai);
          fclose($fp);
          unset($chaineNpai);
          \Log::info("[CampagneUnsubscribeMindbaz] : launch of BaseImportBounces command");
          \Artisan::call('base:import_hbounceone', ['file' => "hardbounce_mb_api_$today.csv",'base_id' => $this->argument('base_id')]);
          \Log::info("[CampagneUnsubscribeMindbaz] : launch of BaseImportBounces command OK");



          for ($i=0; $i < 25; $i++) {
            echo "Sleep" . $i . "\n";
            sleep(1);
          }

          */

          $file = storage_path() . '/desinscrits/unsubscribe_mb_api_' . $today . '.csv';
          $fp = fopen($file,"a+");
          fwrite($fp, $chaineUnsu);
          fclose($fp);
          unset($chaineUnsu);

          $file = storage_path() . '/desinscrits/hardbounce_mb_api_' . $today . '.csv';
          $fp = fopen($file,"a+");
          fwrite($fp, $chaineNpai);
          fclose($fp);
          unset($chaineNpai);

       }

       \Log::info("[CampagneUnsubscribeMindbaz] : launch of BaseImportDesinscrits command");
       \Artisan::call('base:import_desinscritsone', ['file' => "unsubscribe_mb_api_$today.csv",'base_id' => $this->argument('base_id')]);
       \Log::info("[CampagneUnsubscribeMindbaz] : launch of BaseImportDesinscrits command OK");


       \Log::info("[CampagneUnsubscribeMindbaz] : launch of BaseImportBounces command");
       \Artisan::call('base:import_hbounceone', ['file' => "hardbounce_mb_api_$today.csv",'base_id' => $this->argument('base_id')]);
       \Log::info("[CampagneUnsubscribeMindbaz] : launch of BaseImportBounces command OK");

       /*
       $file = storage_path() . '/desinscrits/unsubscribe_mb_api_' . $today . '.csv';
       $fp = fopen($file,"w+");
       fclose($fp);
       $fp = fopen($file,"a+");
       fwrite($fp, $chaineUnsu);
       fclose($fp);

       \Log::info("[CampagneUnsubscribeMindbaz] : launch of BaseImportDesinscrits command");
       \Artisan::call('base:import_desinscritsone', ['file' => "unsubscribe_mb_api_$today.csv",'base_id' => $this->argument('base_id')]);
       \Log::info("[CampagneUnsubscribeMindbaz] : launch of BaseImportDesinscrits command OK");

       $file = storage_path() . '/desinscrits/hardbounce_mb_api_' . $today . '.csv';
       $fp = fopen($file,"w+");
       fclose($fp);
       $fp = fopen($file,"a+");
       fwrite($fp, $chaineNpai);
       fclose($fp);

       \Log::info("[CampagneUnsubscribeMindbaz] : launch of BaseImportBounces command");
       \Artisan::call('base:import_hbounceone', ['file' => "hardbounce_mb_api_$today.csv",'base_id' => $this->argument('base_id')]);
       \Log::info("[CampagneUnsubscribeMindbaz] : launch of BaseImportBounces command OK");
       */

    }

    protected function getArguments()
    {
        return [
            ['base_id', InputArgument::REQUIRED, 'Base'],
            ['sender_id', InputArgument::REQUIRED, 'Sender'],
            ['url', InputArgument::REQUIRED, 'Link'],
            ['limit', InputArgument::REQUIRED, 'limit'],
            ['offset', InputArgument::REQUIRED, 'offset'],
        ];
    }
}
