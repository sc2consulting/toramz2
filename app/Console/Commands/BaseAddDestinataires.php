<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputArgument;
use App\Models\Base;

class BaseAddDestinataires extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'base:add_destinataires';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Ajoute des destinataires depuis un fichier';

	/**
	 * Create a new command instance.ha voilà
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
        \DB::disableQueryLog();

        $ts = date('Y-m-d H:i:s');

        \App\Helpers\Profiler::start('import');

        $pdo = \DB::connection()->getPdo();
        $sql = "INSERT IGNORE INTO destinataires (mail, base_id, fai_id, created_at, updated_at) VALUES (:mail, :base_id, :fai_id, :ts1, :ts2)";
        $stmt = $pdo->prepare($sql);
        $pdo->beginTransaction();
        $bulk_count = 0;

        $fais = \App\Models\Fai::all();
        $default_fai = \App\Models\Fai::find(6);

        $base_id = $this->argument('base_id');
        $file = storage_path().'/listes/'.$this->argument('file');

        if (!is_file($file)) {
            $this->error('File not found : '.$file);
        }

        \Log::info('BASE ID: '.$base_id);


        set_time_limit(0);
        ini_set('max_execution_time', 0);

        $base = Base::find($base_id);

        $extension = \File::extension($file);
        $name = \File::name($file);

        $fh = fopen($file, 'r');

        $total = 0;
        $inserted = 0;
        $rejected = 0;
        $already = 0;


        $base_id = $base->id;
        $liste = str_replace(".$extension",'',$name);

        $bulk = [];
        while (!feof($fh)) {
            $cells = [];

            $row = fgets($fh, 1024);
            $row = trim($row, "\r\n\t ");

            if (strpos($row, ';') !== false) {
                $cells = explode(';', $row);
            } elseif (strpos($row, '|') !== false) {
                $cells = explode('|', $row);
            } else {
                $cells[] = trim($row, " \r\n\t\"");
            }

            foreach($cells as $cell) {
                $fai = $default_fai;

                if (filter_var($cell, FILTER_VALIDATE_EMAIL)) {
                    $total++;

                    foreach($fais as $k_fai) {
                        foreach($k_fai->get_domains() as $dom) {
                            if (strpos($cell,$dom) !== false) {
                                $fai = $k_fai;
                                break 2;
                            }
                        }
                    }

                    if (!isset($fai->nom)) {
                        var_dump($fai);
                        var_dump($cell);
                        die();
                    }

                    $insertData = [$cell, $base_id, $fai->id, $ts, $ts];
                    $stmt->execute($insertData);

                    $bulk_count++;
                    if ($bulk_count >= 250) {
                        $pdo->commit();
                        $pdo->beginTransaction();
                        $bulk_count = 0;
                    }

                    $inserted++;
                    continue;
                } else {
                    $rejected++;
                }
            }
        }

        $pdo->commit();

        if (isset(\Auth::User()->id)) {
            $user_id = \Auth::User()->id;
        } else {
            $user_id = 1;
        }

        \App\Models\Notification::create([
            'user_id' => $user_id,
            'level' => 'info',
            'is_important' => true,
            'message' => "Fichier $name importé. $inserted insérés, $already déjà présents, $rejected rejetés."
        ]);

       \App\Helpers\Profiler::report();

	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return [
			['base_id', InputArgument::REQUIRED, 'Base ID'],
			['file', InputArgument::REQUIRED, 'Path to the file'],
		];
	}

}
