<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class ComputePixel extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'pixel:compute';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Compute Pixel informations.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{

        $pixels = \App\Models\Ouverture::whereRaw('created_at > now() - interval 1 day')->get();

        foreach($pixels as $pixel)
        {

            echo $pixel->destinataire->mail."\n";

            $pixel->destinataire->opened_at = $pixel->created_at;
            $pixel->destinataire->save();


        }


	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return [
//			['example', InputArgument::REQUIRED, 'An example argument.'],
		];
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return [
//			['example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null],
		];
	}

}
