<?php


namespace App\Console\Commands;


use App\Jobs\JobTest;
use Illuminate\Console\Command;

class ListingMail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ListingMail';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command qui list les mails et envoie en base les email non passés ';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $server = '{ssl0.ovh.net:143}INBOX.INBOX.amz';
        $username = 'fabien@lead-factory.net';
        $password = 'aqwzsxedc01';
        $mailbox = imap_open($server , $username, $password);
        $mails = FALSE;
        if (FALSE === $mailbox) {
            $err = 'La connexion a échoué. Vérifiez vos paramètres!';
        } else {
            $info = imap_check($mailbox);
            if (FALSE !== $info) {
                // le nombre de messages affichés est entre 1 et 50
                // libre à vous de modifier ce paramètre
                $nbMessages = min(50, $info->Nmsgs);
                $mails = imap_fetch_overview($mailbox, '1:'.$info->Nmsgs, 0);
            } else {
                $err = 'Impossible de lire le contenu de la boite mail';
            }
        }
        if (FALSE === $mails) {
            echo $err;
        } else {
            $informationboite = 'La boite aux lettres contient '.$info->Nmsgs.' message(s) dont '. $info->Recent.' recent(s)';
            $mailFailure = array();
            $index = 0;
            foreach ($mails as $mail) {
                $index++;
                $date="";
                if(property_exists($mail,'date'))
                {
                    $date = $mail->date;
                }
                $this->info( 'Objet : ' . imap_utf8($mail->subject) . ' Date de réception : ' . $date . '');
                if(imap_utf8($mail->subject) == "Delivery Status Notification (Failure)" || imap_utf8($mail->subject) == "[SPAM] Delivery Status Notification (Failure)")
                {
                    array_push($mailFailure, $index);
                }
            }
            foreach($mailFailure as $mailToDo) {
                $this->info("# Récupération des Informations");
                $textSplit = explode("\n", imap_utf8(imap_body($mailbox, $mailToDo)));
                $emailAdress = ""; $DiagCode = ""; $IdPlanning = "1";
                foreach ($textSplit as $line) {
                    if (preg_match("/^Final-Recipient/", $line)) {
                        if(count(explode('; ', $line)) == 2)
                        {
                            $emailAdress = explode("; ", $line)[1];
                        }
                        else
                        {
                            $emailAdress = "Informations non renseignée (Email)";
                        }
                    }
                    if (preg_match("/^Diagnostic-Code/", $line)) {
                        if(count(explode('; ', $line)) == 2)
                        {
                            $DiagCode = explode('; ', $line)[1];
                        }
                        else
                        {
                            $DiagCode = "Informations non renseignée (Code Erreur)";
                        }
                    }
                    if ($emailAdress != "" && $DiagCode != "") {
                        $this->info("Informations Remplies");
                        $this->info("Email : " . $emailAdress);
                        $this->info("Code : " . $DiagCode);
                        $this->info("Id du Planning : ". $IdPlanning);
                        //if(!\DB::table('mails_failure')->where('id_planning', $IdPlanning)->where('email_address', $emailAdress)->first())
                            \DB::table('mails_failure')->insert(['email_address' => $emailAdress, 'error_msg' => $DiagCode, 'id_planning' => $IdPlanning]);
                        if(\DB::table('planning_percent_failure')->where('id_planning', $IdPlanning)->first())
                        {
                            \DB::table('planning_percent_failure')->where('id_planning', $IdPlanning)->update(
                                ['volume_failure' => \DB::table('planning_percent_failure')->where('id_planning', $IdPlanning)->first()->volume_failure + 1,
                                 'percent_failure' => (\DB::table('planning_percent_failure')->where('id_planning', $IdPlanning)->first()->volume_failure/\DB::table('planning_percent_failure')->where('id_planning', $IdPlanning)->first()->volume_sent) * 100, ]);
                        }
                        break;
                    }
                }
            }
            //$mailFailure = implode(',', $mailFailure);
            //$this->info($mailFailure);
            //$this->info(imap_mail_move($mailbox, $mailFailure, 'INBOX.INBOX.TestFailure', CP_MOVE));
        }
    }
}
