<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Classes\Routeurs\RouteurMailForYou;
use App\Models\Routeur;

class StatsRouteurMailforyou extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'stats:majmailforyou';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command qui met a jour les stats M4Y';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $routeur = new RouteurMailForYou();

        $tableau_sender_id = array();
        // $batchid = array();
        $routeurm4y = Routeur::where('nom','MailForYou')->first();
        $senderm4y = \DB::table('senders')->select('id')->where('routeur_id',$routeurm4y->id)->get();
        foreach ($senderm4y as $val) {
            $tableau_sender_id[] = $val->id;
        }
        $row_a_traiter = \DB::table('campagnes_routeurs')
            ->whereIn('sender_id', $tableau_sender_id)
            ->where('taskid','1')
            ->where('created_at','>',date('Y-m-d H:i:s',time()-345600))
            ->get();

        $lastmaj = \DB::table('stats_m4y')->max('bloc_maj');
        if(!isset($lastmaj)){
            $lastmaj = 1;
        } else {
            $lastmaj = $lastmaj + 1;
        } // on fait +1

        foreach ($row_a_traiter as $r) {
            $touv = array();
            $tebounces = array();

            $dump = $routeur->LecturePlanificationRoutage_new($r->cid_routeur,$r->cid_routeur,$r->sender_id);
            if(isset($dump->records[0])){
                var_dump('Le planning ' . $r->planning_id);
                $lacampagne = \DB::table('campagnes')->where('id',$r->campagne_id)->first();
                if(isset($lacampagne)){
                    var_dump('La campagne ref ' . $lacampagne->ref);
                    $totalb = $dump->records[0][7] + $dump->records[0][8];
                    // var_dump('Total : ' . $dump->records[0][2]);
                    $aboutis = ($totalb/$dump->records[0][2]) * 100;
                    // inverser stats
                    $aboutisreel = 100-$aboutis;
                    echo "Aboutis % : " . round($aboutisreel, 2) . "\n";


                    \Log::info("Lancement appel API");
                    // MAJ FAI
                    $d = $routeur->LectureStatsRoutage_new($r->cid_routeur,$r->sender_id);
                    \Log::info("Appel API termined");

                    $nbbfree = 0 ;
                    $nbbsfr = 0;
                    $nbblaposte = 0;
                    $nbbautre = 0;
                    $totalfree = 0;
                    $totalsfr = 0;
                    $totallaposte = 0;
                    $totalautre = 0;

                    // add
                    $nbborange = 0;
                    $totalorange = 0;
                    $per_cent_orange = 0;
                    $per_cent_orange_ouv = 0;

                    $totalhotmail = 0;
                    $totalaol = 0;
                    $totalbbox = 0;

                    $per_cent_free = 0;
                    $per_cent_sfr = 0;
                    $per_cent_laposte = 0;
                    $per_cent_autre = 0;

                    $per_cent_sfr_ouv = 0;
                    $per_cent_free_ouv = 0;
                    $per_cent_laposte_ouv = 0;
                    $per_cent_autre_ouv = 0;


                    if(isset($d)){
                        foreach ($d->records as $k => $v) {
                            if($v[2] == 0){
                                $teok[] = $v;

                                if($v[3] == 1){
                                    $touv[] = $v;
                                }

                            } else {
                                $tebounces[] = $v;
                            }
                        }


                        \Log::info("Remplissage tableau bounces FAI");
                        foreach ($tebounces as $tebkey => $tebv) {
                            // TODO
                            // foreach tab fai
                            // on recup la key value
                            if(strstr($tebv[1],'@free.fr') || strstr($tebv[1],'@freesbee.fr') || strstr($tebv[1],'@libertysurf.fr') || strstr($tebv[1],'@worldonline.fr') || strstr($tebv[1],'@online.fr') || strstr($tebv[1],'@alicepro.fr') || strstr($tebv[1],'@aliceadsl.fr')){
                                $tresult['bounces']['free'][] = $tebv[1];
                                continue;
                            }

                            if(strstr($tebv[1],'@sfr.fr') || strstr($tebv[1],'@neuf.fr') || strstr($tebv[1],'@cegetel.fr')){
                                $tresult['bounces']['sfr'][] = $tebv[1];
                                continue;
                            }

                            if(strstr($tebv[1],'@laposte.net')){
                                $tresult['bounces']['laposte'][] = $tebv[1];
                                continue;
                            }

                            if(strstr($tebv[1],'@orange.fr') || strstr($tebv[1],'@wanadoo.fr') || strstr($tebv[1],'@voila.fr')){
                                $tresult['bounces']['orange'][] = $tebv[1];
                                continue;
                            }

                            /*
                            if(strstr($tebv[1],'@aol.fr')){
                                $tresult['bounces']['aol'][] = $tebv[1];
                                continue;
                            }

                            if(strstr($tebv[1],'@hotmail.fr') || strstr($tebv[1],'@hotmail.com')){
                                $tresult['bounces']['hotmail'][] = $tebv[1];
                                continue;
                            }

                            if(strstr($tebv[1],'@bbox.fr')){
                                $tresult['bounces']['bbox'][] = $tebv[1];
                                continue;
                            }
                            */

                            $tresult['bounces']['autre'][] = $tebv[1];

                        }

                        \Log::info("Remplissage tableau adresses OK FAI");

                        foreach ($teok as $teokkey => $teokv) {

                            if(strstr($teokv[1],'@free.fr') || strstr($teokv[1],'@freesbee.fr') || strstr($teokv[1],'@libertysurf.fr') || strstr($teokv[1],'@worldonline.fr') || strstr($teokv[1],'@online.fr') || strstr($teokv[1],'@alicepro.fr') || strstr($teokv[1],'@aliceadsl.fr')){
                                $tresult['abouti']['free'][] = $teokv[1];
                                continue;
                            }

                            if(strstr($teokv[1],'@sfr.fr') || strstr($teokv[1],'@neuf.fr') || strstr($teokv[1],'@cegetel.fr')){
                                $tresult['abouti']['sfr'][] = $teokv[1];
                                continue;
                            }

                            if(strstr($teokv[1],'@laposte.net')){
                                $tresult['abouti']['laposte'][] = $teokv[1];
                                continue;
                            }


                            if(strstr($teokv[1],'@orange.fr') || strstr($teokv[1],'@wanadoo.fr') || strstr($teokv[1],'@voila.fr')){
                                $tresult['abouti']['orange'][] = $teokv[1];
                                continue;
                            }

                            /*
                            if(strstr($teokv[1],'@aol.fr')){
                                $tresult['abouti']['aol'][] = $teokv[1];
                                continue;
                            }

                            if(strstr($teokv[1],'@hotmail.fr') || strstr($teokv[1],'@hotmail.com')){
                                $tresult['abouti']['hotmail'][] = $teokv[1];
                                continue;
                            }

                            if(strstr($teokv[1],'@bbox.fr')){
                                $tresult['abouti']['bbox'][] = $teokv[1];
                                continue;
                            }
                            */
                            $tresult['abouti']['autre'][] = $teokv[1];
                        }

                        \Log::info("Remplissage tableau ouv FAI");

                        foreach ($touv as $ouv) {

                            if(strstr($ouv[1],'@free.fr') || strstr($ouv[1],'@freesbee.fr') || strstr($ouv[1],'@libertysurf.fr') || strstr($ouv[1],'@worldonline.fr') || strstr($ouv[1],'@online.fr') || strstr($ouv[1],'@alicepro.fr') || strstr($ouv[1],'@aliceadsl.fr')){
                                $tresult['ouv']['free'][] = $ouv[1];
                                continue;
                            }

                            if(strstr($ouv[1],'@sfr.fr') || strstr($ouv[1],'@neuf.fr') || strstr($ouv[1],'@cegetel.fr')){
                                $tresult['ouv']['sfr'][] = $ouv[1];
                                continue;
                            }

                            if(strstr($ouv[1],'@laposte.net')){
                                $tresult['ouv']['laposte'][] = $ouv[1];
                                continue;
                            }

                            if(strstr($ouv[1],'@orange.fr') || strstr($ouv[1],'@wanadoo.fr') || strstr($ouv[1],'@voila.fr')){
                                $tresult['ouv']['orange'][] = $ouv[1];
                                continue;
                            }

                            /*
                            if(strstr($ouv[1],'@aol.fr')){
                                $tresult['ouv']['aol'][] = $ouv[1];
                                continue;
                            }

                            if(strstr($ouv[1],'@hotmail.fr') || strstr($ouv[1],'@hotmail.com')){
                                $tresult['ouv']['hotmail'][] = $ouv[1];
                                continue;
                            }

                            if(strstr($ouv[1],'@bbox.fr')){
                                $tresult['ouv']['bbox'][] = $ouv[1];
                                continue;
                            }
                            */
                            $tresult['ouv']['autre'][] = $ouv[1];
                        }

                        \Log::info("Count tableau ouv FAI");
                        if(isset($tresult['ouv']['free'])){
                            $nbrouvfree = count($tresult['ouv']['free']);
                        } else {
                            $nbrouvfree = 0;

                        }
                        if(isset($tresult['ouv']['sfr'])){
                            $nbrouvsfr = count($tresult['ouv']['sfr']);
                        } else {
                            $nbrouvsfr = 0;

                        }
                        if(isset($tresult['ouv']['laposte'])){
                            $nbrouvlaposte = count($tresult['ouv']['laposte']);
                        } else {
                            $nbrouvlaposte = 0;

                        }

                        if(isset($tresult['ouv']['orange'])){
                            $nbrouvorange = count($tresult['ouv']['orange']);
                        } else {
                            $nbrouvorange = 0;
                        }
                        /*
                        if(isset($tresult['ouv']['aol'])){
                            $nbrouvaol = count($tresult['ouv']['aol']);
                        } else {
                            $nbrouvaol = 0;

                        }

                        if(isset($tresult['ouv']['hotmail'])){
                            $nbrouvhotmail = count($tresult['ouv']['hotmail']);
                        } else {
                            $nbrouvhotmail = 0;

                        }

                        if(isset($tresult['ouv']['bbox'])){
                            $nbrouvbbox = count($tresult['ouv']['bbox']);
                        } else {
                            $nbrouvbbox = 0;

                        }
                        */
                        if(isset($tresult['ouv']['autre'])){
                            $nbrouvautre = count($tresult['ouv']['autre']);
                        } else {
                            $nbrouvautre = 0;

                        }

                        // echo "Count Ouv Free " . $nbrouvfree . "\n";
                        // echo "Count Ouv SFR " . $nbrouvsfr . "\n";
                        // echo "Count Ouv laposte " . $nbrouvlaposte . "\n";
                        // echo "Count Ouv autre " . $nbrouvautre . "\n";

                        // flag here
                        // autre manière d'avoir le total ?


                        \Log::info("Calcul % tableau Free");

                        if(isset($tresult['bounces']['free'])){

                          if(!isset($tresult['abouti']['free'])){
                            $totalfree = count($tresult['bounces']['free']);
                            echo "0% free \n";
                            $per_cent_free = 0;
                            $nbbfree = 0;
                          } else {
                            $totalfree = count($tresult['bounces']['free']) + count($tresult['abouti']['free']);
                            $nbbfree = count($tresult['bounces']['free']);
                            $per_cent_free = $nbbfree / $totalfree * 100;
                            $per_cent_free = round(100 - $per_cent_free, 2);

                          }
                        }


                        \Log::info("Calcul % tableau SFR");
                        if(isset($tresult['bounces']['sfr'])){


                          if(!isset($tresult['abouti']['sfr'])){
                            $totalsfr = count($tresult['bounces']['sfr']);
                            echo "0% sfr \n";
                            $per_cent_sfr = 0;
                            $nbbsfr = 0;

                          } else {
                            $totalsfr = count($tresult['bounces']['sfr']) + count($tresult['abouti']['sfr']);
                            $nbbsfr = count($tresult['bounces']['sfr']);
                            $per_cent_sfr = $nbbsfr / $totalsfr * 100;
                            $per_cent_sfr = round(100 - $per_cent_sfr, 2);


                          }
                          // flag ajout tempo pour voir
                          // fix temporaire cap
                        } else {
                          if(isset($tresult['abouti']['sfr'])){
                          $totalsfr = count($tresult['abouti']['sfr']);
                          $per_cent_sfr = 100;
                        } else {
                          $per_cent_sfr = 0;
                        }

                        }

                        \Log::info("Calcul % tableau laposte");
                        if(isset($tresult['bounces']['laposte'])){

                          if(!isset($tresult['abouti']['laposte'])){
                            $totallaposte = count($tresult['bounces']['laposte']);
                            echo "0% laposte \n";
                            $per_cent_laposte = 0;
                            $nbblaposte = 0;

                          } else {
                            $totallaposte = count($tresult['bounces']['laposte']) + count($tresult['abouti']['laposte']);
                            $nbblaposte = count($tresult['bounces']['laposte']);
                            $per_cent_laposte = $nbblaposte / $totallaposte * 100;
                            $per_cent_laposte = round(100 - $per_cent_laposte, 2);

                          }
                        }

                        // var_dump($tresult['bounces']['aol']);
                        // var_dump($tresult['abouti']['aol']);

                        /*
                        if(isset($tresult['bounces']['aol'])){

                          // isset($tresult['abouti']['aol']
                            if(!isset($tresult['abouti']['aol'])){
                              $totalaol = count($tresult['bounces']['aol']);
                              echo "0% aol \n";
                              $per_cent_aol = 0;
                              $nbbaol = 0;
                            } else {
                              $totalaol = count($tresult['bounces']['aol']) + count($tresult['abouti']['aol']);
                              $nbbaol = count($tresult['bounces']['aol']);
                              $per_cent_aol = $nbbaol / $totalaol * 100;
                              $per_cent_aol = round(100 - $per_cent_aol, 2);

                            }
                        }

                        // var_dump($tresult['bounces']['hotmail']);
                        // var_dump($tresult['abouti']['hotmail']);

                        if(isset($tresult['bounces']['hotmail'])){

                          if(!isset($tresult['abouti']['hotmail'])){
                            $totalhotmail = count($tresult['bounces']['hotmail']);
                            echo "0% hotmail \n";
                            $per_cent_hotmail = 0;
                            $nbbhotmail = 0;
                          } else {
                            $totalhotmail = count($tresult['bounces']['hotmail']) + count($tresult['abouti']['hotmail']);
                            $nbbhotmail = count($tresult['bounces']['hotmail']);
                            $per_cent_hotmail = $nbbhotmail / $totalhotmail * 100;
                            $per_cent_hotmail = round(100 - $per_cent_hotmail, 2);
                          }

                        }

                        // var_dump($tresult['bounces']['bbox']);
                        // var_dump($tresult['abouti']['bbox']);

                        if(isset($tresult['bounces']['bbox'])){

                          if(!isset($tresult['abouti']['bbox'])){
                            $totalbbox = count($tresult['bounces']['bbox']);
                            echo "0% bbox \n";
                            $per_cent_bbox = 0;
                            $nbbbbox = 0;
                          } else {
                            $totalbbox = count($tresult['bounces']['bbox']) + count($tresult['abouti']['bbox']);
                            $nbbbbox = count($tresult['bounces']['bbox']);
                            $per_cent_bbox = $nbbbbox / $totalbbox * 100;
                            $per_cent_bbox = round(100 - $per_cent_bbox, 2);
                          }

                        }

                        */


                        \Log::info("Calcul % tableau Orange");
                        if(isset($tresult['bounces']['orange'])){

                          if(!isset($tresult['abouti']['orange'])){
                            $totalorange = count($tresult['bounces']['orange']);
                            echo "0% orange \n";
                            $per_cent_orange = 0;
                            $nbborange = 0;

                          } else {
                            $totalorange = count($tresult['bounces']['orange']) + count($tresult['abouti']['orange']);
                            $nbborange = count($tresult['bounces']['orange']);
                            $per_cent_orange = $nbborange / $totalorange * 100;
                            $per_cent_orange = round(100 - $per_cent_orange, 2);

                          }
                          // flag ajout tempo pour voir
                          // fix temporaire cap
                        } else {
                          if(isset($tresult['abouti']['orange'])){
                          $totalorange = count($tresult['abouti']['orange']);
                          $per_cent_orange = 100;
                        } else {
                          $per_cent_orange = 0;
                        }

                        }


                        \Log::info("Calcul % tableau autre");
                        if(isset($tresult['bounces']['autre'])){

                          if(!isset($tresult['abouti']['autre'])){
                            $totalautre = count($tresult['bounces']['autre']);
                            echo "0% autre \n";
                            $per_cent_autre = 0;
                            $nbbautre = 0;
                          } else {
                            $totalautre = count($tresult['bounces']['autre']) + count($tresult['abouti']['autre']);
                            $nbbautre = count($tresult['bounces']['autre']);
                            $per_cent_autre = $nbbautre / $totalautre * 100;
                            $per_cent_autre = round(100 - $per_cent_autre, 2);
                          }

                        }


                        $nbrouvtotal = $dump->records[0][4];
                        $per_cent_free_ouv = 0;
                        $per_cent_sfr_ouv = 0;
                        $per_cent_laposte_ouv = 0;
                        $per_cent_autre_ouv = 0;
                        $per_cent_aol_ouv = 0;
                        $per_cent_hotmail_ouv = 0;
                        $per_cent_bbox_ouv = 0;

                        $per_cent_orange_ouv = 0;

                        echo 'Free' . "\n";
                        echo 'nbr total' . $totalfree . "\n";
                        echo 'nbr ouv' . $nbrouvfree . "\n";

                        if($nbrouvfree != 0 && $totalfree != 0){
                            $per_cent_free_ouv = $nbrouvfree / $totalfree * 100;
                            $per_cent_free_ouv = round($per_cent_free_ouv, 2);
                        } else {
                          $per_cent_free_ouv = 0;
                        }

                        echo 'SFR' . "\n";
                        echo 'nbr total' . $totalsfr . "\n";
                        echo 'nbr ouv' . $nbrouvsfr . "\n";

                        if($nbrouvsfr != 0 && $totalsfr != 0){
                            $per_cent_sfr_ouv = $nbrouvsfr / $totalsfr * 100;
                            $per_cent_sfr_ouv = round($per_cent_sfr_ouv, 2);
                        } else {
                          $per_cent_sfr_ouv = 0;
                        }

                        echo 'Laposte' . "\n";
                        echo 'nbr total' . $totallaposte . "\n";
                        echo 'nbr ouv' . $nbrouvlaposte . "\n";

                        if($nbrouvlaposte != 0 && $totallaposte != 0){
                            $per_cent_laposte_ouv = $nbrouvlaposte / $totallaposte * 100;
                            $per_cent_laposte_ouv = round($per_cent_laposte_ouv, 2);
                        } else {
                          $per_cent_laposte_ouv = 0;
                        }

                        echo 'Autre' . "\n";
                        echo 'nbr total' . $totalautre ."\n";
                        echo 'nbr ouv' . $nbrouvautre . "\n";

                        if($nbrouvautre != 0 && $totalautre != 0){
                            $per_cent_autre_ouv = $nbrouvautre / $totalautre * 100;
                            $per_cent_autre_ouv = round($per_cent_autre_ouv, 2);
                        } else {
                          $per_cent_autre_ouv = 0;
                        }

                        echo 'Autre' . "\n";
                        echo 'nbr total' . $totalorange ."\n";
                        echo 'nbr ouv' . $nbrouvorange . "\n";

                        if($nbrouvorange != 0 && $totalorange != 0){
                            $per_cent_orange_ouv = $nbrouvorange / $totalorange * 100;
                            $per_cent_orange_ouv = round($per_cent_orange_ouv, 2);
                        } else {
                          $per_cent_orange_ouv = 0;
                        }

                        /*
                        echo 'AOL' . "\n";
                        echo 'nbr total' . $totalaol ."\n";
                        echo 'nbr ouv' . $nbrouvaol . "\n";


                        if($nbrouvaol != 0 && $totalaol != 0){
                            $per_cent_aol_ouv = $nbrouvaol / $totalaol * 100;
                            $per_cent_aol_ouv = round($per_cent_aol_ouv, 2);
                        } else {
                          $per_cent_aol_ouv = 0;
                        }


                        echo 'Hotmail' . "\n";
                        echo 'nbr total' . $totalhotmail ."\n";
                        echo 'nbr ouv' . $nbrouvhotmail . "\n";

                        if($nbrouvhotmail != 0 && $totalhotmail != 0){
                            $per_cent_hotmail_ouv = $nbrouvhotmail / $totalhotmail * 100;
                            $per_cent_hotmail_ouv = round($per_cent_hotmail_ouv, 2);
                        } else {
                          $per_cent_hotmail_ouv = 0;
                        }


                        echo 'BBOX' . "\n";
                        echo 'nbr total' . $totalbbox ."\n";
                        echo 'nbr ouv' . $nbrouvbbox . "\n";

                        if($nbrouvbbox != 0 && $totalbbox != 0){
                            $per_cent_bbox_ouv = $nbrouvbbox / $totalbbox * 100;
                            $per_cent_bbox_ouv = round($per_cent_bbox_ouv, 2);
                        } else {
                          $per_cent_bbox_ouv = 0;
                        }

                        */
                        echo 'Calcul abouti FREE = ' . $per_cent_free . ' % ' . "\n";
                        echo 'Calcul abouti SFR = ' . $per_cent_sfr . ' % ' . "\n";
                        echo 'Calcul abouti LAPOSTE = ' . $per_cent_laposte . ' % ' . "\n";
                        echo 'Calcul abouti AUTRE = ' . $per_cent_autre . ' % ' . "\n";
                        echo 'Calcul abouti ORANGE = ' . $per_cent_orange . ' % ' . "\n";

                        // echo 'Calcul abouti aol = ' . $per_cent_aol . ' % ' . "\n";
                        // echo 'Calcul abouti hotmail = ' . $per_cent_hotmail . ' % ' . "\n";
                        // echo 'Calcul abouti bbox = ' . $per_cent_bbox . ' % ' . "\n";


                    }
                    \DB::table('stats_m4y')->insert([
                        'nb_send' => $dump->records[0][2],
                        'desinscriptions' => $dump->records[0][6],
                        'ouvreurs' => $dump->records[0][4],
                        'cliqueurs' => $dump->records[0][5],
                        'soft_bounces' => $dump->records[0][7],
                        'hard_bounces' => $dump->records[0][8],
                        'date_maj' => date("Y-m-d"),
                        'bloc_maj' => $lastmaj,
                        'reference' => $lacampagne->ref,
                        'planning_id' => $r->planning_id,
                        'id_m4y' => $dump->records[0][0],
                        'created_at' => date("Y-m-d H:i:s"),
                        'updated_at' => date("Y-m-d H:i:s"),
                        'aboutis_percent' => round($aboutisreel, 2),

                        // maj

                        'nombrebfree' => $nbbfree,
                        'nombrebsfr' => $nbbsfr,
                        'nombreblaposte' => $nbblaposte,
                        'nombrebautre' => $nbbautre,

                        'nombreborange' => $nbborange,

                        'nombretfree' => $totalfree,
                        'nombretsfr' => $totalsfr,
                        'nombretlaposte' => $totallaposte,
                        'nombretautre' => $totalautre,

                        'nombretorange' => $totalorange,

                        'aboutis_free' => $per_cent_free,
                        'aboutis_sfr' => $per_cent_sfr,
                        'aboutis_laposte' => $per_cent_laposte,
                        'aboutis_autre' => $per_cent_autre,

                        'aboutis_orange' => $per_cent_orange,

                        'per_cent_sfr_ouv' => $per_cent_sfr_ouv,
                        'per_cent_free_ouv' => $per_cent_free_ouv,
                        'per_cent_laposte_ouv' => $per_cent_laposte_ouv,
                        'per_cent_autre_ouv' => $per_cent_autre_ouv,

                        'per_cent_orange_ouv' => $per_cent_orange_ouv,

                        // 'nombrebaol' => $nbbaol,
                        // 'nombretaol' => $totalaol,
                        // 'aboutis_aol' => $per_cent_aol,
                        // 'per_cent_aol_ouv' => $per_cent_aol_ouv,

                        // 'nombrebhotmail' => $nbbhotmail,
                        // 'nombrethotmail' => $totalhotmail,
                        // 'aboutis_hotmail' =>$per_cent_hotmail,
                        // 'per_cent_hotmail_ouv' => $per_cent_hotmail_ouv,

                        // 'nombrebbouygues' => $nbbbbox,
                        // 'nombretbouygues' => $totalbbox,
                        // 'aboutis_bouygues' => $per_cent_bbox,
                        // 'per_cent_bouygues_ouv' => $per_cent_bbox_ouv,


                    ]);

                }
            } else {
                continue;
            }
            unset($teok);
            unset($tebounces);
            unset($tresult);
            unset($touv);
        }
    }
}
