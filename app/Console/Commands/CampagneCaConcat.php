<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class CampagneCaConcat extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ca:init_db_concat';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Concat les campagnes via le nom, plateforme et base_id';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

         // \DB::table('campagnes_ca_concat')->truncate();
         // \DB::table('campagne_ca_relation')->truncate();

         //gère le mois en cours et la date du jour
         $today = date("Y-m-d H:i:s");
         $moisCompta = date("m");

         $lastdayMonthbefore = date("Y-m-d H:i:s", mktime(0,0,0,date("m"), 1, date("Y")));
         $firstdayMonthafter = date("Y-m-d H:i:s", mktime(0,0,0,date("m"),31,date("Y")));

         var_dump('Date début : ' .  $lastdayMonthbefore);
         var_dump('Date fin : ' .  $firstdayMonthafter);

         $lesbases = \DB::table('bases')->get();
         $lesplatesformes = \DB::table('plateformes_affi')->get();

         foreach ($lesbases as $b) {

           echo 'BASE : ' . $b->nom . "\n";

           foreach ($lesplatesformes as $p) {

             echo 'Plateforme : ' . $p->nom_plateforme . "\n";

             $getcampagnesbynom = \DB::table('campagnes')
             ->select('nom','ref','id','base_id', 'created_at', 'volume_total')
             ->where('base_id', $b->id)
             ->where('plateforme_id', $p->id)
             ->whereBetween('created_at', [$lastdayMonthbefore,$firstdayMonthafter])
             ->groupBy('nom')
             ->get();

             foreach ($getcampagnesbynom as $k => $v) {

               echo 'Campagne : id['.$v->id.'] nom['.$v->nom.']'."\n";

               $check = \DB::table('campagnes_ca_concat')
               ->where('base_id', $b->id)
               ->where('nom',$v->nom)
               ->where('plateforme_id',$p->id)
               ->where('mois_compta',$moisCompta)
               ->first();

               if($check == null){

                 \DB::table('campagnes_ca_concat')->insert(
                     ['nom'=> $v->nom,
                     'base_id' => $b->id,
                     'ca_brut' => 0,
                     'ca_net'=> 0,
                     'aaf'=> 0,
                     'envoi_facture'=>0,
                     'state'=>1,
                     'mois_compta'=>$moisCompta,
                     'commentaire'=> " ",
                     'ca_volume_total'=>0,
                     'cout_routage'=>0,
                     'created_at' => $today,
                     'updated_at' => $today,
                     'plateforme_id' => $p->id
                ]);


              } else {
                echo  $v->nom . ' : existe deja' . "\n";
              }

              // where plateformeid
              $recup = \DB::table('campagnes_ca_concat')
              ->where('base_id', $b->id)
              ->where('plateforme_id',$p->id)
              ->where('nom',$v->nom)
              ->where('mois_compta',$moisCompta)
              ->get();

               foreach ($recup as $key => $value) {

                 // where plateformeid
                 $lescampagnesids = \DB::table('campagnes')
                 ->where('nom',$value->nom)
                 ->where('base_id', $value->base_id)
                 ->where('plateforme_id',$p->id)
                 ->whereBetween('created_at', [$lastdayMonthbefore,$firstdayMonthafter])
                 ->get();

                 foreach ($lescampagnesids as $k => $v) {
                   // insert dans l'autre table
                   // ajouter mois compta

                   $checkrelation = \DB::table('campagne_ca_relation')
                   ->where('id_campagnes_ca',$value->id)
                   ->where('base_id_ca', $value->base_id)
                   ->where('id_campagne',$v->id)
                   ->first();

                   if($checkrelation == null){
                     // reste à gerer le non rinsert
                     \DB::table('campagne_ca_relation')->insert([
                        'id_campagnes_ca'=> $value->id,
                        'base_id_ca'=> $value->base_id,
                        'id_campagne'=>$v->id,
                        'created_at' => $today,
                        'updated_at' => $today
                      ]);
                   } else {

                     echo 'existe deja en relation';
                   }
                 }
              }

             }

       }
      }

     }
}
