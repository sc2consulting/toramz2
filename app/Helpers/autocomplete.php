<?php

namespace Sukohi\FormAutocomplete;

use Illuminate\Support\Facades\DB;

class FormAutocomplete {

    private $_selector = '';
    private $_source = [];

    public function __toString() {

        return '$(\''. $this->_selector .'\').autocomplete({minLength: 4, source: '. json_encode($this->_source) .'});';

    }

    public function selector($selector) {

        $this->_selector = $selector;
        return $this;

    }

    public function source($source) {

        if(is_callable($source)) {

            $this->_source = $source();

        } else if(is_array($source)) {

            $this->_source = $source;

        }

        return $this;

    }

    public function db($table, $column) {

        $this->_source = DB::table($table)->lists($column);
        return $this;

    }

    public function db_senders($table) {

        $this->_source = DB::table($table)
            ->select('nom', 'domaine', DB::raw('CONCAT(nom, " | ",domaine) AS fullsender'))
            ->lists('fullsender');

        return $this;

    }

            // Ajout fabien
    public function db2() {

       // Attention ajout d'un espace qui est supprimé dans le controleur
       $this->_source = DB::table('bases')->select('bases.id', 'bases.nom','campagnes.ref',DB::raw('CONCAT(campagnes.ref, " | ",bases.nom) AS fullrefnom'))
            ->join('campagnes', 'bases.id', '=', 'campagnes.base_id')
            ->orderBy('campagnes.created_at', 'desc')
            ->lists('fullrefnom');

    return $this;
	}

	public function dbmail() {

		 $this->_source = DB::table('destinataires')->select('mail')->lists('mail');
		 return $this;
	}


  public function db_ca() {

     $this->_source = DB::table('campagnes')->select('nom')->groupBy('nom')->lists('nom');

  return $this;
}

public function db_ajout_ca() {

   $this->_source = DB::table('campagnes')->select('ref')->orderBy('id','desc')->lists('ref');

return $this;
}



}

?>
