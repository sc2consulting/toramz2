<?php

function big_number($value) {
    return number_format($value, 0, ',',' ');
}

function formatBytes($bytes, $precision = 2) {
    $units = array('B', 'KB', 'MB', 'GB', 'TB');

    $bytes = max($bytes, 0);
    $pow = floor(($bytes ? log($bytes) : 0) / log(1024));
    $pow = min($pow, count($units) - 1);

    // Uncomment one of the following alternatives
    // $bytes /= pow(1024, $pow);
     $bytes /= (1 << (10 * $pow));

    return round($bytes, $precision) . ' ' . $units[$pow];
}

//Convertisseurs des caractères très très spéciaux
function valid_chars($str)
{
    $conv = array(
        chr(129) => '',
        chr(130) => '& #8218',
        chr(131) => '& #402;',
        chr(132) => '& #8222;',
        chr(133) => '& #8230;',
        chr(134) => '& #8224;',
        chr(135) => '& #8225;',
        chr(136) => '& #710;',
        chr(137) => '& #8240;',
        chr(138) => '& #352;',
        chr(139) => '& #8249;',
        chr(140) => '& #338;',
        chr(141) => '',
        chr(142) => '',
        chr(143) => '',
        chr(144) => '',
        chr(145) => '& #8216;',
        chr(39) => '& #8217;',
        chr(147) => '& #8220;',
        chr(148) => '& #8221;',
        chr(149) => '& #8226;',
        chr(150) => '& #8211;',
        chr(151) => '& #8212;',
        chr(152) => '& #732;',
        chr(153) => '& #8482;',
        chr(154) => '& #353;',
        chr(155) => '& #8250;',
        chr(156) => '& #339;',
        chr(157) => '',
        chr(158) => '',
        chr(159) => '& #376;'
    );

    return str_replace(array_values($conv),array_keys($conv),$str);
    return $str;
}

function generateWSSEHeader($username, $password)
{
    $created = date('c');
    $nonce   = substr(md5(uniqid('nonce_', true)),0,16);
    $nonce64 = base64_encode($nonce);
    $passwordDigest = base64_encode(sha1($nonce . $created . $password, true));

    return array('X-WSSE: UsernameToken Username="' . $username . '", PasswordDigest="' . $passwordDigest . '", Nonce="' . $nonce64 . '", Created="'. $created . '"');
}

function stripAccents($string)
{
    return strtr($string,'àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ',
        'aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY');
}

function removeAccents($string)
{
    return iconv('UTF-8', 'ASCII//TRANSLIT//IGNORE', $string);
}

function stripAccents2($texte)
{
    $texte = str_replace(
        array(
            'à', 'â', 'ä', 'á', 'ã', 'å',
            'î', 'ï', 'ì', 'í',
            'ô', 'ö', 'ò', 'ó', 'õ', 'ø',
            'ù', 'û', 'ü', 'ú',
            'é', 'è', 'ê', 'ë',
            'ç', 'ÿ', 'ñ',
            'À', 'Â', 'Ä', 'Á', 'Ã', 'Å',
            'Î', 'Ï', 'Ì', 'Í',
            'Ô', 'Ö', 'Ò', 'Ó', 'Õ',
            'Ù', 'Û', 'Ü', 'Ú',
            'É', 'È', 'Ê', 'Ë',
            'Ç', 'Ÿ', 'Ñ'
        ),
        array(
            'a', 'a', 'a', 'a', 'a', 'a',
            'i', 'i', 'i', 'i',
            'o', 'o', 'o', 'o', 'o', 'o',
            'u', 'u', 'u', 'u',
            'e', 'e', 'e', 'e',
            'c', 'y', 'n',
            'A', 'A', 'A', 'A', 'A', 'A',
            'I', 'I', 'I', 'I',
            'O', 'O', 'O', 'O', 'O',
            'U', 'U', 'U', 'U',
            'E', 'E', 'E', 'E',
            'C', 'Y', 'N'
        ),$texte);
    return $texte;
}

function pre($var)
{
    echo "<pre>";
    print_r($var);
    echo "</pre>";
}

function is_utf8($str)
{
    $c=0; $b=0;
    $bits=0;
    $len=strlen($str);
    for($i=0; $i<$len; $i++){
        $c=ord($str[$i]);
        if($c > 128){
            if(($c >= 254)) return false;
            elseif($c >= 252) $bits=6;
            elseif($c >= 248) $bits=5;
            elseif($c >= 240) $bits=4;
            elseif($c >= 224) $bits=3;
            elseif($c >= 192) $bits=2;
            else return false;
            if(($i+$bits) > $len) return false;
            while($bits > 1){
                $i++;
                $b=ord($str[$i]);
                if($b < 128 || $b > 191) return false;
                $bits--;
            }
        }
    }
    return true;
}

function parcourir_wori_bon($repertoire,$creche)
{
    static  $retour_rep = "";
    $le_repertoire = opendir($repertoire) or die("Erreur le repertoire $repertoire existe pas");
    while($fichier = @readdir($le_repertoire))
    {
        if ($fichier == "." || $fichier == "..") continue; 	// enlever les traitements inutile
        if(is_dir($repertoire.'/'.$fichier))
        {
            parcourir_repertoire($repertoire.'/'.$fichier,$creche);
        }
        else
        {
            if ( strpos($fichier , $creche) )
            {
                $retour_rep = $repertoire ."/". $fichier ;
                return $retour_rep ;
            }
        }		//			FIN DE 		if(is_dir($repertoire.'/'.$fichier)) {
    }		//			FIN DE 		while($fichier = @readdir($le_repertoire))	{
    closedir($le_repertoire);
}


function corrige ($nom) 	//			//		Mise en forme des accents
{
    $retour = "" ;
    for ( $i = 0 ; $i < strlen($nom) ; $i ++){
//echo $i . " ==>" .  substr($nom, $i , 1) ."<==".ord(substr($nom, $i , 1))."==  <br>" ;
        if (ord(substr($nom, $i , 1))>223 ) {
//echo "chr234 ===".chr(234)."=="."&#".ord(substr($nom, $i , 1)).";"."== <br><br>";
            $retour = $retour . "&#".ord(substr($nom, $i , 1)).";" ;
        } else {
            $retour = $retour . substr($nom, $i , 1) ;
        }
    }
    return $retour ;
}


function ecrit($nom) 	//			//		Mise en forme des accents
{
    $retour = "" ;
    for ( $i = 0 ; $i < strlen($nom) ; $i ++){
//echo $i ." ==>" .  $nom[$i] ."<==". ord( $nom[$i] ) ."== " ;
//echo "i + 1 ==" . ($i + 1)  ." ==>" .  $nom[$i+1] ."<==". ord( $nom[$i+1] )."<br>\n" ;

        if ( ord( $nom[$i] ) == 195 ){
//		$carac = "&#". (ord( $nom[$i +1] ) + 64 ).";" ;
            $carac = chr( (ord( $nom[$i +1] ) + 64 ) ) ;
//	echo "nouveau $i exa ==>" .  (ord( $nom[$i +1] ) + 64 ) ."<==". $carac ."==  <br>\n" ;
            $retour = $retour . $carac  ;
            $i++ ;
        } else {
            $retour = $retour . $nom[$i] ;
        }
    }
    return $retour ;
}


function ecrit_abc($nom) 	//			//		Mise en forme des accents
{
    $retour = "" ;
    for ( $i = 0 ; $i < strlen($nom) ; $i ++){
//echo $i ." ==>" .  $nom[$i] ."<==". ord( $nom[$i] ) ."== " ;
//echo "i + 1 ==" . ($i + 1)  ." ==>" .  $nom[$i+1] ."<==". ord( $nom[$i+1] )."<br>\n" ;

        if ( (ord( $nom[$i] ) == 194) or (ord( $nom[$i] ) == 195) ){

            if ( ord( $nom[$i] ) == 194 ){ $car = (ord( $nom[$i +1] )		) ; }		///		CAS DE L'APOSTROPHE

            if ( ord( $nom[$i] ) == 195 ){ $car = (ord( $nom[$i +1] ) + 64	) ; }		///		CAS DES ACCENT

            $carac = html_entity_decode( "&#".$car.";") ;
//		$carac = "&#". (ord( $nom[$i +1] ) + 64 ).";" ; chr
            $retour = $retour . $carac  ;
            $i++ ;
        } else {
            $retour = $retour . $nom[$i] ;
        }
    }
    return $retour ;
}

function ecrit_def($nom) 	//			//		Mise en forme des accents
{
    $retour = "" ;
    for ( $i = 0 ; $i < strlen($nom) ; $i ++){

        if ( (ord( $nom[$i] ) == 194) or (ord( $nom[$i] ) == 195) ){

            if ( ord( $nom[$i] ) == 194 ){ $car = (ord( $nom[$i +1] )		) ; }		///		CAS DE L'APOSTROPHE

            if ( ord( $nom[$i] ) == 195 ){ $car = (ord( $nom[$i +1] ) + 64	) ; }		///		CAS DES ACCENT

            $carac = chr($car) ;

            $retour = $retour . $carac  ;
            $i++ ;
        } else {
            $retour = $retour . $nom[$i] ;
        }
    }
    return $retour ;
}




$sansacc = array( "a", "a", "e", "e", "e", "e", "i", "i", "o", "u", "u", "c", "euro", "'", "o", "«", "»");
$accent = array( "à", "â", "é", "è", "ê", "ë", "î", "ï", "ô", "ù", "û", "ç", "€", "'", "°", "«", "»");
$bonacc   = array( "&agrave;", "&acirc;", "&eacute;", "&egrave;", "&ecirc;", "&euml;", "&icirc;", "&iuml;", "&ocirc;", "&ugrave;", "&ucirc;", "&ccedil;", "&euro;" , "&#039;", "&deg;", "&laquo;", "&raquo;");

function accentb($phrase) {
    //			//		Mise en forme des accents
    $accent = $GLOBALS['accent'] ;
    $bonacc = $GLOBALS['bonacc'] ;
    $newphrase = str_replace($accent, $bonacc, $phrase);
    return $newphrase ;
}
function accentsans($phrase) {
    $accent = $GLOBALS['accent'] ;
    $sansacc = $GLOBALS['sansacc'];
    //			//		Mise en forme des accents
    $newphrase = str_replace($accent, $sansacc, $phrase);
    return $newphrase ;
}


?>