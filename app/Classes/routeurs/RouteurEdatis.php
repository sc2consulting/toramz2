<?php namespace App\Classes\Routeurs;

use App\Models\Campagne;
use App\Models\Sender;
use App\Models\Routeur;

class RouteurEdatis
{
    private $client;

    public function __construct()
    {
        $this->url = 'https://dialog.apis.edatis.com:8081/api/';

        $this->client = new RouteurEdatisCurl();
        /*
        *
        Clé pour pouvoir debug
        *
        SC 2 – 1= 21f5f1f10f74b4b9d5d0ebab09302bef

        SC 2 – 2= 8e407de4c1248c3215d9e47f9b6b27cb

        SC 2 – 3= 65349767ce58099dd6ea2c902f726017

        SC 2 – 4= 914ef64973a6e20af33f83d5dbabc76d

        SC 2 – 5= fe40bd0786d605919abf6afed1b5d645

        SC 2 – 6= dd5c17524500d78bc5ed4d5632439616

        SC 2 – 7= 0e3b0deb485bfe1b0a200ab0d411005b

        SC 2 – 8= 38771891c86cbacaede5d9ce421cba49

        SC 2 – 9= c3d551016308bdfe334f4e9ef498ee04

        SC 2 – 10= 5e568b68d0a55516db8fcc701fd512fd

        // curl_setopt($ch, CURLOPT_URL,  'https://dialog.apis.edatis.com:8081/api/campaign.json'); // Creation [POST]
        //~ curl_setopt($ch, CURLOPT_URL,  'https://dialog.apis.edatis.com:8081/api/campaigns/{campaignId}.json'); // Edit [PUT]
        //curl_setopt($ch, CURLOPT_URL,  'https://dialog.apis.edatis.com:8081/api/campaigns/{campaignId}.json'); // [Delete]
        //curl_setopt($ch, CURLOPT_URL,  'https://dialog.apis.edatis.com:8081/api/campaigns/{campaignId}.json'); //  [GET]
        //curl_setopt($ch, CURLOPT_URL,  'https://dialog.apis.edatis.com:8081/api/campaigns/578/send.json'); // Envoi [POST]
        //~ curl_setopt($ch, CURLOPT_URL,  'https://dialog.apis.edatis.com:8081/api/campaigns/661/send.json'); // Envoi [POST]

        */

    }

    function create_campagne($key, $labelcampagne, $subject, $sendername, $senderemail, $replyname, $replyemail, $text, $html)
    {
        $this->client->AddHeader('apikey', $key);

        $params = array
        (
            "label" => $labelcampagne,
            "isAutoOptimizedContent" =>true,
            "isGoogleAnalytics" =>true,
            "isAutoTracking" =>true,
            "content" =>
                array
                (
                    "subject" => $subject,
                    "senderName" => $sendername,
                    "senderEmail" => $senderemail,
                    "replyToName" => $replyname,
                    "replyToEmail" => $replyemail,
                    "text" => $text,
                    "html" => $html
                )
        );

        $this->client->SetParams($params);
        $response = $this->client->post($this->url.'campaign.json');

        $ApiResponse = json_decode($response->body);
        var_dump($ApiResponse);
        var_dump($response->headers);

        if($response->headers['Status-Code'] == 100
        or $response->headers['Status-Code'] == 200
        or $response->headers['Status-Code'] == 201) {
            return $ApiResponse->campaignId;
        }
        \Log::error('[RouteurEdatis] : create_campagne return error '.json_encode($ApiResponse));
        return 0;
    }

    function send_campagne($key, $cid_routeur, $contacts)
    {
        \Log::error('[RouteurEdatis] : Import de :' . json_encode($contacts));
        $this->client->AddHeader('apikey', $key);
        $params = array("contacts" => $contacts);
        $this->client->SetParams($params);
        // pour le moment car chunck limité à 100
        if(count($contacts) > 100) {
          \Log::error('[RouteurEdatis] : Trop de contact');
            die();
        }

        //TODO : code foreach when contacts > 100
        $response = $this->client->post($this->url.'campaigns/'.$cid_routeur.'/send.json');

        $ApiResponse = json_decode($response->body);
        \Log::error('[RouteurEdatis] : responseAIpi Send : '.json_encode($ApiResponse));
        if($response->headers['Status-Code'] == 200
        or $response->headers['Status-Code'] == 201
        or $response->headers['Status-Code'] == 100 ) {
            return $ApiResponse;
        }
        \Log::error('[RouteurEdatis] : send_campagne return error '.json_encode($ApiResponse));
        return 0;
    }

    function send_bat($idcampagne, $contacts)
    {
        // $labelcampagne, $subject, $sendername, $senderemail, $replyname, $replyemail, $text, $html
        $key = '21f5f1f10f74b4b9d5d0ebab09302bef';
        // $campagne = \DB::table('campagnes')->where('id', $idcampagne)->first();
        $campagne = Campagne::where('id',$idcampagne)->first();
        $redatis = Routeur::where('nom','Edatis')->first();
        $html = $campagne->generateHtml($redatis);

        $c_id = $this->create_campagne($key,'BAT-'.time().$campagne->ref,$campagne->objet,$campagne->expediteur,'news@info.lafolieduweb.fr','reply','reply@info.lafolieduweb.fr','',$html);

        $this->client->AddHeader('apikey', $key);
        $params = array("contacts" => $contacts);
        $this->client->SetParams($params);

        $response = $this->client->post($this->url.'campaigns/'.$c_id.'/send/test.json');

        $ApiResponse = json_decode($response->body);

        if($response->headers['Status-Code'] == 200 or $response->headers['Status-Code'] == 201 ) {
            return true;
        }
        \Log::error('[RouteurEdatis] : send_bat return error '.json_encode($ApiResponse));
        return 0;
    }

    function lists_campagne($key)
    {
        $this->client->AddHeader('apikey', $key);
        $response = $this->client->get($this->url.'campaign/list.json');

        $ApiResponse = json_decode($response->body);

        if($response->headers['Status-Code'] == 200 or $response->headers['Status-Code'] == 201 ) {
            return $ApiResponse->results;
        }
        \Log::error('[RouteurEdatis] : lists_campagne return error '.json_encode($ApiResponse));
        return 0;
    }

    function info_campagne($key, $cid_routeur)
    {
        $this->client->AddHeader('apikey', $key);
        $response = $this->client->get($this->url.'campaigns/'.$cid_routeur.'.json');

        $ApiResponse = json_decode($response->body);

        if($response->headers['Status-Code'] == 200 or $response->headers['Status-Code'] == 201 ) {
            return $ApiResponse->results;
        }

        \Log::error('[RouteurEdatis] : info_campagne return error '.json_encode($ApiResponse));
        return 0;
    }

    function get_bounces_infos($key, $cid_routeur)
    {
        $this->client->AddHeader('apikey', $key);
        $response = $this->client->get($this->url.'report/campaigns/'.$cid_routeur.'/bounces.json');

        $ApiResponse = json_decode($response->body);

        if($response->headers['Status-Code'] == 200 or $response->headers['Status-Code'] == 201 ) {
            return $ApiResponse;
        }

        \Log::error('[RouteurEdatis] : get_bounces_infos return error '.json_encode($ApiResponse));
        return 0;
    }

    function get_bounces($key, $cid_routeur, $page_number)
    {
        $offset = 1000;
        $this->client->AddHeader('apikey', $key);
        $response = $this->client->get($this->url.'report/campaigns/'.$cid_routeur.'/bounces.json?&page='.$page_number.'&pageSize='.$offset);

        $ApiResponse = json_decode($response->body);

        if($response->headers['Status-Code'] == 200 or $response->headers['Status-Code'] == 201 ) {
            return $ApiResponse;
        }

        \Log::error('[RouteurEdatis] : get_bounces return error '.json_encode($ApiResponse));
        return 0;
    }


    function get_clickers_nb_infos($key, $cid_routeur)
    {
        $this->client->AddHeader('apikey', $key);
        $response = $this->client->get($this->url.'report/campaigns/'.$cid_routeur.'/clicks.json');
        $ApiResponse = json_decode($response->body);

        if($response->headers['Status-Code'] == 200 or $response->headers['Status-Code'] == 201 ) {
            return $ApiResponse;
        }

        \Log::error('[RouteurEdatis] : get_clickers_nb_infos return error '.json_encode($ApiResponse));
        return 0;
    }

    function get_clickers($key, $cid_routeur, $page_number)
    {
        $offset = 1000;
        $this->client->AddHeader('apikey', $key);
        $response = $this->client->get($this->url.'report/campaigns/'.$cid_routeur.'/clicks.json?&page='.$page_number.'&pageSize='.$offset);

        $ApiResponse = json_decode($response->body);

        if($response->headers['Status-Code'] == 200 or $response->headers['Status-Code'] == 201 ) {
            return $ApiResponse;
        }

        \Log::error('[RouteurEdatis] : get_clickers return error '.json_encode($ApiResponse));
        return 0;
    }

    function get_openers_infos($key, $cid_routeur)
    {
        $this->client->AddHeader('apikey', $key);
        $response = $this->client->get($this->url.'report/campaigns/'.$cid_routeur.'/opens.json');

        $ApiResponse = json_decode($response->body);

        if($response->headers['Status-Code'] == 200 or $response->headers['Status-Code'] == 201 ) {
            return $ApiResponse;
        }

        \Log::error('[RouteurEdatis] : get_openers_infos return error '.json_encode($ApiResponse));
        return 0;
    }

    function get_openers($key, $cid_routeur, $page_number)
    {
        $offset = 1000;
        $this->client->AddHeader('apikey', $key);
        $response = $this->client->get($this->url.'report/campaigns/'.$cid_routeur.'/opens.json?&page='.$page_number.'&pageSize='.$offset);
        $ApiResponse = json_decode($response->body);

        if($response->headers['Status-Code'] == 200 or $response->headers['Status-Code'] == 201 ) {
            return $ApiResponse;
        }

        \Log::error('[RouteurEdatis] : get_openers return error '.json_encode($ApiResponse));
        return 0;
    }

    function get_unsubscribers_infos($senderid, $cid_routeur)
    {
        $routagesender = \DB::table('senders')->where('id',$senderid)->first();

        $this->client->AddHeader('apikey', $routagesender->password);
        $response = $this->client->get($this->url.'report/campaigns/'.$cid_routeur.'/unsubscribes.json');
        $ApiResponse = json_decode($response->body);

        if($response->headers['Status-Code'] == 200 or $response->headers['Status-Code'] == 201 ) {
            return $ApiResponse;
        }

        \Log::error('[RouteurEdatis] : get_unsubscribers_infos return error '.json_encode($ApiResponse));
        return 0;
    }

    function get_unsubscribers($senderid, $cid_routeur, $page_number)
    {
        $routagesender = \DB::table('senders')->where('id',$senderid)->first();
        $offset = 1000;
        $this->client->AddHeader('apikey', $routagesender->password);
        $response = $this->client->get($this->url.'report/campaigns/'.$cid_routeur.'/unsubscribes.json?&page='.$page_number.'&pageSize='.$offset);
        $ApiResponse = json_decode($response->body);

        if($response->headers['Status-Code'] == 200 or $response->headers['Status-Code'] == 201 ) {
            return $ApiResponse;
        }

        \Log::error('[RouteurEdatis] : get_unsubscribers return error '.json_encode($ApiResponse));
        return 0;

    }

    // marche pas
    function get_complaints_spam_infos($key, $cid_routeur)
    {
        $this->client->AddHeader('apikey', $key);
        $response = $this->client->get($this->url.'/report/campaigns/'.$cid_routeur.'/spams.json');

        $ApiResponse = json_decode($response->body);

        if($response->headers['Status-Code'] == 200 or $response->headers['Status-Code'] == 201 ) {
            return $ApiResponse;
        }

        \Log::error('[RouteurEdatis] : get_complaints_spam_infos return error '.json_encode($ApiResponse));
        return 0;
    }

    function get_delivrability_infos($key, $cid_routeur)
    {
        $this->client->AddHeader('apikey', $key);
        $response = $this->client->get($this->url.'report/campaigns/'.$cid_routeur.'/received.json');
        $ApiResponse = json_decode($response->body);

        if($response->headers['Status-Code'] == 200 or $response->headers['Status-Code'] == 201 ) {
            return $ApiResponse;
        }

        \Log::error('[RouteurEdatis] : get_delivrability_infos return error '.json_encode($ApiResponse));
        return 0;
    }


    function get_delivrability($key, $cid_routeur, $page_number)
    {

        $offset = 1000;
        $this->client->AddHeader('apikey', $key);
        $response = $this->client->get($this->url.'report/campaigns/'.$cid_routeur.'/received.json?&page='.$page_number.'&pageSize='.$offset);
        $ApiResponse = json_decode($response->body);

        if($response->headers['Status-Code'] == 200 or $response->headers['Status-Code'] == 201 ) {
            return $ApiResponse;
        }

        \Log::error('[RouteurEdatis] : get_delivrability_infos return error '.json_encode($ApiResponse));
        return 0;
    }


    function get_sent_infos($key, $cid_routeur)
    {
        $this->client->AddHeader('apikey', $key);
        $response = $this->client->get($this->url.'report/campaigns/'.$cid_routeur.'/sent.json');
        $ApiResponse = json_decode($response->body);

        if($response->headers['Status-Code'] == 200 or $response->headers['Status-Code'] == 201 ) {
            return $ApiResponse;
        }

        \Log::error('[RouteurEdatis] : get_delivrability_infos return error '.json_encode($ApiResponse));
        return 0;
    }


    function get_sent($key, $cid_routeur, $page_number)
    {

        $offset = 1000;
        $this->client->AddHeader('apikey', $key);
        $response = $this->client->get($this->url.'report/campaigns/'.$cid_routeur.'/sent.json?&page='.$page_number.'&pageSize='.$offset);
        $ApiResponse = json_decode($response->body);

        if($response->headers['Status-Code'] == 200 or $response->headers['Status-Code'] == 201 ) {
            return $ApiResponse;
        }

        \Log::error('[RouteurEdatis] : get_delivrability_infos return error '.json_encode($ApiResponse));
        return 0;
    }


    function get_summary_campagne($key, $cid_routeur)
    {
        $this->client->AddHeader('apikey', $key);
        $response = $this->client->get($this->url.'report/campaigns/'.$cid_routeur.'/summary.json');
        $ApiResponse = json_decode($response->body);

        if($response->headers['Status-Code'] == 200 or $response->headers['Status-Code'] == 201 ) {
            return $ApiResponse;
        }

        \Log::error('[RouteurEdatis] : get_summary_campagne return error '.json_encode($ApiResponse));
        return 0;
    }
}
