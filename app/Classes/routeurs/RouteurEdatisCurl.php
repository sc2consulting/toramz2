<?php namespace  App\Classes\Routeurs;

class RouteurEdatisCurl
{
    public $headers = array();
    public $options = array();

    protected $_Username;
    protected $_Password;

    private $Params = array();

    protected $handle;

    public function __construct()
    {
    }

    public function AddParam( $Key , $Value )
    {
        if( trim($Key == '') ) {
            return;
        }
        $this->Params[$Key] = $Value;
    }

    public function SetParams( $Params )
    {
//        $this->Params = array_merge( $this->Params, $Params );
        $this->Params = $Params;
    }

    public function AddHeader($Key, $Value)
    {
        $this->headers[$Key] = $Value;
    }

    public function delete( $url )
    {
        return $this->request('DELETE', $url, $this->Params );
    }

    public function get( $url )
    {
        if ( ! empty( $this->Params ) ) {
            $url .= (stripos($url, '?') !== false) ? '&' : '?';
            $url .= http_build_query($this->Params, '', '&');
        }
        return $this->request('GET', $url);
    }

    public function post( $url )
    {
        return $this->request('POST', $url, $this->Params);
    }

    public function put( $url )
    {
        return $this->request('PUT', $url, $this->Params);
    }

    protected function request($method, $url, $vars = array())
    {
        $this->handle = curl_init();

        # Set some default CURL options
        curl_setopt($this->handle, CURLOPT_HEADER, true);
        curl_setopt($this->handle, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($this->handle, CURLOPT_URL, $url);
        curl_setopt($this->handle, CURLOPT_SSL_VERIFYPEER, 0);

        # Determine the request method and set the correct CURL option
        switch ($method) {
            case 'GET':
                curl_setopt($this->handle, CURLOPT_HTTPGET, true);
                break;
            case ('POST' || 'DELETE' || 'PUT'):
                curl_setopt($this->handle, CURLOPT_POST, true);
                $params = json_encode($vars);
                $json = (is_array($params) ? http_build_query($params) : $params);
                curl_setopt($this->handle, CURLOPT_POSTFIELDS, $json);
                $this->AddHeader('Content-Length', strlen($json));
                break;
        }

        curl_setopt($this->handle, CURLOPT_CUSTOMREQUEST, strtoupper($method));

        # Format custom headers for this request and set CURL option
        $headers = array();
        foreach ($this->headers as $key => $value)
        {
            $headers[] = $key.': '.$value;
        }

        curl_setopt($this->handle, CURLOPT_HTTPHEADER, $headers);

        # Set any custom CURL options
        foreach ($this->options as $option => $value)
        {
            curl_setopt($this->handle, constant('CURLOPT_'.str_replace('CURLOPT_', '', strtoupper($option))), $value);
        }

        $response = curl_exec($this->handle);
        // var_dump('La rep : [' . $response . ']');
        // \Log::info($response);
        if ( $response !== false ) {
            $response = new CurlResponseEdatis($response);
        } else {
            // throw new CurlExceptionEdatis( curl_errno($this->handle).' - '.curl_error($this->handle) );
            \Log::error(curl_errno($this->handle).' - '.curl_error($this->handle));
            return false;
        }
        curl_close($this->handle);
        return $response;
    }
}

class CurlExceptionEdatis extends \Exception
{
}


class CurlResponseEdatis
{
    public $body = '';
    public $headers = array();

    public function __construct( $response )
    {
# Extract headers from response
        $pattern = '#HTTP/\d\.\d.*?$.*?\r\n\r\n#ims';
        preg_match_all($pattern, $response, $matches);
        $headers = explode("\r\n", str_replace("\r\n\r\n", '', array_pop($matches[0])));

# Extract the version and status from the first header
        $version_and_status = array_shift($headers);
        preg_match('#HTTP/(\d\.\d)\s(\d\d\d)\s(.*)#', $version_and_status, $matches);
        $this->headers['Http-Version'] = $matches[1];
        $this->headers['Status-Code'] = $matches[2];
        $this->headers['Status'] = $matches[2].' '.$matches[3];

# Convert headers into an associative array
        foreach ($headers as $header)
        {
            preg_match('#(.*?)\:\s(.*)#', $header, $matches);
            $this->headers[$matches[1]] = $matches[2];
        }

# Remove the headers from the response body
        $this->body = preg_replace($pattern, '', $response);
    }

    public function __toString()
    {
        return $this->body;
    }
}
