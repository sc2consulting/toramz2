<?php namespace App\Classes\Routeurs;

use \App\Models\Campagne;
use App\Models\Routeur;

class RouteurMailKitchen
{
    private $routeur;
    private $header;

    public function __construct()
    {
        // passer les ids de prod
        $this->url = 'http://webservices.mailkitchen.com/';
        /*$this->login = 'hernoux.fabien@gmail.com';
        $this->password = '$C2-4S%n3e';*/

      $this->login = 'yveschaponic@gmail.com';
      $this->password = '*i35VgCd';


    }

    /**
     *  Return list's id (string) or null
     *
     **/
    function createList(){

        $login = $this->login;
        $password = $this->password;
        error_reporting(0);
        try {

            $ws = new \SoapClient(
                "http://webservices.mailkitchen.com/server.wsdl",
                array('trace' => 1, 'soap_version'   => SOAP_1_2)
            );
            $token = $ws->Authenticate($login, $password);

            $SubscriberList = $ws->CreateSubscriberList('wsList' . date("Ymdhis"));
            echo 'Successfull subscriber list creation : ' . $SubscriberList['name'];
            return $SubscriberList['id'];
        }
        catch (\SoapFault $exception) {
        /*header('Content-Type: text/plain; charset: utf-8');
        echo $exception->faultcode . ' : ' . $exception->getMessage();*/
            \Log::error("[RouteurMailKitchen@createList] Exception raised:($exception->faultcode) ".$exception->getMessage());
            return null;
        }
        // return l'id de la liste
        // var_dump($SubscriberList);
    }

    /**
     *
     * Return true when the list is populated else false
     *
     * */
    function populateList($listId, $arrayMember){

        $login = $this->login;
        $password = $this->password;

        error_reporting(0);
        try {

            $ws = new \SoapClient(
                "http://webservices.mailkitchen.com/server.wsdl",
                array('trace' => 1, 'soap_version'   => SOAP_1_2)
            );
            $token = $ws->Authenticate($login, $password);

            $datas = array (
                'header'	=> array ('email'),
                'datas'		=> $arrayMember
            );
            $report = $ws->ImportMember(array($listId), $datas);
            return true;
        } catch (\SoapFault $exception) {
            // header('Content-Type: text/plain; charset: utf-8');
            // echo $exception->faultcode . ' : ' . $exception->getMessage();
            \Log::error("[RouteurMailKitchen@populateList] Exception raised:($exception->faultcode) ".$exception->getMessage());
            return false;
        }
    }


    /**
     *
     *  Return true when it's success else false
     *
     * */
    function SetTarget($campaignId, $listId){

        $login = $this->login;
        $password = $this->password;
        error_reporting(0);

        try {
            $ws = new \SoapClient(
                "http://webservices.mailkitchen.com/server.wsdl",
                array('trace' => 1, 'soap_version'   => SOAP_1_2)
            );
            $token = $ws->Authenticate($login, $password);

            $aSubscriberList1 = array($listId);
            $ws->SelectCampaign($campaignId);
            $data = array(
                'AddSL' => $aSubscriberList1 // list to add
            );
            $ws->SetTargeting($data);
            return true;
        }
        catch (\SoapFault $exception) {
        /*header('Content-Type: text/plain; charset: utf-8');
        echo $exception->faultcode . ' : ' . $exception->getMessage();*/
            \Log::error("[RouteurMailKitchen@SetTarget] Exception raised:($exception->faultcode) ".$exception->getMessage());
            return false;
        }

    }

    /**
     *  Return id or null
     *
     *
     * */
    function CreateCampaign($campagneName,$senderName,$campagneObject, $html, $idsender){

        $login = $this->login;
        $password = $this->password;
        error_reporting(0);
        try {

            $ws = new \SoapClient(
                "http://webservices.mailkitchen.com/server.wsdl",
                array('trace' => 1, 'soap_version'   => SOAP_1_2)
            );
            $token = $ws->Authenticate($login, $password);
            // $htmlCode = '<html><body><p>test - {{email}}</p></body></html>';
            $ws->SelectCampaign();
            $ws->SetCampaignOption ('name', $campagneName);
            $ws->SetCampaignOption ('SenderName', $senderName);
            $ws->SetCampaignOption ('subject', $campagneObject);
            $ws->SetCampaignOption ('idSender', $idsender);
            $ws->SetCampaignOption ('html', $html);
            $r = $ws->SaveCampaign();

            return $r['id'];
        }
        catch (\SoapFault $exception) {
            /*header('Content-Type: text/plain; charset: utf-8');
            echo $exception->faultcode . ' : ' . $exception->getMessage();*/
            \Log::error("[RouteurMailKitchen@CreateCampaign] Exception raised:($exception->faultcode) ". $exception->getMessage());
            return null;
        }
        //var_dump($r);

    }


    function GetListSender(){

        $login = $this->login;
        $password = $this->password;
        error_reporting(0);

        try {
            $ws = new \SoapClient(
                "http://webservices.mailkitchen.com/server.wsdl",
                array('trace' => 1, 'soap_version'   => SOAP_1_2)
            );
            $token = $ws->Authenticate($login, $password);

            $list = $ws->ListSender();
            var_dump($list);

        }
        catch (SoapFault $exception) {
            \Log::error("[RouteurMailKitchen@GetListSender] Exception raised:($exception->faultcode) ". $exception->getMessage());
            header('Content-Type: text/plain; charset: utf-8');
            echo $exception->faultcode . ' : ' . $exception->getMessage();
        }


    }


    function GetListCampaign(){

        $login = $this->login;
        $password = $this->password;

        error_reporting(0);
        try {
            $ws = new \SoapClient(
                "http://webservices.mailkitchen.com/server.wsdl",
                array('trace' => 1, 'soap_version'   => SOAP_1_2)
            );
            $token = $ws->Authenticate($login, $password);

            $list = $ws->ListCampaign();
            var_dump($list);

        }
        catch (SoapFault $exception) {
            \Log::error("[RouteurMailKitchen@GetListCampaign] Exception raised:($exception->faultcode) ". $exception->getMessage());
            header('Content-Type: text/plain; charset: utf-8');
            echo $exception->faultcode . ' : ' . $exception->getMessage();
        }

    }


    function GetUnsubscribers($date){

        $login = $this->login;
        $password = $this->password;

        error_reporting(0);
        try {
            $ws = new \SoapClient(
                "http://webservices.mailkitchen.com/server.wsdl",
                array('trace' => 1, 'soap_version'   => SOAP_1_2)
            );
            $token = $ws->Authenticate($login, $password);

            // $list = $ws->GetUnSubscriberLists();
            // var_dump("list");
            // var_dump($list);
            // var_dump("array list");
            // var_dump($list[0]["id"]);
            // $memberList = $ws->GetMemberList(167193);
            // var_dump("memberlist");
            $desabo = $ws->GetUnsubscribeMember(167193,$date);
            // var_dump($desabo);
            return $desabo;

        }
        catch (SoapFault $exception) {
            header('Content-Type: text/plain; charset: utf-8');
            \Log::error("[RouteurMailKitchen@GetUnsubscribers] Exception raised:($exception->faultcode) ". $exception->getMessage());
            echo $exception->faultcode . ' : ' . $exception->getMessage();
        }

    }


    function DelivrabilityTest($CampaignId){

        $login = $this->login;
        $password = $this->password;

        error_reporting(0);
        try {

            $ws = new \SoapClient(
                "http://webservices.mailkitchen.com/server.wsdl",
                array('trace' => 1, 'soap_version'   => SOAP_1_2)
            );
            $token = $ws->Authenticate($login, $password);
            $ws->SelectCampaign($CampaignId);
            $test = $ws->DelivrabilityTest();

            \Log::info("[CampagneSegmentMailKitchen] DelivrabilityTest OK CLASS");
            return $test;
        }
        catch (\SoapFault $fault) {
            // header('Content-Type: text/plain; charset: utf-8');
            // echo $exception->faultcode . ' : ' . $exception->getMessage();
            \Log::info("[CampagneSegmentMailKitchen] DelivrabilityTest erreur catch soap fault");

            \Log::info("[RouteurMailKitchen@DelivrabilityTest] ".json_encode($fault));
        } catch (Exception $exception) {
            \Log::info("[RouteurMailKitchen@DelivrabilityTest] DelivrabilityTest erreur catch except");
        }

    }


    function SendCampaign($CampaignId){

        $login = $this->login;
        $password = $this->password;
        error_reporting(0);
        try {
            $ws = new \SoapClient(
                "http://webservices.mailkitchen.com/server.wsdl",
                array('trace' => 1, 'soap_version'   => SOAP_1_2)
            );
            $token = $ws->Authenticate($login, $password);

            $ws->SelectCampaign($CampaignId);
            if ($ws->ValidateCampaign()) {
                echo 'Successfull campaign validate';
                return true;
            }

        }
        catch (\SoapFault $exception) {
            /*header('Content-Type: text/plain; charset: utf-8');
            echo $exception->faultcode . ' : ' . $exception->getMessage();*/

            \Log::error("[RouteurMailKitchen@SendCampaign] Exception raised:($exception->faultcode)". $exception->getMessage());
            return false;
        }

    }


    function populateListBAT($listId){

        $login = $this->login;
        $password = $this->password;

        try {

            $ws = new \SoapClient(
                "http://webservices.mailkitchen.com/server.wsdl",
                array('trace' => 1, 'soap_version'   => SOAP_1_2)
            );
            $token = $ws->Authenticate($login, $password);

            $datas = array (
                'header'	=> array ('email'),
                'datas'		=> array (
                    //$arrayMember
                    // changer var here

                    0 => array('fabien@lead-factory.net'),
                    1 => array('test2@gmail.com'),
                    2 => array('test3@outlook.com'),
                    3 => array('test4@aol.com')
                )
            );
            $report = $ws->ImportMember(array($listId), $datas);

        }
        catch (SoapFault $exception) {
            header('Content-Type: text/plain; charset: utf-8');
            echo $exception->faultcode . ' : ' . $exception->getMessage();
        }

    }


    function sendBatAt($campaignid){

        $login = $this->login;
        $password = $this->password;
        error_reporting(0);
        try {

            $ws = new \SoapClient(
                "http://webservices.mailkitchen.com/server.wsdl",
                array('trace' => 1, 'soap_version'   => SOAP_1_2)
            );
            $token = $ws->Authenticate($login, $password);

            $htmlCode = ''; // source code campaign HTML

            $ws->SelectCampaign($campaignid);
            $ws->SendBat('zouir@sc2consulting.fr');
            $ws->SendBat('fabien@sc2consulting.fr');
            $ws->SendBat('hernoux.fabien@gmail.com');
            $ws->SendBat('olivier@plandefou.com');
            $ws->SendBat('alexandre@sc2consulting.fr');
            $ws->SendBat('moustanir@sc2consulting.fr');
            $ws->SendBat('shiraze@sc2consulting.fr');
            $ws->SendBat('alexandra@sc2consulting.fr');
            return true;
        }
        catch (SoapFault $exception) {
            \Log::error("[RouteurMailKitchen@sendBatAt] Exception raised: ($expection->faultcode)".$exception.getMessage());
            return false;
        }
    }

}
