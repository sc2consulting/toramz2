<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Clic extends Model {

    protected $table = 'clics';

    protected $fillable = array('id', 'destinataire_id', 'campagne_id');

    function scopeByDate($query, $date)
    {
        return $query->where('created_at', $date);
    }

    function destinataire()
    {
        return $this->belongsTo('\App\Models\Destinataire', 'mail_id', 'id');
    }

}
