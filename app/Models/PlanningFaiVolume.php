<?php namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class PlanningFaiVolume extends Model {

    protected $table = 'plannings_fais_volumes';
    protected $fillable = ['planning_id', 'fai_id', 'volume'];

    public function planning() {
        return $this->hasOne('\App\Models\Planning', 'id', 'planning_id');
    }

    public function fai() {
        return $this->hasOne('\App\Models\Fai', 'id', 'fai_id');
    }
}
