<?php namespace App\Models;

use \App\Models\ViewBasesCountDestinataires;
use Illuminate\Database\Eloquent\Model;

class Base extends Model {
    protected $fillable = ['nom', 'code'];

    public $timestamps = false;

    static function scopeByNom($query, $label) {
        return $query->where('nom', $label);
    }

    static function scopeByCode($query, $code) {
        return $query->where('code', $code);
    }

    static function scopeById($query, $id) {
        return $query->where('id', $id);
    }

    public function blacklist() {
        return $this->hasMany('\App\Models\Blacklist', 'id', 'base_id');
    }

    public function destinataires() {
        return $this->hasMany('\App\Models\Destinataire', 'base_id', 'id');
    }

    static function getGroups() {
        return [
            1 => 'PDF + CDD',
            3 => 'PDM + EP',
        ];
    }

    public function ViewCountDestinataires() {
        return $this->hasOne('\App\Models\ViewBasesCountDestinataires', 'base_id', 'id');
    }
}
