<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DestinataireGroupe extends Model {

    protected $table = 'destinataires_groupes';

    protected $fillable = ['destinataire_id'];

}
