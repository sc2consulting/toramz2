<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Sent extends Model {

    protected $table = 'tokens';

    protected $fillable = ['destinataire_id', 'base_id', 'fai_id', 'date_envoi'];

    function scopeByBase($query, $base) {
        return $query->where('base', $base);
    }

    function scopeByFAI($query, $fai) {
        return $query->where('fai', $fai);
    }

    function scopeByDate($query, $date) {
        return $query->where('date_envoi', $date);
    }

}
