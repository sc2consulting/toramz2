<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ouverture extends Model {

    protected $table = 'ouvertures';
    protected $fillable = [
        'destinataire_id',
        'campagne_id',
        'isMobile',
        'isTablet',
        'isDesktop',
        'browserFamily',
        'browserVersionMajor',
        'browserVersionMinor',
        'browserVersionPatch',
        'osFamily',
        'osVersionMajor',
        'osVersionMinor',
        'osVersionPatch',
        'deviceFamily',
        'deviceModel',
        'mobileGrade',
        'cssVersion',
        'javaScriptSupport',
        'ip'
    ];

    function destinataire() {
        return $this->belongsTo('\App\Models\Destinataire', 'destinataire_id', 'id');
    }

}
