<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Sender extends Model {

    protected $fillable = ['nom', 'password', 'domaine', 'quota', 'quota_left', 'routeur_id', 'is_bat', 'bat_liste', 'nb_campagnes', 'nb_campagnes_left', 'type', 'is_enable', 'branch_id', 'site_id'];

    function routeur() {
        return $this->hasOne('\App\Models\Routeur', 'id', 'routeur_id');
    }

    function fais() {
        return $this->belongsToMany('\App\Models\Fai');
    }

    function fai_sender() {
        return $this->hasMany('\App\Models\SenderFai');
    }

    function getFullIdAttribute() {
        return $this->nom.' ('.$this->id.')';
    }

    function branch() {
        return $this->hasOne('\App\Models\Branch', 'id', 'branch_id');
    }
}
