let planningsEvent = [];
let stateCalendar = "closed";
$(document).ready(()=>{
    /*Begin part:Generate Calendar*/
    new Promise((resolve,reject)=>{
        $.post('/planning/history',{_token:token},response=>{
            resolve(JSON.parse(response));
        });
    }).then(arrayPlannings=>{
        arrayPlannings.plannings.forEach(planning=>{
            planningsEvent.push({
                title:planning.ref,
                start:planning.date_campagne,
                className:planning.code
            });
        });
    }).then(()=>{
        $('#plannings-calendar').fullCalendar({
            lang:'fr',
            events:planningsEvent,
            eventRender:(event,element)=>{
                let base = basesColor.find(base=>{
                    return base.name === event.className[0];
                });
                let color = (base !== undefined) ? base.color : getRandomColor()
                element.css('cssText','background:'+color+' !important');
            },
        });

    });
    /*End part:Generate Calendar*/

    $('#calendarPlannings').click(()=>{
        if(stateCalendar == "closed"){
            $('#calendar-plannings').fadeIn();
            stateCalendar = "open";
        }else{
            $('#calendar-plannings').fadeOut();
            stateCalendar = "closed";
        }
    });
});
