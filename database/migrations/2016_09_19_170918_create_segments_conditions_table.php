<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSegmentsConditionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('segments_conditions', function (Blueprint $table) {
            $table->increments('id');
            
            $table->integer('segment_id');

            $table->string('condition_type'); //geoloc, ouvreurs, cliqueurs ou thématique
//            $table->string('condition_table'); // table concernée
            $table->string('condition_column'); // colonne concernée
            $table->string('condition_operator'); //opérateur voulue = > <
            $table->string('condition_value'); // valeur souhaitée
            
            $table->index(['segment_id']);
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('segments_conditions');
    }
}
