<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewRouteurEdatisToRouteursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        \DB::table('routeurs')->insert(
            [
                'nom' => 'Edatis',
                'variable_email' => '[[%FIELD_EMAIL%]]',
                'variable_unsubscribe' => '[[%UNSUBSCRIBE%]]',
                'variable_mirror' => '[[%WEBVERSION%]]',
                'variable_email_sha1' => '[[%PFIELD_SHA1%]]',
                'variable_email_md5' => '[[%PFIELD_MD5%]]',
                'variable_prenom' => '[[%PFIELD_FIRSTNAME%]]',
                'variable_nom' => '[[%PFIELD_LASTNAME%]]',
                'variable_tor_id' => '[[%PFIELD_TORID%]]',
                'is_active' => 1,
                'created_at' => '2017-03-14 14:56:00',
                'updated_at' => '2017-03-14 14:56:00'
            ]
        );

        $edatis = \DB::table('routeurs')->where('nom', 'Edatis')->first();

        \DB::table('settings')->insert(
            [
                'parameter' => 'is_edatis',
                'value' => '1',
                'context' => 'routeur',
                'info' => $edatis->id
            ]
        );

        // env('CLIENT_NAME')
        if(getenv('CLIENT_NAME') == 'sc2'){

        \DB::table('senders')->insert([

                'routeur_id' => $edatis->id,
                'nom' => 'Edatis_SC2_1',
                'password' => '21f5f1f10f74b4b9d5d0ebab09302bef',
                'domaine' => 'info.leplusdunet.fr',
                'quota' => '60000',
                'quota_left' => '60000',
                'created_at' => '2017-03-22 10:00:00',
                'updated_at' =>  '2017-03-22 10:00:00',
                'md_list_id' => '',
                'remaining_slots' => '',
                'is_bat' => '',
                'bat_liste' => '',
                'nb_campagnes' => '1',
                'nb_campagnes_left' => '1',
                'type' => 'fr',
                'is_enable' => '1',
                'branch_id' =>'1',
                'api_login' =>'',
                'api_key' =>'',
                'site_id' => '0'
            ]);

      \DB::table('senders')->insert([
                'routeur_id' => $edatis->id,
                'nom' => 'Edatis_SC2_2',
                'password' => '8e407de4c1248c3215d9e47f9b6b27cb',
                'domaine' => 'info.lemegadeal.fr',
                'quota' => '60000',
                'quota_left' => '60000',
                'created_at' => '2017-03-22 10:00:00',
                'updated_at' =>  '2017-03-22 10:00:00',
                'md_list_id' => '',
                'remaining_slots' => '',
                'is_bat' => '',
                'bat_liste' => '',
                'nb_campagnes' => '1',
                'nb_campagnes_left' => '1',
                'type' => 'fr',
                'is_enable' => '1',
                'branch_id' =>'1',
                'api_login' =>'',
                'api_key' =>'',
                'site_id' => '0'
          ]);

          \DB::table('senders')->insert([
                'routeur_id' => $edatis->id,
                'nom' => 'Edatis_SC2_3',
                'password' => '65349767ce58099dd6ea2c902f726017',
                'domaine' => 'info.latendanceduweb.fr',
                'quota' => '60000',
                'quota_left' => '60000',
                'created_at' => '2017-03-22 10:00:00',
                'updated_at' =>  '2017-03-22 10:00:00',
                'md_list_id' => '',
                'remaining_slots' => '',
                'is_bat' => '',
                'bat_liste' => '',
                'nb_campagnes' => '1',
                'nb_campagnes_left' => '1',
                'type' => 'fr',
                'is_enable' => '1',
                'branch_id' =>'1',
                'api_login' =>'',
                'api_key' =>'',
                'site_id' => '0'
          ]);

          \DB::table('senders')->insert([
                'routeur_id' => $edatis->id,
                'nom' => 'Edatis_SC2_4',
                'password' => '914ef64973a6e20af33f83d5dbabc76d',
                'domaine' => 'info.loffreprivee.fr',
                'quota' => '60000',
                'quota_left' => '60000',
                'created_at' => '2017-03-22 10:00:00',
                'updated_at' =>  '2017-03-22 10:00:00',
                'md_list_id' => '',
                'remaining_slots' => '',
                'is_bat' => '',
                'bat_liste' => '',
                'nb_campagnes' => '1',
                'nb_campagnes_left' => '1',
                'type' => 'fr',
                'is_enable' => '1',
                'branch_id' =>'1',
                'api_login' =>'',
                'api_key' =>'',
                'site_id' => '0'
          ]);

          \DB::table('senders')->insert([
                'routeur_id' => $edatis->id,
                'nom' => 'Edatis_SC2_5',
                'password' => 'fe40bd0786d605919abf6afed1b5d645',
                'domaine' => 'info.lasupernews.fr',
                'quota' => '60000',
                'quota_left' => '60000',
                'created_at' => '2017-03-22 10:00:00',
                'updated_at' =>  '2017-03-22 10:00:00',
                'md_list_id' => '',
                'remaining_slots' => '',
                'is_bat' => '',
                'bat_liste' => '',
                'nb_campagnes' => '1',
                'nb_campagnes_left' => '1',
                'type' => 'fr',
                'is_enable' => '1',
                'branch_id' =>'1',
                'api_login' =>'',
                'api_key' =>'',
                'site_id' => '0'
          ]);

          \DB::table('senders')->insert([
                'routeur_id' => $edatis->id,
                'nom' => 'Edatis_SC2_6',
                'password' => 'dd5c17524500d78bc5ed4d5632439616',
                'domaine' => 'info.lasurprisedunet.fr',
                'quota' => '60000',
                'quota_left' => '60000',
                'created_at' => '2017-03-22 10:00:00',
                'updated_at' =>  '2017-03-22 10:00:00',
                'md_list_id' => '',
                'remaining_slots' => '',
                'is_bat' => '',
                'bat_liste' => '',
                'nb_campagnes' => '1',
                'nb_campagnes_left' => '1',
                'type' => 'fr',
                'is_enable' => '1',
                'branch_id' =>'1',
                'api_login' =>'',
                'api_key' =>'',
                'site_id' => '0'
          ]);

          \DB::table('senders')->insert([
                'routeur_id' => $edatis->id,
                'nom' => 'Edatis_SC2_7',
                'password' => '0e3b0deb485bfe1b0a200ab0d411005b',
                'domaine' => 'info.lafolieduweb.fr',
                'quota' => '60000',
                'quota_left' => '60000',
                'created_at' => '2017-03-22 10:00:00',
                'updated_at' =>  '2017-03-22 10:00:00',
                'md_list_id' => '',
                'remaining_slots' => '',
                'is_bat' => '',
                'bat_liste' => '',
                'nb_campagnes' => '1',
                'nb_campagnes_left' => '1',
                'type' => 'fr',
                'is_enable' => '1',
                'branch_id' =>'1',
                'api_login' =>'',
                'api_key' =>'',
                'site_id' => '0'
          ]);

          \DB::table('senders')->insert([
                'routeur_id' => $edatis->id,
                'nom' => 'Edatis_SC2_8',
                'password' => '38771891c86cbacaede5d9ce421cba49',
                'domaine' => 'info.loffreagogo.fr',
                'quota' => '60000',
                'quota_left' => '60000',
                'created_at' => '2017-03-22 10:00:00',
                'updated_at' =>  '2017-03-22 10:00:00',
                'md_list_id' => '',
                'remaining_slots' => '',
                'is_bat' => '',
                'bat_liste' => '',
                'nb_campagnes' => '1',
                'nb_campagnes_left' => '1',
                'type' => 'fr',
                'is_enable' => '1',
                'branch_id' =>'1',
                'api_login' =>'',
                'api_key' =>'',
                'site_id' => '0'
          ]);

          \DB::table('senders')->insert([
                'routeur_id' => $edatis->id,
                'nom' => 'Edatis_SC2_9',
                'password' => 'c3d551016308bdfe334f4e9ef498ee04',
                'domaine' => 'info.lamagiedunet.fr',
                'quota' => '60000',
                'quota_left' => '60000',
                'created_at' => '2017-03-22 10:00:00',
                'updated_at' =>  '2017-03-22 10:00:00',
                'md_list_id' => '',
                'remaining_slots' => '',
                'is_bat' => '',
                'bat_liste' => '',
                'nb_campagnes' => '1',
                'nb_campagnes_left' => '1',
                'type' => 'fr',
                'is_enable' => '1',
                'branch_id' =>'1',
                'api_login' =>'',
                'api_key' =>'',
                'site_id' => '0'
          ]);

          \DB::table('senders')->insert([
                'routeur_id' => $edatis->id,
                'nom' => 'Edatis_SC2_10',
                'password' => '5e568b68d0a55516db8fcc701fd512fd',
                'domaine' => 'info.ledealsurprise.fr',
                'quota' => '60000',
                'quota_left' => '60000',
                'created_at' => '2017-03-22 10:00:00',
                'updated_at' =>  '2017-03-22 10:00:00',
                'md_list_id' => '',
                'remaining_slots' => '',
                'is_bat' => '',
                'bat_liste' => '',
                'nb_campagnes' => '1',
                'nb_campagnes_left' => '1',
                'type' => 'fr',
                'is_enable' => '1',
                'branch_id' =>'1',
                'api_login' =>'',
                'api_key' =>'',
                'site_id' => '0'
          ]);

          }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
