<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableCampagneStats extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campagne_stats', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('campagne_id');
            $table->date('date');
            $table->integer('envoyes');
            $table->integer('ouvertures');
            $table->integer('clicks');
            $table->integer('bounces');
            $table->integer('optout');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('campagne_stats');
    }
}
