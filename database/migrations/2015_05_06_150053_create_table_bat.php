<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableBat extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('bats', function(Blueprint $table)
		{
			$table->increments('id');

            $table->integer('email_number');
            $table->string('nom');
            $table->string('reference');
            $table->string('top');
            $table->string('bottom');
            $table->string('basbas');
            $table->string('idaf');
            $table->string('perso');
            $table->string('theme_id');
            $table->string('base_id');
            $table->string('coul_wori');
            $table->string('objet');
            $table->string('expediteur');
            $table->string('dossier');
            $table->string('https');
            $table->string('j');
            $table->text('html');
            $table->text('txt_suppr');

            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('bats');
	}

}
