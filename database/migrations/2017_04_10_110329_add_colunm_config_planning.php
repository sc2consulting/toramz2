<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColunmConfigPlanning extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('planning_defaut', function (Blueprint $table) {
        $table->text('config_key')->default(null);
        $table->text('value')->default(null);
      });

      Schema::table('planning_defaut', function (Blueprint $table) {
          $table->dropColumn('volume_total');
          $table->dropColumn('created_at');
          $table->dropColumn('updated_at');
      });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
