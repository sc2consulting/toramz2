<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGeolocDepartement extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('geoloc_departement', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('code_departement')->nullable();
            $table->integer('code')->nullable();
            $table->integer('nombre_ip')->nullable();
            $table->timestamps();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('geoloc_departement');
    }
}
