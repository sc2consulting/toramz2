<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableCampagneAndAddPlateforme extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {


      Schema::create('plateformes_affi', function (Blueprint $table) {

        $table->increments('id');
        $table->string('nom_plateforme');
        $table->string('lien');
        $table->integer('state');
        $table->timestamps();

      });

      DB::table('plateformes_affi')->insert(
          ['nom_plateforme' => 'Non renseigné',
          'lien' => '-',
          'state' => '1']
      );

      Schema::table('campagnes', function (Blueprint $table) {
          $table->integer('plateforme_id')->default('1');
      });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('campagnes_ca');
    }
}
