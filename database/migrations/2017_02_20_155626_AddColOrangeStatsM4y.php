<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColOrangeStatsM4y extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

      Schema::table('stats_m4y', function (Blueprint $table) {

      $table->integer('nombreborange')->default(null);

      $table->integer('nombretorange')->default(null);

      $table->float('aboutis_orange',6,2)->default(null);

      $table->float('per_cent_orange_ouv',6,2)->default(null);

      });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
