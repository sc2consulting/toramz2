<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUniqueIndexToDestinatairesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('destinataires', function (Blueprint $table) {
            \DB::update('ALTER TABLE destinataires DROP INDEX destinataires_base_id_mail_index');
            \DB::update('ALTER TABLE destinataires ADD CONSTRAINT destinataires_base_id_mail_index UNIQUE (base_id,mail)');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
