<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePlanningPercentFailure extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('planning_percent_failure', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('id_planning');
            $table->integer('volume_sent');
            $table->integer('volume_failure');
            $table->double('percent_failure');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('planning_percent_failure');
    }
}
