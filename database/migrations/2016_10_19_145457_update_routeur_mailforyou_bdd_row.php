<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateRouteurMailforyouBddRow extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // maj de ligne
         \DB::statement("UPDATE routeurs SET variable_email ='[Email]', variable_unsubscribe = '[Balise_Desabonnement]', variable_mirror = '[Balise_Message_En_Ligne]', variable_tor_id = '[tor_id]'  WHERE nom ='MailForYou'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
