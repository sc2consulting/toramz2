<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlanningsConditionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plannings_conditions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('planning_id');
            $table->string('condition_type'); //geoloc, ouvreurs, cliqueurs ou thématique
            $table->string('condition_table'); // table
            $table->string('condition_column'); //
            $table->string('condition_operator');
            $table->string('condition_value');

            $table->timestamps();

            $table->index(['planning_id', 'condition_type']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('plannings_conditions');
    }
}
