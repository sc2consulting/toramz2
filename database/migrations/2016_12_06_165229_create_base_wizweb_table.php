<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBaseWizwebTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('base_wizweb', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('base_id');
            $table->string('trigram');
            $table->timestamps();

            $table->unique('base_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('base_wizweb');
    }
}
