<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSendersHistory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('senders_history', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('sender_id')->nullable()->default(null);
          $table->integer('fai_id')->nullable()->default(null);
          $table->integer('planning_id')->nullable()->default(null);
          $table->integer('used_quota')->nullable()->default(null);
          $table->integer('quota')->nullable()->default(null);
          $table->integer('quota_before')->nullable()->default(null);
          $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('senders_history');
    }
}
