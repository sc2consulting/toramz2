<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTokensWeek extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
       Schema::create('tokens_week', function (Blueprint $table) {
           $table->increments('id');
           $table->integer('base_id');
           $table->integer('nb_semaine');
           $table->timestamps();

       });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
         Schema::drop('tokens_week');
     }
}
