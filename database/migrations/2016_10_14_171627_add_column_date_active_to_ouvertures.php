<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnDateActiveToOuvertures extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ouvertures', function (Blueprint $table) {
            $table->date('date_active');
            $table->index('date_active', 'campagne_id');
        });

        \DB::query('UPDATE ouvertures SET date_active = DATE(created_at)');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}

?>