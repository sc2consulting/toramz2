<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableBlacklists extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('blacklists', function (Blueprint $table) {
            $table->unique('email');
            $table->string('liste');
            $table->index(['liste','email']);
            $table->timestamps();
        });
        \DB::statement('RENAME TABLE blacklists TO blacklistes');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('blacklistes');
    }
}
