<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableDestinataires extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('destinataires', function(Blueprint $table)
		{
			$table->increments('id');

            $table->string('mail');
            $table->integer('base_id');
            $table->string('ip');
            $table->string('ville');
            $table->integer('departement');
            $table->string('os');
            $table->string('machine');
            $table->string('navigateur');
            $table->integer('routeur_id');
            $table->integer('sender_id');
            $table->string('liste');
            $table->string('origine');

            $table->integer('statut');

            $table->dateTime('optout_at');
            $table->dateTime('sent_at');
            $table->dateTime('opened_at');
            $table->dateTime('clicked_at');

            $table->boolean('utilise');
            $table->integer('fai_id');

            $table->string('hash');

            $table->dateTime('tokenized_at');
            $table->timestamps();

            $table->index( [ 'base_id', 'mail' ] );
            $table->index( 'statut' );
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('destinataires');
	}

}
