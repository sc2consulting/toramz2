<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableOuverturesStatsBases extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('ouvertures_stats_bases', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('base_id');
          $table->integer('count');
          $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ouvertures_stats_bases');
    }
}
