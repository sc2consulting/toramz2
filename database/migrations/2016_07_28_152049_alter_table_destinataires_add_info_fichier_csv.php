<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableDestinatairesAddInfoFichierCsv extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('destinataires', function (Blueprint $table) {

        $table->string('nom')->nullable();
        $table->string('prenom')->nullable();
        $table->string('datenaissance')->nullable();
        $table->string('tel')->nullable();
        $table->string('adresse')->nullable();

      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
