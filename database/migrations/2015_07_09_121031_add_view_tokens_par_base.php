<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddViewTokensParBase extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        \DB::statement("create DEFINER = ".env('DB_USERNAME')." view bases_count_tokens as select base_id, count(*) as nombre from tokens where campagne_id is null and sender_id is null and date_active = CURDATE() group by base_id");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \DB::statement('drop view bases_count_tokens');
    }
}
