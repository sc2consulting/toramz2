<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableBases extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('bases', function(Blueprint $table) {
            $table->increments('id');
            $table->string('code');
            $table->string('nom');

            $table->string('img_url');
            $table->text('mentions_legales');

            $table->string('abuse_hostname');
            $table->string('abuse_username');
            $table->string('abuse_password');

            $table->index('code');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('bases');
	}
}
