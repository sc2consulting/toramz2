<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableRepoussoirs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('repoussoirs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('hash');
            $table->integer('mail_id');
            $table->integer('campagne_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('repoussoirs');
    }
}
