<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Eloquent\Model;

class AddPhoenixToRouteursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Model::unguard();
        $routeur = new \App\Models\Routeur();
        $routeur->nom                  = 'Phoenix';
        $routeur->variable_email       = '%%email%%';
        $routeur->variable_unsubscribe = '%%unsubscribe%%';
        $routeur->variable_mirror      = '%%webversion%%';
        $routeur->variable_email_sha1  = '';
        $routeur->variable_email_md5   = '%%md5%%';
        $routeur->variable_prenom      = '%%prenom%%';
        $routeur->variable_nom         = '%%nom%%';
        $routeur->variable_tor_id      = '%%perso_tor_id%%';
        $routeur->save();
        Model::reguard();
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
