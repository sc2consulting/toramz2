<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableStatsMaildropTotal extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('stats_maildrop_total', function (Blueprint $table) {

          $table->increments('id');

          $table->string('reference');
          $table->integer('desinscriptions');
          $table->integer('ouvreurs');
          $table->integer('cliqueurs');
          $table->integer('erreurs');
          $table->integer('date_maj');
          $table->integer('bloc_maj');

      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('stats_maildrop_total');
    }
}
