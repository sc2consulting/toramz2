<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableCampagnesRouteurs extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('campagnes_routeurs', function($table)
		{
			$table->increments('id');
            
			$table->integer('campagne_id');
			$table->integer('sender_id');
			$table->integer('cid_routeur');

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('campagnes_routeurs');
	}

}
