<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTokens extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tokens', function(Blueprint $table)
		{
			$table->increments('id');

            $table->integer('base_id');
            $table->integer('destinataire_id');
            $table->integer('campagne_id')->nullable();
            $table->integer('fai_id');
            $table->integer('sender_id')->nullable();

            $table->date('date_active');

            $table->integer('priority');

            $table->timestamp('uploaded_at');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tokens');
	}

}
