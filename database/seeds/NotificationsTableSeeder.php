<?php

use Illuminate\Database\Seeder;
use App\Models\Notification;

class NotificationsTableSeeder extends Seeder {

    public function run()
    {

        $items[] = ['user_id' => 1, 'level'=> 'info', 'message' => 'a rempli le Seeder' ];
        $items[] = ['user_id' => 2, 'level'=> 'info', 'message' => 'a appris à rebaser' ];
        $items[] = ['user_id' => 1, 'level'=> 'error', 'message' => 'Le fichier est introuvable', 'is_important' => true ];

        foreach($items as $item)
        {
            Notification::create($item);
        }
    }
}
