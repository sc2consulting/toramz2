<?php
use Illuminate\Database\Seeder;
use App\Models\Routeur;

class RouteursTableSeeder extends Seeder {
    public function run()
    {
        $r = Routeur::firstOrCreate(['nom' => 'Smessage']);
        $r->nom = 'Smessage';
        $r->variable_email = '%email%';
        $r->variable_unsubscribe = '@@@';
        $r->variable_mirror = '&&&';
        $r->variable_email_sha1 = '';
        $r->variable_email_md5 = '';
        $r->variable_prenom = '%prenom%';
        $r->variable_nom = '%nom%';
        $r->variable_tor_id = '%p1%';
        $r->save();

        $r = Routeur::firstOrCreate(['nom' => 'Maildrop']);
        $r->variable_email = '{$Member.email}';
        $r->variable_unsubscribe = '{$Unsubscribe_Link}';
        $r->variable_mirror = '{$Web_View_Link}';
        $r->variable_email_sha1 = '{$Member.email|sha1}';
        $r->variable_email_md5 = '{$Member.email|md5}';
        $r->variable_prenom = '{$Member.first_name}';
        $r->variable_nom = '{$Member.last_name}';
        $r->variable_tor_id = '{$Member.tor_id}';
        $r->save();

        $r = Routeur::firstOrCreate(['nom' => 'Phoenix']);
        $r->variable_email = '%%email%%';
        $r->variable_unsubscribe = '%%unsubscribe%%';
        $r->variable_mirror = '%%webversion%%';
        $r->variable_email_sha1 = '';
        $r->variable_email_md5 = '%%md5%%';
        $r->variable_prenom = '%%prenom%%';
        $r->variable_nom = '%%nom%%';
        $r->variable_tor_id = '%%perso_tor_id%%';
        $r->save();
    }
}
