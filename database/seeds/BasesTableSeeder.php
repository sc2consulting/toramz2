<?php
use Illuminate\Database\Seeder;
use App\Models\Base;

class BasesTableSeeder extends Seeder {

    public function run()
    {
        Base::flushEventListeners();

        $items[] =
            [
                'id' => 1,
                'code'=>'pdf',
                'nom' => 'Plandefou',
                'abuse_username' => 'abuse@dgcnit.fr',
                'abuse_hostname' => '{ssl0.ovh.net:993/imap/ssl}INBOX',
                'abuse_password' => 'aqwzsx01'
            ];

        $items[] = ['id' => 2, 'code'=>'cdd', 'nom' => 'Comptoir des deals'];
        $items[] = ['id' => 3, 'code'=>'pdm', 'nom' => 'Palmares des Marques'];
        $items[] = ['id' => 4, 'code'=> 'ep', 'nom' => 'Espace Promos'];
        $items[] = ['id' => 5, 'code'=> 'adt', 'nom' => 'AdThink'];
        $items[] = ['id' => 6, 'code'=> 'tst', 'nom' => 'Base de Tests'];

        foreach($items as $item)
        {
            Base::create($item);
        }
    }
}
