<?php
use Illuminate\Database\Seeder;
use App\Models\User;

class UsersTableSeeder extends Seeder {

    public function run()
    {
        \DB::table('users')->truncate();

        $now = date('Y-m-d H:i:s');

        $users[] = [
            'name'              => 'System',
            'email'             => '',
            'password'          => '',
            'created_at'        => $now,
            'updated_at'        => $now,
            'is_valid'          => 0,
            'user_group_id'     => 1,
        ];

        $users[] = [
            'name'              => 'Super Admin',
            'email'             => 'dev@lead-factory.net',
            'password'          => Hash::make('aqwzsx'),
            'created_at'        => $now,
            'updated_at'        => $now,
            'is_valid'          => 1,
            'user_group_id'     => 3,
        ];

        foreach($users as $user)
        {
            User::create($user);
        }
    }
}