<?php
use Illuminate\Database\Seeder;
use App\Models\Fai;

class FaisTableSeeder extends Seeder {

    public function run()
    {
        \DB::table('fais')->delete();

        $items[] = ['id' => 1, 'nom' => 'orange', 'domains' => 'orange.fr|wanadoo.fr|voila.fr', 'quota_pression' => 1, 'quota_sender' => 0];
        $items[] = ['id' => 2, 'nom' => 'free', 'domains' => 'free.fr|freesbee.fr|libertysurf.fr|worldonline.fr|online.fr|alicepro.fr|aliceadsl.fr', 'quota_pression' => 1, 'quota_sender' => 0];
        $items[] = ['id' => 3, 'nom' => 'gmail', 'domains' => 'gmail.com', 'quota_pression' => 1, 'quota_sender' => 0];
        $items[] = ['id' => 4, 'nom' => 'sfr', 'domains' => 'sfr.fr|neuf.fr|cegetel.net', 'quota_pression' => 2, 'quota_sender' => 0];
        $items[] = ['id' => 5, 'nom' => 'laposte', 'domains' => 'laposte.net', 'quota_pression' => 2, 'quota_sender' => 0];
        $items[] = ['id' => 6, 'nom' => 'autre', 'domains'=>'blabla.com', 'quota_pression' => 2, 'quota_sender' => 0];
        $items[] = ['id' => 7, 'nom' => 'hotmail', 'domains' => 'hotmail.fr|msn.fr|live.fr|outlook.fr', 'quota_pression' => 1, 'quota_sender' => 0];
        $items[] = ['id' => 8, 'nom' => 'yahoo', 'domains' => 'yahoo.fr|yahoo.com', 'quota_pression' => 2, 'quota_sender' => 0];
        $items[] = ['id' => 9, 'nom' => 'aol', 'domains' => 'aol.com', 'quota_pression' => 1, 'quota_sender' => 0];

        foreach($items as $item)
        {
            Fai::create($item);
        }
    }
}
